```lua
function Atr_GetAuctionPrice (item)  -- itemName or itemID
  local itemName;

  if (type (item) == "number") then
    itemName = GetItemInfo (item);
  else
    itemName = item;
  end

  if (itemName == nil) then
    return nil;
  end

  if (gAtr_ScanDB and type (gAtr_ScanDB) ~= "table") then
    zc.msg_badErr ("Scanning history database appears to be corrupt")
    zc.msg_badErr ("gAtr_ScanDB:", gAtr_ScanDB)
    return nil
  end

  if ((type(gAtr_ScanDB) == "table") and gAtr_ScanDB[itemName] and gAtr_ScanDB[itemName].mr) then
    return gAtr_ScanDB[itemName].mr;
  end

  return nil;
end
```


mr is indeed last price that appears in database.
L/H are lowest/highest prices for that day.
the number in there is number since day 0, the equivalent of WoW's unix time: 2000 01 01 00:00
The addon specifies a tight zero which is 2000 08 01 00:00 for some reason
cc/sc are item's class/subclass ids

```lua
function Atr_UpdateScanDBclassInfo (itemName, class, subclass)

  if (not gAtr_ScanDB[itemName]) then
    gAtr_ScanDB[itemName] = {};
  end

  gAtr_ScanDB[itemName].cc = class;
  gAtr_ScanDB[itemName].sc = subclass;

--  zc.md ("Setting class info for:", itemName, "    ", class, subclass);
end
```

`is` in pricing history is not used for tight time parsing.
but instead the elements around it
`is` field itself is just item id condensed with all the modifiers for that item
Which follows this format

function Auctionator.ItemLink:ItemIdString()
  if not self.item_id_string then
    self.item_id_string = self:GetField( Auctionator.Constants.ItemLink.ID ) .. ':' ..
      self:Tier() .. ':' .. self:Stage() .. ':' .. self:Suffix() .. ':' ..
      self:GetField( Auctionator.Constants.ItemLink.BONUS_ID_1 )
  end

  return self.item_id_string
end

@JustWill
Unless you have sold the item, auctionator does not append id field to price database
```lua
AUCTIONATOR_PRICE_DATABASE = {
    ["__dbversion"] = 4,
    ["Flamelash_Horde"] = {
        ["Pattern: Azure Shoulders"] = {
            ["mr"] = 5000,
            ["cc"] = 9,
            ["H3245"] = 5000,
            ["sc"] = 2,
            ["id"] = "7085:0:0:0:0",
        },
        ["Battleforge Wristguards of the Bear"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Arcane Elixir"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        }
```
