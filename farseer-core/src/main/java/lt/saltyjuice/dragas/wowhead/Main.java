package lt.saltyjuice.dragas.wowhead;

import org.apache.camel.CamelContext;
import org.apache.camel.main.MainListener;
import org.apache.camel.main.MainSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Main {
    public static void main(String[] args) throws Exception {
        org.apache.camel.main.Main camelMain = new org.apache.camel.main.Main();
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUsername("postgres");
        ds.setPassword(System.getProperty("wowhead.db.pw"));
        ds.setUrl(System.getProperty("wowhead.db.url"));
        camelMain.bind("psqlds", ds);
        camelMain.addMainListener(new MainListener() {
            @Override
            public void beforeStart(MainSupport main) {

            }

            @Override
            public void configure(CamelContext context) {
                try {
                    //context.addRoutes(new WowheadRouteBuilder());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Protocol.getProtocol("https");
            }

            @Override
            public void afterStart(MainSupport main) {
//                ProducerTemplate p = null;
//                try {
//                    p = main.getCamelTemplate();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                for (int i = 0; i < 200; i++) {
//                    Map<String, Object> headers = new HashMap<>();
//                    headers.put("from", i * 1000);
//                    headers.put("to", (i + 1) * 1000);
//                    p.sendBodyAndHeaders("seda:crawlSearch", null, headers);
//                }
            }

            @Override
            public void beforeStop(MainSupport main) {

            }

            @Override
            public void afterStop(MainSupport main) {

            }
        });
        camelMain.run();
    }


}
