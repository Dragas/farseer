package lt.saltyjuice.dragas.spring;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.saltyjuice.dragas.farseer.spring.MainController;
import lt.saltyjuice.dragas.farseer.spring.RestController;
import lt.saltyjuice.dragas.farseer.spring.model.Item;
import lt.saltyjuice.dragas.farseer.spring.repository.ItemRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static lt.saltyjuice.dragas.spring.Utility.mockItemList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = RestController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class RestControllerTest {

    @MockBean
    private ItemRepository repository;

    @Autowired
    private RestController controller;

    @Autowired
    private MockMvc mvc;

    private ObjectMapper mapper = new ObjectMapper();


    @Test
    public void restRequestSuggestsItemNamesByPartOfName() throws Exception {
        List<Item> items = mockItemList(repository);
        MvcResult it = mvc.perform(get("/api/suggest?{}", "F"))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        String data = it.getResponse().getContentAsString();
        List<Item> deserialized = mapper.readValue(data, new TypeReference<List<Item>>() {
        });
        assertEquals(1, deserialized.size());
        assertEquals(1, deserialized.get(0).getId());
        assertEquals("Foo", deserialized.get(0).getName());
    }

    @Test
    public void restRequestSuggestItemReturnsNoContentWhenNoSuggestionsAreAvailable() throws Exception {
        when(repository.getItemsByNameLikeness("F", MainController.DEFAULT_AMOUNT)).thenReturn(List.of());
        MvcResult it = mvc.perform(get("/api/suggest?{}", "F"))
                .andExpect(status().isNoContent())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
    }
}
