package lt.saltyjuice.dragas.spring;

import lt.saltyjuice.dragas.farseer.spring.MainController;
import lt.saltyjuice.dragas.farseer.spring.model.Item;
import lt.saltyjuice.dragas.farseer.spring.repository.ItemRepository;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class Utility {
    public static List<Item> mockItemList(ItemRepository repository) {
        List<Item> items = createItemList();
        when(repository.getItemsByNameLikeness("F", MainController.DEFAULT_AMOUNT)).thenReturn(items.subList(0, 1));
        when(repository.getItemsByNameLikeness("B", MainController.DEFAULT_AMOUNT)).thenReturn(items.subList(1, MainController.DEFAULT_AMOUNT));
        when(repository.getItemsByNameLikeness("B", 3)).thenReturn(items.subList(1, 4));
        return items;
    }

    public static List<Item> createItemList() {
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setId(1);
        item.setName("Foo");
        items.add(item);
        item = new Item();
        item.setId(2);
        item.setName("Bar");
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        return items;
    }
}
