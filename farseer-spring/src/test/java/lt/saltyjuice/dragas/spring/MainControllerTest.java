package lt.saltyjuice.dragas.spring;


import com.fasterxml.jackson.databind.ObjectMapper;
import lt.saltyjuice.dragas.farseer.spring.MainController;
import lt.saltyjuice.dragas.farseer.spring.repository.AuctionRepository;
import lt.saltyjuice.dragas.farseer.spring.repository.ItemRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = MainController.class)
public class MainControllerTest {

    @Autowired(required = false)
    private MainController mainController;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuctionRepository auctionRepository;

    @MockBean
    private ItemRepository itemRepository;


    private ObjectMapper mapper = new ObjectMapper();


    @Test
    public void contextLoads() {
        assertNotNull(mainController, "Sanity test");
    }

    @Test
    public void indexRequestReturnsTemplateWithServers() throws Exception {
        List<String> servers = Arrays.asList("Flamelash", "Argentina");
        when(auctionRepository.getAllServers()).thenReturn(servers);
        MvcResult it = mvc
                .perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(xpath("//option[@value = 'Flamelash']").exists())
                .andExpect(xpath("//option[@value = 'Argentina']").exists())
                .andReturn();
        Map<String, Object> datamap = it.getModelAndView().getModel();
        assertTrue(datamap.containsKey("servers"));
        List<String> data = (List<String>) datamap.get("servers");
        assertTrue(data.containsAll(servers));
    }
}

