package lt.saltyjuice.dragas.farseer.spring;

import lt.saltyjuice.dragas.farseer.spring.repository.AuctionRepository;
import lt.saltyjuice.dragas.farseer.spring.repository.ItemRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    public static final int DEFAULT_AMOUNT = 5;
    private final ItemRepository itemRepository;
    private final AuctionRepository auctionRepository;

    public MainController(
            AuctionRepository auctionRepository,
            ItemRepository itemRepository) {
        this.auctionRepository = auctionRepository;
        this.itemRepository = itemRepository;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("servers", auctionRepository.getAllServers());
        return "index";
    }
}
