package lt.saltyjuice.dragas.farseer.spring.model;

import javax.persistence.*;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "get_all_servers",
                procedureName = "get_all_servers",
                resultClasses = {String.class}
        ),
        @NamedStoredProcedureQuery(
                name = "get_price_comparison_for_item",
                procedureName = "get_price_comparison_for_item",
                parameters = {
                        @StoredProcedureParameter(
                                name = "item_id",
                                type = Integer.class
                        ),
                        @StoredProcedureParameter(
                                name = "side",
                                type = Faction.class
                        ),
                        @StoredProcedureParameter(
                                name = "server",
                                type = String.class
                        ),
                        @StoredProcedureParameter(
                                name = "required_depth",
                                type = Integer.class
                        )
                }
        ),
        @NamedStoredProcedureQuery(
                name = "explain_depth_for_item",
                procedureName = "explain_depth_for_item",
                parameters = {
                        @StoredProcedureParameter(
                                name = "item_id",
                                type = Integer.class
                        ),
                        @StoredProcedureParameter(
                                name = "side",
                                type = Faction.class
                        ),
                        @StoredProcedureParameter(
                                name = "server",
                                type = String.class
                        ),
                        @StoredProcedureParameter(
                                name = "depth",
                                type = Integer.class
                        )
                }
        )
})
@Table(name = "prices")
public class Auction {

    private String server;
    @Column(name = "side")
    private Faction faction;
    private int price;
    private int itemId;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public Faction getFaction() {
        return faction;
    }

    public void setFaction(Faction faction) {
        this.faction = faction;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}
