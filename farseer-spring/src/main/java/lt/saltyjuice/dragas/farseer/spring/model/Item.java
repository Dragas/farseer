package lt.saltyjuice.dragas.farseer.spring.model;

import javax.persistence.*;

@Entity
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "get_item_id_like_name",
                procedureName = "get_item_id_like_name",
                parameters = {
                        @StoredProcedureParameter(
                                name = "item_name",
                                type = String.class
                        ),
                        @StoredProcedureParameter(
                                name = "amount",
                                type = Integer.class
                        )
                }
        )
})
@Table(name = "items")
public class Item {
    @Id
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
