package lt.saltyjuice.dragas.farseer.spring.repository;

import lt.saltyjuice.dragas.farseer.spring.model.Auction;
import lt.saltyjuice.dragas.farseer.spring.model.Faction;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface AuctionRepository extends Repository<Auction, Integer> {

    @Procedure("get_all_servers")
    public List<String> getAllServers();


    @Procedure("get_price_comparison_for_item")
    public List<Auction> getPriceComparisonForItem(
            @Param("item_id") int itemId,
            @Param("side") Faction side,
            @Param("server") String server,
            @Param("required_depth") int depth
    );

    @Procedure("explain_depth_for_item")
    public List<Auction> explainDepthForItem(
            @Param("item_id") int itemId,
            @Param("side") Faction side,
            @Param("server") String server,
            @Param("depth") int depth
    );
}
