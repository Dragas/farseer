package lt.saltyjuice.dragas.farseer.spring.model;

public enum Faction {
    alliance, horde
}
