package lt.saltyjuice.dragas.farseer.spring.repository;

import lt.saltyjuice.dragas.farseer.spring.model.Item;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ItemRepository extends Repository<Item, Integer> {

    @Procedure("get_item_id_like_name")
    public List<Item> getItemsByNameLikeness(@Param("item_name") String name, @Param("amount") Integer amount);

}
