package lt.saltyjuice.dragas.farseer.wowhead;

import org.apache.camel.Exchange;
import org.apache.camel.Expression;

public class FiletypeChangingExpression implements Expression {
    private final String newType;

    public FiletypeChangingExpression(String newType) {
        this.newType = String.format(".%s", newType);
    }


    @Override
    public <T> T evaluate(Exchange exchange, Class<T> type) {
        String originalFilename = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
        int firstDot = originalFilename.indexOf(".");
        return (T) new StringBuffer(originalFilename.substring(0, firstDot)).append(newType).toString();
    }

}

