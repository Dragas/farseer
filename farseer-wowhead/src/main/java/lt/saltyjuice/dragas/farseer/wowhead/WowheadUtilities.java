package lt.saltyjuice.dragas.farseer.wowhead;

public class WowheadUtilities {
    private static final String START_OF_URL = "Icon.create(";

    public String extractImageUrl(String body) {
        String firstLine = body.split("\n")[0];
        int start = firstLine.indexOf(START_OF_URL) + START_OF_URL.length() + 2;
        int end = firstLine.indexOf(",", start) - 1;
        return String.format("%s.jpg", firstLine.substring(start, end));
    }
}
