package lt.saltyjuice.dragas.farseer.wowhead;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;
import org.apache.camel.Expression;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;

public class WowheadRouteBuilder extends RouteBuilder {

    private static final ObjectMapper JACKSON_LENIENT = new ObjectMapper().configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
    private static final JacksonDataFormat JACKSON_DATA_FORMAT = new JacksonDataFormat(JACKSON_LENIENT, List.class);
    private static final String ITEM_NAME = "WowheadItemName";
    private static final String SPELL_ID = "WowheadSpellId";

    private static final String SEDA_INSERT_ITEM = "seda:insert_item";
    private static final String SEDA_INSERT_SPELL = "seda:insert_spell";
    private static final String SEDA_INSERT_REGENT = "seda:insert_regent";
    private static final String SEDA_INSERT_IMAGE = "seda:insert_image";

    private static final String DIRECT_GET_ITEM_ID = "direct:get_item_id";

    @Override
    public void configure() throws Exception {
        from("seda:crawlSearch?concurrentConsumers=8")
                .setHeader(Exchange.HTTP_QUERY, simple("filter=151:151;1:4;${headers.from}:${headers.to}"))
                .to("https://classic.wowhead.com/items?proxyHost=127.0.0.1&proxyPort=54241")
                .setHeader(Exchange.FILE_NAME, new Expression() {
                    @Override
                    public <T> T evaluate(Exchange exchange, Class<T> type) {
                        String uri = exchange.getIn().getHeader(Exchange.HTTP_QUERY, String.class);
                        int idIndex = uri.lastIndexOf(";");
                        String fromToIDs = uri.substring(idIndex + 1).replace(":", " - ");
                        return (T) (fromToIDs + ".html");
                    }
                })
                .to("file:output/html");
        from("file:output/html")
                .unmarshal("jsoup")
                .setBody((e) -> e.getIn().getBody(Document.class).select("script"))
                .split(bodyAs(List.class))
                .setBody((e) -> e.getIn().getBody(Element.class).html())
                .setHeader(Exchange.FILE_NAME, new Expression() {
                    @Override
                    public <T> T evaluate(Exchange exchange, Class<T> type) {
                        StringBuilder filename = new StringBuilder(exchange.getIn().getHeader(Exchange.FILE_NAME, String.class));
                        filename.append(String.format(" - %s.js", exchange.getProperty(Exchange.SPLIT_INDEX)));
                        return (T) filename.toString();
                    }
                })
                .to("file:output/scripts");
        from("file:output/scripts")
                .convertBodyTo(String.class)
                .choice()
                .when(body().startsWith("//<![CDATA["))
                .split(body().tokenize())
                .filter(body().startsWith("var listviewitems = "))
                .setBody((e) -> e.getIn().getBody(String.class).substring("var listviewitems = ".length()))
                .setBody((e) -> {
                    String body = e.getIn().getBody(String.class);
                    return body.substring(0, body.length() - 1);
                })
                .setHeader(Exchange.FILE_NAME, new FiletypeChangingExpression("json"))
                .to("file:output/json")
                .endChoice()
                .when(body().startsWith("WH.ge("))
                .bean(WowheadUtilities.class, "extractImageURL")
                .enrich("bean:chooseProxy", new ProxyAggregationStrategy())
                .setHeader(Exchange.FILE_NAME, new FiletypeChangingExpression("jpg"))
                .toD("https://wow.zamimg.com/images/wow/icons/large/${body}?proxyHost=${header.WH_PROXY}&proxyPort={header.WH_PPORT}")
                .to("file:output/images")
                .endChoice()
                .end()
        ;
/*        from("file:output/json/.camel/.camel/.camel")
                .unmarshal(JACKSON_DATA_FORMAT)
                .split(bodyAs(List.class), new ArrayListAggregationStrategy())
                .setBody((e) -> e.getIn().getBody(Map.class).get("id"))
                .end()
                .log("${body}")
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.FILE_NAME, constant("ids.json"))
                .to("file:output");*/

        from("file:output/item?readLock=markerFile")
                .log("Processing file ${headers.CamelFileName}")
                .unmarshal("jsoup")
                .multicast()
                .to(SEDA_INSERT_ITEM, SEDA_INSERT_IMAGE)
                .end()
                .setBody((e) -> e.getIn().getBody(Document.class).select("script"))
                .split(bodyAs(List.class))
                .setBody((e) -> e.getIn().getBody(Element.class).html())
                .filter(body().isNotEqualTo(""))
                .split(body().tokenize("}\\);\n"))
                .filter(body().contains("new Listview("))
                .filter(body().not().contains("if ("))
                .setBody(body().append("}"))
                .setBody(body().regexReplaceAll(".*new Listview\\(", ""))
                .filter(body().contains("id: 'created-by-spell',"))
                .split(body().tokenize(",\n"))
                .filter(body().startsWith("    data: "))
                .setBody(body().regexReplaceAll("^    data: ", ""))
                .unmarshal(JACKSON_DATA_FORMAT)
                .split(bodyAs(List.class))
                .multicast()
                .to(SEDA_INSERT_SPELL, SEDA_INSERT_ITEM)
                .end();



//        from("file:output?fileName=ids.json")
//                .unmarshal().json(JsonLibrary.Jackson)
//                .split(bodyAs(List.class))
//                .setProperty("itemid", body())
//                .setBody(constant(null))
//                .toD("https://classic.wowhead.com/item=${property.itemid}?proxyHost=127.0.0.1&proxyPort=54241")
//                .setHeader(Exchange.FILE_NAME, new Expression() {
//                    @Override
//                    public <T> T evaluate(Exchange exchange, Class<T> type) {
//                        Integer itemid = exchange.getProperty("itemid", Integer.class);
//                        return (T) (itemid + ".html");
//                    }
//                })
//                .to("file:output/item");
//        from("file:output?fileName=ids.json")
//                .unmarshal().json(JsonLibrary.Jackson)
//                .split(bodyAs(List.class)).parallelProcessing()
//                .filter(new Predicate() {
//                    @Override
//                    public boolean matches(Exchange exchange) {
//                        return !(new File(String.format("output/item/%s.html", exchange.getIn().getBody())).exists());
//                    }
//                })
//                .setProperty("itemid", simple("${body}"))
//                .setBody(constant(null))
//                .toD("https://classic.wowhead.com/item=${property.itemid}?proxyHost=127.0.0.1&proxyPort=54241")
//                .setHeader(Exchange.FILE_NAME, simple("${property.itemid}.html"))
//                .to("file:output/item");
        from(SEDA_INSERT_ITEM) // tbh this is fucking discustang. can't i use Transform or something?
                .process((e) -> {
                    Document d = e.getIn().getBody(Document.class);
                    String name = d.select("h1.heading-size-1").html();
                    Integer id = Integer.valueOf(e.getIn().getHeader(Exchange.FILE_NAME, String.class).split("\\.")[0]);
                    ItemPlaceholder itemPlaceholder = new ItemPlaceholder();
                    itemPlaceholder.setId(id);
                    itemPlaceholder.setName(name);
                    e.getIn().setBody(itemPlaceholder);
                })
                //.to("dozer:generate_item_query?mappingFile=dozer/item_query_mapping.xml&targetModel=lt.saltyjuice.dragas.wowhead.ItemPlaceholder")
                .to("sql:classpath:sql/insert_item_query.sql");
        from(SEDA_INSERT_SPELL)
                .to("dozer:generate_spell_query?mappingFile=dozer/spell_query_mapping.xml&targetModel=lt.saltyjuice.dragas.wowhead.SpellPlaceholder")
                .to("sql:classpath:sql/insert_spell_query.sql");
        from(SEDA_INSERT_REGENT)
                .setHeader(SPELL_ID, simple("${body['id']}"))
                .setBody(simple("${body['reagents']}"))
                .split(bodyAs(List.class))
                .process((e) -> {
                    List<Integer> ids = e.getIn().getBody(List.class);
                    RegentPlaceholder regent = new RegentPlaceholder();
                    regent.setItemId(ids.get(0));
                    regent.setCount(ids.get(1));
                    regent.setSpellId(e.getIn().getHeader(SPELL_ID, Long.class));
                    e.getIn().setBody(regent);
                })
                .to("sql:classpath:sql/insert_regent_query.sql");
    }
}
