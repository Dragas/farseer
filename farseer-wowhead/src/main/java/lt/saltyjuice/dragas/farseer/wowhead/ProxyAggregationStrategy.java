package lt.saltyjuice.dragas.farseer.wowhead;

import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;

public class ProxyAggregationStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        String[] datum = newExchange.getIn().getBody(String[].class);
        String host = datum[0];
        Integer port = Integer.valueOf(datum[1]);
        oldExchange.getIn().setHeader("WH_PROXY", host);
        oldExchange.getIn().setHeader("WH_PPORT", port);
        return oldExchange;
    }
}
