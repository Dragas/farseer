insert into items(id, name)
VALUES (:#${body.id}, :#${body.name})
on conflict (id) do nothing;
