insert into regents(item_id, spell_id, "count")
VALUES (:#${body.itemId}, :#${body.spellId}, :#${body.count})
on conflict (
   item_id,
   spell_id)
   do nothing;
