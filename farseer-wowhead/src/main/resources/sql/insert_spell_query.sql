insert into spells(id, name, creates_item_id)
VALUES (:#${body.id}, :#${body.name}, :#${body.createsItemId})
on conflict (id) do nothing;
