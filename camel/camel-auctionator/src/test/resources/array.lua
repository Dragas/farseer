AUCTIONATOR_SHOPPING_LISTS = {
    {
        ["items"] = {
            "bolt of runecloth", -- [1]
            "linen cloth", -- [2]
            "bolt of linen cloth", -- [3]
            "linen bag", -- [4]
            "red linen bag", -- [5]
            "felcloth", -- [6]
            "Devilsaur", -- [7]
            "\"silk cloth\"", -- [8]
            "naga scale", -- [9]
            "silk cloth", -- [10]
            "skeleton key", -- [11]
            "key", -- [12]
            "spider's silk", -- [13]
            "robes of arcana", -- [14]
            "pattern: robes of arcana", -- [15]
            "shrimp", -- [16]
            "stranglekelp", -- [17]
            "rugged leather", -- [18]
            "runecloth", -- [19]
            "mageweave bag", -- [20]
            "mageweave cloth", -- [21]
            "Azure shoulders", -- [22]
            "Azure silk pants", -- [23]
            "spider ", -- [24]
            "Goblin jumper cables", -- [25]
            "fused wiring", -- [26]
            "pattern:", -- [27]
            "heavy leather", -- [28]
            "shredder operating manual", -- [29]
            "salt shaker", -- [30]
            "heavy stone", -- [31]
            "heavy blasting power", -- [32]
            "wildvine", -- [33]
            "Dreamweave vest", -- [34]
            "bronze bar", -- [35]
            "silver bar", -- [36]
            "coarse", -- [37]
            "shiny fish scales", -- [38]
            "wool cloth", -- [39]
            "small silk pack", -- [40]
            "fine thread", -- [41]
            "small ", -- [42]
            "silken", -- [43]
            "silk bag", -- [44]
            "woolen bag", -- [45]
        },
        ["isRecents"] = 1,
        ["name"] = "Recent Searches",
    }, -- [1]
    {
        ["items"] = {
            "Greater Cosmic Essence", -- [1]
            "Infinite Dust", -- [2]
            "Dream Shard", -- [3]
            "Abyss Crystal", -- [4]
        },
        ["name"] = "Sample Shopping List #1",
        ["isSorted"] = false,
    }, -- [2]
}
