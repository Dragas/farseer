AUCTIONATOR_SAVEDVARS = {
    ["_50000"] = 500,
    ["_2000"] = 100,
    ["STARTING_DISCOUNT"] = 5,
    ["LOG_DE_DATA_X"] = true,
    ["_1000000"] = 2500,
    ["_5000000"] = 10000,
    ["_500"] = 5,
    ["_10000"] = 200,
    ["_200000"] = 1000,
}
AUCTIONATOR_PRICING_HISTORY = {
    ["Red Firework"] = {
        ["is"] = "9318:0:0:0:0",
        ["5850373"] = "95:5",
    },
    ["Sentry's Shoulderguards of the Bear"] = {
        ["is"] = "15531:0:0:1191:0",
        ["5857866"] = "24800:1",
        ["5863243"] = "24912:1",
        ["5858529"] = "24912:1",
    },
    ["Azure Shoulders"] = {
        ["5879198"] = "25033:1",
        ["5879320"] = "24800:1",
        ["is"] = "7060:0:0:0:0",
        ["5888141"] = "18600:1",
        ["5885063"] = "18600:1",
        ["5890293"] = "18598:1",
    },
    ["Buccaneer's Pants of the Eagle"] = {
        ["is"] = "14171:0:0:851:0",
        ["5847832"] = "4800:1",
    },
    ["Dwarven Magestaff of the Boar"] = {
        ["is"] = "2072:0:0:1109:0",
        ["5849879"] = "8900:1",
    },
    ["Wool Cloth"] = {
        ["5859211"] = "221:1",
        ["5870535"] = "247:1",
        ["5865060"] = "179:1",
        ["5857863"] = "240:20",
        ["is"] = "2592:0:0:0:0",
        ["5857865"] = "240:19",
        ["5869354"] = "191:1",
        ["5885087"] = "319:1",
    },
    ["Azure Silk Pants"] = {
        ["5842551"] = "4400:1",
        ["5844713"] = "3500:1",
        ["is"] = "7046:0:0:0:0",
        ["5841962"] = "1595:1",
        ["5843257"] = "3400:1",
        ["5843965"] = "3500:1",
    },
    ["Linen Cloth"] = {
        ["5859211"] = "19:1",
        ["5870536"] = "44:1",
        ["5869353"] = "31:1",
        ["5857865"] = "28:1",
        ["is"] = "2589:0:0:0:0",
    },
    ["Barbarian War Axe of the Monkey"] = {
        ["is"] = "3201:0:0:601:0",
        ["5846395"] = "13200:1",
    },
    ["Formula: Enchant Shield - Lesser Protection"] = {
        ["is"] = "11081:0:0:0:0",
        ["5852077"] = "585:1",
    },
    ["Precision Bow"] = {
        ["is"] = "8183:0:0:0:0",
        ["5854956"] = "4999:1",
    },
    ["Fighter Broadsword of Stamina"] = {
        ["is"] = "15212:0:0:96:0",
        ["5859311"] = "9400:1",
        ["5853600:hd"] = "11000:1",
        ["5857866"] = "10000:1",
    },
    ["Gloves of Meditation"] = {
        ["5840640:hd"] = "1500:2",
        ["5843520:hd"] = "1810:1",
        ["5849245"] = "2000:1",
        ["is"] = "4318:0:0:0:0",
        ["5847714"] = "2090:1",
        ["5842080:hd"] = "1750:2",
        ["5844960:hd"] = "2090:1",
    },
    ["Bolt of Woolen Cloth"] = {
        ["5848916"] = "487:10",
        ["5858532"] = "396:1",
        ["5848481"] = "488:10",
        ["is"] = "2997:0:0:0:0",
        ["5865064"] = "238:1",
    },
    ["Khadgar's Whisker"] = {
        ["is"] = "3358:0:0:0:0",
        ["5886451"] = "178:3",
    },
    ["Small Silk Pack"] = {
        ["is"] = "4245:0:0:0:0",
        ["5847840:hd"] = "3540:5",
        ["5860444"] = "3500:1",
        ["5856228"] = "3600:1",
        ["5843520:hd"] = "3300:1",
        ["5857872"] = "3400:1",
        ["5842080:hd"] = "3500:1",
        ["5844960:hd"] = "3500:1",
        ["5846400:hd"] = "3566.6666666667:3",
        ["5858529"] = "3500:1",
    },
    ["Bolt of Silk Cloth"] = {
        ["is"] = "4305:0:0:0:0",
        ["5846269"] = "1099:10",
    },
    ["Light Feather"] = {
        ["is"] = "17056:0:0:0:0",
        ["5857871"] = "47:1",
    },
    ["Shadowgem"] = {
        ["is"] = "1210:0:0:0:0",
        ["5842551"] = "270:1",
    },
    ["Shredder Operating Manual - Page 6"] = {
        ["is"] = "16650:0:0:0:0",
        ["5852078"] = "164:1",
    },
    ["Bright Mantle"] = {
        ["5854823"] = "12400:1",
        ["is"] = "4661:0:0:0:0",
        ["5857866"] = "3800:1",
    },
    ["Glimmering Mail Girdle"] = {
        ["is"] = "4712:0:0:0:0",
        ["5854957"] = "3000:1",
        ["5857873"] = "3800:1",
    },
    ["Bristlebark Amice"] = {
        ["is"] = "14573:0:0:0:0",
        ["5846395"] = "4500:1",
    },
    ["Cross-stitched Vest"] = {
        ["is"] = "1786:0:0:0:0",
        ["5859204"] = "1790:1",
    },
    ["Cross Dagger of Arcane Wrath"] = {
        ["is"] = "2819:0:0:1803:0",
        ["5857866"] = "10000:1",
        ["5858529"] = "10000:1",
    },
    ["Shredder Operating Manual - Page 11"] = {
        ["is"] = "16655:0:0:0:0",
        ["5852079"] = "297:1",
    },
    ["Shredder Operating Manual - Page 2"] = {
        ["is"] = "16646:0:0:0:0",
        ["5850353"] = "500:1",
        ["5852078"] = "278:1",
    },
    ["Citrine"] = {
        ["is"] = "3864:0:0:0:0",
        ["5886451"] = "795:1",
    },
    ["Bolt of Linen Cloth"] = {
        ["5858532"] = "116:1",
        ["is"] = "2996:0:0:0:0",
        ["5865064"] = "89:1",
    },
    ["Plans: Golden Scale Shoulders"] = {
        ["is"] = "3871:0:0:0:0",
        ["5886451"] = "19600:1",
        ["5888141"] = "19602:1",
    },
    ["Shredder Operating Manual - Page 7"] = {
        ["is"] = "16651:0:0:0:0",
        ["5852078"] = "495:1",
    },
    ["Dire Wand"] = {
        ["is"] = "8186:0:0:0:0",
        ["5850338"] = "3200:1",
    },
    ["Raincaller Mitts of the Owl"] = {
        ["5865059"] = "6500:1",
        ["is"] = "14191:0:0:766:0",
        ["5866275"] = "6500:1",
    },
    ["Shredder Operating Manual - Page 9"] = {
        ["is"] = "16653:0:0:0:0",
        ["5852078"] = "194:1",
    },
    ["Robes of Arcana"] = {
        ["5876425"] = "12299:1",
        ["5877942"] = "17400:1",
        ["5885083"] = "18000:1",
        ["5885079"] = "18000:1",
        ["5857920:hd"] = "15000:1",
        ["5878067"] = "17400:1",
        ["5863680:hd"] = "14900:1",
        ["5869360"] = "20400:1",
        ["5879187"] = "17400:1",
        ["5867879"] = "12699:1",
        ["5874903"] = "12800:1",
        ["5859360:hd"] = "18200:1",
        ["5890294"] = "15200:1",
        ["5888141"] = "18000:1",
        ["is"] = "5770:0:0:0:0",
        ["5871990"] = "20000:1",
        ["5862240:hd"] = "11000:1",
    },
    ["Bloody Brass Knuckles"] = {
        ["is"] = "7683:0:0:0:0",
        ["5878053"] = "8000:1",
    },
    ["Acrobatic Staff of Stamina"] = {
        ["is"] = "3185:0:0:218:0",
        ["5878053"] = "9800:1",
    },
    ["Large Blue Sack"] = {
        ["is"] = "804:0:0:0:0",
        ["5846395"] = "3700:1",
    },
    ["Shredder Operating Manual - Page 8"] = {
        ["is"] = "16652:0:0:0:0",
        ["5852078"] = "149:1",
    },
}
AUCTIONATOR_SHOPPING_LISTS = {
    {
        ["items"] = {
            "bolt of runecloth", -- [1]
            "linen cloth", -- [2]
            "bolt of linen cloth", -- [3]
            "linen bag", -- [4]
            "red linen bag", -- [5]
            "felcloth", -- [6]
            "Devilsaur", -- [7]
            "\"silk cloth\"", -- [8]
            "naga scale", -- [9]
            "silk cloth", -- [10]
            "skeleton key", -- [11]
            "key", -- [12]
            "spider's silk", -- [13]
            "robes of arcana", -- [14]
            "pattern: robes of arcana", -- [15]
            "shrimp", -- [16]
            "stranglekelp", -- [17]
            "rugged leather", -- [18]
            "runecloth", -- [19]
            "mageweave bag", -- [20]
            "mageweave cloth", -- [21]
            "Azure shoulders", -- [22]
            "Azure silk pants", -- [23]
            "spider ", -- [24]
            "Goblin jumper cables", -- [25]
            "fused wiring", -- [26]
            "pattern:", -- [27]
            "heavy leather", -- [28]
            "shredder operating manual", -- [29]
            "salt shaker", -- [30]
            "heavy stone", -- [31]
            "heavy blasting power", -- [32]
            "wildvine", -- [33]
            "Dreamweave vest", -- [34]
            "bronze bar", -- [35]
            "silver bar", -- [36]
            "coarse", -- [37]
            "shiny fish scales", -- [38]
            "wool cloth", -- [39]
            "small silk pack", -- [40]
            "fine thread", -- [41]
            "small ", -- [42]
            "silken", -- [43]
            "silk bag", -- [44]
            "woolen bag", -- [45]
        },
        ["isRecents"] = 1,
        ["name"] = "Recent Searches",
    }, -- [1]
    {
        ["items"] = {
            "Greater Cosmic Essence", -- [1]
            "Infinite Dust", -- [2]
            "Dream Shard", -- [3]
            "Abyss Crystal", -- [4]
        },
        ["name"] = "Sample Shopping List #1",
        ["isSorted"] = false,
    }, -- [2]
}
AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2 = true
AUCTIONATOR_PRICE_DATABASE = {
    ["__dbversion"] = 4,
    ["Flamelash_Horde"] = {
        ["Pattern: Azure Shoulders"] = {
            ["mr"] = 5000,
            ["sc"] = 2,
            ["id"] = "7085:0:0:0:0",
            ["cc"] = 9,
            ["H3245"] = 5000,
        },
        ["Battleforge Wristguards of the Bear"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Arcane Elixir"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Outrunner's Slippers of the Tiger"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Spined Bat Wing"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Murphstar of the Bear"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Chieftain's Headdress of the Monkey"] = {
            ["mr"] = 30000,
            ["cc"] = 4,
            ["id"] = "9953:0:0:623:0",
            ["H3246"] = 30000,
            ["sc"] = 2,
        },
        ["Furious Falchion of the Bear"] = {
            ["H3220"] = 22099,
            ["mr"] = 22099,
        },
        ["Banded Cloak of the Bear"] = {
            ["H3220"] = 2097,
            ["mr"] = 2097,
        },
        ["Phalanx Cloak of the Bear"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Jade Serpentblade"] = {
            ["H3220"] = 18900,
            ["mr"] = 18900,
        },
        ["Field Plate Girdle of Strength"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Rigid Leggings of Arcane Wrath"] = {
            ["H3220"] = 3330,
            ["mr"] = 3330,
        },
        ["Sanguine Robe"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Stormgale Fists"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Disciple's Vest of Frozen Wrath"] = {
            ["H3220"] = 615,
            ["mr"] = 615,
        },
        ["Defender Shield of the Tiger"] = {
            ["H3220"] = 5571,
            ["mr"] = 5571,
        },
        ["Heavy Lamellar Chestpiece of the Bear"] = {
            ["H3220"] = 84999,
            ["mr"] = 84999,
        },
        ["Raw Sagefish"] = {
            ["H3220"] = 125,
            ["mr"] = 125,
        },
        ["Mystic's Bracelets"] = {
            ["H3220"] = 1450,
            ["mr"] = 1450,
        },
        ["Vital Boots of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Ironpatch Blade"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Pattern: Felcloth Robe"] = {
            ["mr"] = 33999,
            ["sc"] = 2,
            ["id"] = "14506:0:0:0:0",
            ["H3246"] = 33999,
            ["cc"] = 9,
        },
        ["Recipe: Elixir of Giants"] = {
            ["mr"] = 150000,
            ["H3254"] = 150000,
            ["H3220"] = 44799,
        },
        ["Renegade Leggings of the Monkey"] = {
            ["mr"] = 9900,
            ["cc"] = 4,
            ["id"] = "9871:0:0:611:0",
            ["H3246"] = 9900,
            ["sc"] = 3,
        },
        ["Ritual Bands of the Whale"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Felcloth Hood"] = {
            ["mr"] = 197499,
            ["cc"] = 4,
            ["id"] = "14111:0:0:0:0",
            ["H3246"] = 197499,
            ["sc"] = 1,
        },
        ["Fine Leather Gloves"] = {
            ["H3220"] = 496,
            ["mr"] = 496,
        },
        ["Soldier's Armor of the Gorilla"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Sage's Bracers of Stamina"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Watcher's Cuffs of Healing"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Native Vest of the Whale"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Bronze Bar"] = {
            ["L3226"] = 131,
            ["mr"] = 148,
            ["sc"] = 0,
            ["id"] = "2841:0:0:0:0",
            ["cc"] = 7,
            ["H3226"] = 157,
        },
        ["Twilight Armor of Intellect"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Naga Scale"] = {
            ["mr"] = 170,
            ["sc"] = 0,
            ["id"] = "7072:0:0:0:0",
            ["H3246"] = 170,
            ["cc"] = 5,
        },
        ["Sentry's Sash of Healing"] = {
            ["H3220"] = 1596,
            ["mr"] = 1596,
        },
        ["Stonecutter Claymore of Agility"] = {
            ["H3220"] = 19600,
            ["mr"] = 19600,
        },
        ["Deadly Kris of the Monkey"] = {
            ["mr"] = 15339,
            ["sc"] = 15,
            ["id"] = "15243:0:0:592:0",
            ["H3246"] = 15339,
            ["cc"] = 2,
        },
        ["Firebane Cloak"] = {
            ["H3220"] = 14900,
            ["mr"] = 14900,
        },
        ["Raincaller Mantle of Shadow Wrath"] = {
            ["H3220"] = 16600,
            ["mr"] = 16600,
        },
        ["Leaden Mace of Power"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Sorcerer Mantle of Frozen Wrath"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Hacking Cleaver of the Tiger"] = {
            ["H3220"] = 7536,
            ["mr"] = 7536,
        },
        ["Azora's Will"] = {
            ["H3220"] = 6338,
            ["mr"] = 6338,
        },
        ["Schematic: Parachute Cloak"] = {
            ["H3220"] = 7100,
            ["mr"] = 7100,
        },
        ["Mithril Head Trout"] = {
            ["H3220"] = 26,
            ["mr"] = 26,
        },
        ["High Councillor's Cloak of Intellect"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Withered Staff"] = {
            ["H3220"] = 399,
            ["mr"] = 399,
        },
        ["Defender Leggings of the Gorilla"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Ritual Sandals of Arcane Wrath"] = {
            ["H3220"] = 1142,
            ["mr"] = 1142,
        },
        ["Barbed Club of Shadow Wrath"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Barbarian War Axe of the Wolf"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Recipe: Shadow Protection Potion"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Lesser Healing Potion"] = {
            ["H3220"] = 29,
            ["mr"] = 29,
        },
        ["Raider's Belt of Power"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Stonecutter Claymore of Power"] = {
            ["H3220"] = 17700,
            ["mr"] = 17700,
        },
        ["Feral Bindings of Nature's Wrath"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Raider's Chestpiece of Stamina"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Grunt's Shield of Agility"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Runecloth Belt"] = {
            ["mr"] = 9100,
            ["cc"] = 4,
            ["H3239"] = 9100,
            ["id"] = "13856:0:0:0:0",
            ["sc"] = 1,
        },
        ["Serpent's Kiss"] = {
            ["H3220"] = 14800,
            ["mr"] = 14800,
        },
        ["Goldenbark Apple"] = {
            ["H3220"] = 595,
            ["mr"] = 595,
        },
        ["Ritual Leggings of the Owl"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Greenweave Sash of Intellect"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Mithril Ore"] = {
            ["H3220"] = 1980,
            ["mr"] = 1980,
        },
        ["Superior Tunic of Nature's Wrath"] = {
            ["H3220"] = 5487,
            ["mr"] = 5487,
        },
        ["Schematic: Small Seaforium Charge"] = {
            ["mr"] = 398,
            ["cc"] = 9,
            ["L3220"] = 398,
            ["id"] = "4409:0:0:0:0",
            ["sc"] = 3,
            ["H3220"] = 399,
        },
        ["Pattern: Hillman's Belt"] = {
            ["mr"] = 795,
            ["cc"] = 9,
            ["id"] = "4294:0:0:0:0",
            ["L3229"] = 795,
            ["H3229"] = 800,
            ["sc"] = 1,
        },
        ["Strong Troll's Blood Potion"] = {
            ["H3220"] = 1780,
            ["mr"] = 1780,
        },
        ["Rich Purple Silk Shirt"] = {
            ["H3220"] = 25099,
            ["mr"] = 25099,
        },
        ["Leaden Mace of the Monkey"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Deckhand's Shirt"] = {
            ["H3220"] = 233,
            ["mr"] = 233,
        },
        ["Chromite Girdle"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Ceremonial Leather Gloves"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Ghostwalker Pads of the Monkey"] = {
            ["H3220"] = 11999,
            ["mr"] = 11999,
        },
        ["Interlaced Pants"] = {
            ["H3254"] = 5076,
            ["mr"] = 5076,
        },
        ["Bloodpike"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Ranger Wristguards of Spirit"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Black Dragonscale Shoulders"] = {
            ["mr"] = 17400,
            ["sc"] = 1,
            ["id"] = "15770:0:0:0:0",
            ["H3229"] = 17400,
            ["cc"] = 9,
        },
        ["Swashbuckler's Eyepatch of the Monkey"] = {
            ["mr"] = 73000,
            ["cc"] = 4,
            ["id"] = "10187:0:0:629:0",
            ["H3246"] = 73000,
            ["sc"] = 2,
        },
        ["Warmonger's Bracers of the Eagle"] = {
            ["H3220"] = 10499,
            ["mr"] = 10499,
        },
        ["Infiltrator Cloak of the Whale"] = {
            ["H3220"] = 5307,
            ["mr"] = 5307,
        },
        ["Simple Linen Pants"] = {
            ["H3220"] = 2295,
            ["mr"] = 2295,
        },
        ["Ceremonial Buckler"] = {
            ["H3220"] = 1445,
            ["mr"] = 1445,
        },
        ["Wyvern Tailspike"] = {
            ["H3220"] = 4100,
            ["mr"] = 4100,
        },
        ["Sorcerer Cloak of the Eagle"] = {
            ["H3220"] = 3499,
            ["mr"] = 3499,
        },
        ["Jagged Star of Strength"] = {
            ["H3220"] = 3499,
            ["mr"] = 3499,
        },
        ["Skystriker Bow"] = {
            ["H3220"] = 33477,
            ["mr"] = 33477,
        },
        ["Ember Wand of Healing"] = {
            ["H3254"] = 24500,
            ["mr"] = 24500,
        },
        ["Plans: Hardened Iron Shortsword"] = {
            ["H3220"] = 19800,
            ["mr"] = 19800,
        },
        ["Oak Mallet of Spirit"] = {
            ["H3220"] = 3107,
            ["mr"] = 3107,
        },
        ["Spiked Club of Power"] = {
            ["H3220"] = 928,
            ["mr"] = 928,
        },
        ["Thick Scale Shield of the Tiger"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Scroll of Spirit II"] = {
            ["H3220"] = 1229,
            ["mr"] = 1229,
        },
        ["Skinning Knife"] = {
            ["H3220"] = 840,
            ["mr"] = 840,
        },
        ["Mechanical Squirrel Box"] = {
            ["H3220"] = 1998,
            ["mr"] = 1998,
        },
        ["Cabalist Chestpiece of the Eagle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Bloodspattered Loincloth of the Bear"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Phalanx Spaulders of the Bear"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Greenweave Leggings of the Monkey"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Sage's Circlet of Fiery Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Twilight Gloves of the Whale"] = {
            ["H3220"] = 3667,
            ["mr"] = 3667,
        },
        ["Brackwater Shield"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Embersilk Cord of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Mystic's Sphere"] = {
            ["H3220"] = 1188,
            ["mr"] = 1188,
        },
        ["Revenant Boots of the Tiger"] = {
            ["H3220"] = 34400,
            ["mr"] = 34400,
        },
        ["Buccaneer's Boots of the Falcon"] = {
            ["H3220"] = 1172,
            ["mr"] = 1172,
        },
        ["Bright Pants"] = {
            ["H3220"] = 3129,
            ["mr"] = 3129,
        },
        ["Decapitating Sword of the Bear"] = {
            ["H3220"] = 3375,
            ["mr"] = 3375,
        },
        ["Elder's Boots of the Eagle"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Scalping Tomahawk of Strength"] = {
            ["H3220"] = 323,
            ["mr"] = 323,
        },
        ["Native Pants of Fiery Wrath"] = {
            ["H3220"] = 2831,
            ["mr"] = 2831,
        },
        ["Wrangler's Gloves of the Falcon"] = {
            ["H3220"] = 3458,
            ["mr"] = 3458,
        },
        ["Aurora Mantle"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Burnished Girdle"] = {
            ["H3220"] = 1304,
            ["mr"] = 1304,
        },
        ["Felcloth"] = {
            ["mr"] = 25000,
            ["sc"] = 0,
            ["id"] = "14256:0:0:0:0",
            ["H3246"] = 25000,
            ["cc"] = 7,
        },
        ["Elixir of Giant Growth"] = {
            ["H3220"] = 128,
            ["mr"] = 128,
        },
        ["Prospector's Woolies"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Recipe: Sagefish Delight"] = {
            ["H3220"] = 7300,
            ["mr"] = 7300,
        },
        ["Pattern: Mooncloth Bag"] = {
            ["mr"] = 930000,
            ["sc"] = 2,
            ["id"] = "14499:0:0:0:0",
            ["H3229"] = 930000,
            ["cc"] = 9,
        },
        ["Banded Pauldrons of the Bear"] = {
            ["H3220"] = 8999,
            ["mr"] = 8999,
        },
        ["Battlesmasher of the Monkey"] = {
            ["mr"] = 4988,
            ["sc"] = 4,
            ["id"] = "15224:0:0:588:0",
            ["L3246"] = 4988,
            ["H3246"] = 6141,
            ["cc"] = 2,
        },
        ["Amethyst Band of Shadow Resistance"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Dwarven Magestaff of Stamina"] = {
            ["H3220"] = 13242,
            ["mr"] = 13242,
        },
        ["Battlesmasher of Arcane Wrath"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Golden Scale Boots"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Gothic Plate Helmet of the Bear"] = {
            ["H3220"] = 18999,
            ["mr"] = 18999,
        },
        ["Craftsman's Monocle"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Mooncloth"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Dwarven Hatchet of Stamina"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Hook Dagger of Healing"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Salt Shaker"] = {
            ["mr"] = 105000,
            ["cc"] = 7,
            ["H3226"] = 105000,
            ["id"] = "15846:0:0:0:0",
            ["sc"] = 3,
        },
        ["Tender Wolf Steak"] = {
            ["H3220"] = 485,
            ["mr"] = 485,
        },
        ["Powerful Mojo"] = {
            ["H3220"] = 34800,
            ["mr"] = 34800,
        },
        ["Twilight Cowl of Shadow Wrath"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Sentinel Girdle of the Monkey"] = {
            ["mr"] = 18000,
            ["sc"] = 2,
            ["id"] = "7448:0:0:602:0",
            ["H3246"] = 18000,
            ["cc"] = 4,
        },
        ["Insignia Cap"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Umbral Wand of the Falcon"] = {
            ["H3220"] = 22000,
            ["mr"] = 22000,
        },
        ["Sentinel Buckler of Agility"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Outrunner's Chestguard of the Bear"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Evocator's Blade"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Rugged Armor Kit"] = {
            ["H3220"] = 4700,
            ["mr"] = 4700,
        },
        ["Headhunter's Bands of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Cured Medium Hide"] = {
            ["H3220"] = 580,
            ["mr"] = 580,
        },
        ["Bloodforged Legplates of the Bear"] = {
            ["H3220"] = 28200,
            ["mr"] = 28200,
        },
        ["Zircon Band of Arcane Resistance"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["Spiked Club of the Gorilla"] = {
            ["H3220"] = 890,
            ["mr"] = 890,
        },
        ["Serpent Slicer"] = {
            ["H3220"] = 298544,
            ["mr"] = 298544,
        },
        ["Wolf Rider's Belt of Defense"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Buccaneer's Pants of the Owl"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Feral Cloak of the Wolf"] = {
            ["H3220"] = 1063,
            ["mr"] = 1063,
        },
        ["Bard's Trousers of the Eagle"] = {
            ["H3220"] = 1249,
            ["mr"] = 1249,
        },
        ["Recipe: Elixir of Fortitude"] = {
            ["mr"] = 5000,
            ["H3254"] = 5000,
            ["H3220"] = 2000,
        },
        ["Pattern: Felcloth Hood"] = {
            ["mr"] = 86500,
            ["cc"] = 9,
            ["id"] = "14496:0:0:0:0",
            ["H3246"] = 86500,
            ["sc"] = 2,
        },
        ["Elder's Boots of Frozen Wrath"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Deadly Kris of the Bear"] = {
            ["H3220"] = 12999,
            ["mr"] = 12999,
        },
        ["Deviate Scale Cloak"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Embersilk Cord of Fiery Wrath"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Gothic Shield of Stamina"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Shredder Operating Manual - Page 1"] = {
            ["mr"] = 143,
            ["cc"] = 15,
            ["id"] = "16645:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 143,
        },
        ["Redbeard Crest"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Overlord's Greaves of the Monkey"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Ranger Boots of the Falcon"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Brown Leather Satchel"] = {
            ["H3220"] = 1159,
            ["mr"] = 1159,
        },
        ["Pattern: Runic Leather Gauntlets"] = {
            ["mr"] = 7500,
            ["cc"] = 9,
            ["id"] = "15731:0:0:0:0",
            ["L3229"] = 7500,
            ["H3229"] = 7550,
            ["sc"] = 1,
        },
        ["Durable Gloves of the Owl"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Band of Thorns"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Dreamless Sleep Potion"] = {
            ["H3220"] = 1950,
            ["mr"] = 1950,
        },
        ["Red Wolf Meat"] = {
            ["mr"] = 1548,
            ["H3254"] = 1548,
            ["H3220"] = 129,
        },
        ["Cutthroat's Belt of the Falcon"] = {
            ["H3220"] = 6068,
            ["mr"] = 6068,
        },
        ["Nightsky Gloves"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Watcher's Mantle of the Wolf"] = {
            ["H3220"] = 6088,
            ["mr"] = 6088,
        },
        ["Harbinger Boots"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Royal Mallet of the Monkey"] = {
            ["mr"] = 55440,
            ["cc"] = 2,
            ["id"] = "15263:0:0:623:0",
            ["H3246"] = 55440,
            ["sc"] = 5,
        },
        ["Gallant Flamberge of the Tiger"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Huntsman's Gloves of the Eagle"] = {
            ["H3220"] = 8100,
            ["mr"] = 8100,
        },
        ["Scorpion Sting"] = {
            ["H3220"] = 48900,
            ["mr"] = 48900,
        },
        ["Stonecloth Circlet"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Archer's Belt of Defense"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Cabalist Spaulders of the Monkey"] = {
            ["mr"] = 24925,
            ["sc"] = 2,
            ["id"] = "7532:0:0:610:0",
            ["H3246"] = 24925,
            ["cc"] = 4,
        },
        ["Jazeraint Chestguard of the Whale"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Hulking Cloak"] = {
            ["H3220"] = 794,
            ["mr"] = 794,
        },
        ["The Shadowfoot Stabber"] = {
            ["H3220"] = 490000,
            ["mr"] = 490000,
        },
        ["Flash Bomb"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Scroll of Protection III"] = {
            ["H3220"] = 855,
            ["mr"] = 855,
        },
        ["Banded Girdle of the Boar"] = {
            ["H3220"] = 4296,
            ["mr"] = 4296,
        },
        ["Robes of Arcana"] = {
            ["id"] = "5770:0:0:0:0",
            ["H3251"] = 16800,
            ["mr"] = 15400,
            ["H3250"] = 17999,
            ["cc"] = 4,
            ["H3254"] = 15400,
            ["H3252"] = 15599,
            ["H3253"] = 15099,
            ["sc"] = 1,
        },
        ["Huntsman's Armor of the Monkey"] = {
            ["mr"] = 17500,
            ["sc"] = 2,
            ["id"] = "9887:0:0:615:0",
            ["H3246"] = 17500,
            ["cc"] = 4,
        },
        ["Acrobatic Staff of Stamina"] = {
            ["mr"] = 9800,
            ["cc"] = 2,
            ["id"] = "3185:0:0:218:0",
            ["H3246"] = 9800,
            ["sc"] = 10,
        },
        ["Cerulean Ring of the Whale"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Huntsman's Shoulders of the Monkey"] = {
            ["H3220"] = 17999,
            ["mr"] = 17999,
        },
        ["Sharp Claw"] = {
            ["H3220"] = 48,
            ["mr"] = 48,
        },
        ["Greenweave Mantle of Shadow Wrath"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Emblazoned Bracers"] = {
            ["H3220"] = 1975,
            ["mr"] = 1975,
        },
        ["Exquisite Flamberge of Agility"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Formula: Enchant Gloves - Advanced Herbalism"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Battering Hammer of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Lord's Legguards of the Owl"] = {
            ["H3220"] = 28800,
            ["mr"] = 28800,
        },
        ["Schematic: World Enlarger"] = {
            ["H3220"] = 34999,
            ["mr"] = 34999,
        },
        ["Forest Tracker Epaulets"] = {
            ["H3220"] = 42934,
            ["mr"] = 42934,
        },
        ["Bloodspattered Sash of Power"] = {
            ["H3220"] = 1452,
            ["mr"] = 1452,
        },
        ["Ironhide Cloak of the Falcon"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Hillborne Axe of the Monkey"] = {
            ["mr"] = 10000,
            ["sc"] = 0,
            ["id"] = "2080:0:0:591:0",
            ["H3246"] = 10000,
            ["cc"] = 2,
        },
        ["Sage's Mantle of Stamina"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Gossamer Tunic of the Eagle"] = {
            ["H3220"] = 28400,
            ["mr"] = 28400,
        },
        ["Lupine Handwraps of Spirit"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Pattern: Chimeric Leggings"] = {
            ["mr"] = 28500,
            ["cc"] = 9,
            ["id"] = "15746:0:0:0:0",
            ["L3229"] = 28500,
            ["H3229"] = 68975,
            ["sc"] = 1,
        },
        ["Gloom Reaper of the Bear"] = {
            ["H3220"] = 15015,
            ["mr"] = 15015,
        },
        ["Rough Dynamite"] = {
            ["H3220"] = 80,
            ["mr"] = 80,
        },
        ["Silver Contact"] = {
            ["H3220"] = 30,
            ["mr"] = 30,
        },
        ["Runic Stave"] = {
            ["H3220"] = 1495,
            ["mr"] = 1495,
        },
        ["Sequoia Branch of the Bear"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Sentry's Leggings of the Bear"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Ritual Sandals of the Owl"] = {
            ["H3220"] = 1636,
            ["mr"] = 1636,
        },
        ["Scaled Leather Leggings of the Gorilla"] = {
            ["H3220"] = 36372,
            ["mr"] = 36372,
        },
        ["Robust Girdle of the Bear"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Ballast Maul of the Boar"] = {
            ["H3220"] = 12953,
            ["mr"] = 12953,
        },
        ["Aboriginal Vest of the Whale"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Sage's Circlet of the Owl"] = {
            ["H3220"] = 21642,
            ["mr"] = 21642,
        },
        ["Vital Headband of the Owl"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Silksand Girdle"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Barbaric Battle Axe of the Bear"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Alterac Swiss"] = {
            ["mr"] = 1700,
            ["H3254"] = 1700,
            ["H3220"] = 2223,
        },
        ["Skull Splitting Crossbow"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Abjurer's Sash of the Monkey"] = {
            ["mr"] = 12372,
            ["cc"] = 4,
            ["id"] = "9945:0:0:611:0",
            ["H3246"] = 12372,
            ["sc"] = 1,
        },
        ["Soothing Turtle Bisque"] = {
            ["H3220"] = 440,
            ["mr"] = 440,
        },
        ["Heavy Scorpid Scale"] = {
            ["H3220"] = 4698,
            ["mr"] = 4698,
        },
        ["Brackwater Vest"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Brutal War Axe of the Whale"] = {
            ["H3220"] = 16600,
            ["mr"] = 16600,
        },
        ["Conjurer's Breeches of the Whale"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Infiltrator Bracers of the Wolf"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Light Armor Kit"] = {
            ["H3220"] = 17,
            ["mr"] = 17,
        },
        ["War Paint Anklewraps"] = {
            ["H3220"] = 845,
            ["mr"] = 845,
        },
        ["Fighter Broadsword of Stamina"] = {
            ["mr"] = 4000,
            ["cc"] = 2,
            ["id"] = "15212:0:0:96:0",
            ["H3235"] = 4000,
            ["sc"] = 7,
        },
        ["Elixir of Firepower"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Fine Scimitar"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Leaded Vial"] = {
            ["H3220"] = 195,
            ["mr"] = 195,
        },
        ["Parrot Cage (Green Wing Macaw)"] = {
            ["H3220"] = 124250,
            ["mr"] = 124250,
        },
        ["Hefty Battlehammer of the Gorilla"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Scaled Leather Belt of the Monkey"] = {
            ["H3220"] = 2438,
            ["mr"] = 2438,
        },
        ["Hefty Battlehammer of the Boar"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Watcher's Star of Healing"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Crested Scepter"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Nocturnal Tunic of the Monkey"] = {
            ["H3220"] = 20202,
            ["mr"] = 20202,
        },
        ["Duskwoven Gloves of the Owl"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Blood Shard"] = {
            ["H3220"] = 25,
            ["mr"] = 25,
        },
        ["Birchwood Maul of the Monkey"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Monk's Staff of the Eagle"] = {
            ["H3220"] = 31000,
            ["mr"] = 31000,
        },
        ["Book: Gift of the Wild"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Lupine Buckler of Spirit"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Killmaim"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Magician Staff of the Wolf"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Cutthroat's Mitts of the Monkey"] = {
            ["mr"] = 25000,
            ["cc"] = 4,
            ["id"] = "15137:0:0:597:0",
            ["H3246"] = 25000,
            ["sc"] = 2,
        },
        ["Wolf Rider's Shoulder Pads of the Monkey"] = {
            ["mr"] = 18000,
            ["sc"] = 2,
            ["id"] = "15375:0:0:608:0",
            ["H3246"] = 18000,
            ["cc"] = 4,
        },
        ["Brigade Girdle of Defense"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Dwarven Magestaff of Intellect"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Lodestone Necklace of the Bear"] = {
            ["H3220"] = 42000,
            ["mr"] = 42000,
        },
        ["Notched Shortsword of Power"] = {
            ["H3220"] = 695,
            ["mr"] = 695,
        },
        ["Lupine Handwraps of the Falcon"] = {
            ["H3220"] = 528,
            ["mr"] = 528,
        },
        ["Symbolic Vambraces"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Willow Boots of Shadow Wrath"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Wrangler's Mantle of the Whale"] = {
            ["H3220"] = 10100,
            ["mr"] = 10100,
        },
        ["Bloodspattered Gloves of the Whale"] = {
            ["H3220"] = 878,
            ["mr"] = 878,
        },
        ["Silksand Bracers"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Training Sword of the Boar"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Pattern: Red Mageweave Vest"] = {
            ["mr"] = 8400,
            ["sc"] = 2,
            ["id"] = "10300:0:0:0:0",
            ["H3229"] = 8400,
            ["cc"] = 9,
        },
        ["Infiltrator Armor of the Falcon"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Training Sword of the Monkey"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Globe of Water"] = {
            ["H3220"] = 790,
            ["mr"] = 790,
        },
        ["Double-stitched Woolen Shoulders"] = {
            ["H3220"] = 390,
            ["mr"] = 390,
        },
        ["Wrangler's Boots of the Falcon"] = {
            ["H3220"] = 3903,
            ["mr"] = 3903,
        },
        ["Scroll of Stamina III"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Sanguine Cape"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Cow King's Hide"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Edged Bastard Sword of the Whale"] = {
            ["H3220"] = 2417,
            ["mr"] = 2417,
        },
        ["Formula: Enchant Shield - Stamina"] = {
            ["H3220"] = 5599,
            ["mr"] = 5599,
        },
        ["Charger's Armor of the Gorilla"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Keeper's Wreath"] = {
            ["H3220"] = 24100,
            ["mr"] = 24100,
        },
        ["Sniper Rifle of the Falcon"] = {
            ["H3220"] = 28000,
            ["mr"] = 28000,
        },
        ["Grizzly Jerkin of the Owl"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Quickdraw Quiver"] = {
            ["H3220"] = 13555,
            ["mr"] = 13555,
        },
        ["Freezing Shard"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Handstitched Leather Boots"] = {
            ["H3220"] = 50,
            ["mr"] = 50,
        },
        ["Crafted Light Shot"] = {
            ["H3220"] = 49,
            ["mr"] = 49,
        },
        ["Lupine Slippers of the Monkey"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Rage Potion"] = {
            ["H3220"] = 224,
            ["mr"] = 224,
        },
        ["Magic Resistance Potion"] = {
            ["H3220"] = 1950,
            ["mr"] = 1950,
        },
        ["Nightblade"] = {
            ["H3220"] = 401100,
            ["mr"] = 401100,
        },
        ["Goblin Deviled Clams"] = {
            ["H3220"] = 104,
            ["mr"] = 104,
        },
        ["Nightsky Armor"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Troll's Bane Leggings"] = {
            ["H3220"] = 23888,
            ["mr"] = 23888,
        },
        ["Jet Chain of the Bear"] = {
            ["H3220"] = 10355,
            ["mr"] = 10355,
        },
        ["Knightly Longsword of the Tiger"] = {
            ["H3220"] = 14000,
            ["mr"] = 14000,
        },
        ["Monstrous War Axe of Stamina"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Bloodspattered Sabatons of Defense"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Monk's Staff of Frozen Wrath"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Sequoia Hammer of Healing"] = {
            ["H3220"] = 17874,
            ["mr"] = 17874,
        },
        ["Magician Staff of the Monkey"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Sorcerer Cloak of the Owl"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["War Torn Tunic of the Boar"] = {
            ["H3220"] = 1399,
            ["mr"] = 1399,
        },
        ["Pattern: Red Linen Vest"] = {
            ["mr"] = 293,
            ["sc"] = 2,
            ["id"] = "6271:0:0:0:0",
            ["H3229"] = 293,
            ["cc"] = 9,
        },
        ["Acrobatic Staff of Shadow Wrath"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Fortified Gauntlets of the Bear"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Magician Staff of the Eagle"] = {
            ["H3220"] = 7800,
            ["mr"] = 7800,
        },
        ["Aegis of Stormwind"] = {
            ["H3220"] = 130000,
            ["mr"] = 130000,
        },
        ["Raider's Legguards of Power"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Wicked Chain Legguards of the Eagle"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Goblin Rocket Boots"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Pattern: Shadow Hood"] = {
            ["mr"] = 9333,
            ["sc"] = 2,
            ["id"] = "4351:0:0:0:0",
            ["H3229"] = 9333,
            ["cc"] = 9,
        },
        ["Scroll of Protection"] = {
            ["H3220"] = 58,
            ["mr"] = 58,
        },
        ["Grunt's Belt of Strength"] = {
            ["H3220"] = 995,
            ["mr"] = 995,
        },
        ["Gold Ore"] = {
            ["H3220"] = 985,
            ["mr"] = 985,
        },
        ["Burnished Cloak"] = {
            ["H3220"] = 1060,
            ["mr"] = 1060,
        },
        ["Mature Blue Dragon Sinew"] = {
            ["H3220"] = 6000000,
            ["mr"] = 6000000,
        },
        ["Core of Earth"] = {
            ["H3220"] = 740,
            ["mr"] = 740,
        },
        ["Trueshot Bow of Spirit"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Pattern: Big Voodoo Cloak"] = {
            ["mr"] = 3000,
            ["sc"] = 1,
            ["id"] = "8390:0:0:0:0",
            ["H3229"] = 3000,
            ["cc"] = 9,
        },
        ["Wrangler's Cloak of Intellect"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Lunar Raiment of the Owl"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Scouting Boots of the Falcon"] = {
            ["H3220"] = 2856,
            ["mr"] = 2856,
        },
        ["Magician Staff of Spirit"] = {
            ["H3220"] = 9720,
            ["mr"] = 9720,
        },
        ["Crystal Basilisk Spine"] = {
            ["H3220"] = 611,
            ["mr"] = 611,
        },
        ["Brutish Boots of the Bear"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Ritual Leggings of Fiery Wrath"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Sequoia Branch of Strength"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Raw Mithril Head Trout"] = {
            ["H3220"] = 96,
            ["mr"] = 96,
        },
        ["Bandit Bracers of Spirit"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Marauder's Leggings of the Monkey"] = {
            ["H3220"] = 14500,
            ["mr"] = 14500,
        },
        ["Tusker Sword of the Monkey"] = {
            ["mr"] = 43097,
            ["cc"] = 2,
            ["id"] = "15252:0:0:622:0",
            ["H3246"] = 43097,
            ["sc"] = 8,
        },
        ["Lunar Mantle of the Whale"] = {
            ["H3220"] = 11500,
            ["mr"] = 11500,
        },
        ["Sheepshear Mantle"] = {
            ["H3220"] = 99000,
            ["mr"] = 99000,
        },
        ["Nightsky Cowl"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Revenant Chestplate of the Bear"] = {
            ["H3220"] = 29900,
            ["mr"] = 29900,
        },
        ["Gryphon Mail Bracelets of Agility"] = {
            ["H3220"] = 24500,
            ["mr"] = 24500,
        },
        ["Skullsplitter Helm"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Forester's Axe of the Bear"] = {
            ["H3220"] = 4404,
            ["mr"] = 4404,
        },
        ["Recipe: Mighty Rage Potion"] = {
            ["H3220"] = 119999,
            ["mr"] = 119999,
        },
        ["Archer's Cloak of the Whale"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Stonecloth Epaulets"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Gossamer Shoulderpads of Stamina"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Lupine Handwraps of the Monkey"] = {
            ["mr"] = 20000,
            ["cc"] = 4,
            ["id"] = "15016:0:0:589:0",
            ["H3246"] = 20000,
            ["sc"] = 2,
        },
        ["Glimmering Mail Gauntlets"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Hacking Cleaver of Agility"] = {
            ["H3220"] = 9168,
            ["mr"] = 9168,
        },
        ["Robust Boots of the Whale"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Pathfinder Bracers of Stamina"] = {
            ["H3220"] = 3078,
            ["mr"] = 3078,
        },
        ["Rageclaw Helm of the Monkey"] = {
            ["mr"] = 27465,
            ["sc"] = 2,
            ["id"] = "15384:0:0:623:0",
            ["H3246"] = 27465,
            ["cc"] = 4,
        },
        ["Wildheart Bracers"] = {
            ["H3220"] = 90000,
            ["mr"] = 90000,
        },
        ["Sungrass"] = {
            ["H3220"] = 2368,
            ["mr"] = 2368,
        },
        ["Rigid Tunic of Nature's Wrath"] = {
            ["H3220"] = 6969,
            ["mr"] = 6969,
        },
        ["Wicked Chain Waistband of the Gorilla"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Chieftain's Shoulders of the Monkey"] = {
            ["mr"] = 102360,
            ["sc"] = 2,
            ["id"] = "9955:0:0:613:0",
            ["H3246"] = 102360,
            ["cc"] = 4,
        },
        ["Overlord's Girdle of the Eagle"] = {
            ["H3220"] = 9200,
            ["mr"] = 9200,
        },
        ["Sniper Rifle of the Monkey"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Coral Band of Strength"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Sunscale Sabatons"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Battle Slayer of Agility"] = {
            ["H3220"] = 6786,
            ["mr"] = 6786,
        },
        ["Stranglekelp"] = {
            ["mr"] = 1911,
            ["cc"] = 7,
            ["id"] = "3820:0:0:0:0",
            ["H3244"] = 441,
            ["H3254"] = 1911,
            ["sc"] = 0,
        },
        ["Defender Shield of the Bear"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Burning Charm"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Gnarled Ash Staff"] = {
            ["H3220"] = 18800,
            ["mr"] = 18800,
        },
        ["Gloves of Old"] = {
            ["H3220"] = 500000,
            ["mr"] = 500000,
        },
        ["Headhunter's Buckler of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Wicked Leather Pants"] = {
            ["mr"] = 20500,
            ["cc"] = 9,
            ["id"] = "15757:0:0:0:0",
            ["H3229"] = 20500,
            ["sc"] = 1,
        },
        ["Forest Buckler"] = {
            ["H3220"] = 3990,
            ["mr"] = 3990,
        },
        ["Arena Bands"] = {
            ["H3220"] = 49899,
            ["mr"] = 49899,
        },
        ["Aquamarine Ring of Fire Resistance"] = {
            ["H3220"] = 80000,
            ["mr"] = 80000,
        },
        ["Pattern: Runecloth Gloves"] = {
            ["mr"] = 200000,
            ["sc"] = 2,
            ["H3239"] = 121500,
            ["id"] = "14481:0:0:0:0",
            ["H3254"] = 200000,
            ["cc"] = 9,
        },
        ["Aboriginal Vest of Stamina"] = {
            ["H3220"] = 1463,
            ["mr"] = 1463,
        },
        ["Lil Timmy's Peashooter"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Gypsy Buckler of Blocking"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Ogron's Sash"] = {
            ["H3220"] = 455000,
            ["mr"] = 455000,
        },
        ["Wicked Chain Shoulder Pads of the Bear"] = {
            ["H3220"] = 4200,
            ["mr"] = 4200,
        },
        ["Ceremonial Leather Harness"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Jazeraint Bracers of the Monkey"] = {
            ["H3220"] = 4444,
            ["mr"] = 4444,
        },
        ["Soldier's Shield of the Monkey"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Spiked Chain Shoulder Pads of the Bear"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Brilliant Smallfish"] = {
            ["H3220"] = 87,
            ["mr"] = 87,
        },
        ["Journeyman's Vest"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Pattern: Living Leggings"] = {
            ["mr"] = 12600,
            ["cc"] = 9,
            ["id"] = "15752:0:0:0:0",
            ["L3229"] = 12600,
            ["H3229"] = 13000,
            ["sc"] = 1,
        },
        ["Pagan Mantle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Greenstone Talisman of the Boar"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Adventurer's Pith Helmet"] = {
            ["H3220"] = 120000,
            ["mr"] = 120000,
        },
        ["Wicked Leather Bracers"] = {
            ["H3220"] = 49900,
            ["mr"] = 49900,
        },
        ["Battleforge Cloak of Stamina"] = {
            ["H3220"] = 2635,
            ["mr"] = 2635,
        },
        ["Hefty Battlehammer of Power"] = {
            ["H3220"] = 7699,
            ["mr"] = 7699,
        },
        ["Green Hills of Stranglethorn - Page 24"] = {
            ["mr"] = 1458,
            ["H3254"] = 1458,
            ["H3220"] = 487,
        },
        ["Bandit Cloak of Stamina"] = {
            ["H3220"] = 959,
            ["mr"] = 959,
        },
        ["White Bandit Mask"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Rugged Hide"] = {
            ["H3220"] = 693,
            ["mr"] = 693,
        },
        ["Short-handled Battle Axe"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Scaled Leather Belt of the Falcon"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Twilight Cowl of Intellect"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Windchaser Amice"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Recipe: Fire Protection Potion"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Ritual Shroud of Arcane Wrath"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Sturdy Quarterstaff of the Whale"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Sentry's Surcoat of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Recipe: Holy Protection Potion"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Polished Jazeraint Armor"] = {
            ["H3220"] = 39000,
            ["mr"] = 39000,
        },
        ["War Paint Chestpiece"] = {
            ["H3220"] = 2899,
            ["mr"] = 2899,
        },
        ["Twin-bladed Axe of the Boar"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Primal Wraps of Nature's Wrath"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Arthas' Tears"] = {
            ["H3220"] = 1485,
            ["mr"] = 1485,
        },
        ["Dokebi Chestguard"] = {
            ["H3220"] = 5200,
            ["mr"] = 5200,
        },
        ["Bloodspiller"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Cutthroat's Hat of Intellect"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Raincaller Cord of Healing"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Scaled Leather Boots of Stamina"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Native Pants of the Owl"] = {
            ["H3220"] = 899,
            ["mr"] = 899,
        },
        ["Sentinel Boots of the Owl"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Banded Girdle of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Feral Gloves of the Falcon"] = {
            ["H3220"] = 2730,
            ["mr"] = 2730,
        },
        ["Plans: Arcanite Reaper"] = {
            ["H3220"] = 4000000,
            ["mr"] = 4000000,
        },
        ["Small Blue Pouch"] = {
            ["mr"] = 299,
            ["cc"] = 1,
            ["id"] = "828:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 299,
        },
        ["Goblin Nutcracker of the Tiger"] = {
            ["H3220"] = 22000,
            ["mr"] = 22000,
        },
        ["Feral Cord of the Eagle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Ritual Cape of the Whale"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Big Voodoo Cloak"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Superior Boots of Stamina"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Nightfin Soup"] = {
            ["H3254"] = 3800,
            ["mr"] = 3800,
        },
        ["Oaken War Staff"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Necromancer Leggings"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Conjurer's Cinch of the Whale"] = {
            ["H3220"] = 4104,
            ["mr"] = 4104,
        },
        ["Plated Fist of Hakoo"] = {
            ["H3220"] = 159000,
            ["mr"] = 159000,
        },
        ["Shredder Operating Manual - Page 3"] = {
            ["mr"] = 2400,
            ["cc"] = 15,
            ["id"] = "16647:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 2400,
        },
        ["Embossed Plate Girdle of the Bear"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Ironweb Spider Silk"] = {
            ["mr"] = 2614,
            ["cc"] = 7,
            ["id"] = "14227:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 2614,
        },
        ["Conjurer's Mantle of the Whale"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Feral Buckler of the Gorilla"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Nightscape Pants"] = {
            ["H3220"] = 9600,
            ["mr"] = 9600,
        },
        ["Gleaming Claymore of the Tiger"] = {
            ["H3220"] = 2599,
            ["mr"] = 2599,
        },
        ["Thistlefur Cap of the Owl"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Blackmouth Oil"] = {
            ["H3220"] = 481,
            ["mr"] = 481,
        },
        ["Pillager's Gloves of the Bear"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Phalanx Headguard of the Tiger"] = {
            ["H3220"] = 4980,
            ["mr"] = 4980,
        },
        ["Black Mageweave Vest"] = {
            ["H3220"] = 7300,
            ["mr"] = 7300,
        },
        ["Rough Stone"] = {
            ["H3220"] = 2,
            ["mr"] = 2,
        },
        ["Linked Chain Cloak"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Ancient Cloak of the Wolf"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Rough Boomstick"] = {
            ["H3220"] = 625,
            ["mr"] = 625,
        },
        ["Cabalist Boots of the Monkey"] = {
            ["mr"] = 22200,
            ["cc"] = 4,
            ["id"] = "7531:0:0:609:0",
            ["H3246"] = 22200,
            ["sc"] = 2,
        },
        ["Wild Hog Shank"] = {
            ["H3220"] = 86,
            ["mr"] = 86,
        },
        ["Marsh Ring of the Monkey"] = {
            ["mr"] = 30000,
            ["sc"] = 0,
            ["id"] = "12012:0:0:600:0",
            ["H3246"] = 30000,
            ["cc"] = 4,
        },
        ["Trickster's Leggings of Power"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Vital Tunic of the Owl"] = {
            ["H3220"] = 8099,
            ["mr"] = 8099,
        },
        ["Resilient Cord"] = {
            ["H3220"] = 5400,
            ["mr"] = 5400,
        },
        ["Elder's Pants of the Eagle"] = {
            ["H3220"] = 494900,
            ["mr"] = 494900,
        },
        ["Bent Staff"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Obsidian Pendant of the Bear"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Disciple's Robe of the Eagle"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Sentinel Trousers of the Wolf"] = {
            ["H3220"] = 15126,
            ["mr"] = 15126,
        },
        ["Plans: Golden Iron Destroyer"] = {
            ["H3220"] = 1845,
            ["mr"] = 1845,
        },
        ["Spiritchaser Staff of the Wolf"] = {
            ["H3220"] = 35000,
            ["mr"] = 35000,
        },
        ["Recipe: Swiftness Potion"] = {
            ["H3220"] = 19600,
            ["mr"] = 19600,
        },
        ["Rigid Bracelets of the Owl"] = {
            ["H3220"] = 2890,
            ["mr"] = 2890,
        },
        ["Mountainside Buckler"] = {
            ["H3220"] = 49999,
            ["mr"] = 49999,
        },
        ["Magician Staff of Stamina"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Lupine Cord of the Wolf"] = {
            ["H3220"] = 5100,
            ["mr"] = 5100,
        },
        ["Venomshroud Mask"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Scroll of Spirit III"] = {
            ["H3220"] = 1490,
            ["mr"] = 1490,
        },
        ["Gloom Reaper of the Eagle"] = {
            ["H3220"] = 24800,
            ["mr"] = 24800,
        },
        ["Obsidian Greaves"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Vicar's Robe"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Renegade Chestguard of the Bear"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Wrangler's Gloves of the Eagle"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Crisp Spider Meat"] = {
            ["mr"] = 50,
            ["cc"] = 7,
            ["id"] = "1081:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 50,
        },
        ["Scouting Bracers of the Monkey"] = {
            ["mr"] = 3000,
            ["cc"] = 4,
            ["id"] = "6583:0:0:587:0",
            ["H3246"] = 3000,
            ["sc"] = 2,
        },
        ["Thick Scale Crown of the Eagle"] = {
            ["H3220"] = 9362,
            ["mr"] = 9362,
        },
        ["Battle Knife of Nature's Wrath"] = {
            ["H3220"] = 4738,
            ["mr"] = 4738,
        },
        ["Burnished Gloves"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Wicked Chain Helmet of the Bear"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Shadow Protection Potion"] = {
            ["H3220"] = 1930,
            ["mr"] = 1930,
        },
        ["Green Hills of Stranglethorn - Page 8"] = {
            ["mr"] = 695,
            ["H3254"] = 695,
            ["H3220"] = 497,
        },
        ["Elder's Mantle of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Swampchill Fetish"] = {
            ["H3220"] = 38800,
            ["mr"] = 38800,
        },
        ["Gloves of Holy Might"] = {
            ["H3220"] = 482345,
            ["mr"] = 482345,
        },
        ["Shadoweave Pants"] = {
            ["H3220"] = 37350,
            ["mr"] = 37350,
        },
        ["War Torn Tunic of the Gorilla"] = {
            ["H3220"] = 1485,
            ["mr"] = 1485,
        },
        ["Green Hills of Stranglethorn - Page 10"] = {
            ["H3220"] = 595,
            ["mr"] = 595,
        },
        ["Infantry Gauntlets"] = {
            ["H3220"] = 150,
            ["mr"] = 150,
        },
        ["Nocturnal Shoulder Pads of Stamina"] = {
            ["H3220"] = 12200,
            ["mr"] = 12200,
        },
        ["Bloodspattered Surcoat of the Bear"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Medicine Staff of the Bear"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Warbringer's Belt of the Bear"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Battleforge Armor of Stamina"] = {
            ["H3220"] = 29364,
            ["mr"] = 29364,
        },
        ["Stonecloth Belt"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Silver-thread Amice"] = {
            ["H3220"] = 1695,
            ["mr"] = 1695,
        },
        ["Greenweave Leggings of the Owl"] = {
            ["H3220"] = 5375,
            ["mr"] = 5375,
        },
        ["Sentry's Armsplints of the Whale"] = {
            ["H3220"] = 1420,
            ["mr"] = 1420,
        },
        ["Scouting Belt of the Owl"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Feet of the Lynx"] = {
            ["H3220"] = 80999,
            ["mr"] = 80999,
        },
        ["Runecloth Pants"] = {
            ["mr"] = 32731,
            ["cc"] = 4,
            ["H3239"] = 32731,
            ["id"] = "13865:0:0:0:0",
            ["sc"] = 1,
        },
        ["Knight's Pauldrons of Stamina"] = {
            ["H3220"] = 18972,
            ["mr"] = 18972,
        },
        ["Thick Scale Bracelets of the Bear"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Wild Leather Helmet of Stamina"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Adventurer's Bracers of the Monkey"] = {
            ["mr"] = 55500,
            ["sc"] = 2,
            ["id"] = "10256:0:0:609:0",
            ["H3246"] = 55500,
            ["cc"] = 4,
        },
        ["Adventurer's Cape of the Monkey"] = {
            ["mr"] = 36876,
            ["L3246"] = 36876,
            ["id"] = "10258:0:0:610:0",
            ["sc"] = 1,
            ["H3246"] = 113653,
            ["cc"] = 4,
        },
        ["Aquadynamic Fish Attractor"] = {
            ["H3220"] = 1165,
            ["mr"] = 1165,
        },
        ["Archer's Buckler of the Tiger"] = {
            ["H3220"] = 8363,
            ["mr"] = 8363,
        },
        ["Wild Thornroot"] = {
            ["H3254"] = 2050,
            ["mr"] = 2050,
        },
        ["Rugged Leather"] = {
            ["mr"] = 790,
            ["cc"] = 7,
            ["H3239"] = 714,
            ["id"] = "8170:0:0:0:0",
            ["H3254"] = 790,
            ["sc"] = 0,
        },
        ["Chromite Gauntlets"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Large Blue Sack"] = {
            ["H3223"] = 3784,
            ["mr"] = 3784,
            ["cc"] = 1,
            ["id"] = "804:0:0:0:0",
            ["sc"] = 0,
        },
        ["Vital Headband of the Eagle"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Lesser Invisibility Potion"] = {
            ["H3220"] = 419,
            ["mr"] = 419,
        },
        ["Catseye Elixir"] = {
            ["mr"] = 499,
            ["H3254"] = 499,
            ["H3220"] = 859,
        },
        ["Scroll of Stamina"] = {
            ["H3220"] = 110,
            ["mr"] = 110,
        },
        ["Pattern: Dark Leather Gloves"] = {
            ["mr"] = 2633,
            ["cc"] = 9,
            ["id"] = "7360:0:0:0:0",
            ["L3229"] = 2633,
            ["H3229"] = 4500,
            ["sc"] = 1,
        },
        ["Grilled Squid"] = {
            ["H3254"] = 4918,
            ["mr"] = 4918,
        },
        ["Green Dragonscale"] = {
            ["mr"] = 2108,
            ["H3254"] = 2108,
            ["H3220"] = 635,
        },
        ["Splitting Hatchet of the Boar"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Lunar Vest of Intellect"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Golden Pearl"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Green Iron Shoulders"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Simple Cord"] = {
            ["H3220"] = 3789,
            ["mr"] = 3789,
        },
        ["Dokebi Buckler"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Wrangler's Wristbands of Stamina"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Warden's Woolies"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Shield of Thorsen"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Sentinel Boots of Agility"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Tellurium Necklace of the Eagle"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Feral Leggings of the Monkey"] = {
            ["H3220"] = 1117,
            ["mr"] = 1117,
        },
        ["Sentinel Girdle of Defense"] = {
            ["H3220"] = 5718,
            ["mr"] = 5718,
        },
        ["Slayer's Surcoat"] = {
            ["H3220"] = 8400,
            ["mr"] = 8400,
        },
        ["Jet Chain of Spirit"] = {
            ["H3220"] = 10700,
            ["mr"] = 10700,
        },
        ["Recipe: Mighty Troll's Blood Potion"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Warden Staff"] = {
            ["mr"] = 2200000,
            ["H3254"] = 2200000,
            ["H3220"] = 3500000,
        },
        ["Duskbringer"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Bard's Gloves of the Wolf"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Scaled Leather Leggings of Agility"] = {
            ["H3220"] = 24546,
            ["mr"] = 24546,
        },
        ["Aboriginal Sash of the Whale"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Plans: Dark Iron Sunderer"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Forest Leather Mantle"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Knight's Cloak of Strength"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Rigid Belt of the Owl"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Mail Combat Headguard"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Northern Shortsword of Power"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Ricochet Blunderbuss of Marksmanship"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Forest Leather Pants"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Ember Wand of Spirit"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Gleaming Claymore of the Boar"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["War Torn Pants of the Whale"] = {
            ["H3220"] = 809,
            ["mr"] = 809,
        },
        ["Renegade Chestguard of the Boar"] = {
            ["H3220"] = 8887,
            ["mr"] = 8887,
        },
        ["Outrunner's Chestguard of Stamina"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Old Greatsword"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Glyphed Boots"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Bandit Gloves of the Eagle"] = {
            ["H3220"] = 1107,
            ["mr"] = 1107,
        },
        ["Chromite Barbute"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Tundra Ring of the Wolf"] = {
            ["H3220"] = 6522,
            ["mr"] = 6522,
        },
        ["Shadow Oil"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Wild Steelbloom"] = {
            ["H3220"] = 144,
            ["mr"] = 144,
        },
        ["Heavy Leather Ball"] = {
            ["mr"] = 2500,
            ["cc"] = 0,
            ["H3238"] = 2500,
            ["id"] = "18662:0:0:0:0",
            ["sc"] = 0,
        },
        ["Hi-Impact Mithril Slugs"] = {
            ["H3220"] = 4,
            ["mr"] = 4,
        },
        ["Lambent Scale Cloak"] = {
            ["H3220"] = 891,
            ["mr"] = 891,
        },
        ["Insignia Belt"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Polished Zweihander of Agility"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Dreamweave Vest"] = {
            ["mr"] = 64500,
            ["cc"] = 4,
            ["H3226"] = 64500,
            ["id"] = "10021:0:0:0:0",
            ["sc"] = 1,
        },
        ["Rigid Leggings of the Wolf"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Jouster's Chestplate"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Priest's Mace of Shadow Wrath"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Elemental Raiment"] = {
            ["H3220"] = 79500,
            ["mr"] = 79500,
        },
        ["Spiked Chain Shoulder Pads of the Boar"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Pagan Bands of Healing"] = {
            ["H3220"] = 2907,
            ["mr"] = 2907,
        },
        ["Mighty Rage Potion"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Dervish Spaulders of Intellect"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Mystic's Gloves"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Battleforge Wristguards of Power"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Grimclaw"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Formula: Enchant Cloak - Minor Agility"] = {
            ["H3220"] = 650,
            ["mr"] = 650,
        },
        ["Bright Gloves"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Swampwalker Boots"] = {
            ["H3220"] = 18400,
            ["mr"] = 18400,
        },
        ["Outrunner's Gloves of the Gorilla"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Aurora Armor"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Red Mageweave Gloves"] = {
            ["H3220"] = 24800,
            ["mr"] = 24800,
        },
        ["Pathfinder Hat of Defense"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Hefty Battlehammer of the Whale"] = {
            ["H3220"] = 3953,
            ["mr"] = 3953,
        },
        ["Midnight Mace"] = {
            ["H3220"] = 23300,
            ["mr"] = 23300,
        },
        ["Coarse Thread"] = {
            ["H3220"] = 132,
            ["mr"] = 132,
        },
        ["Greenweave Mantle of the Owl"] = {
            ["H3220"] = 7750,
            ["mr"] = 7750,
        },
        ["Voodoo Band"] = {
            ["H3220"] = 7300,
            ["mr"] = 7300,
        },
        ["Murloc Skin Bag"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Black Velvet Robes"] = {
            ["H3220"] = 20500,
            ["mr"] = 20500,
        },
        ["Headhunter's Cloak of Agility"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Knight's Girdle of the Tiger"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Wrangler's Mantle of Intellect"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Warrior's Boots"] = {
            ["H3220"] = 1000000,
            ["mr"] = 1000000,
        },
        ["Mithril Heavy-bore Rifle"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Sentry's Sash of the Bear"] = {
            ["H3220"] = 2004,
            ["mr"] = 2004,
        },
        ["Seer's Pants"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Trueshot Bow of the Monkey"] = {
            ["H3220"] = 27548,
            ["mr"] = 27548,
        },
        ["Banded Armor of Power"] = {
            ["H3220"] = 6899,
            ["mr"] = 6899,
        },
        ["Grunt's Shield of Stamina"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Pattern: Devilsaur Gauntlets"] = {
            ["mr"] = 38400,
            ["cc"] = 9,
            ["id"] = "15758:0:0:0:0",
            ["H3246"] = 38400,
            ["sc"] = 1,
        },
        ["Bloodspattered Wristbands of Power"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Duskwoven Sandals of Shadow Wrath"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Formula: Enchant Gloves - Greater Agility"] = {
            ["H3220"] = 199900,
            ["mr"] = 199900,
        },
        ["Dwarven Magestaff of the Owl"] = {
            ["H3220"] = 7899,
            ["mr"] = 7899,
        },
        ["Bard's Trousers of Spirit"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Valorous Girdle"] = {
            ["mr"] = 30000,
            ["H3254"] = 30000,
            ["H3220"] = 9899,
        },
        ["Pattern: Guardian Cloak"] = {
            ["mr"] = 19900,
            ["sc"] = 1,
            ["id"] = "5974:0:0:0:0",
            ["H3229"] = 19900,
            ["cc"] = 9,
        },
        ["Scroll of Agility"] = {
            ["H3220"] = 98,
            ["mr"] = 98,
        },
        ["Pattern: White Leather Jerkin"] = {
            ["mr"] = 3657,
            ["cc"] = 9,
            ["id"] = "2407:0:0:0:0",
            ["sc"] = 1,
            ["H3254"] = 3657,
            ["H3227"] = 200,
        },
        ["Wing of the Whelpling"] = {
            ["H3220"] = 39000,
            ["mr"] = 39000,
        },
        ["Lupine Vest of the Owl"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Battleforge Wristguards of the Monkey"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Khoo's Point"] = {
            ["H3220"] = 59499,
            ["mr"] = 59499,
        },
        ["Explosive Sheep"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Instant Poison III"] = {
            ["H3220"] = 699,
            ["mr"] = 699,
        },
        ["Robust Cloak of Stamina"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Savannah Ring of the Monkey"] = {
            ["H3220"] = 4980,
            ["mr"] = 4980,
        },
        ["Gloves of Meditation"] = {
            ["H3225"] = 1300,
            ["sc"] = 1,
            ["id"] = "4318:0:0:0:0",
            ["cc"] = 4,
            ["mr"] = 1300,
        },
        ["Yorgen Bracers"] = {
            ["H3220"] = 19000,
            ["mr"] = 19000,
        },
        ["Pattern: Reinforced Woolen Shoulders"] = {
            ["mr"] = 1995,
            ["cc"] = 9,
            ["id"] = "4347:0:0:0:0",
            ["L3229"] = 1995,
            ["H3229"] = 2000,
            ["sc"] = 2,
        },
        ["Saltstone Surcoat of the Bear"] = {
            ["H3220"] = 23500,
            ["mr"] = 23500,
        },
        ["Infiltrator Shoulders of the Eagle"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Canvas Shoulderpads"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Instant Poison IV"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Plans: Searing Golden Blade"] = {
            ["H3220"] = 2850,
            ["mr"] = 2850,
        },
        ["Spiked Chain Belt of Healing"] = {
            ["H3220"] = 2532,
            ["mr"] = 2532,
        },
        ["Geomancer's Bracers of Intellect"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Outrunner's Cloak of the Bear"] = {
            ["H3220"] = 655,
            ["mr"] = 655,
        },
        ["Superior Cloak of Spirit"] = {
            ["H3220"] = 940,
            ["mr"] = 940,
        },
        ["Sentry's Sash of Strength"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Phalanx Shield of the Tiger"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Ornate Spyglass"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Recipe: Savory Deviate Delight"] = {
            ["mr"] = 185000,
            ["H3254"] = 185000,
            ["H3220"] = 24850,
        },
        ["Gossamer Headpiece of Healing"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Wrangler's Boots of the Eagle"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Elder's Mantle of the Owl"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Ridge Cleaver of Stamina"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Pattern: Wicked Leather Armor"] = {
            ["mr"] = 121290,
            ["cc"] = 9,
            ["id"] = "15773:0:0:0:0",
            ["H3229"] = 121290,
            ["sc"] = 1,
        },
        ["Thick Leather Ammo Pouch"] = {
            ["H3220"] = 21500,
            ["mr"] = 21500,
        },
        ["Heraldic Cloak"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Sparkleshell Shoulder Pads of the Monkey"] = {
            ["H3220"] = 22500,
            ["mr"] = 22500,
        },
        ["Knight's Pauldrons of the Gorilla"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Bristle Whisker Catfish"] = {
            ["H3220"] = 7,
            ["mr"] = 7,
        },
        ["Rock Maul"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Spiked Chain Gauntlets of the Bear"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Pillager's Chestguard of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Training Sword of Strength"] = {
            ["H3220"] = 920,
            ["mr"] = 920,
        },
        ["Common White Shirt"] = {
            ["H3220"] = 2322,
            ["mr"] = 2322,
        },
        ["Ragehammer"] = {
            ["H3220"] = 350000,
            ["mr"] = 350000,
        },
        ["Imperial Leather Gloves"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Battleforge Legguards of the Bear"] = {
            ["H3220"] = 4980,
            ["mr"] = 4980,
        },
        ["Durable Belt of Healing"] = {
            ["H3220"] = 1799,
            ["mr"] = 1799,
        },
        ["Enduring Breastplate"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Dervish Belt of Intellect"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Sparkleshell Gauntlets of the Tiger"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Battleforge Girdle of the Bear"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Durable Gloves of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Cadet Leggings of the Monkey"] = {
            ["H3220"] = 4920,
            ["mr"] = 4920,
        },
        ["Merc Sword of the Monkey"] = {
            ["H3220"] = 1353,
            ["mr"] = 1353,
        },
        ["Aurora Cowl"] = {
            ["mr"] = 6000,
            ["H3250"] = 6000,
            ["id"] = "4041:0:0:0:0",
            ["sc"] = 1,
            ["cc"] = 4,
        },
        ["Infiltrator Boots of Spirit"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Grunt's Shield of the Monkey"] = {
            ["mr"] = 4734,
            ["cc"] = 4,
            ["id"] = "15512:0:0:589:0",
            ["H3246"] = 4734,
            ["sc"] = 6,
        },
        ["Disciple's Robe of Frozen Wrath"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Conjurer's Bracers of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Wolf Rider's Boots of the Monkey"] = {
            ["mr"] = 16200,
            ["cc"] = 4,
            ["id"] = "15370:0:0:608:0",
            ["H3246"] = 16200,
            ["sc"] = 2,
        },
        ["Sorcerer Sash of the Whale"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Banded Gauntlets of the Whale"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Blackforge Bracers"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Dervish Cape of Nature's Wrath"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Pattern: Tough Scorpid Boots"] = {
            ["mr"] = 2200,
            ["cc"] = 9,
            ["id"] = "8399:0:0:0:0",
            ["L3229"] = 2200,
            ["H3229"] = 2400,
            ["sc"] = 1,
        },
        ["Pattern: Felcloth Pants"] = {
            ["mr"] = 49400,
            ["cc"] = 9,
            ["id"] = "14483:0:0:0:0",
            ["H3246"] = 49400,
            ["sc"] = 2,
        },
        ["Ritual Sandals of the Whale"] = {
            ["H3220"] = 2259,
            ["mr"] = 2259,
        },
        ["Symbolic Breastplate"] = {
            ["H3220"] = 21333,
            ["mr"] = 21333,
        },
        ["Linen Bag"] = {
            ["mr"] = 210,
            ["sc"] = 0,
            ["id"] = "4238:0:0:0:0",
            ["H3246"] = 210,
            ["cc"] = 1,
        },
        ["Aurora Bracers"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Ironfeather"] = {
            ["H3220"] = 228,
            ["mr"] = 228,
        },
        ["Lesser Stoneshield Potion"] = {
            ["H3220"] = 595,
            ["mr"] = 595,
        },
        ["Sentinel Bracers of the Monkey"] = {
            ["mr"] = 12998,
            ["cc"] = 4,
            ["id"] = "7447:0:0:595:0",
            ["H3246"] = 12998,
            ["sc"] = 2,
        },
        ["Polished Zweihander of the Eagle"] = {
            ["H3220"] = 6190,
            ["mr"] = 6190,
        },
        ["Coarse Dynamite"] = {
            ["mr"] = 211,
            ["cc"] = 7,
            ["id"] = "4365:0:0:0:0",
            ["sc"] = 2,
            ["H3225"] = 211,
        },
        ["Elder's Sash of the Owl"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Bandit Buckler of Stamina"] = {
            ["H3220"] = 2820,
            ["mr"] = 2820,
        },
        ["Stormwind Brie"] = {
            ["H3220"] = 86,
            ["mr"] = 86,
        },
        ["Ring of the Underwood"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Lambent Scale Gloves"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Tundra Ring of the Monkey"] = {
            ["H3220"] = 10006,
            ["mr"] = 10006,
        },
        ["Mageweave Bag"] = {
            ["mr"] = 9999,
            ["cc"] = 1,
            ["H3239"] = 10000,
            ["id"] = "10050:0:0:0:0",
            ["sc"] = 0,
            ["L3239"] = 9999,
        },
        ["Bloodrazor"] = {
            ["H3220"] = 700000,
            ["mr"] = 700000,
        },
        ["Chieftain's Cloak of the Monkey"] = {
            ["mr"] = 18900,
            ["cc"] = 4,
            ["id"] = "9951:0:0:602:0",
            ["H3246"] = 18900,
            ["sc"] = 1,
        },
        ["Glowing Magical Bracelets"] = {
            ["H3220"] = 7400,
            ["mr"] = 7400,
        },
        ["Phalanx Leggings of Strength"] = {
            ["H3220"] = 8031,
            ["mr"] = 8031,
        },
        ["Forked Tongue"] = {
            ["H3220"] = 995,
            ["mr"] = 995,
        },
        ["Tigerseye"] = {
            ["H3220"] = 98,
            ["mr"] = 98,
        },
        ["Durable Belt of the Whale"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Brutish Helmet of the Boar"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Blackened Defias Belt"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Lambent Scale Bracers"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Skeletal Shoulders"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Tower Shield"] = {
            ["H3220"] = 1295,
            ["mr"] = 1295,
        },
        ["Stonecloth Boots"] = {
            ["H3220"] = 6798,
            ["mr"] = 6798,
        },
        ["Beaststalker's Gloves"] = {
            ["H3220"] = 109000,
            ["mr"] = 109000,
        },
        ["Wrangler's Cloak of the Owl"] = {
            ["H3220"] = 2808,
            ["mr"] = 2808,
        },
        ["High Chief's Bindings of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Mooncloth Bag"] = {
            ["H3220"] = 400000,
            ["mr"] = 400000,
        },
        ["Heavy Wool Bandage"] = {
            ["H3220"] = 68,
            ["mr"] = 68,
        },
        ["Small Glowing Shard"] = {
            ["mr"] = 499,
            ["cc"] = 7,
            ["L3220"] = 499,
            ["id"] = "11138:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 500,
        },
        ["Brigade Girdle of the Owl"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Acrobatic Staff of the Boar"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Coarse Stone"] = {
            ["mr"] = 699,
            ["cc"] = 7,
            ["id"] = "2836:0:0:0:0",
            ["sc"] = 0,
            ["H3254"] = 699,
            ["H3225"] = 149,
        },
        ["Righteous Helmet of the Monkey"] = {
            ["mr"] = 57969,
            ["cc"] = 4,
            ["id"] = "10073:0:0:626:0",
            ["H3246"] = 57969,
            ["sc"] = 2,
        },
        ["Battleforge Armor of the Whale"] = {
            ["H3220"] = 5363,
            ["mr"] = 5363,
        },
        ["Obsidian Band of the Bear"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Twilight Cowl of Fiery Wrath"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Iron Bar"] = {
            ["H3220"] = 494,
            ["mr"] = 494,
        },
        ["Shimmering Gloves of Spirit"] = {
            ["H3220"] = 4361,
            ["mr"] = 4361,
        },
        ["Aboriginal Loincloth of the Eagle"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Elixir of the Mongoose"] = {
            ["H3220"] = 19500,
            ["mr"] = 19500,
        },
        ["Ivycloth Pants of the Falcon"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Feral Shoes of Nature's Wrath"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Durable Bracers of Stamina"] = {
            ["H3220"] = 6777,
            ["mr"] = 6777,
        },
        ["Moonberry Juice"] = {
            ["H3220"] = 990,
            ["mr"] = 990,
        },
        ["Durable Shoulders of the Eagle"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Lesser Mystic Wand"] = {
            ["H3220"] = 6200,
            ["mr"] = 6200,
        },
        ["Greater Scythe of Agility"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Small Flame Sac"] = {
            ["mr"] = 3100,
            ["cc"] = 7,
            ["L3220"] = 3000,
            ["id"] = "4402:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 3999,
        },
        ["Glyphed Helm"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Chief Brigadier Boots"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Carving Knife of Healing"] = {
            ["H3220"] = 2099,
            ["mr"] = 2099,
        },
        ["Strange Dust"] = {
            ["mr"] = 400,
            ["H3254"] = 400,
            ["H3220"] = 90,
        },
        ["Sentry's Armsplints of Power"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Enchanted Sea Kelp"] = {
            ["H3220"] = 38500,
            ["mr"] = 38500,
        },
        ["Viking Sword of the Bear"] = {
            ["H3220"] = 6600,
            ["mr"] = 6600,
        },
        ["Bloodspattered Wristbands of the Whale"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Recipe: Invisibility Potion"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Marauder's Leggings of the Bear"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Short Bastard Sword of Power"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Outrunner's Pauldrons"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Heavy Earthen Gloves"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Sentinel Breastplate of the Whale"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Shellfish"] = {
            ["H3220"] = 1631,
            ["mr"] = 1631,
        },
        ["Sword of Decay"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Sorcerer Cloak of Healing"] = {
            ["H3220"] = 5990,
            ["mr"] = 5990,
        },
        ["Ricochet Blunderbuss of the Eagle"] = {
            ["H3220"] = 32999,
            ["mr"] = 32999,
        },
        ["Large Glowing Shard"] = {
            ["H3220"] = 398,
            ["mr"] = 398,
        },
        ["Elder's Pants of the Monkey"] = {
            ["H3220"] = 8031,
            ["mr"] = 8031,
        },
        ["Bandit Boots of Stamina"] = {
            ["H3220"] = 1586,
            ["mr"] = 1586,
        },
        ["Giant Club of Strength"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Pattern: Black Swashbuckler's Shirt"] = {
            ["mr"] = 9698,
            ["sc"] = 2,
            ["id"] = "10728:0:0:0:0",
            ["H3229"] = 9698,
            ["cc"] = 9,
        },
        ["Greenweave Bracers of Frozen Wrath"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Schematic: Ice Deflector"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Superior Shoulders of the Falcon"] = {
            ["H3220"] = 3765,
            ["mr"] = 3765,
        },
        ["Exploding Shot"] = {
            ["H3220"] = 12,
            ["mr"] = 12,
        },
        ["Militant Shortsword of the Bear"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Ritual Amice"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Twilight Pants of the Whale"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Embersilk Tunic of Shadow Wrath"] = {
            ["H3220"] = 55492,
            ["mr"] = 55492,
        },
        ["Warbear Leather"] = {
            ["H3220"] = 9653,
            ["mr"] = 9653,
        },
        ["Twilight Cowl of Healing"] = {
            ["H3220"] = 5888,
            ["mr"] = 5888,
        },
        ["Maple Seed"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["12 Pound Mud Snapper"] = {
            ["H3220"] = 990,
            ["mr"] = 990,
        },
        ["Massive Battle Axe of the Bear"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Ballast Maul of the Bear"] = {
            ["H3220"] = 13900,
            ["mr"] = 13900,
        },
        ["Twilight Belt of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Recipe: Minor Magic Resistance Potion"] = {
            ["H3254"] = 19000,
            ["mr"] = 19000,
        },
        ["\"Mage-Eye\" Blunderbuss"] = {
            ["H3220"] = 3898,
            ["mr"] = 3898,
        },
        ["Marsh Chain of the Tiger"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Ornate Bracers of the Monkey"] = {
            ["mr"] = 16066,
            ["cc"] = 4,
            ["id"] = "10126:0:0:605:0",
            ["H3246"] = 16066,
            ["sc"] = 3,
        },
        ["Huntsman's Shoulders of the Eagle"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Thistlefur Gloves of Healing"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Small Lustrous Pearl"] = {
            ["mr"] = 421,
            ["cc"] = 7,
            ["L3220"] = 199,
            ["id"] = "5498:0:0:0:0",
            ["sc"] = 0,
            ["H3254"] = 421,
            ["H3220"] = 200,
        },
        ["Evil Bat Eye"] = {
            ["H3220"] = 2599,
            ["mr"] = 2599,
        },
        ["Knightly Longsword of the Bear"] = {
            ["H3220"] = 27000,
            ["mr"] = 27000,
        },
        ["Carving Knife of Strength"] = {
            ["H3220"] = 1295,
            ["mr"] = 1295,
        },
        ["Sturdy Quarterstaff of the Owl"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Raw Bristle Whisker Catfish"] = {
            ["H3220"] = 23,
            ["mr"] = 23,
        },
        ["Greenweave Vest of Spirit"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Cavalier Two-hander of the Monkey"] = {
            ["mr"] = 7000,
            ["sc"] = 8,
            ["id"] = "3206:0:0:602:0",
            ["H3246"] = 7000,
            ["cc"] = 2,
        },
        ["Banded Helm of the Boar"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Feral Cloak of Stamina"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Medicine Staff of the Eagle"] = {
            ["H3220"] = 2750,
            ["mr"] = 2750,
        },
        ["Stone Hammer of the Gorilla"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Cured Heavy Hide"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Ivycloth Pants of the Owl"] = {
            ["H3220"] = 5460,
            ["mr"] = 5460,
        },
        ["Faded Photograph"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Merc Sword of the Boar"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Phoenix Gloves"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Flask of Mojo"] = {
            ["mr"] = 9500,
            ["H3254"] = 9500,
            ["H3220"] = 359,
        },
        ["Sage's Cloth of the Owl"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Benedict's Key"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Lesser Astral Essence"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Ghost Mushroom"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Regal Sash of the Owl"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Raw Slitherskin Mackerel"] = {
            ["H3220"] = 4,
            ["mr"] = 4,
        },
        ["Schematic: EZ-Thro Dynamite II"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Rune Sword of the Monkey"] = {
            ["mr"] = 40000,
            ["cc"] = 2,
            ["id"] = "15216:0:0:598:0",
            ["H3246"] = 40000,
            ["sc"] = 7,
        },
        ["Primal Wraps of Power"] = {
            ["H3220"] = 2146,
            ["mr"] = 2146,
        },
        ["Rod of Molten Fire"] = {
            ["H3220"] = 4387,
            ["mr"] = 4387,
        },
        ["Lupine Buckler of the Eagle"] = {
            ["H3220"] = 1496,
            ["mr"] = 1496,
        },
        ["Plans: Runed Mithril Hammer"] = {
            ["H3254"] = 149917,
            ["mr"] = 149917,
        },
        ["Willow Belt of the Whale"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Gouging Pick"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Scaled Leather Leggings of Power"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pagan Shoes of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Marauder's Bracers of the Bear"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Harpyclaw Short Bow"] = {
            ["H3220"] = 500000,
            ["mr"] = 500000,
        },
        ["Stout Battlehammer of the Tiger"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Major Mana Potion"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Wicked Chain Bracers of Power"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Gryphon Cloak of the Owl"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Pathfinder Belt of the Whale"] = {
            ["H3220"] = 2865,
            ["mr"] = 2865,
        },
        ["Sentinel Girdle of the Whale"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Scouting Belt of the Tiger"] = {
            ["H3220"] = 1266,
            ["mr"] = 1266,
        },
        ["Bloodwoven Boots of Stamina"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Sentry's Cape of Healing"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Gypsy Trousers of the Whale"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Handful of Copper Bolts"] = {
            ["H3220"] = 283,
            ["mr"] = 283,
        },
        ["Pillager's Pauldrons of the Monkey"] = {
            ["H3220"] = 12332,
            ["mr"] = 12332,
        },
        ["Shimmering Gloves of the Eagle"] = {
            ["H3220"] = 1175,
            ["mr"] = 1175,
        },
        ["Green Hills of Stranglethorn - Page 6"] = {
            ["mr"] = 3700,
            ["H3254"] = 3700,
            ["H3220"] = 799,
        },
        ["Watcher's Leggings of Healing"] = {
            ["H3220"] = 5130,
            ["mr"] = 5130,
        },
        ["Raincaller Pants of the Owl"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Spiked Chain Breastplate of Stamina"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Smoked Sagefish"] = {
            ["H3220"] = 68,
            ["mr"] = 68,
        },
        ["Acrobatic Staff of Fiery Wrath"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Recipe: Giant Clam Scorcho"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Grinning Axe of the Tiger"] = {
            ["H3220"] = 85380,
            ["mr"] = 85380,
        },
        ["Savage Axe of the Bear"] = {
            ["H3220"] = 17000,
            ["mr"] = 17000,
        },
        ["Stoneraven"] = {
            ["H3220"] = 250000,
            ["mr"] = 250000,
        },
        ["Raincaller Mantle of the Owl"] = {
            ["H3220"] = 4868,
            ["mr"] = 4868,
        },
        ["Bright Cloak"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Pattern: Red Woolen Bag"] = {
            ["mr"] = 530,
            ["cc"] = 9,
            ["id"] = "5772:0:0:0:0",
            ["L3229"] = 530,
            ["H3229"] = 533,
            ["sc"] = 2,
        },
        ["Heraldic Headpiece"] = {
            ["H3220"] = 17800,
            ["mr"] = 17800,
        },
        ["Scouting Buckler of Agility"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Small Red Pouch"] = {
            ["mr"] = 295,
            ["cc"] = 1,
            ["id"] = "805:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 295,
        },
        ["Birchwood Maul of the Boar"] = {
            ["H3220"] = 1050,
            ["mr"] = 1050,
        },
        ["Satyr's Rod"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Jacinth Circle of Fire Resistance"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Infiltrator Buckler of Agility"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Razor Blade of the Bear"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Pattern: Blue Linen Robe"] = {
            ["mr"] = 1200,
            ["cc"] = 9,
            ["id"] = "6272:0:0:0:0",
            ["L3229"] = 1200,
            ["H3229"] = 1736,
            ["sc"] = 2,
        },
        ["Glimmering Flamberge of the Whale"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Winter's Bite"] = {
            ["H3220"] = 500000,
            ["mr"] = 500000,
        },
        ["Welken Ring"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Marauder's Crest of Blocking"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Hook Dagger of Power"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Green Hills of Stranglethorn - Page 4"] = {
            ["mr"] = 599,
            ["H3254"] = 599,
            ["H3220"] = 660,
        },
        ["Spider Palp"] = {
            ["mr"] = 885,
            ["cc"] = 15,
            ["id"] = "4428:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 885,
        },
        ["Ironhide Belt of Healing"] = {
            ["H3220"] = 49999,
            ["mr"] = 49999,
        },
        ["Gossamer Cape of Stamina"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Willow Cape of Healing"] = {
            ["H3220"] = 657,
            ["mr"] = 657,
        },
        ["Umbral Wand of Frozen Wrath"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Double Mail Shoulderpads"] = {
            ["H3220"] = 5673,
            ["mr"] = 5673,
        },
        ["Feral Gloves of the Whale"] = {
            ["H3220"] = 1745,
            ["mr"] = 1745,
        },
        ["Jazeraint Pauldrons of Nature's Wrath"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Sorcerer Sash of the Owl"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Amy's Blanket"] = {
            ["H3220"] = 7400,
            ["mr"] = 7400,
        },
        ["Emblazoned Gloves"] = {
            ["H3220"] = 2866,
            ["mr"] = 2866,
        },
        ["Hulking Shield"] = {
            ["H3220"] = 2599,
            ["mr"] = 2599,
        },
        ["Pathfinder Gloves of the Monkey"] = {
            ["mr"] = 129999,
            ["cc"] = 4,
            ["id"] = "15343:0:0:596:0",
            ["H3246"] = 129999,
            ["sc"] = 2,
        },
        ["Slimy Ichor"] = {
            ["H3220"] = 199,
            ["mr"] = 199,
        },
        ["Captain's Cloak of the Bear"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Plans: Frost Tiger Blade"] = {
            ["H3220"] = 5499,
            ["mr"] = 5499,
        },
        ["Thundering Charm"] = {
            ["H3220"] = 4100,
            ["mr"] = 4100,
        },
        ["Grunt's Legguards of Healing"] = {
            ["H3220"] = 3191,
            ["mr"] = 3191,
        },
        ["Poison-tipped Bone Spear"] = {
            ["H3220"] = 29900,
            ["mr"] = 29900,
        },
        ["Bright Armor"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Myrmidon's Girdle"] = {
            ["H3220"] = 39999,
            ["mr"] = 39999,
        },
        ["Frostsaber Leather"] = {
            ["H3220"] = 90000,
            ["mr"] = 90000,
        },
        ["Basalt Ring of Stamina"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Dusky Leather Armor"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Grunt's Bracers of the Gorilla"] = {
            ["H3220"] = 1092,
            ["mr"] = 1092,
        },
        ["Grunt's Legguards of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["The Shoveler"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Green Firework"] = {
            ["H3220"] = 280,
            ["mr"] = 280,
        },
        ["Tree Bark Jacket"] = {
            ["H3220"] = 141413,
            ["mr"] = 141413,
        },
        ["Formula: Enchant Gloves - Advanced Mining"] = {
            ["mr"] = 4901,
            ["H3254"] = 4901,
            ["H3220"] = 1985,
        },
        ["Ghostwalker Bindings of Stamina"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Sentinel Shoulders of the Wolf"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Shredder Operating Manual - Page 2"] = {
            ["mr"] = 279,
            ["cc"] = 15,
            ["id"] = "16646:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 279,
        },
        ["Cabalist Belt of the Monkey"] = {
            ["mr"] = 19868,
            ["cc"] = 4,
            ["id"] = "7535:0:0:611:0",
            ["H3246"] = 19868,
            ["sc"] = 2,
        },
        ["Elder's Sash of the Eagle"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Archer's Trousers of Power"] = {
            ["H3220"] = 10137,
            ["mr"] = 10137,
        },
        ["Wrangler's Boots of the Whale"] = {
            ["H3220"] = 2499,
            ["mr"] = 2499,
        },
        ["Recipe: Roast Raptor"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Pattern: Fine Leather Pants"] = {
            ["mr"] = 600,
            ["sc"] = 1,
            ["id"] = "5972:0:0:0:0",
            ["H3229"] = 600,
            ["cc"] = 9,
        },
        ["Fortified Bracers of Strength"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Tracker's Shoulderpads of the Monkey"] = {
            ["mr"] = 18348,
            ["cc"] = 4,
            ["id"] = "9923:0:0:608:0",
            ["H3246"] = 18348,
            ["sc"] = 2,
        },
        ["Sequoia Hammer of Power"] = {
            ["H3220"] = 9685,
            ["mr"] = 9685,
        },
        ["Dwarven Hatchet of Power"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Rabbit Crate (Snowshoe)"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Sentinel Bracers of the Whale"] = {
            ["H3220"] = 9198,
            ["mr"] = 9198,
        },
        ["BKP \"Sparrow\" Smallbore"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Deadly Kris of the Tiger"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Pattern: Admiral's Hat"] = {
            ["mr"] = 19900,
            ["cc"] = 9,
            ["id"] = "10318:0:0:0:0",
            ["L3229"] = 19900,
            ["H3229"] = 29999,
            ["sc"] = 2,
        },
        ["Ravager's Crown"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Heavy Armor Kit"] = {
            ["H3220"] = 658,
            ["mr"] = 658,
        },
        ["Helm of Fire"] = {
            ["H3220"] = 135000,
            ["mr"] = 135000,
        },
        ["Prospector's Sash"] = {
            ["H3220"] = 1098,
            ["mr"] = 1098,
        },
        ["Recipe: Barbecued Buzzard Wing"] = {
            ["H3220"] = 560,
            ["mr"] = 560,
        },
        ["Windchaser Footpads"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Rough Bronze Leggings"] = {
            ["H3220"] = 964,
            ["mr"] = 964,
        },
        ["Tiny Crimson Whelpling"] = {
            ["H3220"] = 499000,
            ["mr"] = 499000,
        },
        ["Tracker's Shoulderpads of the Eagle"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Imposing Bracers of Stamina"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Restorative Potion"] = {
            ["mr"] = 8997,
            ["H3254"] = 8997,
            ["H3220"] = 24800,
        },
        ["Pattern: Ironfeather Shoulders"] = {
            ["mr"] = 759999,
            ["cc"] = 9,
            ["id"] = "15735:0:0:0:0",
            ["H3229"] = 759999,
            ["sc"] = 1,
        },
        ["Spiked Chain Belt of the Monkey"] = {
            ["H3220"] = 1675,
            ["mr"] = 1675,
        },
        ["Recipe: Frost Oil"] = {
            ["H3220"] = 13800,
            ["mr"] = 13800,
        },
        ["Banded Armor of the Boar"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Spiked Chain Breastplate of Strength"] = {
            ["H3220"] = 5600,
            ["mr"] = 5600,
        },
        ["Feral Gloves of the Gorilla"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Sentinel Cloak of Spirit"] = {
            ["H3220"] = 5055,
            ["mr"] = 5055,
        },
        ["Infiltrator Gloves of the Falcon"] = {
            ["H3220"] = 7595,
            ["mr"] = 7595,
        },
        ["Enchanted Water"] = {
            ["H3220"] = 155,
            ["mr"] = 155,
        },
        ["Watcher's Jerkin of the Owl"] = {
            ["H3220"] = 4510,
            ["mr"] = 4510,
        },
        ["Handstitched Linen Britches"] = {
            ["H3220"] = 399,
            ["mr"] = 399,
        },
        ["Holy Protection Potion"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Recipe: Nature Protection Potion"] = {
            ["H3220"] = 3998,
            ["mr"] = 3998,
        },
        ["Shimmering Stave of Intellect"] = {
            ["H3220"] = 4827,
            ["mr"] = 4827,
        },
        ["Lightning Eel"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Sniper Rifle of Spirit"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Recipe: Hot Lion Chops"] = {
            ["H3220"] = 495,
            ["mr"] = 495,
        },
        ["Stonecloth Gloves"] = {
            ["H3220"] = 3431,
            ["mr"] = 3431,
        },
        ["Hillborne Axe of the Eagle"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["War Paint Bindings"] = {
            ["H3220"] = 790,
            ["mr"] = 790,
        },
        ["Splitting Hatchet of Stamina"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Expert Cookbook"] = {
            ["H3220"] = 11099,
            ["mr"] = 11099,
        },
        ["Silver-thread Pants"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Pillager's Boots of the Whale"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Spiritchaser Staff of the Owl"] = {
            ["H3220"] = 46164,
            ["mr"] = 46164,
        },
        ["Small Brilliant Shard"] = {
            ["mr"] = 52999,
            ["cc"] = 7,
            ["L3220"] = 52999,
            ["id"] = "14343:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 53000,
        },
        ["Banded Gauntlets of Strength"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Agile Boots"] = {
            ["H3220"] = 4075,
            ["mr"] = 4075,
        },
        ["Glyphed Belt"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Honed Stiletto of Strength"] = {
            ["H3220"] = 9467,
            ["mr"] = 9467,
        },
        ["Crescent Edge of the Tiger"] = {
            ["H3220"] = 41200,
            ["mr"] = 41200,
        },
        ["Wrangler's Leggings of the Falcon"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Pattern: Nightscape Shoulders"] = {
            ["mr"] = 16999,
            ["sc"] = 1,
            ["id"] = "8409:0:0:0:0",
            ["H3229"] = 16999,
            ["cc"] = 9,
        },
        ["Feral Gloves of the Monkey"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Monk's Staff of the Monkey"] = {
            ["H3220"] = 44928,
            ["mr"] = 44928,
        },
        ["Superior Boots of the Falcon"] = {
            ["H3220"] = 3625,
            ["mr"] = 3625,
        },
        ["Sniper Rifle of Stamina"] = {
            ["H3220"] = 41200,
            ["mr"] = 41200,
        },
        ["Resilient Tunic"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Formula: Enchant Weapon - Demonslaying"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Nightscape Shoulders"] = {
            ["H3220"] = 22000,
            ["mr"] = 22000,
        },
        ["Cross Dagger of Healing"] = {
            ["H3220"] = 8691,
            ["mr"] = 8691,
        },
        ["Hacking Cleaver of Strength"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Bloodspattered Shield of the Gorilla"] = {
            ["H3220"] = 1545,
            ["mr"] = 1545,
        },
        ["Conjurer's Vest of the Whale"] = {
            ["H3220"] = 9784,
            ["mr"] = 9784,
        },
        ["Sacrificial Kris of the Monkey"] = {
            ["mr"] = 42910,
            ["sc"] = 15,
            ["id"] = "3187:0:0:595:0",
            ["H3246"] = 42910,
            ["cc"] = 2,
        },
        ["Simple Britches of the Owl"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Rumsey Rum Dark"] = {
            ["H3220"] = 146,
            ["mr"] = 146,
        },
        ["Slayer's Sash"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Recipe: Bristle Whisker Catfish"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Ivory Wand of the Falcon"] = {
            ["H3220"] = 80000,
            ["mr"] = 80000,
        },
        ["Searing Needle"] = {
            ["H3220"] = 198400,
            ["mr"] = 198400,
        },
        ["War Torn Pants of the Gorilla"] = {
            ["H3220"] = 795,
            ["mr"] = 795,
        },
        ["Boiled Clams"] = {
            ["H3220"] = 193,
            ["mr"] = 193,
        },
        ["Large Knapsack"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Gloom Reaper of Strength"] = {
            ["H3220"] = 14027,
            ["mr"] = 14027,
        },
        ["Savage Axe of the Boar"] = {
            ["H3220"] = 11999,
            ["mr"] = 11999,
        },
        ["Knight's Boots of Stamina"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Zircon Band of Fire Resistance"] = {
            ["H3220"] = 3624,
            ["mr"] = 3624,
        },
        ["Feral Gloves of the Wolf"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Thaumaturgist Staff of the Monkey"] = {
            ["mr"] = 113699,
            ["cc"] = 2,
            ["id"] = "15275:0:0:626:0",
            ["H3246"] = 113699,
            ["sc"] = 10,
        },
        ["Mithril Casing"] = {
            ["mr"] = 16335,
            ["H3254"] = 16335,
            ["H3220"] = 9500,
        },
        ["Conjurer's Gloves of Spirit"] = {
            ["H3220"] = 8756,
            ["mr"] = 8756,
        },
        ["Schematic: Shadow Goggles"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Short Bastard Sword of the Tiger"] = {
            ["H3220"] = 795,
            ["mr"] = 795,
        },
        ["Hawkeye's Cord"] = {
            ["mr"] = 5966,
            ["sc"] = 2,
            ["id"] = "14588:0:0:0:0",
            ["H3246"] = 5966,
            ["cc"] = 4,
        },
        ["Bracers of Valor"] = {
            ["H3220"] = 80000,
            ["mr"] = 80000,
        },
        ["Staunch Hammer of Healing"] = {
            ["H3220"] = 749,
            ["mr"] = 749,
        },
        ["Formula: Enchant Cloak - Superior Defense"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Steadfast Girdle of Power"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Sunscale Belt"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Instant Poison"] = {
            ["H3220"] = 67,
            ["mr"] = 67,
        },
        ["Burnished Boots"] = {
            ["H3220"] = 5200,
            ["mr"] = 5200,
        },
        ["Malachite"] = {
            ["H3220"] = 20,
            ["mr"] = 20,
        },
        ["Gelatinous Goo"] = {
            ["H3220"] = 380,
            ["mr"] = 380,
        },
        ["Rigid Bracelets of the Eagle"] = {
            ["H3220"] = 2150,
            ["mr"] = 2150,
        },
        ["Sage's Pants of Fiery Wrath"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Barbed Club of Nature's Wrath"] = {
            ["H3220"] = 1975,
            ["mr"] = 1975,
        },
        ["Shimmering Robe of Stamina"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Cavalier Two-hander of Power"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Charger's Pants of the Bear"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Barbarian War Axe of Strength"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Thorbia's Gauntlets"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Expert First Aid - Under Wraps"] = {
            ["H3220"] = 11599,
            ["mr"] = 11599,
        },
        ["Shadowfang"] = {
            ["H3220"] = 1000000,
            ["mr"] = 1000000,
        },
        ["Pattern: Dusky Boots"] = {
            ["mr"] = 44400,
            ["cc"] = 9,
            ["id"] = "7452:0:0:0:0",
            ["H3229"] = 44400,
            ["sc"] = 1,
        },
        ["Spiked Club of the Whale"] = {
            ["H3220"] = 745,
            ["mr"] = 745,
        },
        ["Bloodspattered Sash of the Boar"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Recipe: Elixir of Dream Vision"] = {
            ["H3220"] = 2500000,
            ["mr"] = 2500000,
        },
        ["Linen Cloth"] = {
            ["mr"] = 33,
            ["cc"] = 7,
            ["id"] = "2589:0:0:0:0",
            ["H3246"] = 33,
            ["sc"] = 0,
        },
        ["Sage's Stave of the Eagle"] = {
            ["H3220"] = 3410,
            ["mr"] = 3410,
        },
        ["Silvered Bronze Boots"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Infiltrator Cord of the Monkey"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Lord Sakrasis' Scepter"] = {
            ["H3220"] = 16611,
            ["mr"] = 16611,
        },
        ["Scouting Gloves of the Owl"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Elixir of Ogre's Strength"] = {
            ["H3220"] = 460,
            ["mr"] = 460,
        },
        ["Durable Shoulders of Stamina"] = {
            ["H3220"] = 4100,
            ["mr"] = 4100,
        },
        ["Buccaneer's Orb of the Eagle"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Conjurer's Gloves of the Eagle"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Merc Sword of the Tiger"] = {
            ["H3220"] = 1950,
            ["mr"] = 1950,
        },
        ["Sturdy Quarterstaff of Arcane Wrath"] = {
            ["H3220"] = 7400,
            ["mr"] = 7400,
        },
        ["Aboriginal Rod of Spirit"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Overlord's Legplates of the Whale"] = {
            ["H3254"] = 53122,
            ["mr"] = 53122,
        },
        ["Banded Bracers of the Whale"] = {
            ["H3220"] = 2865,
            ["mr"] = 2865,
        },
        ["Ancestral Robe"] = {
            ["H3220"] = 299,
            ["mr"] = 299,
        },
        ["Trickster's Cloak of the Monkey"] = {
            ["H3220"] = 11463,
            ["mr"] = 11463,
        },
        ["Hulking Spaulders"] = {
            ["H3220"] = 4700,
            ["mr"] = 4700,
        },
        ["Journeyman's Pants"] = {
            ["H3220"] = 530,
            ["mr"] = 530,
        },
        ["Journeyman's Backpack"] = {
            ["H3220"] = 42000,
            ["mr"] = 42000,
        },
        ["Conjurer's Shoes of the Falcon"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Willow Robe of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Rigid Leggings of the Whale"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Band of Purification"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Infiltrator Pants of the Eagle"] = {
            ["H3220"] = 5129,
            ["mr"] = 5129,
        },
        ["Ritual Shroud of Stamina"] = {
            ["H3220"] = 5733,
            ["mr"] = 5733,
        },
        ["Grunt's Handwraps of the Gorilla"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Grizzly Buckler of Spirit"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Notched Shortsword of Strength"] = {
            ["H3220"] = 691,
            ["mr"] = 691,
        },
        ["Recipe: Purification Potion"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Pillager's Chestguard of Strength"] = {
            ["H3220"] = 16969,
            ["mr"] = 16969,
        },
        ["Rageclaw Leggings of the Monkey"] = {
            ["mr"] = 30000,
            ["cc"] = 4,
            ["id"] = "15385:0:0:623:0",
            ["H3246"] = 30000,
            ["sc"] = 2,
        },
        ["Zealot Blade"] = {
            ["H3220"] = 27481,
            ["mr"] = 27481,
        },
        ["Watcher's Jerkin of Stamina"] = {
            ["H3220"] = 6257,
            ["mr"] = 6257,
        },
        ["Hibernal Bracers"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Copper Rod"] = {
            ["H3220"] = 540,
            ["mr"] = 540,
        },
        ["Formula: Enchant Cloak - Greater Resistance"] = {
            ["H3220"] = 1019800,
            ["mr"] = 1019800,
        },
        ["Crimson Silk Belt"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Elegant Circlet of the Eagle"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Roast Raptor"] = {
            ["H3220"] = 358,
            ["mr"] = 358,
        },
        ["Grunt's Chestpiece of the Bear"] = {
            ["H3220"] = 2949,
            ["mr"] = 2949,
        },
        ["Renegade Cloak of Strength"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Canvas Vest"] = {
            ["H3220"] = 1950,
            ["mr"] = 1950,
        },
        ["Tigerbane"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Ironhide Breastplate of the Eagle"] = {
            ["H3220"] = 46600,
            ["mr"] = 46600,
        },
        ["Soldier's Gauntlets of the Boar"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Headsplitter"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Pattern: Frostsaber Tunic"] = {
            ["mr"] = 27600,
            ["cc"] = 9,
            ["id"] = "15779:0:0:0:0",
            ["sc"] = 1,
            ["H3229"] = 38799,
            ["L3229"] = 27600,
        },
        ["Elder's Boots of the Owl"] = {
            ["H3220"] = 5990,
            ["mr"] = 5990,
        },
        ["Native Robe of Spirit"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Sentry's Shoulderguards of the Monkey"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Militant Shortsword of Stamina"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Field Plate Girdle of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Sword of Corruption"] = {
            ["H3220"] = 30768,
            ["mr"] = 30768,
        },
        ["Sorcerer Hat of the Owl"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Formula: Enchant Gloves - Herbalism"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Defender Shield of the Monkey"] = {
            ["mr"] = 4753,
            ["sc"] = 6,
            ["id"] = "6572:0:0:589:0",
            ["H3246"] = 4753,
            ["cc"] = 4,
        },
        ["Arachnidian Circlet of Healing"] = {
            ["H3220"] = 599999,
            ["mr"] = 599999,
        },
        ["Recipe: Heavy Crocolisk Stew"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Outrunner's Cord of Strength"] = {
            ["H3220"] = 1455,
            ["mr"] = 1455,
        },
        ["Superior Tunic of Stamina"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Gutrender"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Dervish Boots of the Eagle"] = {
            ["H3220"] = 3880,
            ["mr"] = 3880,
        },
        ["Imposing Cape of the Monkey"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Champion's Helmet of the Falcon"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Dreamslayer"] = {
            ["H3220"] = 22770,
            ["mr"] = 22770,
        },
        ["Feral Leggings of Spirit"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Thunderwood"] = {
            ["H3220"] = 39999,
            ["mr"] = 39999,
        },
        ["Twilight Robe of the Whale"] = {
            ["H3220"] = 15482,
            ["mr"] = 15482,
        },
        ["Copper Modulator"] = {
            ["H3220"] = 334,
            ["mr"] = 334,
        },
        ["Scaled Leather Gloves of the Tiger"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Pattern: Red Linen Robe"] = {
            ["mr"] = 49,
            ["cc"] = 9,
            ["id"] = "2598:0:0:0:0",
            ["L3229"] = 49,
            ["H3229"] = 50,
            ["sc"] = 2,
        },
        ["Spider Sausage"] = {
            ["mr"] = 2986,
            ["cc"] = 0,
            ["id"] = "17222:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 2986,
        },
        ["Spiked Club of the Boar"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Scouting Trousers of Arcane Wrath"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Plans: Copper Chain Vest"] = {
            ["H3220"] = 25,
            ["mr"] = 25,
        },
        ["Pagan Vest of Arcane Wrath"] = {
            ["H3220"] = 14832,
            ["mr"] = 14832,
        },
        ["Tiger Meat"] = {
            ["H3220"] = 456,
            ["mr"] = 456,
        },
        ["Cutthroat's Boots of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Banded Boots of the Monkey"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Watchman Pauldrons"] = {
            ["H3220"] = 49800,
            ["mr"] = 49800,
        },
        ["Scorpashi Cape"] = {
            ["H3220"] = 31103,
            ["mr"] = 31103,
        },
        ["Embersilk Leggings of Healing"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Manual: Heavy Silk Bandage"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Dervish Buckler of the Boar"] = {
            ["H3220"] = 6624,
            ["mr"] = 6624,
        },
        ["Brocade Vest"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Harvest Cloak"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Corpse Harvester of Agility"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Grizzly Pants of the Eagle"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Mighty Cloak of the Monkey"] = {
            ["mr"] = 18000,
            ["cc"] = 4,
            ["id"] = "10148:0:0:610:0",
            ["H3246"] = 18000,
            ["sc"] = 1,
        },
        ["Dig Rat"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Knightly Longsword of Power"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Hulking Boots"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Pattern: Crimson Silk Cloak"] = {
            ["mr"] = 11229,
            ["sc"] = 2,
            ["id"] = "7087:0:0:0:0",
            ["H3229"] = 11229,
            ["cc"] = 9,
        },
        ["Needle Threader"] = {
            ["H3220"] = 199999,
            ["mr"] = 199999,
        },
        ["Phalanx Leggings of the Monkey"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Double Link Tunic"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Outrunner's Legguards of Power"] = {
            ["H3220"] = 1166,
            ["mr"] = 1166,
        },
        ["Leaden Mace of the Tiger"] = {
            ["H3220"] = 8596,
            ["mr"] = 8596,
        },
        ["Mercenary Blade of Strength"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Willow Gloves of the Monkey"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Raincaller Cuffs of Spirit"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Shredder Operating Manual - Page 7"] = {
            ["mr"] = 496,
            ["cc"] = 15,
            ["id"] = "16651:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 496,
        },
        ["Chief Brigadier Leggings"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Humbert's Pants"] = {
            ["H3220"] = 4095,
            ["mr"] = 4095,
        },
        ["Primal Leggings of the Owl"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Recipe: Elixir of Minor Agility"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Moss Agate"] = {
            ["mr"] = 1178,
            ["H3254"] = 1178,
            ["H3220"] = 600,
        },
        ["Giant Club of the Tiger"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Bard's Tunic of the Whale"] = {
            ["H3220"] = 7212,
            ["mr"] = 7212,
        },
        ["Cured Ham Steak"] = {
            ["H3220"] = 219,
            ["mr"] = 219,
        },
        ["Jouster's Greaves"] = {
            ["H3220"] = 13999,
            ["mr"] = 13999,
        },
        ["Salt"] = {
            ["H3220"] = 1470,
            ["mr"] = 1470,
        },
        ["Regal Star of Intellect"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Blue Dye"] = {
            ["H3220"] = 640,
            ["mr"] = 640,
        },
        ["Militant Shortsword of the Monkey"] = {
            ["mr"] = 16600,
            ["sc"] = 7,
            ["id"] = "15211:0:0:587:0",
            ["H3246"] = 16600,
            ["cc"] = 2,
        },
        ["Huntsman's Cape of Nature's Wrath"] = {
            ["H3220"] = 8432,
            ["mr"] = 8432,
        },
        ["Mail Combat Belt"] = {
            ["H3220"] = 3413,
            ["mr"] = 3413,
        },
        ["Champion's Cape of Spirit"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Gray Woolen Robe"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Green Hills of Stranglethorn - Page 1"] = {
            ["H3254"] = 4655,
            ["mr"] = 4655,
        },
        ["Scouting Boots of Power"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Carefully Folded Note"] = {
            ["H3220"] = 20200,
            ["mr"] = 20200,
        },
        ["Pattern: Crimson Silk Shoulders"] = {
            ["mr"] = 594,
            ["cc"] = 9,
            ["id"] = "7084:0:0:0:0",
            ["L3229"] = 594,
            ["H3229"] = 999,
            ["sc"] = 2,
        },
        ["Pattern: Green Dragonscale Leggings"] = {
            ["mr"] = 12200,
            ["cc"] = 9,
            ["id"] = "15733:0:0:0:0",
            ["L3229"] = 12200,
            ["H3229"] = 12400,
            ["sc"] = 1,
        },
        ["Silver-thread Gloves"] = {
            ["H3220"] = 6300,
            ["mr"] = 6300,
        },
        ["Ivy Orb of the Whale"] = {
            ["H3220"] = 3099,
            ["mr"] = 3099,
        },
        ["Elite Shoulders"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Insignia Leggings"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Barbaric Shoulders"] = {
            ["H3220"] = 2698,
            ["mr"] = 2698,
        },
        ["Chief Brigadier Cloak"] = {
            ["H3220"] = 4299,
            ["mr"] = 4299,
        },
        ["Grunt's Handwraps of the Bear"] = {
            ["H3220"] = 1250,
            ["mr"] = 1250,
        },
        ["Gypsy Trousers of the Owl"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Wool Bandage"] = {
            ["H3220"] = 28,
            ["mr"] = 28,
        },
        ["Sentinel Trousers of Power"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Wild Leather Helmet of the Monkey"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Schematic: Spellpower Goggles Xtreme"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Aboriginal Loincloth of Fiery Wrath"] = {
            ["H3220"] = 1495,
            ["mr"] = 1495,
        },
        ["Abjurer's Robe of the Owl"] = {
            ["H3220"] = 34999,
            ["mr"] = 34999,
        },
        ["Native Branch of Fiery Wrath"] = {
            ["H3220"] = 1201,
            ["mr"] = 1201,
        },
        ["Star Belt"] = {
            ["H3220"] = 46000,
            ["mr"] = 46000,
        },
        ["Goblin Nutcracker of the Monkey"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Darkweave Breeches"] = {
            ["H3220"] = 47508,
            ["mr"] = 47508,
        },
        ["Grunt Axe of Power"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Lower Map Fragment"] = {
            ["mr"] = 9500,
            ["H3254"] = 9500,
            ["H3220"] = 4900,
        },
        ["Sorcerer Mantle of Stamina"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Overlord's Greaves of the Bear"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Feral Shoes of the Whale"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Aurora Sphere"] = {
            ["H3220"] = 7600,
            ["mr"] = 7600,
        },
        ["Defender Girdle of the Monkey"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Scaled Leather Leggings of the Wolf"] = {
            ["H3220"] = 6606,
            ["mr"] = 6606,
        },
        ["Buccaneer's Vest of Stamina"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Sorcerer Robe of the Owl"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Dervish Gloves of the Falcon"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Bloodspattered Sash of the Monkey"] = {
            ["H3220"] = 861,
            ["mr"] = 861,
        },
        ["Barbaric Cloth Boots"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Plans: Solid Iron Maul"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Green Whelp Armor"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Feral Leggings of the Gorilla"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Sentinel Bracers of Agility"] = {
            ["H3220"] = 24816,
            ["mr"] = 24816,
        },
        ["Wanderer's Belt of the Monkey"] = {
            ["mr"] = 40000,
            ["cc"] = 4,
            ["id"] = "10109:0:0:614:0",
            ["H3246"] = 40000,
            ["sc"] = 2,
        },
        ["Sacrificial Kris of Stamina"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Pillager's Cloak of the Bear"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Outrunner's Cuffs of the Tiger"] = {
            ["H3220"] = 1303,
            ["mr"] = 1303,
        },
        ["Engraved Helm of the Monkey"] = {
            ["mr"] = 150000,
            ["cc"] = 4,
            ["id"] = "10235:0:0:632:0",
            ["H3246"] = 150000,
            ["sc"] = 3,
        },
        ["War Torn Pants of the Eagle"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Acrobatic Staff of Frozen Wrath"] = {
            ["H3220"] = 28900,
            ["mr"] = 28900,
        },
        ["Wirt's Third Leg"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Pattern: Red Mageweave Shoulders"] = {
            ["mr"] = 40000,
            ["sc"] = 2,
            ["id"] = "10315:0:0:0:0",
            ["H3229"] = 40000,
            ["cc"] = 9,
        },
        ["Pattern: Green Whelp Armor"] = {
            ["mr"] = 50000,
            ["cc"] = 9,
            ["id"] = "7450:0:0:0:0",
            ["L3229"] = 2989,
            ["H3229"] = 50000,
            ["sc"] = 1,
        },
        ["Dried King Bolete"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Pattern: Black Dragonscale Breastplate"] = {
            ["mr"] = 76524,
            ["cc"] = 9,
            ["id"] = "15759:0:0:0:0",
            ["L3229"] = 76524,
            ["H3229"] = 79500,
            ["sc"] = 1,
        },
        ["Unbalanced Axe"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Pattern: Stormshroud Armor"] = {
            ["H3220"] = 90000,
            ["mr"] = 90000,
        },
        ["Twin-bladed Axe of the Bear"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Kaleidoscope Chain"] = {
            ["H3220"] = 34000,
            ["mr"] = 34000,
        },
        ["Twilight Mantle of the Whale"] = {
            ["H3220"] = 9401,
            ["mr"] = 9401,
        },
        ["Heavy Lamellar Helm of the Bear"] = {
            ["H3220"] = 33333,
            ["mr"] = 33333,
        },
        ["Renegade Shield of Strength"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Formula: Enchant Bracer - Minor Strength"] = {
            ["H3220"] = 180,
            ["mr"] = 180,
        },
        ["Dervish Spaulders of the Eagle"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Martyr's Chain"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Scarlet Boots"] = {
            ["H3220"] = 34999,
            ["mr"] = 34999,
        },
        ["Spiked Chain Leggings of the Boar"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Bandit Jerkin of the Monkey"] = {
            ["mr"] = 90999,
            ["cc"] = 4,
            ["id"] = "9782:0:0:596:0",
            ["H3246"] = 90999,
            ["sc"] = 2,
        },
        ["Recipe: Roasted Kodo Meat"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Large Brilliant Shard"] = {
            ["H3220"] = 130000,
            ["mr"] = 130000,
        },
        ["Grunt's AnkleWraps of the Boar"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Lupine Leggings of the Falcon"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Stonecutter Claymore of Strength"] = {
            ["H3220"] = 19998,
            ["mr"] = 19998,
        },
        ["Scorpashi Wristbands"] = {
            ["H3220"] = 17000,
            ["mr"] = 17000,
        },
        ["Magician Staff of Intellect"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Knight's Boots of the Bear"] = {
            ["H3220"] = 6066,
            ["mr"] = 6066,
        },
        ["Living Essence"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Scaled Leather Shoulders of Stamina"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Superior Shoulders of the Owl"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Huge Stone Club"] = {
            ["H3220"] = 18999,
            ["mr"] = 18999,
        },
        ["Sentinel Bracers of the Owl"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Glowing Scorpid Blood"] = {
            ["H3220"] = 7899,
            ["mr"] = 7899,
        },
        ["Knight's Gauntlets of the Monkey"] = {
            ["H3220"] = 10100,
            ["mr"] = 10100,
        },
        ["Conjurer's Mantle of Stamina"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Expert Fishing - The Bass and You"] = {
            ["H3220"] = 14767,
            ["mr"] = 14767,
        },
        ["Sorcerer Gloves of the Monkey"] = {
            ["mr"] = 10000,
            ["sc"] = 1,
            ["id"] = "9880:0:0:605:0",
            ["H3246"] = 10000,
            ["cc"] = 4,
        },
        ["Raider's Gauntlets of the Bear"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Regent's Cloak"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Battle Slayer of the Boar"] = {
            ["H3220"] = 4700,
            ["mr"] = 4700,
        },
        ["Combatant Claymore"] = {
            ["H3220"] = 14800,
            ["mr"] = 14800,
        },
        ["Raw Redgill"] = {
            ["H3220"] = 41,
            ["mr"] = 41,
        },
        ["Superior Bracers of the Eagle"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Pattern: Murloc Scale Breastplate"] = {
            ["mr"] = 487,
            ["sc"] = 1,
            ["id"] = "5787:0:0:0:0",
            ["H3229"] = 487,
            ["cc"] = 9,
        },
        ["Stranglethorn Seed"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Phalanx Leggings of the Eagle"] = {
            ["H3220"] = 6825,
            ["mr"] = 6825,
        },
        ["Wrangler's Cloak of Spirit"] = {
            ["H3220"] = 3222,
            ["mr"] = 3222,
        },
        ["Vital Boots of the Owl"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Necklace of Harmony"] = {
            ["H3220"] = 11700,
            ["mr"] = 11700,
        },
        ["Fine Leather Belt"] = {
            ["H3220"] = 328,
            ["mr"] = 328,
        },
        ["Embersilk Tunic of the Owl"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Scouting Cloak of Agility"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Cutthroat's Cape of the Monkey"] = {
            ["H3220"] = 4071,
            ["mr"] = 4071,
        },
        ["Knight's Headguard of the Tiger"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Robust Boots of the Monkey"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Priest's Mace of Power"] = {
            ["H3220"] = 895,
            ["mr"] = 895,
        },
        ["Imperial Leather Breastplate"] = {
            ["H3220"] = 28111,
            ["mr"] = 28111,
        },
        ["Bloodspattered Sash of the Gorilla"] = {
            ["H3220"] = 801,
            ["mr"] = 801,
        },
        ["Bristlebark Britches"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Ironweaver"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Sacrificial Kris of Power"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Raider's Belt of the Monkey"] = {
            ["H3220"] = 2902,
            ["mr"] = 2902,
        },
        ["Recipe: Dragonbreath Chili"] = {
            ["H3220"] = 16448,
            ["mr"] = 16448,
        },
        ["Monk's Staff of Fiery Wrath"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Sentinel Cap of the Wolf"] = {
            ["H3220"] = 11671,
            ["mr"] = 11671,
        },
        ["Recipe: Rainbow Fin Albacore"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Spider Silk Slippers"] = {
            ["mr"] = 1744,
            ["cc"] = 4,
            ["id"] = "4321:0:0:0:0",
            ["sc"] = 1,
            ["H3232"] = 1744,
        },
        ["Spider Belt"] = {
            ["mr"] = 24099,
            ["cc"] = 4,
            ["id"] = "4328:0:0:0:0",
            ["sc"] = 1,
            ["H3232"] = 24099,
        },
        ["Disciple's Robe of the Owl"] = {
            ["H3220"] = 799,
            ["mr"] = 799,
        },
        ["Jazeraint Belt of the Monkey"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Smoothbore Gun"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Copper Tube"] = {
            ["H3220"] = 275,
            ["mr"] = 275,
        },
        ["Stonescale Eel"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Dusky Bracers"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Windweaver Staff"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Pattern: White Bandit Mask"] = {
            ["mr"] = 78700,
            ["sc"] = 2,
            ["id"] = "10301:0:0:0:0",
            ["H3229"] = 78700,
            ["cc"] = 9,
        },
        ["Elder's Gloves of the Whale"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Shadow Wand"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Libram of Constitution"] = {
            ["H3220"] = 255520,
            ["mr"] = 255520,
        },
        ["Pattern: Stylish Blue Shirt"] = {
            ["mr"] = 1000,
            ["sc"] = 2,
            ["id"] = "6390:0:0:0:0",
            ["H3229"] = 1000,
            ["cc"] = 9,
        },
        ["Swiftness Potion"] = {
            ["H3220"] = 294,
            ["mr"] = 294,
        },
        ["Renegade Pauldrons of the Tiger"] = {
            ["H3220"] = 11500,
            ["mr"] = 11500,
        },
        ["Silk Headband"] = {
            ["H3220"] = 1290,
            ["mr"] = 1290,
        },
        ["Sanguine Sandals"] = {
            ["mr"] = 1500,
            ["H3254"] = 1500,
            ["H3220"] = 1100,
        },
        ["Slayer's Gloves"] = {
            ["H3220"] = 1440,
            ["mr"] = 1440,
        },
        ["Woolen Bag"] = {
            ["mr"] = 2599,
            ["cc"] = 1,
            ["id"] = "4240:0:0:0:0",
            ["sc"] = 0,
            ["H3254"] = 2599,
            ["H3220"] = 496,
        },
        ["Amethyst Band of Fire Resistance"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Ritual Leggings of the Eagle"] = {
            ["H3220"] = 1745,
            ["mr"] = 1745,
        },
        ["Grinning Axe of the Whale"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Darkmist Wizard Hat of Arcane Wrath"] = {
            ["H3220"] = 59200,
            ["mr"] = 59200,
        },
        ["Ebon Scimitar of the Tiger"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Marauder's Cloak of the Bear"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Heavy Lamellar Girdle of the Whale"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Tribal Vest"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["War Torn Pants of Healing"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Pillager's Cloak of Strength"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Recipe: Goblin Rocket Fuel"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Mistscape Stave"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Banded Shield of the Monkey"] = {
            ["mr"] = 8091,
            ["sc"] = 6,
            ["id"] = "9843:0:0:593:0",
            ["H3246"] = 8091,
            ["cc"] = 4,
        },
        ["Scaled Leather Gloves of the Wolf"] = {
            ["H3220"] = 3799,
            ["mr"] = 3799,
        },
        ["Lambent Scale Legguards"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Battering Hammer of the Tiger"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Brutal War Axe of the Bear"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Bard's Boots of the Monkey"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Twin-bladed Axe of the Eagle"] = {
            ["H3220"] = 12288,
            ["mr"] = 12288,
        },
        ["Pysan's Old Greatsword"] = {
            ["H3220"] = 18599,
            ["mr"] = 18599,
        },
        ["Crafted Heavy Shot"] = {
            ["H3220"] = 1,
            ["mr"] = 1,
        },
        ["Insignia Gloves"] = {
            ["H3220"] = 6400,
            ["mr"] = 6400,
        },
        ["Jet Chain of the Boar"] = {
            ["H3220"] = 21429,
            ["mr"] = 21429,
        },
        ["Geomancer's Spaulders of the Eagle"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Conk Hammer of Strength"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Ruffled Feather"] = {
            ["H3220"] = 122,
            ["mr"] = 122,
        },
        ["Sage's Pants of the Owl"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Greenweave Branch of the Whale"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Scouting Cloak of Arcane Wrath"] = {
            ["H3220"] = 1825,
            ["mr"] = 1825,
        },
        ["Recipe: Lean Wolf Steak"] = {
            ["H3220"] = 7523,
            ["mr"] = 7523,
        },
        ["Schematic: Thorium Rifle"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Imperial Leather Helm"] = {
            ["H3220"] = 18575,
            ["mr"] = 18575,
        },
        ["Monstrous War Axe of the Tiger"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Dark Whelpling"] = {
            ["mr"] = 997829,
            ["H3254"] = 997829,
            ["H3220"] = 550000,
        },
        ["Twilight Mantle of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Inscribed Buckler"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Pattern: Tough Scorpid Helm"] = {
            ["mr"] = 1920,
            ["cc"] = 9,
            ["id"] = "8402:0:0:0:0",
            ["L3229"] = 1920,
            ["H3229"] = 1926,
            ["sc"] = 1,
        },
        ["Twilight Cowl of Frozen Wrath"] = {
            ["H3220"] = 12833,
            ["mr"] = 12833,
        },
        ["Imperial Red Mantle"] = {
            ["H3220"] = 17700,
            ["mr"] = 17700,
        },
        ["Infiltrator Shoulders of the Boar"] = {
            ["H3220"] = 6843,
            ["mr"] = 6843,
        },
        ["Soul Pouch"] = {
            ["H3220"] = 98500,
            ["mr"] = 98500,
        },
        ["Knight's Girdle of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Rough Weightstone"] = {
            ["H3220"] = 150,
            ["mr"] = 150,
        },
        ["Birchwood Maul of Stamina"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Bow of Searing Arrows"] = {
            ["H3220"] = 402200,
            ["mr"] = 402200,
        },
        ["Savannah Ring of Eluding"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["Sorcerer Sash of Fiery Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Duskwoven Robe of Frozen Wrath"] = {
            ["H3220"] = 36459,
            ["mr"] = 36459,
        },
        ["Marsh Ring of Agility"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Grizzly Jerkin of the Monkey"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Rigid Moccasins of the Wolf"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Mountain Silversage"] = {
            ["H3220"] = 5454,
            ["mr"] = 5454,
        },
        ["Jade Legplates of the Bear"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Green Leather Belt"] = {
            ["H3220"] = 3989,
            ["mr"] = 3989,
        },
        ["Ravager's Sandals"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Willow Branch of the Eagle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Raptor Flesh"] = {
            ["H3220"] = 223,
            ["mr"] = 223,
        },
        ["Mercenary Blade of Stamina"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Limited Invulnerability Potion"] = {
            ["H3220"] = 19000,
            ["mr"] = 19000,
        },
        ["Feral Cloak of Arcane Wrath"] = {
            ["H3220"] = 980,
            ["mr"] = 980,
        },
        ["Green Leather Bracers"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Exquisite Flamberge of Strength"] = {
            ["H3220"] = 32000,
            ["mr"] = 32000,
        },
        ["Thick War Axe"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Plans: Silvered Bronze Shoulders"] = {
            ["H3220"] = 487,
            ["mr"] = 487,
        },
        ["Burning War Axe"] = {
            ["H3220"] = 117000,
            ["mr"] = 117000,
        },
        ["Lupine Leggings of the Whale"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Rough Wooden Staff"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Sage's Mantle of the Whale"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Green Lens of Healing"] = {
            ["H3220"] = 90000,
            ["mr"] = 90000,
        },
        ["Fortified Boots of Stamina"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Horizon Choker"] = {
            ["H3220"] = 377000,
            ["mr"] = 377000,
        },
        ["Thistlefur Robe of the Owl"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Gold Bar"] = {
            ["H3220"] = 678,
            ["mr"] = 678,
        },
        ["Schematic: Large Seaforium Charge"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Native Branch of the Falcon"] = {
            ["H3220"] = 1950,
            ["mr"] = 1950,
        },
        ["Sage's Cloth of the Whale"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Claw of the Shadowmancer"] = {
            ["H3220"] = 48400,
            ["mr"] = 48400,
        },
        ["Winged Helm"] = {
            ["H3220"] = 49400,
            ["mr"] = 49400,
        },
        ["Merc Sword of Power"] = {
            ["H3220"] = 3954,
            ["mr"] = 3954,
        },
        ["Elven Spirit Claws"] = {
            ["H3220"] = 39800,
            ["mr"] = 39800,
        },
        ["Pattern: Azure Silk Gloves"] = {
            ["mr"] = 12200,
            ["sc"] = 2,
            ["id"] = "7114:0:0:0:0",
            ["H3229"] = 12200,
            ["cc"] = 9,
        },
        ["Sentry's Armsplints of the Bear"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Azure Shoulders"] = {
            ["mr"] = 18598,
            ["cc"] = 4,
            ["H3250"] = 18799,
            ["id"] = "7060:0:0:0:0",
            ["H3253"] = 18598,
            ["H3251"] = 18600,
            ["sc"] = 1,
        },
        ["Robust Cloak of the Monkey"] = {
            ["mr"] = 10164,
            ["sc"] = 1,
            ["id"] = "15124:0:0:590:0",
            ["H3246"] = 10164,
            ["cc"] = 4,
        },
        ["Percussion Shotgun of the Monkey"] = {
            ["mr"] = 40000,
            ["cc"] = 2,
            ["id"] = "15323:0:0:593:0",
            ["H3246"] = 40000,
            ["sc"] = 3,
        },
        ["Wicked Chain Waistband of the Whale"] = {
            ["H3220"] = 2950,
            ["mr"] = 2950,
        },
        ["Coarse Blasting Powder"] = {
            ["mr"] = 162,
            ["cc"] = 7,
            ["id"] = "4364:0:0:0:0",
            ["sc"] = 1,
            ["H3225"] = 162,
        },
        ["Bloodspattered Loincloth of the Monkey"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Jade Gauntlets of the Bear"] = {
            ["H3220"] = 13999,
            ["mr"] = 13999,
        },
        ["Spiked Chain Breastplate of the Monkey"] = {
            ["mr"] = 10000,
            ["cc"] = 4,
            ["id"] = "15518:0:0:602:0",
            ["H3246"] = 10000,
            ["sc"] = 3,
        },
        ["War Torn Shield of Defense"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Twilight Cowl of the Eagle"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Battleforge Girdle of the Eagle"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Green Hills of Stranglethorn - Page 25"] = {
            ["mr"] = 472,
            ["H3254"] = 472,
            ["H3220"] = 474,
        },
        ["Large Seaforium Charge"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Enduring Gauntlets"] = {
            ["H3220"] = 6300,
            ["mr"] = 6300,
        },
        ["Vanadium Talisman of the Eagle"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Murphstar of the Tiger"] = {
            ["H3220"] = 10200,
            ["mr"] = 10200,
        },
        ["Twilight Boots of the Whale"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Elder's Mantle of the Whale"] = {
            ["H3220"] = 3683,
            ["mr"] = 3683,
        },
        ["Vital Bracelets of Healing"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Warmonger's Belt of the Wolf"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Recipe: Gift of Arthas"] = {
            ["H3220"] = 39999,
            ["mr"] = 39999,
        },
        ["Scouting Belt of the Monkey"] = {
            ["H3220"] = 5784,
            ["mr"] = 5784,
        },
        ["Scouting Boots of Stamina"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Truesilver Bar"] = {
            ["mr"] = 1360,
            ["cc"] = 7,
            ["id"] = "6037:0:0:0:0",
            ["sc"] = 0,
            ["H3225"] = 1360,
        },
        ["Renegade Chestguard of the Gorilla"] = {
            ["H3220"] = 8888,
            ["mr"] = 8888,
        },
        ["Sweet Nectar"] = {
            ["H3220"] = 219,
            ["mr"] = 219,
        },
        ["Frostreaver Crown"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["War Knife of Agility"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Heavy Kodo Meat"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Buzzard Wing"] = {
            ["mr"] = 1333,
            ["H3254"] = 1333,
            ["H3220"] = 227,
        },
        ["Imperial Red Gloves"] = {
            ["H3220"] = 39900,
            ["mr"] = 39900,
        },
        ["Security DELTA Data Access Card"] = {
            ["H3220"] = 990,
            ["mr"] = 990,
        },
        ["Scaled Leather Shoulders of the Owl"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Twilight Boots of the Falcon"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Tough Hunk of Bread"] = {
            ["H3220"] = 3,
            ["mr"] = 3,
        },
        ["Formula: Runed Arcanite Rod"] = {
            ["H3220"] = 49800,
            ["mr"] = 49800,
        },
        ["Kingsblood"] = {
            ["H3220"] = 36,
            ["mr"] = 36,
        },
        ["Glimmering Mail Girdle"] = {
            ["H3231"] = 3904,
            ["mr"] = 3904,
            ["sc"] = 3,
            ["id"] = "4712:0:0:0:0",
            ["cc"] = 4,
        },
        ["Mail Combat Boots"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Durable Gloves of Healing"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Primal Leggings of the Falcon"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Destiny"] = {
            ["H3220"] = 750000,
            ["mr"] = 750000,
        },
        ["Bolt of Silk Cloth"] = {
            ["mr"] = 1799,
            ["cc"] = 7,
            ["id"] = "4305:0:0:0:0",
            ["sc"] = 0,
            ["H3254"] = 1799,
            ["H3246"] = 1180,
        },
        ["Embossed Leather Gloves"] = {
            ["H3220"] = 95,
            ["mr"] = 95,
        },
        ["Grunt's Chestpiece of Power"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Charger's Pants of the Boar"] = {
            ["H3220"] = 699,
            ["mr"] = 699,
        },
        ["Arcane Cover"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Gaea's Cloak of the Whale"] = {
            ["H3220"] = 13896,
            ["mr"] = 13896,
        },
        ["Haunch of Meat"] = {
            ["H3220"] = 24,
            ["mr"] = 24,
        },
        ["Elixir of Giants"] = {
            ["H3220"] = 29000,
            ["mr"] = 29000,
        },
        ["Bandit Jerkin of the Owl"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Greenweave Mantle of Intellect"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Brawler Gloves"] = {
            ["H3220"] = 10499,
            ["mr"] = 10499,
        },
        ["Hillborne Axe of Agility"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Rock Chip"] = {
            ["H3220"] = 706,
            ["mr"] = 706,
        },
        ["Pattern: Guardian Armor"] = {
            ["mr"] = 975,
            ["cc"] = 9,
            ["id"] = "4299:0:0:0:0",
            ["L3229"] = 975,
            ["H3229"] = 1000,
            ["sc"] = 1,
        },
        ["Essence of Undeath"] = {
            ["H3220"] = 129999,
            ["mr"] = 129999,
        },
        ["Sword of the Magistrate"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Onyx Claymore"] = {
            ["H3220"] = 11505,
            ["mr"] = 11505,
        },
        ["Watcher's Boots of Arcane Wrath"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Recipe: Crispy Bat Wing"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Enduring Cap"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Pillager's Leggings of the Bear"] = {
            ["H3220"] = 8969,
            ["mr"] = 8969,
        },
        ["Green Lens of Frozen Wrath"] = {
            ["H3220"] = 189500,
            ["mr"] = 189500,
        },
        ["Ranger Wristguards of the Monkey"] = {
            ["mr"] = 10125,
            ["cc"] = 4,
            ["id"] = "7484:0:0:599:0",
            ["H3246"] = 10125,
            ["sc"] = 2,
        },
        ["Dense Triangle Mace"] = {
            ["H3220"] = 39499,
            ["mr"] = 39499,
        },
        ["Pillager's Chestguard of the Monkey"] = {
            ["H3220"] = 21100,
            ["mr"] = 21100,
        },
        ["Shining Silver Breastplate"] = {
            ["H3220"] = 12800,
            ["mr"] = 12800,
        },
        ["Raider's Gauntlets of Healing"] = {
            ["H3220"] = 1240,
            ["mr"] = 1240,
        },
        ["Superior Boots of the Eagle"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Trickster's Protector of Fiery Wrath"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Wall of the Dead"] = {
            ["H3220"] = 1399999,
            ["mr"] = 1399999,
        },
        ["Dokebi Hat"] = {
            ["H3220"] = 14500,
            ["mr"] = 14500,
        },
        ["Silver-thread Robe"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Cadet Leggings of Strength"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Practice Lock"] = {
            ["H3220"] = 201,
            ["mr"] = 201,
        },
        ["Greater Healing Potion"] = {
            ["H3220"] = 334,
            ["mr"] = 334,
        },
        ["Sentinel Cap of the Monkey"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Pattern: Tough Scorpid Bracers"] = {
            ["mr"] = 1800,
            ["sc"] = 1,
            ["id"] = "8397:0:0:0:0",
            ["H3229"] = 1800,
            ["cc"] = 9,
        },
        ["Lean Wolf Flank"] = {
            ["H3220"] = 140,
            ["mr"] = 140,
        },
        ["Poached Sunscale Salmon"] = {
            ["mr"] = 2000,
            ["H3254"] = 2000,
            ["H3220"] = 375,
        },
        ["Meadow Ring of the Monkey"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Girdle of Golem Strength"] = {
            ["H3220"] = 6400,
            ["mr"] = 6400,
        },
        ["Twilight Cape of the Whale"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Packet of Tharlendris Seeds"] = {
            ["mr"] = 3590,
            ["H3254"] = 3590,
            ["H3220"] = 1091,
        },
        ["Woolen Boots"] = {
            ["H3220"] = 1038,
            ["mr"] = 1038,
        },
        ["Cutthroat's Pants of the Wolf"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Dark Leather Gloves"] = {
            ["H3220"] = 5400,
            ["mr"] = 5400,
        },
        ["Royal Amice of the Owl"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Ridge Cleaver of the Monkey"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Archer's Shoulderpads of the Monkey"] = {
            ["mr"] = 38484,
            ["cc"] = 4,
            ["id"] = "9863:0:0:603:0",
            ["H3246"] = 38484,
            ["sc"] = 2,
        },
        ["Feral Cord of Healing"] = {
            ["H3220"] = 1077,
            ["mr"] = 1077,
        },
        ["Enduring Pauldrons"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Gossamer Headpiece of Intellect"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Sentry's Cape of Strength"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Mageroyal"] = {
            ["H3220"] = 26,
            ["mr"] = 26,
        },
        ["Flask of Oil"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Sturdy Quarterstaff of Shadow Wrath"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Warmonger's Belt of Defense"] = {
            ["H3220"] = 10464,
            ["mr"] = 10464,
        },
        ["Precision Bow"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Formula: Enchant Boots - Lesser Spirit"] = {
            ["H3220"] = 1584,
            ["mr"] = 1584,
        },
        ["Fortified Leggings of Strength"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Pattern: Heavy Scorpid Vest"] = {
            ["mr"] = 9000,
            ["cc"] = 9,
            ["id"] = "15727:0:0:0:0",
            ["L3229"] = 9000,
            ["H3229"] = 9050,
            ["sc"] = 1,
        },
        ["Brittle Molting"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Ta'Kierthan Songblade"] = {
            ["H3220"] = 190000,
            ["mr"] = 190000,
        },
        ["Huntsman's Bands of Stamina"] = {
            ["H3220"] = 7767,
            ["mr"] = 7767,
        },
        ["Broken Blade of Heroes"] = {
            ["H3220"] = 360000,
            ["mr"] = 360000,
        },
        ["Reinforced Linen Cape"] = {
            ["H3220"] = 126,
            ["mr"] = 126,
        },
        ["Thistlefur Pants of Fiery Wrath"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Diamond Hammer"] = {
            ["H3220"] = 54600,
            ["mr"] = 54600,
        },
        ["Feral Shoes of the Owl"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Twin-bladed Axe of the Tiger"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Infiltrator Bracers of the Whale"] = {
            ["H3220"] = 3522,
            ["mr"] = 3522,
        },
        ["Winterfall Firewater"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Belamoore's Research Journal"] = {
            ["H3220"] = 1775,
            ["mr"] = 1775,
        },
        ["Thistlefur Cap of the Eagle"] = {
            ["H3220"] = 52100,
            ["mr"] = 52100,
        },
        ["Outrunner's Chestguard of the Whale"] = {
            ["H3220"] = 2799,
            ["mr"] = 2799,
        },
        ["Raincaller Robes of the Eagle"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Beheading Blade of the Monkey"] = {
            ["mr"] = 49900,
            ["cc"] = 2,
            ["id"] = "15253:0:0:625:0",
            ["H3246"] = 49900,
            ["sc"] = 8,
        },
        ["Wicked Chain Waistband of the Tiger"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Vital Sash of the Owl"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Azure Silk Cloak"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Twilight Cape of the Owl"] = {
            ["H3220"] = 5490,
            ["mr"] = 5490,
        },
        ["Geomancer's Cord of the Monkey"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Pattern: Boots of the Enchanter"] = {
            ["mr"] = 3463,
            ["sc"] = 2,
            ["id"] = "4352:0:0:0:0",
            ["H3229"] = 3463,
            ["cc"] = 9,
        },
        ["Un'Goro Soil"] = {
            ["H3220"] = 2082,
            ["mr"] = 2082,
        },
        ["Mercenary Blade of the Monkey"] = {
            ["mr"] = 8866,
            ["sc"] = 7,
            ["id"] = "15213:0:0:592:0",
            ["H3246"] = 8866,
            ["cc"] = 2,
        },
        ["Lifeless Stone"] = {
            ["H3220"] = 399,
            ["mr"] = 399,
        },
        ["Grave Moss"] = {
            ["H3220"] = 855,
            ["mr"] = 855,
        },
        ["Large Venom Sac"] = {
            ["mr"] = 1055,
            ["H3254"] = 1055,
            ["H3220"] = 198,
        },
        ["Pattern: Guardian Belt"] = {
            ["mr"] = 58500,
            ["sc"] = 1,
            ["id"] = "4298:0:0:0:0",
            ["H3229"] = 58500,
            ["cc"] = 9,
        },
        ["Durable Shoulders of Intellect"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Champion's Cape of Intellect"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Watcher's Cinch of the Owl"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Simple Branch of the Owl"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Formula: Enchant Bracer - Greater Spirit"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Truesilver Rod"] = {
            ["H3220"] = 12800,
            ["mr"] = 12800,
        },
        ["Elder's Amber Stave of Intellect"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Rugged Leather Pants"] = {
            ["H3220"] = 340,
            ["mr"] = 340,
        },
        ["Geomancer's Cloak of the Owl"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Feral Leggings of the Wolf"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Ritual Shroud of the Whale"] = {
            ["H3220"] = 2997,
            ["mr"] = 2997,
        },
        ["Schematic: Bright-Eye Goggles"] = {
            ["H3220"] = 780,
            ["mr"] = 780,
        },
        ["Cavalier Two-hander of the Whale"] = {
            ["H3220"] = 16046,
            ["mr"] = 16046,
        },
        ["Long Redwood Bow"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Mistscape Gloves"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Birchwood Maul of the Whale"] = {
            ["H3220"] = 1379,
            ["mr"] = 1379,
        },
        ["Archer's Gloves of Arcane Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Trickster's Sash of the Eagle"] = {
            ["H3220"] = 5775,
            ["mr"] = 5775,
        },
        ["Royal Gloves of the Whale"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Belt of Valor"] = {
            ["H3220"] = 135000,
            ["mr"] = 135000,
        },
        ["Dire Wand"] = {
            ["mr"] = 3267,
            ["cc"] = 2,
            ["H3226"] = 3267,
            ["id"] = "8186:0:0:0:0",
            ["sc"] = 19,
        },
        ["Pagan Britches of the Whale"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Fortified Leggings of the Bear"] = {
            ["H3220"] = 3399,
            ["mr"] = 3399,
        },
        ["Mug O' Hurt"] = {
            ["H3220"] = 62400,
            ["mr"] = 62400,
        },
        ["Tracker's Boots of the Monkey"] = {
            ["mr"] = 12683,
            ["cc"] = 4,
            ["id"] = "9917:0:0:607:0",
            ["H3246"] = 12683,
            ["sc"] = 2,
        },
        ["Battlefield Destroyer of the Monkey"] = {
            ["mr"] = 50000,
            ["cc"] = 2,
            ["id"] = "8199:0:0:620:0",
            ["H3246"] = 50000,
            ["sc"] = 8,
        },
        ["Bandit Pants of the Monkey"] = {
            ["H3220"] = 4582,
            ["mr"] = 4582,
        },
        ["Fire Wand"] = {
            ["H3220"] = 394,
            ["mr"] = 394,
        },
        ["Thorium Ore"] = {
            ["H3220"] = 11622,
            ["mr"] = 11622,
        },
        ["Pattern: Deviate Scale Gloves"] = {
            ["mr"] = 12999,
            ["cc"] = 9,
            ["id"] = "6475:0:0:0:0",
            ["L3229"] = 12999,
            ["H3229"] = 14000,
            ["sc"] = 1,
        },
        ["Sage's Stave of Spirit"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Regal Cloak of Intellect"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Rune of Teleportation"] = {
            ["H3220"] = 1954,
            ["mr"] = 1954,
        },
        ["Honed Stiletto of the Bear"] = {
            ["H3220"] = 7256,
            ["mr"] = 7256,
        },
        ["Short Bastard Sword of Strength"] = {
            ["H3220"] = 2050,
            ["mr"] = 2050,
        },
        ["Lord's Boots of the Bear"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Conjurer's Gloves of the Falcon"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Disciple's Robe of Spirit"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Linen Boots"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Training Sword of the Whale"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Tyrant's Greaves"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Huntsman's Cap of Nature's Wrath"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Aboriginal Gloves of the Whale"] = {
            ["H3220"] = 950,
            ["mr"] = 950,
        },
        ["Mystic's Woolies"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Heart of Fire"] = {
            ["H3220"] = 4100,
            ["mr"] = 4100,
        },
        ["Soldier's Leggings of Power"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Prospector's Boots"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Lambent Scale Pauldrons"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Sergeant's Warhammer of Power"] = {
            ["H3220"] = 1925,
            ["mr"] = 1925,
        },
        ["Hulking Chestguard"] = {
            ["H3220"] = 3003,
            ["mr"] = 3003,
        },
        ["Banded Bracers of Power"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Deadly Kris of Power"] = {
            ["H3220"] = 13800,
            ["mr"] = 13800,
        },
        ["Defender Girdle of the Bear"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Magician Staff of the Bear"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Blackforge Buckler"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Icemail Jerkin"] = {
            ["H3220"] = 366000,
            ["mr"] = 366000,
        },
        ["Gloom Reaper of the Wolf"] = {
            ["H3220"] = 11500,
            ["mr"] = 11500,
        },
        ["Polished Zweihander of Stamina"] = {
            ["H3220"] = 7985,
            ["mr"] = 7985,
        },
        ["Tracker's Headband of the Monkey"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Sentinel Shoulders of the Owl"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Aboriginal Sash of the Monkey"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Manual: Mageweave Bandage"] = {
            ["H3220"] = 5898,
            ["mr"] = 5898,
        },
        ["Hands of Darkness"] = {
            ["H3220"] = 11999,
            ["mr"] = 11999,
        },
        ["Archer's Trousers of the Wolf"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Rough Blasting Powder"] = {
            ["H3220"] = 7,
            ["mr"] = 7,
        },
        ["Forester's Axe of Strength"] = {
            ["H3220"] = 5007,
            ["mr"] = 5007,
        },
        ["Thick Scaly Tail"] = {
            ["H3220"] = 220,
            ["mr"] = 220,
        },
        ["Knight's Headguard of the Gorilla"] = {
            ["H3220"] = 24900,
            ["mr"] = 24900,
        },
        ["Watcher's Cuffs of the Eagle"] = {
            ["H3220"] = 6100,
            ["mr"] = 6100,
        },
        ["Archer's Gloves of the Bear"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Greenweave Sash of the Whale"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Incendosaur Scale"] = {
            ["H3220"] = 202,
            ["mr"] = 202,
        },
        ["Sentinel Cap of the Whale"] = {
            ["H3220"] = 16700,
            ["mr"] = 16700,
        },
        ["Conjurer's Cinch of the Eagle"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Slayer's Cape"] = {
            ["H3220"] = 1354,
            ["mr"] = 1354,
        },
        ["Battle Chain Pants"] = {
            ["H3220"] = 356,
            ["mr"] = 356,
        },
        ["Plans: Green Iron Shoulders"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Golem Shard Leggings"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Ballast Maul of the Monkey"] = {
            ["mr"] = 15500,
            ["L3246"] = 15500,
            ["id"] = "1990:0:0:607:0",
            ["sc"] = 5,
            ["H3246"] = 120924,
            ["cc"] = 2,
        },
        ["Conjurer's Hood of the Owl"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Lofty Shoulder Pads of the Bear"] = {
            ["H3220"] = 29000,
            ["mr"] = 29000,
        },
        ["Iridium Chain of the Owl"] = {
            ["H3220"] = 12799,
            ["mr"] = 12799,
        },
        ["Short Bastard Sword of the Bear"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Forest Pendant of the Monkey"] = {
            ["mr"] = 15000,
            ["cc"] = 4,
            ["id"] = "12040:0:0:596:0",
            ["H3246"] = 15000,
            ["sc"] = 0,
        },
        ["Mugthol's Helm"] = {
            ["H3220"] = 149998,
            ["mr"] = 149998,
        },
        ["Hawkeye's Breeches"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Disciple's Stein of the Whale"] = {
            ["H3220"] = 850,
            ["mr"] = 850,
        },
        ["Conjurer's Bracers of Fiery Wrath"] = {
            ["H3220"] = 8700,
            ["mr"] = 8700,
        },
        ["Pattern: Volcanic Shoulders"] = {
            ["mr"] = 72500,
            ["cc"] = 9,
            ["id"] = "15775:0:0:0:0",
            ["sc"] = 1,
            ["H3229"] = 75000,
            ["L3229"] = 72500,
        },
        ["Hacking Cleaver of the Whale"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["The Queen's Jewel"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Outrunner's Cloak of the Boar"] = {
            ["H3220"] = 528,
            ["mr"] = 528,
        },
        ["Buccaneer's Boots of Arcane Wrath"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Battlesmasher of the Tiger"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Scouting Trousers of Agility"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Carnelian Loop of Nature Resistance"] = {
            ["H3220"] = 10016,
            ["mr"] = 10016,
        },
        ["Regal Boots of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Jouster's Girdle"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Plans: Green Iron Boots"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Marauder's Circlet of the Bear"] = {
            ["H3220"] = 8700,
            ["mr"] = 8700,
        },
        ["Ranger Helm of Stamina"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Gypsy Buckler of the Gorilla"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["War Torn Tunic of the Eagle"] = {
            ["H3220"] = 927,
            ["mr"] = 927,
        },
        ["Severing Axe of Agility"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Trickster's Bindings of the Falcon"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Mystic's Slippers"] = {
            ["H3220"] = 990,
            ["mr"] = 990,
        },
        ["Barbaric Battle Axe of the Boar"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Cross Dagger of the Monkey"] = {
            ["mr"] = 15000,
            ["cc"] = 2,
            ["id"] = "2819:0:0:588:0",
            ["H3246"] = 15000,
            ["sc"] = 15,
        },
        ["Slayer's Skullcap"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Battleforge Gauntlets of the Bear"] = {
            ["H3220"] = 3589,
            ["mr"] = 3589,
        },
        ["Dark Iron Ore"] = {
            ["mr"] = 4150,
            ["H3254"] = 4150,
            ["H3220"] = 20000,
        },
        ["Imperial Leather Belt"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Plans: Iron Counterweight"] = {
            ["H3220"] = 1755,
            ["mr"] = 1755,
        },
        ["Oak Mallet of the Gorilla"] = {
            ["H3220"] = 2340,
            ["mr"] = 2340,
        },
        ["Wanderer's Shoulders of the Monkey"] = {
            ["mr"] = 162492,
            ["cc"] = 4,
            ["id"] = "10113:0:0:616:0",
            ["H3246"] = 162492,
            ["sc"] = 2,
        },
        ["Pattern: Tough Scorpid Leggings"] = {
            ["mr"] = 2100,
            ["sc"] = 1,
            ["id"] = "8401:0:0:0:0",
            ["H3229"] = 2100,
            ["cc"] = 9,
        },
        ["Opulent Boots of the Whale"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Citrine"] = {
            ["mr"] = 5000,
            ["sc"] = 0,
            ["L3251"] = 795,
            ["id"] = "3864:0:0:0:0",
            ["H3251"] = 800,
            ["H3254"] = 5000,
            ["cc"] = 7,
        },
        ["Bloodspattered Wristbands of the Bear"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Bandit Cloak of the Monkey"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Burnside Rifle of the Monkey"] = {
            ["mr"] = 39800,
            ["cc"] = 2,
            ["id"] = "15324:0:0:596:0",
            ["H3246"] = 39800,
            ["sc"] = 3,
        },
        ["Robust Boots of the Owl"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Viscous Hammer"] = {
            ["H3220"] = 39900,
            ["mr"] = 39900,
        },
        ["Wrangler's Wraps of the Owl"] = {
            ["H3220"] = 4385,
            ["mr"] = 4385,
        },
        ["Iron Ore"] = {
            ["H3220"] = 347,
            ["mr"] = 347,
        },
        ["Recipe: Rage Potion"] = {
            ["H3220"] = 7605,
            ["mr"] = 7605,
        },
        ["Embossed Leather Cloak"] = {
            ["H3220"] = 180,
            ["mr"] = 180,
        },
        ["Infiltrator Boots of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Tin Bar"] = {
            ["H3220"] = 100,
            ["mr"] = 100,
        },
        ["Ivory Band of the Bear"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Pattern: Phoenix Gloves"] = {
            ["mr"] = 1795,
            ["cc"] = 9,
            ["id"] = "4348:0:0:0:0",
            ["L3229"] = 1795,
            ["H3229"] = 1800,
            ["sc"] = 2,
        },
        ["Short Bastard Sword of the Boar"] = {
            ["H3220"] = 785,
            ["mr"] = 785,
        },
        ["Schematic: Craftsman's Monocle"] = {
            ["H3220"] = 450000,
            ["mr"] = 450000,
        },
        ["Bristlebark Amice"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Gossamer Belt of Fiery Wrath"] = {
            ["H3220"] = 43600,
            ["mr"] = 43600,
        },
        ["Raider's Chestpiece of the Eagle"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Nightsky Mantle"] = {
            ["H3220"] = 3799,
            ["mr"] = 3799,
        },
        ["Polished Zweihander of the Monkey"] = {
            ["mr"] = 7500,
            ["sc"] = 8,
            ["id"] = "15249:0:0:599:0",
            ["H3246"] = 7500,
            ["cc"] = 2,
        },
        ["Big Bear Meat"] = {
            ["H3220"] = 77,
            ["mr"] = 77,
        },
        ["Spiked Chain Wristbands of Stamina"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Hunting Tunic"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Small Spider Leg"] = {
            ["mr"] = 4,
            ["cc"] = 7,
            ["id"] = "5465:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 4,
        },
        ["Huntsman's Gloves of the Monkey"] = {
            ["mr"] = 14000,
            ["cc"] = 4,
            ["id"] = "9892:0:0:605:0",
            ["H3246"] = 14000,
            ["sc"] = 2,
        },
        ["Greater Scythe of the Monkey"] = {
            ["mr"] = 22000,
            ["sc"] = 0,
            ["id"] = "15234:0:0:594:0",
            ["H3246"] = 22000,
            ["cc"] = 2,
        },
        ["Sniper Rifle of Agility"] = {
            ["H3220"] = 43000,
            ["mr"] = 43000,
        },
        ["Recipe: Mithril Head Trout"] = {
            ["H3220"] = 2799,
            ["mr"] = 2799,
        },
        ["Chieftain's Leggings of the Monkey"] = {
            ["mr"] = 48500,
            ["cc"] = 4,
            ["id"] = "9954:0:0:623:0",
            ["H3246"] = 48500,
            ["sc"] = 2,
        },
        ["Raider's Gauntlets of the Whale"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Falcon's Hook"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Brown Linen Robe"] = {
            ["H3220"] = 350,
            ["mr"] = 350,
        },
        ["Torn Bear Pelt"] = {
            ["H3220"] = 124,
            ["mr"] = 124,
        },
        ["Silver-thread Cloak"] = {
            ["H3220"] = 1599,
            ["mr"] = 1599,
        },
        ["Dry Pork Ribs"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Infiltrator Cord of the Whale"] = {
            ["H3220"] = 3483,
            ["mr"] = 3483,
        },
        ["Cutthroat's Mitts of the Tiger"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Huntsman's Cap of Stamina"] = {
            ["H3220"] = 13890,
            ["mr"] = 13890,
        },
        ["Headhunting Spear"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Pattern: Heavy Leather Ball"] = {
            ["H3231"] = 6981,
            ["mr"] = 6981,
            ["cc"] = 9,
            ["id"] = "18731:0:0:0:0",
            ["sc"] = 1,
        },
        ["Skullance Shield"] = {
            ["H3220"] = 25099,
            ["mr"] = 25099,
        },
        ["Shredder Operating Manual - Page 12"] = {
            ["mr"] = 2400,
            ["cc"] = 15,
            ["id"] = "16656:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 2400,
        },
        ["Wrangler's Gloves of the Monkey"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Native Branch of Arcane Wrath"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Greenstone Circle of the Bear"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Scouting Buckler of the Bear"] = {
            ["H3220"] = 6590,
            ["mr"] = 6590,
        },
        ["Raincaller Cloak of the Whale"] = {
            ["H3220"] = 2946,
            ["mr"] = 2946,
        },
        ["Heavy Leather Ammo Pouch"] = {
            ["mr"] = 3200,
            ["sc"] = 3,
            ["H3238"] = 3200,
            ["id"] = "7372:0:0:0:0",
            ["cc"] = 11,
        },
        ["Infantry Leggings of the Bear"] = {
            ["H3220"] = 677,
            ["mr"] = 677,
        },
        ["Rawhide Shoulderpads"] = {
            ["H3220"] = 970,
            ["mr"] = 970,
        },
        ["Spiked Club of Stamina"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Feral Gloves of the Eagle"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Shimmering Armor of Arcane Wrath"] = {
            ["H3220"] = 14622,
            ["mr"] = 14622,
        },
        ["Lesser Eternal Essence"] = {
            ["H3220"] = 22229,
            ["mr"] = 22229,
        },
        ["Thorium Bar"] = {
            ["mr"] = 13167,
            ["H3254"] = 13167,
            ["H3220"] = 8300,
        },
        ["Red Fireworks Rocket"] = {
            ["H3220"] = 75,
            ["mr"] = 75,
        },
        ["Sentry's Headdress of Defense"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Royal Gown of the Eagle"] = {
            ["H3220"] = 69900,
            ["mr"] = 69900,
        },
        ["Burnished Tunic"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Small Venom Sac"] = {
            ["mr"] = 200,
            ["cc"] = 7,
            ["L3220"] = 199,
            ["id"] = "1475:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 347,
        },
        ["Edgemaster's Handguards"] = {
            ["H3220"] = 10849999,
            ["mr"] = 10849999,
        },
        ["Small Radiant Shard"] = {
            ["mr"] = 9999,
            ["cc"] = 7,
            ["id"] = "11177:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 9999,
        },
        ["Wolf Rider's Leggings of the Monkey"] = {
            ["mr"] = 34500,
            ["cc"] = 4,
            ["id"] = "15374:0:0:615:0",
            ["H3246"] = 34500,
            ["sc"] = 2,
        },
        ["Black Mageweave Boots"] = {
            ["H3220"] = 45600,
            ["mr"] = 45600,
        },
        ["Beaststalker's Belt"] = {
            ["H3220"] = 139990,
            ["mr"] = 139990,
        },
        ["Spiritchaser Staff of Healing"] = {
            ["H3220"] = 29000,
            ["mr"] = 29000,
        },
        ["Blanchard's Stout"] = {
            ["H3220"] = 199899,
            ["mr"] = 199899,
        },
        ["Knight's Cloak of the Boar"] = {
            ["H3220"] = 5002,
            ["mr"] = 5002,
        },
        ["Pagan Vest of the Whale"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Wrangler's Leggings of Agility"] = {
            ["H3220"] = 6800,
            ["mr"] = 6800,
        },
        ["Snapvine Watermelon"] = {
            ["H3220"] = 39,
            ["mr"] = 39,
        },
        ["Sanguine Trousers"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Twilight Mantle of Stamina"] = {
            ["H3220"] = 9661,
            ["mr"] = 9661,
        },
        ["Bloodspattered Gloves of the Boar"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Fletcher's Gloves"] = {
            ["H3220"] = 1950,
            ["mr"] = 1950,
        },
        ["Trueshot Bow of the Wolf"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Dervish Leggings of the Gorilla"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Champion's Cape of Agility"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Acrobatic Staff of Arcane Wrath"] = {
            ["H3220"] = 13083,
            ["mr"] = 13083,
        },
        ["Peacebloom"] = {
            ["mr"] = 250,
            ["H3254"] = 250,
            ["H3220"] = 8,
        },
        ["Raider's Chestpiece of the Boar"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Swashbuckler's Boots of the Monkey"] = {
            ["mr"] = 39800,
            ["cc"] = 4,
            ["id"] = "10183:0:0:614:0",
            ["H3246"] = 39800,
            ["sc"] = 2,
        },
        ["Crusader Bow of Spirit"] = {
            ["H3220"] = 37548,
            ["mr"] = 37548,
        },
        ["Stormbringer Belt"] = {
            ["H3220"] = 35055,
            ["mr"] = 35055,
        },
        ["Recipe: Carrion Surprise"] = {
            ["H3220"] = 19200,
            ["mr"] = 19200,
        },
        ["Insignia Chestguard"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Sequoia Hammer of the Bear"] = {
            ["H3220"] = 6300,
            ["mr"] = 6300,
        },
        ["Short Bastard Sword of the Wolf"] = {
            ["H3220"] = 799,
            ["mr"] = 799,
        },
        ["Prospector's Cloak"] = {
            ["H3220"] = 425,
            ["mr"] = 425,
        },
        ["Splitting Hatchet of the Bear"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Barbaric Battle Axe of the Wolf"] = {
            ["H3220"] = 1895,
            ["mr"] = 1895,
        },
        ["Schematic: Goblin Land Mine"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Schematic: Catseye Ultra Goggles"] = {
            ["H3220"] = 3299,
            ["mr"] = 3299,
        },
        ["Robust Cloak of the Wolf"] = {
            ["H3220"] = 2541,
            ["mr"] = 2541,
        },
        ["Sentinel Bracers of the Wolf"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["Dark Leather Tunic"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Runecloth Headband"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Tender Crocolisk Meat"] = {
            ["H3220"] = 57,
            ["mr"] = 57,
        },
        ["Silver-linked Footguards"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Cat Carrier (White Kitten)"] = {
            ["H3220"] = 149900,
            ["mr"] = 149900,
        },
        ["Drakewing Bands"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Blue Dragonscale"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Outrunner's Shield of Strength"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Renegade Cloak of Stamina"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Wrangler's Leggings of the Monkey"] = {
            ["H3220"] = 5007,
            ["mr"] = 5007,
        },
        ["Brutal War Axe of the Wolf"] = {
            ["H3220"] = 7400,
            ["mr"] = 7400,
        },
        ["Righteous Waistguard of the Owl"] = {
            ["H3220"] = 7200,
            ["mr"] = 7200,
        },
        ["Ritual Tunic of the Whale"] = {
            ["H3220"] = 3253,
            ["mr"] = 3253,
        },
        ["Jade Breastplate of the Bear"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Razor Arrow"] = {
            ["H3220"] = 1,
            ["mr"] = 1,
        },
        ["Buccaneer's Mantle"] = {
            ["H3220"] = 1755,
            ["mr"] = 1755,
        },
        ["Conjurer's Sphere of Intellect"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Brutal War Axe of the Boar"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Disciple's Vest of Stamina"] = {
            ["H3220"] = 785,
            ["mr"] = 785,
        },
        ["Grunt's Belt of the Monkey"] = {
            ["H3220"] = 924,
            ["mr"] = 924,
        },
        ["Jazeraint Cloak of the Boar"] = {
            ["H3220"] = 8154,
            ["mr"] = 8154,
        },
        ["Thick Scale Breastplate of the Bear"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Glimmering Flamberge of Power"] = {
            ["H3220"] = 14000,
            ["mr"] = 14000,
        },
        ["Phalanx Shield of Stamina"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Edged Bastard Sword of the Tiger"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Shredder Operating Manual - Page 4"] = {
            ["mr"] = 441,
            ["cc"] = 15,
            ["id"] = "16648:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 441,
        },
        ["Aboriginal Vest of Shadow Wrath"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Jouster's Visor"] = {
            ["H3220"] = 5363,
            ["mr"] = 5363,
        },
        ["Elder's Robe of the Eagle"] = {
            ["H3220"] = 11816,
            ["mr"] = 11816,
        },
        ["Gryphon Mail Crown of the Wolf"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Lesser Mystic Essence"] = {
            ["mr"] = 5000,
            ["H3254"] = 5000,
            ["H3220"] = 1420,
        },
        ["Sanguine Handwraps"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Ballast Maul of the Gorilla"] = {
            ["H3220"] = 69999,
            ["mr"] = 69999,
        },
        ["Short Ash Bow"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Infiltrator Buckler of Stamina"] = {
            ["H3220"] = 7402,
            ["mr"] = 7402,
        },
        ["Scroll of Stamina II"] = {
            ["H3220"] = 321,
            ["mr"] = 321,
        },
        ["Plans: Gemmed Copper Gauntlets"] = {
            ["H3220"] = 195,
            ["mr"] = 195,
        },
        ["Battleforge Shield of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Seer's Robe"] = {
            ["H3220"] = 5700,
            ["mr"] = 5700,
        },
        ["Fighter Broadsword of the Tiger"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Jade Bracers of the Bear"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Severing Axe of the Bear"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Regal Sash of the Whale"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Bandit Jerkin of Stamina"] = {
            ["H3220"] = 3339,
            ["mr"] = 3339,
        },
        ["Schematic: Deepdive Helmet"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["The Ziggler"] = {
            ["H3220"] = 48600,
            ["mr"] = 48600,
        },
        ["Gossamer Cape of Spirit"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Recipe: Spotted Yellowtail"] = {
            ["H3220"] = 21445,
            ["mr"] = 21445,
        },
        ["Scaled Leather Leggings of the Eagle"] = {
            ["H3220"] = 6600,
            ["mr"] = 6600,
        },
        ["Stout Battlehammer of Strength"] = {
            ["H3220"] = 3098,
            ["mr"] = 3098,
        },
        ["Righteous Leggings of the Monkey"] = {
            ["mr"] = 60000,
            ["cc"] = 4,
            ["id"] = "10074:0:0:626:0",
            ["H3246"] = 60000,
            ["sc"] = 2,
        },
        ["Blush Ember Ring"] = {
            ["H3220"] = 13400,
            ["mr"] = 13400,
        },
        ["Dervish Cape of the Owl"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Aboriginal Loincloth of Healing"] = {
            ["H3220"] = 1579,
            ["mr"] = 1579,
        },
        ["Medicine Staff of the Wolf"] = {
            ["H3220"] = 2230,
            ["mr"] = 2230,
        },
        ["Bloodspattered Shield of Agility"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Flask of Big Mojo"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Pattern: Devilsaur Leggings"] = {
            ["mr"] = 150000,
            ["cc"] = 9,
            ["id"] = "15772:0:0:0:0",
            ["H3246"] = 150000,
            ["sc"] = 1,
        },
        ["Shadowcat Hide"] = {
            ["mr"] = 1666,
            ["H3254"] = 1666,
            ["H3220"] = 287,
        },
        ["Sorcerer Cloak of Spirit"] = {
            ["H3220"] = 8042,
            ["mr"] = 8042,
        },
        ["Outrunner's Cuffs of Stamina"] = {
            ["H3220"] = 1166,
            ["mr"] = 1166,
        },
        ["Dalaran Wizard's Robe"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Aboriginal Rod of Intellect"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Native Pants of the Eagle"] = {
            ["H3220"] = 1298,
            ["mr"] = 1298,
        },
        ["Green Hills of Stranglethorn - Page 18"] = {
            ["mr"] = 470,
            ["H3254"] = 470,
            ["H3220"] = 450,
        },
        ["Resilient Handgrips"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Nimble Leather Gloves"] = {
            ["H3220"] = 682,
            ["mr"] = 682,
        },
        ["Wrangler's Leggings of the Bear"] = {
            ["H3220"] = 11256,
            ["mr"] = 11256,
        },
        ["Black Mageweave Gloves"] = {
            ["H3220"] = 5599,
            ["mr"] = 5599,
        },
        ["Recipe: Lesser Stoneshield Potion"] = {
            ["H3220"] = 5300,
            ["mr"] = 5300,
        },
        ["Weak Troll's Blood Potion"] = {
            ["H3220"] = 19,
            ["mr"] = 19,
        },
        ["Sanguine Star"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Dwarven Magestaff of Healing"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Mishandled Recurve Bow"] = {
            ["H3220"] = 1553,
            ["mr"] = 1553,
        },
        ["Ghostwalker Buckler of the Falcon"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Defender Boots of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Native Pants of the Whale"] = {
            ["H3220"] = 1499,
            ["mr"] = 1499,
        },
        ["Mighty Armsplints of Power"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Superior Mana Potion"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Aboriginal Rod of the Whale"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Coarse Gorilla Hair"] = {
            ["mr"] = 607,
            ["cc"] = 15,
            ["id"] = "4096:0:0:0:0",
            ["sc"] = 0,
            ["H3225"] = 607,
        },
        ["Mystic's Cape"] = {
            ["H3220"] = 448,
            ["mr"] = 448,
        },
        ["Greater Fire Protection Potion"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Meadow Ring of the Tiger"] = {
            ["H3220"] = 43300,
            ["mr"] = 43300,
        },
        ["Wrangler's Cloak of Stamina"] = {
            ["H3220"] = 1746,
            ["mr"] = 1746,
        },
        ["Silverleaf"] = {
            ["mr"] = 500,
            ["H3254"] = 500,
            ["H3220"] = 11,
        },
        ["Lesser Bloodstone Ore"] = {
            ["mr"] = 1125,
            ["H3254"] = 1125,
            ["H3220"] = 45,
        },
        ["Barbaric Battle Axe of the Eagle"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Thistlefur Cloak of Frozen Wrath"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Small Green Pouch"] = {
            ["mr"] = 285,
            ["cc"] = 1,
            ["L3220"] = 285,
            ["id"] = "5572:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 290,
        },
        ["Sorcerer Mantle of the Eagle"] = {
            ["H3220"] = 21400,
            ["mr"] = 21400,
        },
        ["Dusky Leather Leggings"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Pioneer Trousers of the Falcon"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Knightly Longsword of Strength"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Pattern: Spider Belt"] = {
            ["mr"] = 9900,
            ["cc"] = 9,
            ["id"] = "4353:0:0:0:0",
            ["sc"] = 2,
            ["H3232"] = 9900,
        },
        ["Phalanx Spaulders of the Whale"] = {
            ["H3220"] = 4927,
            ["mr"] = 4927,
        },
        ["Bard's Gloves of Arcane Wrath"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Cutthroat's Armguards of the Eagle"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Recipe: Great Rage Potion"] = {
            ["H3220"] = 40659,
            ["mr"] = 40659,
        },
        ["Cobalt Ring of Concentration"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Twin-bladed Axe of Strength"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Long Silken Cloak"] = {
            ["mr"] = 3600,
            ["cc"] = 4,
            ["id"] = "4326:0:0:0:0",
            ["sc"] = 1,
            ["H3220"] = 3600,
        },
        ["Banded Gauntlets of the Bear"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Headhunter's Woolies of the Owl"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Frost Oil"] = {
            ["H3220"] = 7149,
            ["mr"] = 7149,
        },
        ["Green Lens of Regeneration"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Jazeraint Boots of Stamina"] = {
            ["H3220"] = 15695,
            ["mr"] = 15695,
        },
        ["Pattern: Hillman's Leather Vest"] = {
            ["mr"] = 197,
            ["cc"] = 9,
            ["id"] = "4293:0:0:0:0",
            ["L3229"] = 197,
            ["H3229"] = 200,
            ["sc"] = 1,
        },
        ["Ruthless Shiv"] = {
            ["H3220"] = 52085,
            ["mr"] = 52085,
        },
        ["Edged Bastard Sword of the Bear"] = {
            ["H3220"] = 1579,
            ["mr"] = 1579,
        },
        ["Edged Bastard Sword of the Eagle"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Discolored Worg Heart"] = {
            ["H3220"] = 72,
            ["mr"] = 72,
        },
        ["Prospector's Mitts"] = {
            ["H3220"] = 995,
            ["mr"] = 995,
        },
        ["Recipe: Smoked Sagefish"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Large Bear Tooth"] = {
            ["H3220"] = 100,
            ["mr"] = 100,
        },
        ["Scaled Leather Belt of the Wolf"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Insignia Cloak"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Azure Silk Gloves"] = {
            ["H3220"] = 6400,
            ["mr"] = 6400,
        },
        ["Enduring Circlet"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Light Leather Pants"] = {
            ["H3220"] = 648,
            ["mr"] = 648,
        },
        ["Scroll of Spirit"] = {
            ["H3220"] = 33,
            ["mr"] = 33,
        },
        ["Shadowgem"] = {
            ["mr"] = 1000,
            ["sc"] = 0,
            ["id"] = "1210:0:0:0:0",
            ["cc"] = 7,
            ["H3254"] = 1000,
            ["H3221"] = 271,
        },
        ["Journeyman's Stave"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Murloc Fin"] = {
            ["H3220"] = 36,
            ["mr"] = 36,
        },
        ["Hunting Bow"] = {
            ["H3220"] = 525,
            ["mr"] = 525,
        },
        ["Deadly Blunderbuss"] = {
            ["H3220"] = 3899,
            ["mr"] = 3899,
        },
        ["Buccaneer's Pants of Healing"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Gothic Plate Vambraces of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Thallium Hoop of the Owl"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Emblazoned Hat"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Heavy Quiver"] = {
            ["H3220"] = 1439,
            ["mr"] = 1439,
        },
        ["Banded Bracers of the Bear"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Sequoia Branch of the Monkey"] = {
            ["mr"] = 40590,
            ["cc"] = 2,
            ["id"] = "15261:0:0:614:0",
            ["H3246"] = 40590,
            ["sc"] = 5,
        },
        ["Laced Mail Shoulderpads"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Witching Stave"] = {
            ["H3220"] = 699999,
            ["mr"] = 699999,
        },
        ["Skeletal Club"] = {
            ["H3220"] = 15010,
            ["mr"] = 15010,
        },
        ["Curved Dagger of Power"] = {
            ["H3220"] = 1194,
            ["mr"] = 1194,
        },
        ["Stonerender Gauntlets"] = {
            ["H3220"] = 800000,
            ["mr"] = 800000,
        },
        ["Ritual Belt of the Whale"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Ranger Shoulders of the Eagle"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Greater Scythe of the Wolf"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Headhunter's Headdress of the Eagle"] = {
            ["H3220"] = 6554,
            ["mr"] = 6554,
        },
        ["Ghostwalker Cloak of Stamina"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Scaled Cloak of the Owl"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Heavy Sharpening Stone"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Raw Glossy Mightfish"] = {
            ["H3220"] = 196,
            ["mr"] = 196,
        },
        ["Phalanx Leggings of the Bear"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Nobles Brand of the Bear"] = {
            ["H3220"] = 23000,
            ["mr"] = 23000,
        },
        ["Captain's Leggings of the Falcon"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Pillager's Pauldrons of the Bear"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Prospector's Buckler"] = {
            ["H3220"] = 1195,
            ["mr"] = 1195,
        },
        ["Gothic Plate Gauntlets of the Gorilla"] = {
            ["H3220"] = 10179,
            ["mr"] = 10179,
        },
        ["Recipe: Big Bear Steak"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Commander's Leggings of the Bear"] = {
            ["H3220"] = 59750,
            ["mr"] = 59750,
        },
        ["Templar Pauldrons of the Tiger"] = {
            ["H3220"] = 22899,
            ["mr"] = 22899,
        },
        ["Wolf Rider's Boots of Stamina"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Thick Scale Cloak of the Monkey"] = {
            ["mr"] = 4000,
            ["sc"] = 1,
            ["id"] = "15547:0:0:592:0",
            ["H3246"] = 4000,
            ["cc"] = 4,
        },
        ["Merc Sword of Agility"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Grizzly Pants of the Wolf"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Regal Wizard Hat of the Whale"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Conjurer's Bracers of the Eagle"] = {
            ["H3220"] = 17148,
            ["mr"] = 17148,
        },
        ["Beaded Gloves"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Recipe: Elixir of Giant Growth"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Bloodspattered Wristbands of the Gorilla"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Birchwood Maul of Strength"] = {
            ["H3220"] = 950,
            ["mr"] = 950,
        },
        ["Jagged Star of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Knight's Headguard of the Bear"] = {
            ["H3220"] = 10200,
            ["mr"] = 10200,
        },
        ["Shimmering Trousers of Spirit"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Sentinel Cloak of Nature's Wrath"] = {
            ["H3220"] = 9276,
            ["mr"] = 9276,
        },
        ["Rigid Tunic of the Owl"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Watcher's Mantle of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Twilight Armor of the Eagle"] = {
            ["H3220"] = 9100,
            ["mr"] = 9100,
        },
        ["Ivycloth Mantle of Stamina"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Icy Cloak"] = {
            ["H3220"] = 12800,
            ["mr"] = 12800,
        },
        ["Shadowcraft Belt"] = {
            ["H3220"] = 138000,
            ["mr"] = 138000,
        },
        ["Soothing Spices"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Sparkleshell Mantle"] = {
            ["H3220"] = 11200,
            ["mr"] = 11200,
        },
        ["Star Ruby"] = {
            ["mr"] = 12188,
            ["H3254"] = 12188,
            ["H3220"] = 7000,
        },
        ["Raincaller Pants of Healing"] = {
            ["H3220"] = 5217,
            ["mr"] = 5217,
        },
        ["Durable Pants of the Whale"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Conjurer's Breeches of the Eagle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Oak Mallet of Strength"] = {
            ["H3220"] = 3257,
            ["mr"] = 3257,
        },
        ["Renegade Boots of the Boar"] = {
            ["H3220"] = 7199,
            ["mr"] = 7199,
        },
        ["Phalanx Cloak of Defense"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Wanderer's Cloak of the Monkey"] = {
            ["mr"] = 46314,
            ["L3246"] = 46314,
            ["id"] = "10108:0:0:605:0",
            ["sc"] = 1,
            ["H3246"] = 55555,
            ["cc"] = 4,
        },
        ["Plans: Heavy Mithril Pants"] = {
            ["mr"] = 5597,
            ["H3254"] = 5597,
            ["H3220"] = 9900,
        },
        ["Opulent Belt of Healing"] = {
            ["H3220"] = 110000,
            ["mr"] = 110000,
        },
        ["Zircon Band of Shadow Resistance"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Glimmering Flamberge of the Wolf"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Gold Power Core"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Gryphon Mail Belt of the Eagle"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Crystalpine Stinger"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Battleforge Armor of the Eagle"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Schematic: Sniper Scope"] = {
            ["H3220"] = 499999,
            ["mr"] = 499999,
        },
        ["Sentry Cloak"] = {
            ["H3220"] = 69000,
            ["mr"] = 69000,
        },
        ["Conjurer's Hood of Healing"] = {
            ["H3220"] = 15018,
            ["mr"] = 15018,
        },
        ["Rotting Bear Carcass"] = {
            ["H3220"] = 252,
            ["mr"] = 252,
        },
        ["Medicine Staff of Spirit"] = {
            ["H3220"] = 5692,
            ["mr"] = 5692,
        },
        ["Plans: Dazzling Mithril Rapier"] = {
            ["H3220"] = 7800,
            ["mr"] = 7800,
        },
        ["Captain's Key"] = {
            ["mr"] = 4900,
            ["sc"] = 0,
            ["id"] = "9249:0:0:0:0",
            ["H3246"] = 4900,
            ["cc"] = 13,
        },
        ["Ranger Wristguards of the Eagle"] = {
            ["H3220"] = 9186,
            ["mr"] = 9186,
        },
        ["Scorpashi Sash"] = {
            ["H3220"] = 23500,
            ["mr"] = 23500,
        },
        ["Renegade Pauldrons of the Whale"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Regal Wizard Hat of Healing"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Resilient Mantle"] = {
            ["H3220"] = 2048,
            ["mr"] = 2048,
        },
        ["Kodo Meat"] = {
            ["H3220"] = 198,
            ["mr"] = 198,
        },
        ["Pattern: Dark Silk Shirt"] = {
            ["mr"] = 8000,
            ["sc"] = 2,
            ["id"] = "6401:0:0:0:0",
            ["H3229"] = 8000,
            ["cc"] = 9,
        },
        ["Headhunter's Slippers of the Gorilla"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Native Pants of Spirit"] = {
            ["H3220"] = 599,
            ["mr"] = 599,
        },
        ["Mithril Bar"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Archer's Trousers of the Monkey"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Holy Shroud"] = {
            ["H3220"] = 32500,
            ["mr"] = 32500,
        },
        ["Wrangler's Belt of the Boar"] = {
            ["H3220"] = 2392,
            ["mr"] = 2392,
        },
        ["Wastewander Water Pouch"] = {
            ["mr"] = 1485,
            ["H3254"] = 1485,
            ["H3220"] = 1990,
        },
        ["Bloodwoven Cord of the Owl"] = {
            ["H3220"] = 12200,
            ["mr"] = 12200,
        },
        ["Decapitating Sword of the Monkey"] = {
            ["mr"] = 5500,
            ["sc"] = 7,
            ["id"] = "3740:0:0:587:0",
            ["H3246"] = 5500,
            ["cc"] = 2,
        },
        ["Field Plate Pauldrons of the Bear"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Goblin Rocket Helmet"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Mighty Troll's Blood Potion"] = {
            ["H3220"] = 1180,
            ["mr"] = 1180,
        },
        ["Schematic: Gnomish Cloaking Device"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Night Watch Shortsword"] = {
            ["H3220"] = 9998,
            ["mr"] = 9998,
        },
        ["Interlaced Shoulderpads"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Trickster's Pauldrons of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Laminated Recurve Bow"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Red Dye"] = {
            ["H3220"] = 556,
            ["mr"] = 556,
        },
        ["Tyrant's Epaulets"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["White Linen Shirt"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Cutthroat's Belt of Intellect"] = {
            ["H3220"] = 4315,
            ["mr"] = 4315,
        },
        ["Cerulean Ring of Intellect"] = {
            ["H3220"] = 9700,
            ["mr"] = 9700,
        },
        ["Plans: Mithril Shield Spike"] = {
            ["H3220"] = 14998,
            ["mr"] = 14998,
        },
        ["Whirring Bronze Gizmo"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Stone Hammer of the Eagle"] = {
            ["H3220"] = 34668,
            ["mr"] = 34668,
        },
        ["Willow Pants of the Owl"] = {
            ["H3220"] = 1293,
            ["mr"] = 1293,
        },
        ["Infiltrator Cap of Intellect"] = {
            ["H3220"] = 9796,
            ["mr"] = 9796,
        },
        ["Quillfire Bow of the Monkey"] = {
            ["H3220"] = 110000,
            ["mr"] = 110000,
        },
        ["Arcane Powder"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Archer's Longbow"] = {
            ["H3220"] = 5654,
            ["mr"] = 5654,
        },
        ["Schematic: Goblin Jumper Cables"] = {
            ["mr"] = 27500,
            ["sc"] = 3,
            ["id"] = "7561:0:0:0:0",
            ["H3229"] = 27500,
            ["cc"] = 9,
        },
        ["Elixir of Water Breathing"] = {
            ["H3220"] = 1609,
            ["mr"] = 1609,
        },
        ["Wicked Chain Bracers of the Bear"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Grizzly Buckler of the Owl"] = {
            ["H3220"] = 1070,
            ["mr"] = 1070,
        },
        ["Tracker's Boots of Spirit"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Sparkleshell Shoulder Pads of Stamina"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Earthen Leather Shoulders"] = {
            ["mr"] = 6500,
            ["cc"] = 9,
            ["id"] = "7362:0:0:0:0",
            ["L3229"] = 6500,
            ["H3229"] = 8288,
            ["sc"] = 1,
        },
        ["Fortified Belt of the Bear"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["High Chief's Sabatons of the Bear"] = {
            ["H3220"] = 17060,
            ["mr"] = 17060,
        },
        ["Ancestral Woollies"] = {
            ["H3220"] = 479,
            ["mr"] = 479,
        },
        ["Murloc Eye"] = {
            ["H3220"] = 17,
            ["mr"] = 17,
        },
        ["Small Silk Pack"] = {
            ["mr"] = 2700,
            ["cc"] = 1,
            ["H3233"] = 2700,
            ["id"] = "4245:0:0:0:0",
            ["sc"] = 0,
        },
        ["Raider's Boots of the Monkey"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Enchanted Mageweave Pouch"] = {
            ["H3220"] = 38600,
            ["mr"] = 38600,
        },
        ["Reinforced Chain Shoulderpads"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Trickster's Headdress of the Eagle"] = {
            ["H3220"] = 7583,
            ["mr"] = 7583,
        },
        ["Bloodspattered Shield of the Monkey"] = {
            ["mr"] = 3000,
            ["sc"] = 6,
            ["id"] = "15494:0:0:585:0",
            ["H3246"] = 3000,
            ["cc"] = 4,
        },
        ["Bonechewer"] = {
            ["H3220"] = 79999,
            ["mr"] = 79999,
        },
        ["Outrunner's Cuffs of the Bear"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Trickster's Headdress of the Owl"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Black Lotus"] = {
            ["H3220"] = 600000,
            ["mr"] = 600000,
        },
        ["Pillager's Gloves of the Monkey"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Bandit Buckler of the Tiger"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Sentinel Boots of the Monkey"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Fine Thread"] = {
            ["mr"] = 370,
            ["cc"] = 7,
            ["id"] = "2321:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 370,
        },
        ["Scorpid Scale"] = {
            ["H3220"] = 865,
            ["mr"] = 865,
        },
        ["Green Hills of Stranglethorn - Page 27"] = {
            ["mr"] = 3899,
            ["H3254"] = 3899,
            ["H3220"] = 513,
        },
        ["Bronze Tube"] = {
            ["H3220"] = 494,
            ["mr"] = 494,
        },
        ["Heavy Crocolisk Stew"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Gnomish Harm Prevention Belt"] = {
            ["H3220"] = 51034,
            ["mr"] = 51034,
        },
        ["Massive Iron Axe"] = {
            ["H3220"] = 27500,
            ["mr"] = 27500,
        },
        ["Disciple's Cloak"] = {
            ["H3220"] = 100,
            ["mr"] = 100,
        },
        ["Shimmering Armor of Intellect"] = {
            ["H3220"] = 14623,
            ["mr"] = 14623,
        },
        ["Dervish Boots of the Owl"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Widowmaker"] = {
            ["H3220"] = 101898,
            ["mr"] = 101898,
        },
        ["Shimmering Boots of Shadow Wrath"] = {
            ["H3220"] = 1298,
            ["mr"] = 1298,
        },
        ["Spiked Chain Leggings of the Bear"] = {
            ["H3220"] = 5800,
            ["mr"] = 5800,
        },
        ["Ebon Scimitar of the Monkey"] = {
            ["mr"] = 41937,
            ["sc"] = 7,
            ["id"] = "8196:0:0:596:0",
            ["H3246"] = 41937,
            ["cc"] = 2,
        },
        ["Beaded Britches of Spirit"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Lupine Buckler of the Falcon"] = {
            ["H3220"] = 1501,
            ["mr"] = 1501,
        },
        ["Renegade Circlet of the Eagle"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Wand of Allistarj"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Bloodspattered Sabatons of the Gorilla"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Boar Intestines"] = {
            ["H3220"] = 22,
            ["mr"] = 22,
        },
        ["Bloodspattered Shield of the Tiger"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Aboriginal Gloves of Fiery Wrath"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Ironhide Bracers of Agility"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Thallium Hoop of Spirit"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Oak Mallet of the Bear"] = {
            ["H3220"] = 4122,
            ["mr"] = 4122,
        },
        ["Massive Battle Axe of the Tiger"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Pattern: Blue Dragonscale Breastplate"] = {
            ["mr"] = 30000,
            ["sc"] = 1,
            ["id"] = "15751:0:0:0:0",
            ["H3229"] = 30000,
            ["cc"] = 9,
        },
        ["Sentinel Shoulders of Intellect"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Charred Wolf Meat"] = {
            ["H3220"] = 28,
            ["mr"] = 28,
        },
        ["Scalping Tomahawk of Stamina"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Elder's Hat of the Wolf"] = {
            ["H3220"] = 12048,
            ["mr"] = 12048,
        },
        ["Wrangler's Wraps of the Eagle"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Crusader's Belt of the Bear"] = {
            ["H3220"] = 35000,
            ["mr"] = 35000,
        },
        ["Bubbling Water"] = {
            ["H3220"] = 70,
            ["mr"] = 70,
        },
        ["Feral Cord of the Owl"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Recipe: Superior Mana Potion"] = {
            ["H3220"] = 28800,
            ["mr"] = 28800,
        },
        ["Scorpashi Skullcap"] = {
            ["H3220"] = 32000,
            ["mr"] = 32000,
        },
        ["Amber Hoop of Arcane Resistance"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Infiltrator Boots of the Monkey"] = {
            ["H3220"] = 6867,
            ["mr"] = 6867,
        },
        ["Iron Counterweight"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Spiked Chain Breastplate of Power"] = {
            ["H3220"] = 17900,
            ["mr"] = 17900,
        },
        ["Fisherman Knife"] = {
            ["H3220"] = 650,
            ["mr"] = 650,
        },
        ["Aurora Robe"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Thick Wolfhide"] = {
            ["H3220"] = 1579,
            ["mr"] = 1579,
        },
        ["Handstitched Leather Pants"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Goblin Nutcracker of the Bear"] = {
            ["H3220"] = 22000,
            ["mr"] = 22000,
        },
        ["Plans: Mithril Scale Shoulders"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Rigid Cape of the Whale"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Wizard's Hand of the Eagle"] = {
            ["H3220"] = 79900,
            ["mr"] = 79900,
        },
        ["Mighty Helmet of Defense"] = {
            ["H3220"] = 290000,
            ["mr"] = 290000,
        },
        ["Merc Sword of the Bear"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Giant Club of Stamina"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Sage's Pants of Healing"] = {
            ["H3220"] = 14444,
            ["mr"] = 14444,
        },
        ["Carving Knife of Power"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Umbral Crystal"] = {
            ["H3220"] = 64500,
            ["mr"] = 64500,
        },
        ["Native Pants of the Falcon"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Wrangler's Boots of Stamina"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Silksand Circlet"] = {
            ["H3220"] = 13800,
            ["mr"] = 13800,
        },
        ["Defender Girdle of the Eagle"] = {
            ["H3220"] = 1636,
            ["mr"] = 1636,
        },
        ["Blunt Claymore"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Merciless Bracers of the Monkey"] = {
            ["mr"] = 15000,
            ["cc"] = 4,
            ["id"] = "15649:0:0:603:0",
            ["H3246"] = 15000,
            ["sc"] = 3,
        },
        ["Bloodwoven Bracers of Healing"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Parachute Cloak"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Tough Jerky"] = {
            ["H3220"] = 3,
            ["mr"] = 3,
        },
        ["Righteous Spaulders of the Eagle"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Elixir of Fortitude"] = {
            ["H3220"] = 925,
            ["mr"] = 925,
        },
        ["Wicked Leather Gauntlets"] = {
            ["H3220"] = 14000,
            ["mr"] = 14000,
        },
        ["Sentinel Gloves of the Falcon"] = {
            ["H3220"] = 8766,
            ["mr"] = 8766,
        },
        ["Icecap"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Black Pearl"] = {
            ["mr"] = 2108,
            ["H3254"] = 2108,
            ["H3220"] = 1255,
        },
        ["Heavy Silk Bandage"] = {
            ["mr"] = 625,
            ["H3254"] = 625,
            ["H3220"] = 399,
        },
        ["Geomancer's Cord of the Whale"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Empty Vial"] = {
            ["H3220"] = 111,
            ["mr"] = 111,
        },
        ["Pattern: Shadowskin Gloves"] = {
            ["mr"] = 23500,
            ["cc"] = 9,
            ["id"] = "18239:0:0:0:0",
            ["L3229"] = 10000,
            ["H3229"] = 23500,
            ["sc"] = 1,
        },
        ["Conjurer's Robe of the Whale"] = {
            ["H3220"] = 5730,
            ["mr"] = 5730,
        },
        ["Mystical Gloves of the Eagle"] = {
            ["H3220"] = 44500,
            ["mr"] = 44500,
        },
        ["Twin-bladed Axe of Stamina"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Fen Ring of the Monkey"] = {
            ["H3220"] = 12061,
            ["mr"] = 12061,
        },
        ["Cross Dagger of Strength"] = {
            ["H3220"] = 7313,
            ["mr"] = 7313,
        },
        ["Bard's Trousers of the Owl"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Jet Loop of Strength"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Cedar Walking Stick"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Chieftain's Shoulders of the Owl"] = {
            ["H3220"] = 18500,
            ["mr"] = 18500,
        },
        ["Pattern: Felcloth Boots"] = {
            ["mr"] = 29055,
            ["cc"] = 9,
            ["id"] = "14492:0:0:0:0",
            ["H3246"] = 29055,
            ["sc"] = 2,
        },
        ["Pattern: Stylish Green Shirt"] = {
            ["mr"] = 1922,
            ["sc"] = 2,
            ["id"] = "6391:0:0:0:0",
            ["H3229"] = 1922,
            ["cc"] = 9,
        },
        ["Fortified Gauntlets of Strength"] = {
            ["H3220"] = 837,
            ["mr"] = 837,
        },
        ["Marauder's Gauntlets of Strength"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Chief Brigadier Coif"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Gauntlets of Elements"] = {
            ["H3220"] = 120000,
            ["mr"] = 120000,
        },
        ["Bolt of Woolen Cloth"] = {
            ["mr"] = 332,
            ["sc"] = 0,
            ["H3236"] = 239,
            ["id"] = "2997:0:0:0:0",
            ["H3254"] = 332,
            ["cc"] = 7,
        },
        ["Ritual Bands of the Eagle"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Thick Armor Kit"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Tanglewood Staff"] = {
            ["H3220"] = 75495,
            ["mr"] = 75495,
        },
        ["Turtle Scale Leggings"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Brackwater Boots"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Plans: Jade Serpentblade"] = {
            ["H3220"] = 1280,
            ["mr"] = 1280,
        },
        ["Six Demon Bag"] = {
            ["mr"] = 300000,
            ["H3254"] = 300000,
            ["H3220"] = 749999,
        },
        ["Pattern: Tough Scorpid Breastplate"] = {
            ["mr"] = 10000,
            ["cc"] = 9,
            ["L3229"] = 3200,
            ["id"] = "8395:0:0:0:0",
            ["H3229"] = 3300,
            ["H3254"] = 10000,
            ["sc"] = 1,
        },
        ["Rigid Belt of the Whale"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Blazing Wand"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Pattern: Living Shoulders"] = {
            ["mr"] = 50000,
            ["cc"] = 9,
            ["L3229"] = 13999,
            ["id"] = "15734:0:0:0:0",
            ["H3229"] = 14000,
            ["H3254"] = 50000,
            ["sc"] = 1,
        },
        ["Wizard's Hand of the Boar"] = {
            ["H3220"] = 69999,
            ["mr"] = 69999,
        },
        ["Sentry's Slippers of Stamina"] = {
            ["H3220"] = 2336,
            ["mr"] = 2336,
        },
        ["Spiked Club of Spirit"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Exquisite Flamberge of the Boar"] = {
            ["mr"] = 40000,
            ["H3254"] = 40000,
            ["H3220"] = 25000,
        },
        ["Hillborne Axe of the Tiger"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Archer's Jerkin of Stamina"] = {
            ["H3220"] = 7300,
            ["mr"] = 7300,
        },
        ["Grizzly Buckler of the Eagle"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Warrior's Pants"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Shimmering Cloak of Intellect"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Raincaller Vest of Arcane Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Glowing Brightwood Staff"] = {
            ["H3220"] = 1990000,
            ["mr"] = 1990000,
        },
        ["Grizzly Pants of the Bear"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Sentry's Gloves of the Eagle"] = {
            ["H3220"] = 1590,
            ["mr"] = 1590,
        },
        ["Tellurium Necklace of Strength"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Emblazoned Chestpiece"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Outrunner's Gloves of Power"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Sage's Cloak of the Owl"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Volatile Rum"] = {
            ["mr"] = 2436,
            ["H3254"] = 2436,
            ["H3220"] = 905,
        },
        ["Sutarn's Ring"] = {
            ["H3220"] = 42900,
            ["mr"] = 42900,
        },
        ["Ritual Bands of Spirit"] = {
            ["H3220"] = 2526,
            ["mr"] = 2526,
        },
        ["Raider Shortsword of Strength"] = {
            ["H3220"] = 1595,
            ["mr"] = 1595,
        },
        ["Cracked Shortbow"] = {
            ["H3220"] = 101,
            ["mr"] = 101,
        },
        ["Rough Bronze Boots"] = {
            ["H3220"] = 605,
            ["mr"] = 605,
        },
        ["Sentinel Cloak of Arcane Wrath"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Cutthroat's Buckler of the Eagle"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Quartz Ring of Shadow Resistance"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Banded Leggings of Power"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Rumsey Rum Light"] = {
            ["H3220"] = 487,
            ["mr"] = 487,
        },
        ["Vital Cape of Stamina"] = {
            ["H3220"] = 4952,
            ["mr"] = 4952,
        },
        ["Warden's Mantle"] = {
            ["H3220"] = 9898,
            ["mr"] = 9898,
        },
        ["Scaled Leather Leggings of the Owl"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Pattern: Ironfeather Breastplate"] = {
            ["mr"] = 20367,
            ["sc"] = 1,
            ["id"] = "15760:0:0:0:0",
            ["H3229"] = 20367,
            ["cc"] = 9,
        },
        ["Feral Leggings of the Eagle"] = {
            ["H3220"] = 1199,
            ["mr"] = 1199,
        },
        ["Ritual Belt of Fiery Wrath"] = {
            ["H3220"] = 699,
            ["mr"] = 699,
        },
        ["Small Scorpid Claw"] = {
            ["mr"] = 2400,
            ["cc"] = 15,
            ["id"] = "19937:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 2400,
        },
        ["Shadowskin Gloves"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Elder's Cloak of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Headhunter's Woolies of the Monkey"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Marsh Chain of the Falcon"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Headhunter's Headdress of the Whale"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Ankh of Life"] = {
            ["H3220"] = 39900,
            ["mr"] = 39900,
        },
        ["Thick Spider's Silk"] = {
            ["mr"] = 1475,
            ["cc"] = 7,
            ["sc"] = 0,
            ["id"] = "4337:0:0:0:0",
            ["L3250"] = 750,
            ["H3254"] = 1475,
            ["H3250"] = 760,
        },
        ["Battlesmasher of the Bear"] = {
            ["H3220"] = 3327,
            ["mr"] = 3327,
        },
        ["Barbecued Buzzard Wing"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Scorching Wand"] = {
            ["mr"] = 6500,
            ["cc"] = 2,
            ["H3245"] = 6500,
            ["id"] = "5213:0:0:0:0",
            ["sc"] = 19,
        },
        ["Deanship Claymore"] = {
            ["H3220"] = 10400,
            ["mr"] = 10400,
        },
        ["Forest Hoop of the Monkey"] = {
            ["mr"] = 13947,
            ["sc"] = 0,
            ["id"] = "12011:0:0:599:0",
            ["H3246"] = 13947,
            ["cc"] = 4,
        },
        ["Ravasaur Scale Boots"] = {
            ["H3220"] = 25800,
            ["mr"] = 25800,
        },
        ["Recipe: Strider Stew"] = {
            ["H3220"] = 5700,
            ["mr"] = 5700,
        },
        ["Jet Black Feather"] = {
            ["H3220"] = 135,
            ["mr"] = 135,
        },
        ["Dokebi Boots"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Pattern: Cindercloth Vest"] = {
            ["mr"] = 100000,
            ["cc"] = 9,
            ["id"] = "14471:0:0:0:0",
            ["L3229"] = 100000,
            ["H3229"] = 109500,
            ["sc"] = 2,
        },
        ["Vorpal Dagger of the Monkey"] = {
            ["H3220"] = 35000,
            ["mr"] = 35000,
        },
        ["Splitting Hatchet of the Wolf"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Serpentskin Gloves"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Bard's Gloves of the Owl"] = {
            ["H3220"] = 2748,
            ["mr"] = 2748,
        },
        ["Watcher's Handwraps of Healing"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Raincaller Mitts of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Barbaric Cloth Gloves"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Templar Girdle of Power"] = {
            ["H3220"] = 13999,
            ["mr"] = 13999,
        },
        ["Gaea's Cuffs of the Owl"] = {
            ["H3220"] = 29900,
            ["mr"] = 29900,
        },
        ["Huntsman's Leggings of the Monkey"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Twilight Gloves of Spirit"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Wrangler's Wristbands of Agility"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Khadgar's Whisker"] = {
            ["mr"] = 176,
            ["sc"] = 0,
            ["id"] = "3358:0:0:0:0",
            ["L3251"] = 176,
            ["H3251"] = 179,
            ["cc"] = 7,
        },
        ["Conjurer's Breeches of Healing"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Ballast Maul of Strength"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Magician Staff of Nature's Wrath"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Dervish Spaulders of the Owl"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Fortified Bracers of the Eagle"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Raw Rainbow Fin Albacore"] = {
            ["H3220"] = 72,
            ["mr"] = 72,
        },
        ["Plans: Massive Iron Axe"] = {
            ["H3254"] = 31368,
            ["mr"] = 31368,
        },
        ["Rumsey Rum Black Label"] = {
            ["H3220"] = 633,
            ["mr"] = 633,
        },
        ["Soldier's Wristguards of Strength"] = {
            ["H3220"] = 2448,
            ["mr"] = 2448,
        },
        ["Darkmist Bands of the Whale"] = {
            ["H3220"] = 6994,
            ["mr"] = 6994,
        },
        ["Guardian Blade"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Overlord's Legplates of the Bear"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Spiritchaser Staff of the Monkey"] = {
            ["mr"] = 40000,
            ["sc"] = 10,
            ["id"] = "1613:0:0:617:0",
            ["H3246"] = 40000,
            ["cc"] = 2,
        },
        ["Schematic: Moonsight Rifle"] = {
            ["H3220"] = 990,
            ["mr"] = 990,
        },
        ["Geomancer's Cord of the Eagle"] = {
            ["H3220"] = 11200,
            ["mr"] = 11200,
        },
        ["Feral Shoes of Stamina"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Red Mageweave Pants"] = {
            ["H3220"] = 13900,
            ["mr"] = 13900,
        },
        ["Goldthorn"] = {
            ["H3220"] = 424,
            ["mr"] = 424,
        },
        ["Sorcerer Sash of Healing"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Furious Falchion of the Monkey"] = {
            ["mr"] = 79499,
            ["cc"] = 2,
            ["id"] = "15215:0:0:596:0",
            ["H3246"] = 79499,
            ["sc"] = 7,
        },
        ["Hawkeye's Bracers"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Plans: Thorium Bracers"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Silksand Shoulder Pads"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["War Knife of Power"] = {
            ["H3220"] = 2350,
            ["mr"] = 2350,
        },
        ["Jagged Star of the Monkey"] = {
            ["mr"] = 4000,
            ["sc"] = 4,
            ["id"] = "15223:0:0:585:0",
            ["H3246"] = 4000,
            ["cc"] = 2,
        },
        ["Skycaller"] = {
            ["H3220"] = 32200,
            ["mr"] = 32200,
        },
        ["Grunt's Bracers of Stamina"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Herb Pouch"] = {
            ["H3220"] = 2799,
            ["mr"] = 2799,
        },
        ["Wolfshead Helm"] = {
            ["H3220"] = 181999,
            ["mr"] = 181999,
        },
        ["Runecloth Bandage"] = {
            ["H3220"] = 2666,
            ["mr"] = 2666,
        },
        ["Dalaran Sharp"] = {
            ["H3220"] = 19,
            ["mr"] = 19,
        },
        ["Phalanx Headguard of the Bear"] = {
            ["H3220"] = 7974,
            ["mr"] = 7974,
        },
        ["Conjurer's Cloak of the Owl"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Warbringer's Chestguard of the Bear"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Wool Cloth"] = {
            ["mr"] = 1000,
            ["sc"] = 0,
            ["id"] = "2592:0:0:0:0",
            ["cc"] = 7,
            ["H3254"] = 1000,
            ["H3250"] = 319,
        },
        ["Embossed Leather Pants"] = {
            ["H3220"] = 594,
            ["mr"] = 594,
        },
        ["Wolf Rider's Headgear of the Monkey"] = {
            ["mr"] = 13000,
            ["sc"] = 2,
            ["id"] = "15373:0:0:620:0",
            ["H3246"] = 13000,
            ["cc"] = 4,
        },
        ["Wicked Chain Legguards of the Bear"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Greater Eternal Essence"] = {
            ["H3220"] = 42400,
            ["mr"] = 42400,
        },
        ["Minor Mana Potion"] = {
            ["H3220"] = 48,
            ["mr"] = 48,
        },
        ["Savage Axe of the Tiger"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Staff of Hale Magefire"] = {
            ["H3220"] = 400000,
            ["mr"] = 400000,
        },
        ["Thistlefur Cap of Intellect"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Gleaming Claymore of the Bear"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Copper Ore"] = {
            ["H3220"] = 124,
            ["mr"] = 124,
        },
        ["Banded Helm of Stamina"] = {
            ["H3220"] = 6600,
            ["mr"] = 6600,
        },
        ["Grunt's Chestpiece of the Gorilla"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Massive Battle Axe of the Monkey"] = {
            ["H3220"] = 7693,
            ["mr"] = 7693,
        },
        ["Rageclaw Gloves of the Monkey"] = {
            ["H3220"] = 22999,
            ["mr"] = 22999,
        },
        ["Shadoweave Shoulders"] = {
            ["H3220"] = 58997,
            ["mr"] = 58997,
        },
        ["Dragonbreath Chili"] = {
            ["H3220"] = 1905,
            ["mr"] = 1905,
        },
        ["Schematic: Goblin Jumper Cables XL"] = {
            ["mr"] = 59900,
            ["cc"] = 9,
            ["id"] = "18653:0:0:0:0",
            ["sc"] = 3,
            ["H3254"] = 59900,
            ["H3229"] = 199500,
        },
        ["Banded Cloak of Strength"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Aboriginal Loincloth of the Owl"] = {
            ["H3220"] = 480,
            ["mr"] = 480,
        },
        ["Plans: Green Iron Gauntlets"] = {
            ["H3220"] = 1410,
            ["mr"] = 1410,
        },
        ["Outrunner's Chestguard of Strength"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Ballast Maul of Stamina"] = {
            ["H3220"] = 12400,
            ["mr"] = 12400,
        },
        ["Pattern: Chimeric Boots"] = {
            ["mr"] = 15000,
            ["sc"] = 1,
            ["id"] = "15737:0:0:0:0",
            ["H3229"] = 15000,
            ["cc"] = 9,
        },
        ["Greater Magic Wand"] = {
            ["H3220"] = 1705,
            ["mr"] = 1705,
        },
        ["Razor Blade of Power"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Thick Scale Crown of the Boar"] = {
            ["H3220"] = 5600,
            ["mr"] = 5600,
        },
        ["Schematic: Thorium Grenade"] = {
            ["H3220"] = 28900,
            ["mr"] = 28900,
        },
        ["Priest's Mace of Nature's Wrath"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Red Firework"] = {
            ["mr"] = 96,
            ["cc"] = 0,
            ["H3226"] = 96,
            ["id"] = "9318:0:0:0:0",
            ["sc"] = 0,
        },
        ["Caverndeep Trudgers"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Curved Dagger of Frozen Wrath"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Merc Sword of the Wolf"] = {
            ["H3220"] = 2213,
            ["mr"] = 2213,
        },
        ["Chrome Ring of Intellect"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Dark Leather Belt"] = {
            ["H3220"] = 1299,
            ["mr"] = 1299,
        },
        ["Spiked Chain Belt of Strength"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Heaven's Light"] = {
            ["mr"] = 209999,
            ["H3254"] = 209999,
            ["H3220"] = 29999,
        },
        ["Cadet's Bow"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["The Butcher"] = {
            ["H3220"] = 59890,
            ["mr"] = 59890,
        },
        ["Elder's Pants of the Whale"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Leaden Mace of Strength"] = {
            ["H3220"] = 7979,
            ["mr"] = 7979,
        },
        ["Gromsblood"] = {
            ["mr"] = 5700,
            ["H3254"] = 5700,
            ["H3220"] = 5900,
        },
        ["Stout Battlehammer of the Monkey"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Iron Buckle"] = {
            ["H3220"] = 695,
            ["mr"] = 695,
        },
        ["Renegade Gauntlets of the Eagle"] = {
            ["H3220"] = 5055,
            ["mr"] = 5055,
        },
        ["Ricochet Blunderbuss of Spirit"] = {
            ["H3220"] = 45500,
            ["mr"] = 45500,
        },
        ["Stonecutter Claymore of the Tiger"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Embossed Plate Pauldrons of the Tiger"] = {
            ["H3220"] = 9499,
            ["mr"] = 9499,
        },
        ["Vital Boots of Stamina"] = {
            ["H3220"] = 3409,
            ["mr"] = 3409,
        },
        ["Summoner's Wand"] = {
            ["H3220"] = 6799,
            ["mr"] = 6799,
        },
        ["Vanadium Loop of Concentration"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Pitchfork"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Ring of the Heavens"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Cerulean Talisman of the Eagle"] = {
            ["H3220"] = 14800,
            ["mr"] = 14800,
        },
        ["Ballast Maul of the Whale"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Enchanter's Cowl"] = {
            ["H3220"] = 5800,
            ["mr"] = 5800,
        },
        ["Durable Rod of Healing"] = {
            ["H3220"] = 12900,
            ["mr"] = 12900,
        },
        ["Renegade Cloak of the Bear"] = {
            ["H3220"] = 2777,
            ["mr"] = 2777,
        },
        ["Pattern: Green Whelp Bracers"] = {
            ["mr"] = 10050,
            ["cc"] = 9,
            ["id"] = "7451:0:0:0:0",
            ["H3229"] = 10050,
            ["sc"] = 1,
        },
        ["Robust Cloak of Defense"] = {
            ["H3220"] = 1937,
            ["mr"] = 1937,
        },
        ["Ivy Orb of the Owl"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Ranger Boots of the Monkey"] = {
            ["mr"] = 14900,
            ["cc"] = 4,
            ["id"] = "7481:0:0:607:0",
            ["H3246"] = 14900,
            ["sc"] = 2,
        },
        ["Bard's Trousers of Agility"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Feral Cloak of Intellect"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Schematic: Truesilver Transformer"] = {
            ["H3220"] = 24400,
            ["mr"] = 24400,
        },
        ["Glimmering Flamberge of the Tiger"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Rigid Shoulders of the Boar"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Greenweave Leggings of Fiery Wrath"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Commander's Girdle of the Monkey"] = {
            ["H3220"] = 99900,
            ["mr"] = 99900,
        },
        ["Pattern: Runecloth Tunic"] = {
            ["mr"] = 44137,
            ["sc"] = 2,
            ["H3239"] = 44137,
            ["id"] = "14470:0:0:0:0",
            ["cc"] = 9,
        },
        ["Ivycloth Mantle of the Whale"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Furen's Boots"] = {
            ["H3220"] = 15999,
            ["mr"] = 15999,
        },
        ["Blue Linen Shirt"] = {
            ["H3220"] = 439,
            ["mr"] = 439,
        },
        ["Flying Tiger Goggles"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Longjaw Mud Snapper"] = {
            ["H3220"] = 2,
            ["mr"] = 2,
        },
        ["Geomancer's Bracers of Arcane Wrath"] = {
            ["H3220"] = 6400,
            ["mr"] = 6400,
        },
        ["Iridium Circle of the Owl"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Great Rage Potion"] = {
            ["H3220"] = 939,
            ["mr"] = 939,
        },
        ["Renegade Belt of the Bear"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Sage's Sash of the Eagle"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Pagan Bands of Frozen Wrath"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Cutthroat's Buckler of the Falcon"] = {
            ["H3220"] = 5100,
            ["mr"] = 5100,
        },
        ["White Spider Meat"] = {
            ["mr"] = 299,
            ["cc"] = 7,
            ["id"] = "12205:0:0:0:0",
            ["sc"] = 0,
            ["H3254"] = 299,
            ["H3232"] = 303,
        },
        ["Severing Axe of Strength"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Recipe: Wildvine Potion"] = {
            ["mr"] = 2339,
            ["cc"] = 9,
            ["H3226"] = 2339,
            ["id"] = "9294:0:0:0:0",
            ["sc"] = 6,
        },
        ["Recipe: Soothing Turtle Bisque"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Renegade Circlet of the Bear"] = {
            ["H3220"] = 6400,
            ["mr"] = 6400,
        },
        ["Ghostwalker Buckler of the Whale"] = {
            ["H3220"] = 7400,
            ["mr"] = 7400,
        },
        ["Heart Ring"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Guardian Belt"] = {
            ["H3220"] = 5800,
            ["mr"] = 5800,
        },
        ["Monk's Staff of the Owl"] = {
            ["H3220"] = 32200,
            ["mr"] = 32200,
        },
        ["Bandit Cinch of the Monkey"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Ranger Bow"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Hellslayer Battle Axe"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Recipe: Mystery Stew"] = {
            ["H3220"] = 7600,
            ["mr"] = 7600,
        },
        ["Saltstone Helm of the Monkey"] = {
            ["mr"] = 18658,
            ["cc"] = 4,
            ["id"] = "14899:0:0:614:0",
            ["H3246"] = 18658,
            ["sc"] = 4,
        },
        ["Plans: Masterwork Stormhammer"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Hillman's Shoulders"] = {
            ["H3220"] = 1209,
            ["mr"] = 1209,
        },
        ["Minor Rejuvenation Potion"] = {
            ["H3220"] = 28,
            ["mr"] = 28,
        },
        ["Spiked Club of the Tiger"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Greenstone Talisman of the Gorilla"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Ancestral Orb"] = {
            ["H3220"] = 590,
            ["mr"] = 590,
        },
        ["Stone Hammer of the Whale"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Abjurer's Hood of the Owl"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Sequoia Hammer of the Tiger"] = {
            ["H3220"] = 6130,
            ["mr"] = 6130,
        },
        ["Pattern: Red Woolen Boots"] = {
            ["mr"] = 549,
            ["cc"] = 9,
            ["id"] = "4345:0:0:0:0",
            ["L3229"] = 549,
            ["H3229"] = 699,
            ["sc"] = 2,
        },
        ["Overlord's Crown of the Monkey"] = {
            ["mr"] = 75000,
            ["cc"] = 4,
            ["id"] = "10207:0:0:621:0",
            ["H3246"] = 75000,
            ["sc"] = 4,
        },
        ["Trueshot Bow of the Eagle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Fortified Chain of the Eagle"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Buccaneer's Robes of Frozen Wrath"] = {
            ["H3220"] = 9899,
            ["mr"] = 9899,
        },
        ["Bandit Pants of the Owl"] = {
            ["H3220"] = 2946,
            ["mr"] = 2946,
        },
        ["Feral Shoes of Spirit"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Rainbow Fin Albacore"] = {
            ["H3220"] = 70,
            ["mr"] = 70,
        },
        ["Greater Mystic Wand"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Fortified Shield of Strength"] = {
            ["H3220"] = 6600,
            ["mr"] = 6600,
        },
        ["Dervish Cape of Defense"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Ivycloth Cloak of the Owl"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Stag Meat"] = {
            ["H3220"] = 80,
            ["mr"] = 80,
        },
        ["Banded Shield of the Bear"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Deadly Kris of Fiery Wrath"] = {
            ["H3254"] = 15221,
            ["mr"] = 15221,
        },
        ["Renegade Belt of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Spiked Chain Wristbands of the Monkey"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Sanguine Armor"] = {
            ["H3220"] = 3599,
            ["mr"] = 3599,
        },
        ["War Knife of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Schematic: Lovingly Crafted Boomstick"] = {
            ["H3220"] = 4896,
            ["mr"] = 4896,
        },
        ["Wrangler's Boots of the Monkey"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Big Voodoo Pants"] = {
            ["H3220"] = 100000,
            ["mr"] = 100000,
        },
        ["Pattern: Icy Cloak"] = {
            ["mr"] = 100000,
            ["sc"] = 2,
            ["id"] = "4355:0:0:0:0",
            ["H3229"] = 100000,
            ["cc"] = 9,
        },
        ["Ancestral Cloak"] = {
            ["H3220"] = 174,
            ["mr"] = 174,
        },
        ["Insignia Boots"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Simple Blouse of the Owl"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Arena Bracers"] = {
            ["H3220"] = 129300,
            ["mr"] = 129300,
        },
        ["Grizzly Pants of the Gorilla"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Heavy Stone"] = {
            ["L3226"] = 305,
            ["mr"] = 305,
            ["cc"] = 7,
            ["H3226"] = 362,
            ["id"] = "2838:0:0:0:0",
            ["sc"] = 0,
        },
        ["Raincaller Vest of the Whale"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Perfect Deviate Scale"] = {
            ["H3220"] = 1014,
            ["mr"] = 1014,
        },
        ["Rigid Gloves of the Gorilla"] = {
            ["H3220"] = 10438,
            ["mr"] = 10438,
        },
        ["Rough Crocolisk Scale"] = {
            ["H3220"] = 995,
            ["mr"] = 995,
        },
        ["Pattern: Robe of the Archmage"] = {
            ["mr"] = 2670000,
            ["sc"] = 2,
            ["id"] = "14513:0:0:0:0",
            ["H3229"] = 2670000,
            ["cc"] = 9,
        },
        ["Sentinel Breastplate of the Owl"] = {
            ["H3220"] = 10075,
            ["mr"] = 10075,
        },
        ["Trickster's Boots of the Whale"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Gleaming Claymore of the Monkey"] = {
            ["mr"] = 15000,
            ["sc"] = 8,
            ["id"] = "15248:0:0:593:0",
            ["H3246"] = 15000,
            ["cc"] = 2,
        },
        ["Fortified Shield of the Bear"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Oak Mallet of the Whale"] = {
            ["H3220"] = 6266,
            ["mr"] = 6266,
        },
        ["Formula: Enchant Boots - Spirit"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Spiked Chain Wristbands of the Whale"] = {
            ["H3220"] = 3666,
            ["mr"] = 3666,
        },
        ["Plaguebloom"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Greater Magic Essence"] = {
            ["H3220"] = 1586,
            ["mr"] = 1586,
        },
        ["Black Malice"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Dwarven Magestaff of the Eagle"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Conjurer's Cloak of the Eagle"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Massive Battle Axe of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Jade"] = {
            ["mr"] = 4499,
            ["H3254"] = 4499,
            ["H3220"] = 699,
        },
        ["Seer's Padded Armor"] = {
            ["H3220"] = 1720,
            ["mr"] = 1720,
        },
        ["Thistlefur Cloak of Intellect"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Massive Battle Axe of the Boar"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Hillborne Axe of Strength"] = {
            ["H3220"] = 12276,
            ["mr"] = 12276,
        },
        ["Thick Hide"] = {
            ["H3220"] = 704,
            ["mr"] = 704,
        },
        ["Plans: Blue Glittering Axe"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Archer's Trousers of the Owl"] = {
            ["H3220"] = 10101,
            ["mr"] = 10101,
        },
        ["Desert Choker of the Tiger"] = {
            ["H3220"] = 23200,
            ["mr"] = 23200,
        },
        ["Flesh Piercer"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Emblazoned Boots"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Knight's Breastplate of the Bear"] = {
            ["H3220"] = 10600,
            ["mr"] = 10600,
        },
        ["Greater Nether Essence"] = {
            ["H3220"] = 11600,
            ["mr"] = 11600,
        },
        ["Robust Leggings of the Eagle"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Ancient Greaves of the Monkey"] = {
            ["mr"] = 18579,
            ["cc"] = 4,
            ["id"] = "15599:0:0:608:0",
            ["H3246"] = 18579,
            ["sc"] = 3,
        },
        ["Pattern: Thick Murloc Armor"] = {
            ["mr"] = 150000,
            ["sc"] = 1,
            ["id"] = "5788:0:0:0:0",
            ["H3229"] = 150000,
            ["cc"] = 9,
        },
        ["Bloodspattered Wristbands of Strength"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Silver Ore"] = {
            ["H3220"] = 1145,
            ["mr"] = 1145,
        },
        ["Barbaric Battle Axe of Power"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Native Robe of the Owl"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Marauder's Bracers of Stamina"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Huntsman's Bands of Power"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Staunch Hammer of Shadow Wrath"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Archer's Gloves of the Falcon"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Battle Slayer of the Wolf"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Feral Shoulder Pads"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Abjurer's Pants of Fiery Wrath"] = {
            ["H3220"] = 49000,
            ["mr"] = 49000,
        },
        ["Curved Dagger of Shadow Wrath"] = {
            ["H3220"] = 3041,
            ["mr"] = 3041,
        },
        ["Scaled Leather Bracers of Spirit"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Warden's Gloves"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Gleaming Claymore of Stamina"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Winterfall E'ko"] = {
            ["H3254"] = 7800,
            ["mr"] = 7800,
        },
        ["Green Silken Shoulders"] = {
            ["mr"] = 3000,
            ["cc"] = 4,
            ["id"] = "7057:0:0:0:0",
            ["sc"] = 1,
            ["H3220"] = 3000,
        },
        ["Bristlebark Blouse"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Mail Combat Gauntlets"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Greenweave Vest of Arcane Wrath"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Giant Egg"] = {
            ["mr"] = 2600,
            ["H3254"] = 2600,
            ["H3220"] = 1560,
        },
        ["Jazeraint Bracers of Stamina"] = {
            ["H3220"] = 7337,
            ["mr"] = 7337,
        },
        ["Infiltrator Buckler of the Monkey"] = {
            ["H3220"] = 5800,
            ["mr"] = 5800,
        },
        ["Captain's Boots of Agility"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Blue Sapphire"] = {
            ["H3220"] = 99500,
            ["mr"] = 99500,
        },
        ["Big Bear Steak"] = {
            ["H3220"] = 181,
            ["mr"] = 181,
        },
        ["Rigid Shoulders of Stamina"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Ranger Cord of the Monkey"] = {
            ["H3220"] = 11999,
            ["mr"] = 11999,
        },
        ["Barbarian War Axe of Stamina"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Brown Snake"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Demon Band"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Wrangler's Leggings of the Eagle"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Arctic Pendant of the Monkey"] = {
            ["mr"] = 69405,
            ["cc"] = 4,
            ["id"] = "12044:0:0:604:0",
            ["H3246"] = 69405,
            ["sc"] = 0,
        },
        ["Razor Blade of the Tiger"] = {
            ["H3220"] = 15900,
            ["mr"] = 15900,
        },
        ["Huntsman's Cap of the Bear"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Diamond-Tip Bludgeon of Healing"] = {
            ["H3220"] = 65775,
            ["mr"] = 65775,
        },
        ["Knight's Crest of Stamina"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Archer's Shoulderpads of the Wolf"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Scarlet Chestpiece"] = {
            ["H3220"] = 79900,
            ["mr"] = 79900,
        },
        ["War Torn Tunic of the Whale"] = {
            ["H3220"] = 1350,
            ["mr"] = 1350,
        },
        ["Willow Boots of the Owl"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Phalanx Headguard of the Gorilla"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Green Leather Bag"] = {
            ["H3220"] = 899,
            ["mr"] = 899,
        },
        ["Sentry's Shield of Strength"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Battle Slayer of the Eagle"] = {
            ["H3220"] = 13400,
            ["mr"] = 13400,
        },
        ["Shadow Silk"] = {
            ["mr"] = 2539,
            ["H3254"] = 2539,
            ["H3220"] = 2000,
        },
        ["War Torn Shield of the Gorilla"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Greenweave Branch of the Eagle"] = {
            ["H3220"] = 1937,
            ["mr"] = 1937,
        },
        ["Black Dye"] = {
            ["H3220"] = 1799,
            ["mr"] = 1799,
        },
        ["Fish Oil"] = {
            ["H3220"] = 14,
            ["mr"] = 14,
        },
        ["Pattern: Felcloth Shoulders"] = {
            ["mr"] = 50000,
            ["cc"] = 9,
            ["id"] = "14508:0:0:0:0",
            ["H3246"] = 50000,
            ["sc"] = 2,
        },
        ["Senggin Root"] = {
            ["H3220"] = 15,
            ["mr"] = 15,
        },
        ["Pattern: Wizardweave Robe"] = {
            ["mr"] = 22500,
            ["cc"] = 9,
            ["id"] = "14500:0:0:0:0",
            ["H3229"] = 22500,
            ["sc"] = 2,
        },
        ["Ruined Leather Scraps"] = {
            ["H3220"] = 7,
            ["mr"] = 7,
        },
        ["Small Leather Ammo Pouch"] = {
            ["mr"] = 160,
            ["cc"] = 11,
            ["id"] = "7279:0:0:0:0",
            ["sc"] = 3,
            ["H3220"] = 160,
        },
        ["Ebony Boneclub"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Light Hide"] = {
            ["H3220"] = 58,
            ["mr"] = 58,
        },
        ["Mighty Armsplints of the Monkey"] = {
            ["mr"] = 150000,
            ["cc"] = 4,
            ["id"] = "10147:0:0:609:0",
            ["H3246"] = 150000,
            ["sc"] = 2,
        },
        ["Gypsy Tunic of the Owl"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Viking Sword of Strength"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Lesser Magic Wand"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Chief Brigadier Armor"] = {
            ["H3220"] = 87216,
            ["mr"] = 87216,
        },
        ["Crawler Claw"] = {
            ["H3220"] = 66,
            ["mr"] = 66,
        },
        ["Silver-lined Belt"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Dirty Blunderbuss"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Outrunner's Cuffs of the Monkey"] = {
            ["H3220"] = 1358,
            ["mr"] = 1358,
        },
        ["Rigid Tunic of Spirit"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Greater Scythe of the Whale"] = {
            ["H3220"] = 20035,
            ["mr"] = 20035,
        },
        ["Azure Silk Pants"] = {
            ["mr"] = 3800,
            ["cc"] = 4,
            ["id"] = "7046:0:0:0:0",
            ["sc"] = 1,
            ["H3245"] = 3800,
        },
        ["Scaled Leather Gloves of the Owl"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Glyphed Bracers"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Infiltrator Cap of the Whale"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Headstriker Sword of the Whale"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Jet Loop of Stamina"] = {
            ["H3220"] = 13980,
            ["mr"] = 13980,
        },
        ["Archer's Boots of the Monkey"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Embossed Plate Shield of Stamina"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Pagan Mitts of Healing"] = {
            ["H3220"] = 1485,
            ["mr"] = 1485,
        },
        ["Cobalt Crusher"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Watcher's Cape of the Whale"] = {
            ["H3220"] = 977,
            ["mr"] = 977,
        },
        ["Hulking Bands"] = {
            ["H3220"] = 895,
            ["mr"] = 895,
        },
        ["Humbert's Chestpiece"] = {
            ["H3220"] = 3607,
            ["mr"] = 3607,
        },
        ["Mithril Tube"] = {
            ["H3220"] = 5400,
            ["mr"] = 5400,
        },
        ["Green Iron Helm"] = {
            ["H3220"] = 13553,
            ["mr"] = 13553,
        },
        ["Veteran Gloves"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Stonecutter Claymore of Stamina"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Slimy Murloc Scale"] = {
            ["H3220"] = 399,
            ["mr"] = 399,
        },
        ["Soldier's Girdle of the Bear"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Bloodforged Shoulder Pads of the Bear"] = {
            ["H3220"] = 35000,
            ["mr"] = 35000,
        },
        ["Scaled Leather Boots of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Royal Sash of Intellect"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Gryphon Mail Gauntlets of the Owl"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Elixir of Greater Agility"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Plans: Mithril Spurs"] = {
            ["H3220"] = 13800,
            ["mr"] = 13800,
        },
        ["Battle Knife of Agility"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Ritual Gloves of the Eagle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Durable Rod of Fiery Wrath"] = {
            ["H3220"] = 9521,
            ["mr"] = 9521,
        },
        ["Windchaser Coronet"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Heavy Leather"] = {
            ["mr"] = 500,
            ["sc"] = 0,
            ["H3238"] = 191,
            ["id"] = "4234:0:0:0:0",
            ["H3254"] = 500,
            ["cc"] = 7,
        },
        ["Opulent Leggings of Healing"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Upper Map Fragment"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Bloodspattered Shoulder Pads"] = {
            ["H3220"] = 3038,
            ["mr"] = 3038,
        },
        ["Pagan Rod of Intellect"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Battleforge Boots of Stamina"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Staunch Hammer of Nature's Wrath"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Brigade Pauldrons of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Crawler Meat"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Shredder Operating Manual - Page 11"] = {
            ["mr"] = 298,
            ["cc"] = 15,
            ["id"] = "16655:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 298,
        },
        ["Banded Pauldrons of the Boar"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Vital Shoulders of the Owl"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Bandit Boots of the Monkey"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Pattern: Green Woolen Bag"] = {
            ["mr"] = 895,
            ["cc"] = 9,
            ["id"] = "4292:0:0:0:0",
            ["L3229"] = 895,
            ["H3229"] = 900,
            ["sc"] = 2,
        },
        ["Beaded Orb of the Falcon"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Scorpashi Slippers"] = {
            ["H3220"] = 19499,
            ["mr"] = 19499,
        },
        ["Schematic: Mechanical Squirrel"] = {
            ["H3220"] = 250,
            ["mr"] = 250,
        },
        ["Golden Skeleton Key"] = {
            ["mr"] = 4900,
            ["sc"] = 0,
            ["id"] = "15870:0:0:0:0",
            ["H3246"] = 4900,
            ["cc"] = 7,
        },
        ["Stylish Red Shirt"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Large Glimmering Shard"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Edged Bastard Sword of the Wolf"] = {
            ["H3220"] = 2199,
            ["mr"] = 2199,
        },
        ["Ranger Cloak of the Monkey"] = {
            ["mr"] = 19300,
            ["cc"] = 4,
            ["id"] = "7483:0:0:599:0",
            ["H3246"] = 19300,
            ["sc"] = 1,
        },
        ["Robust Tunic of Spirit"] = {
            ["H3220"] = 5904,
            ["mr"] = 5904,
        },
        ["Shimmering Cloak of Arcane Wrath"] = {
            ["H3220"] = 1888,
            ["mr"] = 1888,
        },
        ["Bard's Boots of the Eagle"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Superior Buckler of the Monkey"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Recipe: Grilled Squid"] = {
            ["H3220"] = 25400,
            ["mr"] = 25400,
        },
        ["Dreadblade of Healing"] = {
            ["H3220"] = 23500,
            ["mr"] = 23500,
        },
        ["Dervish Boots of the Falcon"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Elemental Earth"] = {
            ["mr"] = 4924,
            ["H3254"] = 4924,
            ["H3220"] = 1890,
        },
        ["Sentinel Gloves of the Monkey"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Circlet of the Order"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Scroll of Intellect III"] = {
            ["H3220"] = 334,
            ["mr"] = 334,
        },
        ["Archer's Boots of the Eagle"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Cerulean Talisman of the Owl"] = {
            ["H3220"] = 15595,
            ["mr"] = 15595,
        },
        ["Barbarian War Axe of the Boar"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Raw Brilliant Smallfish"] = {
            ["H3220"] = 18,
            ["mr"] = 18,
        },
        ["Wrangler's Buckler of the Wolf"] = {
            ["H3220"] = 5234,
            ["mr"] = 5234,
        },
        ["Earthen Leather Shoulders"] = {
            ["H3220"] = 5600,
            ["mr"] = 5600,
        },
        ["Regal Sash of Healing"] = {
            ["H3220"] = 10200,
            ["mr"] = 10200,
        },
        ["Spinel Ring of Frost Resistance"] = {
            ["H3220"] = 9099,
            ["mr"] = 9099,
        },
        ["High Councillor's Mantle of Shadow Wrath"] = {
            ["H3220"] = 300000,
            ["mr"] = 300000,
        },
        ["Staunch Hammer of Strength"] = {
            ["H3220"] = 876,
            ["mr"] = 876,
        },
        ["Elixir of Detect Undead"] = {
            ["H3220"] = 1460,
            ["mr"] = 1460,
        },
        ["Formula: Enchant Chest - Major Health"] = {
            ["H3220"] = 24000,
            ["mr"] = 24000,
        },
        ["Pathfinder Pants of Agility"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Sage's Circlet of the Wolf"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Hacking Cleaver of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Resplendent Cloak of Fiery Wrath"] = {
            ["H3220"] = 28314,
            ["mr"] = 28314,
        },
        ["Gleaming Claymore of the Whale"] = {
            ["H3220"] = 2199,
            ["mr"] = 2199,
        },
        ["Grizzly Pants of Agility"] = {
            ["H3220"] = 1090,
            ["mr"] = 1090,
        },
        ["Gloom Reaper of the Monkey"] = {
            ["mr"] = 11500,
            ["sc"] = 0,
            ["id"] = "863:0:0:593:0",
            ["H3246"] = 11500,
            ["cc"] = 2,
        },
        ["Mining Pick"] = {
            ["H3220"] = 1489,
            ["mr"] = 1489,
        },
        ["Glyphed Breastplate"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Arclight Spanner"] = {
            ["H3220"] = 599,
            ["mr"] = 599,
        },
        ["Infiltrator Armor of the Whale"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Silken Thread"] = {
            ["mr"] = 730,
            ["cc"] = 7,
            ["id"] = "4291:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 730,
        },
        ["Chunk of Boar Meat"] = {
            ["H3220"] = 148,
            ["mr"] = 148,
        },
        ["Raincaller Mitts of the Whale"] = {
            ["H3220"] = 1299,
            ["mr"] = 1299,
        },
        ["Ritual Tunic of the Owl"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Nightshade Girdle of the Monkey"] = {
            ["mr"] = 120000,
            ["cc"] = 4,
            ["id"] = "10221:0:0:619:0",
            ["H3246"] = 120000,
            ["sc"] = 2,
        },
        ["Recipe: Hot Wolf Ribs"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Inscribed Leather Bracers"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Glimmering Mail Breastplate"] = {
            ["H3220"] = 3218,
            ["mr"] = 3218,
        },
        ["Fortified Boots of the Boar"] = {
            ["H3220"] = 6200,
            ["mr"] = 6200,
        },
        ["Heavy Lamellar Chestpiece of Power"] = {
            ["H3220"] = 80000,
            ["mr"] = 80000,
        },
        ["Chieftain's Cloak of Stamina"] = {
            ["H3220"] = 24999,
            ["mr"] = 24999,
        },
        ["Ravager's Mantle"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Pattern: Tuxedo Jacket"] = {
            ["mr"] = 14799,
            ["sc"] = 2,
            ["id"] = "10326:0:0:0:0",
            ["H3229"] = 14799,
            ["cc"] = 9,
        },
        ["Elder's Mantle of Intellect"] = {
            ["H3220"] = 6700,
            ["mr"] = 6700,
        },
        ["Battleforge Shield of the Monkey"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Gazlowe's Charm"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Vibroblade"] = {
            ["H3220"] = 22287,
            ["mr"] = 22287,
        },
        ["Gothic Plate Gauntlets of the Bear"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Fortified Cloak of Stamina"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Archer's Shoulderpads of the Whale"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Infantry Tunic of the Whale"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Formula: Enchant Cloak - Lesser Shadow Resistance"] = {
            ["H3220"] = 1360,
            ["mr"] = 1360,
        },
        ["Scouting Boots of the Owl"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Bloodspattered Loincloth of the Whale"] = {
            ["H3220"] = 1048,
            ["mr"] = 1048,
        },
        ["Shredder Operating Manual - Page 10"] = {
            ["mr"] = 795,
            ["cc"] = 15,
            ["id"] = "16654:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 795,
        },
        ["Superior Cloak of the Falcon"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Glimmering Mail Greaves"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Barbed Club of Strength"] = {
            ["H3220"] = 1648,
            ["mr"] = 1648,
        },
        ["Cross-stitched Vest"] = {
            ["mr"] = 1795,
            ["cc"] = 4,
            ["id"] = "1786:0:0:0:0",
            ["sc"] = 1,
            ["H3232"] = 1795,
        },
        ["Emblazoned Leggings"] = {
            ["H3220"] = 5899,
            ["mr"] = 5899,
        },
        ["Staff of the Blessed Seer"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Sticky Spider Webbing"] = {
            ["mr"] = 87,
            ["cc"] = 15,
            ["id"] = "5602:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 87,
        },
        ["Infiltrator Armor of Intellect"] = {
            ["H3220"] = 10998,
            ["mr"] = 10998,
        },
        ["Tyrant's Helm"] = {
            ["H3220"] = 10800,
            ["mr"] = 10800,
        },
        ["Cavalier Two-hander of Agility"] = {
            ["H3220"] = 7678,
            ["mr"] = 7678,
        },
        ["Sage's Mantle of the Owl"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Gothic Plate Girdle of the Boar"] = {
            ["H3220"] = 12200,
            ["mr"] = 12200,
        },
        ["Skullflame Shield"] = {
            ["H3220"] = 1500000,
            ["mr"] = 1500000,
        },
        ["Raider's Belt of the Bear"] = {
            ["H3220"] = 884,
            ["mr"] = 884,
        },
        ["Cold Basilisk Eye"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pagan Britches of the Monkey"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Grunt's Belt of the Whale"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Pattern: Runic Leather Headband"] = {
            ["mr"] = 70000,
            ["cc"] = 9,
            ["id"] = "15756:0:0:0:0",
            ["H3229"] = 70000,
            ["sc"] = 1,
        },
        ["Truefaith Gloves"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Mercenary Blade of the Bear"] = {
            ["H3220"] = 9165,
            ["mr"] = 9165,
        },
        ["Minor Healing Potion"] = {
            ["H3220"] = 68,
            ["mr"] = 68,
        },
        ["Sage's Gloves of Healing"] = {
            ["H3220"] = 2550,
            ["mr"] = 2550,
        },
        ["Troll Sweat"] = {
            ["H3220"] = 655,
            ["mr"] = 655,
        },
        ["War Torn Shield of Stamina"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Infantry Shield of Agility"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Wrangler's Belt of the Falcon"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Grunt's Legguards of Strength"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Raider's Shield of the Gorilla"] = {
            ["H3220"] = 2140,
            ["mr"] = 2140,
        },
        ["Ivycloth Mantle of the Wolf"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Outrunner's Cloak of Strength"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Reticulated Bone Gauntlets"] = {
            ["H3220"] = 7400,
            ["mr"] = 7400,
        },
        ["Arcane Sash"] = {
            ["H3220"] = 35000,
            ["mr"] = 35000,
        },
        ["Pattern: Runecloth Shoulders"] = {
            ["mr"] = 39176,
            ["sc"] = 2,
            ["id"] = "14504:0:0:0:0",
            ["H3229"] = 39176,
            ["cc"] = 9,
        },
        ["Pagan Vest of Stamina"] = {
            ["H3220"] = 4906,
            ["mr"] = 4906,
        },
        ["Wrangler's Boots of the Gorilla"] = {
            ["H3220"] = 4092,
            ["mr"] = 4092,
        },
        ["Training Sword of the Wolf"] = {
            ["H3220"] = 645,
            ["mr"] = 645,
        },
        ["Watcher's Leggings of Spirit"] = {
            ["H3220"] = 5499,
            ["mr"] = 5499,
        },
        ["Sorcerer Bracelets of Arcane Wrath"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Knight's Girdle of the Gorilla"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Formula: Enchant Weapon - Lesser Beastslayer"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Grunt's Legguards of the Bear"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Tyrant's Gauntlets"] = {
            ["H3220"] = 9653,
            ["mr"] = 9653,
        },
        ["War Knife of Strength"] = {
            ["H3220"] = 1695,
            ["mr"] = 1695,
        },
        ["26 Pound Catfish"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Giant Club of the Monkey"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Black Mageweave Robe"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Knightly Longsword of the Monkey"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Captain's Leggings of the Wolf"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Plans: Dark Iron Pulverizer"] = {
            ["H3220"] = 8099,
            ["mr"] = 8099,
        },
        ["Knight's Girdle of the Bear"] = {
            ["mr"] = 10000,
            ["H3254"] = 10000,
            ["H3220"] = 3499,
        },
        ["War Torn Pants of the Monkey"] = {
            ["mr"] = 2424,
            ["sc"] = 3,
            ["id"] = "15485:0:0:587:0",
            ["H3246"] = 2424,
            ["cc"] = 4,
        },
        ["Blackskull Shield"] = {
            ["H3220"] = 190000,
            ["mr"] = 190000,
        },
        ["Archaic Defender"] = {
            ["H3220"] = 41337,
            ["mr"] = 41337,
        },
        ["Spiked Chain Slippers of the Whale"] = {
            ["H3220"] = 4600,
            ["mr"] = 4600,
        },
        ["Duskwoven Gloves of Fiery Wrath"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Hard Spider Leg Tip"] = {
            ["mr"] = 630,
            ["cc"] = 15,
            ["id"] = "1074:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 630,
        },
        ["Bandit Gloves of the Wolf"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Oil of Immolation"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Shadoweave Boots"] = {
            ["H3220"] = 59499,
            ["mr"] = 59499,
        },
        ["Infiltrator Gloves of the Bear"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Monstrous War Axe of the Monkey"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Pattern: Earthen Silk Belt"] = {
            ["mr"] = 1885,
            ["cc"] = 9,
            ["id"] = "7086:0:0:0:0",
            ["L3229"] = 1885,
            ["H3229"] = 1900,
            ["sc"] = 2,
        },
        ["Superior Tunic of the Monkey"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Field Plate Boots of Stamina"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Training Sword of the Tiger"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Coarse Weightstone"] = {
            ["mr"] = 59,
            ["sc"] = 0,
            ["id"] = "3240:0:0:0:0",
            ["cc"] = 7,
            ["H3225"] = 59,
        },
        ["Pattern: Azure Silk Cloak"] = {
            ["mr"] = 13200,
            ["cc"] = 9,
            ["id"] = "7089:0:0:0:0",
            ["L3229"] = 13200,
            ["H3229"] = 14200,
            ["sc"] = 2,
        },
        ["Slitherskin Mackerel"] = {
            ["H3220"] = 49,
            ["mr"] = 49,
        },
        ["Simple Kilt"] = {
            ["H3220"] = 399,
            ["mr"] = 399,
        },
        ["Lupine Handwraps of the Wolf"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Buccaneer's Cord of Fiery Wrath"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Grizzly Jerkin of the Wolf"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Dervish Buckler of the Gorilla"] = {
            ["H3220"] = 5999,
            ["mr"] = 5999,
        },
        ["Shimmering Amice"] = {
            ["H3220"] = 970,
            ["mr"] = 970,
        },
        ["Forest Cloak"] = {
            ["H3220"] = 1490,
            ["mr"] = 1490,
        },
        ["Robes of Insight"] = {
            ["H3220"] = 2809900,
            ["mr"] = 2809900,
        },
        ["Gleaming Claymore of Agility"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Strider Meat"] = {
            ["H3220"] = 25,
            ["mr"] = 25,
        },
        ["Bristlebark Buckler"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Barbaric Battle Axe of the Tiger"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Banded Leggings of the Tiger"] = {
            ["H3220"] = 3299,
            ["mr"] = 3299,
        },
        ["Battleforge Cloak of the Bear"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Watcher's Robes of the Whale"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Blazing Emblem"] = {
            ["H3220"] = 120000,
            ["mr"] = 120000,
        },
        ["Dreamweave Gloves"] = {
            ["H3220"] = 66000,
            ["mr"] = 66000,
        },
        ["Nightsky Robe"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Nightcrawlers"] = {
            ["H3220"] = 152,
            ["mr"] = 152,
        },
        ["Ring of Defense"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Hacking Cleaver of the Boar"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Formula: Enchant Weapon - Fiery Weapon"] = {
            ["H3220"] = 129900,
            ["mr"] = 129900,
        },
        ["Unstable Trigger"] = {
            ["H3220"] = 18899,
            ["mr"] = 18899,
        },
        ["Briarthorn"] = {
            ["H3220"] = 176,
            ["mr"] = 176,
        },
        ["Formula: Enchant 2H Weapon - Lesser Intellect"] = {
            ["H3220"] = 13503,
            ["mr"] = 13503,
        },
        ["22 Pound Catfish"] = {
            ["H3220"] = 49800,
            ["mr"] = 49800,
        },
        ["Heart of the Wild"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Cutthroat's Hat of the Owl"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Spiked Club of the Bear"] = {
            ["H3220"] = 916,
            ["mr"] = 916,
        },
        ["Gemmed Copper Gauntlets of the Eagle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Tiny Emerald Whelpling"] = {
            ["H3220"] = 2502500,
            ["mr"] = 2502500,
        },
        ["Ghostwalker Buckler of the Owl"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Cross Dagger of Nature's Wrath"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Grunt Axe of Agility"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Scarlet Belt"] = {
            ["H3220"] = 2633,
            ["mr"] = 2633,
        },
        ["Large Green Sack"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Defender Boots of Stamina"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Pattern: Turtle Scale Gloves"] = {
            ["mr"] = 39999,
            ["sc"] = 1,
            ["id"] = "8385:0:0:0:0",
            ["H3229"] = 39999,
            ["cc"] = 9,
        },
        ["Anti-Venom"] = {
            ["H3220"] = 120,
            ["mr"] = 120,
        },
        ["Willow Belt of the Owl"] = {
            ["H3220"] = 486,
            ["mr"] = 486,
        },
        ["Glimmering Flamberge of the Boar"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Grime-Encrusted Object"] = {
            ["H3220"] = 62,
            ["mr"] = 62,
        },
        ["Green Lens of Concentration"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Rigid Bracelets of Power"] = {
            ["H3220"] = 750,
            ["mr"] = 750,
        },
        ["Assault Band"] = {
            ["H3220"] = 59000,
            ["mr"] = 59000,
        },
        ["Phalanx Gauntlets of the Eagle"] = {
            ["H3220"] = 5334,
            ["mr"] = 5334,
        },
        ["Beatstick"] = {
            ["H3220"] = 802,
            ["mr"] = 802,
        },
        ["Cutthroat's Belt of the Boar"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Inlaid Mithril Cylinder"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Curiously Tasty Omelet"] = {
            ["mr"] = 399,
            ["H3254"] = 399,
            ["H3220"] = 400,
        },
        ["Infantry Shield of the Monkey"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Banded Boots of the Gorilla"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Cadet Leggings of the Boar"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Superior Boots of the Monkey"] = {
            ["H3220"] = 7799,
            ["mr"] = 7799,
        },
        ["Simple Britches of the Eagle"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Solid Stone"] = {
            ["H3220"] = 332,
            ["mr"] = 332,
        },
        ["Splitting Hatchet of Strength"] = {
            ["H3220"] = 6899,
            ["mr"] = 6899,
        },
        ["Ring of Precision"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Barbaric Battle Axe of Agility"] = {
            ["H3220"] = 1860,
            ["mr"] = 1860,
        },
        ["Tracker's Cloak of the Monkey"] = {
            ["mr"] = 22643,
            ["sc"] = 1,
            ["id"] = "9919:0:0:601:0",
            ["H3246"] = 22643,
            ["cc"] = 4,
        },
        ["Silver-thread Sash"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Defender Boots of Defense"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Shadowcraft Gloves"] = {
            ["H3220"] = 116902,
            ["mr"] = 116902,
        },
        ["Outrunner's Gloves of the Whale"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Ebon Scimitar of the Bear"] = {
            ["H3220"] = 25387,
            ["mr"] = 25387,
        },
        ["Pattern: Tough Scorpid Gloves"] = {
            ["mr"] = 4999,
            ["sc"] = 1,
            ["id"] = "8398:0:0:0:0",
            ["H3229"] = 4999,
            ["cc"] = 9,
        },
        ["Rigid Cape of the Eagle"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Sorcerer Slippers of the Owl"] = {
            ["H3220"] = 6100,
            ["mr"] = 6100,
        },
        ["Firestarter"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Pattern: Dark Leather Tunic"] = {
            ["mr"] = 399,
            ["sc"] = 1,
            ["id"] = "2409:0:0:0:0",
            ["H3229"] = 399,
            ["cc"] = 9,
        },
        ["Mistscape Sash"] = {
            ["H3220"] = 17900,
            ["mr"] = 17900,
        },
        ["Bright Mantle"] = {
            ["H3231"] = 3899,
            ["mr"] = 3899,
            ["sc"] = 1,
            ["id"] = "4661:0:0:0:0",
            ["cc"] = 4,
        },
        ["Elder's Pants of Fiery Wrath"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Dwarven Magestaff of Spirit"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Aboriginal Loincloth of the Falcon"] = {
            ["H3220"] = 1171,
            ["mr"] = 1171,
        },
        ["Pattern: Deviate Scale Cloak"] = {
            ["mr"] = 2299,
            ["cc"] = 9,
            ["id"] = "6474:0:0:0:0",
            ["L3229"] = 2299,
            ["H3229"] = 2300,
            ["sc"] = 1,
        },
        ["Bolt of Runecloth"] = {
            ["mr"] = 3598,
            ["cc"] = 7,
            ["id"] = "14048:0:0:0:0",
            ["H3254"] = 3598,
            ["sc"] = 0,
        },
        ["Mystery Meat"] = {
            ["mr"] = 1218,
            ["H3254"] = 1218,
            ["H3220"] = 212,
        },
        ["Shadoweave Gloves"] = {
            ["H3220"] = 56500,
            ["mr"] = 56500,
        },
        ["Grunt Axe of Stamina"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Scorpid Stinger"] = {
            ["H3220"] = 81,
            ["mr"] = 81,
        },
        ["Jagged Star of Healing"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Rigid Gloves of the Owl"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Formula: Enchant Weapon - Minor Beastslayer"] = {
            ["H3220"] = 949,
            ["mr"] = 949,
        },
        ["Champion's Leggings of Agility"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Aboriginal Robe of Arcane Wrath"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Rectangular Shield"] = {
            ["H3220"] = 595,
            ["mr"] = 595,
        },
        ["Lion Meat"] = {
            ["H3220"] = 160,
            ["mr"] = 160,
        },
        ["Plains Ring"] = {
            ["H3220"] = 28800,
            ["mr"] = 28800,
        },
        ["Grunt's Bracers of the Monkey"] = {
            ["mr"] = 3095,
            ["cc"] = 4,
            ["id"] = "15507:0:0:587:0",
            ["H3246"] = 3095,
            ["sc"] = 3,
        },
        ["Gooey Spider Leg"] = {
            ["mr"] = 12,
            ["cc"] = 7,
            ["id"] = "2251:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 12,
        },
        ["Twisted Chanter's Staff"] = {
            ["H3220"] = 37600,
            ["mr"] = 37600,
        },
        ["Ancient Pauldrons of the Falcon"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Formula: Enchant Shield - Lesser Protection"] = {
            ["mr"] = 500,
            ["cc"] = 9,
            ["id"] = "11081:0:0:0:0",
            ["sc"] = 8,
            ["H3227"] = 500,
        },
        ["Stone Hammer of the Boar"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Sorcerer Gloves of the Eagle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Disciple's Stein of Spirit"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Scaled Leather Tunic of the Owl"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Regal Star of the Owl"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Gemmed Copper Gauntlets of the Bear"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Scroll of Protection II"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Bristlebark Belt"] = {
            ["H3220"] = 990,
            ["mr"] = 990,
        },
        ["Dwarven Magestaff of Arcane Wrath"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Banded Boots of the Whale"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Grunt's Bracers of the Boar"] = {
            ["H3220"] = 1092,
            ["mr"] = 1092,
        },
        ["Spellbinder Pants"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Gypsy Trousers of Healing"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Rigid Buckler of Stamina"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Big Bronze Bomb"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Savory Deviate Delight"] = {
            ["mr"] = 1998,
            ["H3254"] = 1998,
            ["H3220"] = 995,
        },
        ["Phalanx Girdle of Strength"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Aboriginal Vest of Spirit"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Bloodspattered Gloves of the Monkey"] = {
            ["H3220"] = 1161,
            ["mr"] = 1161,
        },
        ["Knight's Gauntlets of Strength"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Pattern: Red Mageweave Pants"] = {
            ["mr"] = 14305,
            ["sc"] = 2,
            ["id"] = "10302:0:0:0:0",
            ["H3229"] = 14305,
            ["cc"] = 9,
        },
        ["Pattern: Big Voodoo Pants"] = {
            ["mr"] = 3300,
            ["cc"] = 9,
            ["id"] = "8389:0:0:0:0",
            ["L3229"] = 3300,
            ["H3229"] = 3400,
            ["sc"] = 1,
        },
        ["Righteous Gloves of Spirit"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Warmonger's Circlet of the Bear"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Archer's Jerkin of the Monkey"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Chromite Legplates"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Conjurer's Robe of Spirit"] = {
            ["H3220"] = 11460,
            ["mr"] = 11460,
        },
        ["Purple Lotus"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Battlefield Destroyer of Strength"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Deviate Fish"] = {
            ["mr"] = 3000,
            ["H3254"] = 3000,
            ["H3220"] = 141,
        },
        ["Coal"] = {
            ["H3220"] = 299,
            ["mr"] = 299,
        },
        ["Warden's Cloak"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Imperial Red Circlet"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Sage's Mantle of the Eagle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Thin Kodo Leather"] = {
            ["H3220"] = 80,
            ["mr"] = 80,
        },
        ["Essence of Fire"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Double Mail Coif"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Twilight Gloves of the Eagle"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Raw Rockscale Cod"] = {
            ["H3220"] = 21,
            ["mr"] = 21,
        },
        ["Rough Bronze Shoulders"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Superior Shoulders of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Cross Dagger of the Tiger"] = {
            ["H3220"] = 4864,
            ["mr"] = 4864,
        },
        ["Willow Branch of the Whale"] = {
            ["H3220"] = 1770,
            ["mr"] = 1770,
        },
        ["Large Fang"] = {
            ["mr"] = 2999,
            ["H3254"] = 2999,
            ["H3220"] = 191,
        },
        ["Recipe: Elixir of Lesser Agility"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Formula: Enchant Gloves - Fishing"] = {
            ["mr"] = 48661,
            ["H3254"] = 48661,
            ["H3220"] = 1608,
        },
        ["Twilight Cowl of the Owl"] = {
            ["H3220"] = 11382,
            ["mr"] = 11382,
        },
        ["Soldier's Armor of the Tiger"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Marauder's Bracers of the Eagle"] = {
            ["H3220"] = 4232,
            ["mr"] = 4232,
        },
        ["Cured Light Hide"] = {
            ["H3220"] = 110,
            ["mr"] = 110,
        },
        ["Durable Shoulders of the Owl"] = {
            ["H3220"] = 2290,
            ["mr"] = 2290,
        },
        ["Wolf Rider's Belt of the Monkey"] = {
            ["H3220"] = 9991,
            ["mr"] = 9991,
        },
        ["Bandit Bracers of the Eagle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Brigade Girdle of the Whale"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Stone Hammer of Spirit"] = {
            ["H3220"] = 23873,
            ["mr"] = 23873,
        },
        ["War Paint Gloves"] = {
            ["H3220"] = 595,
            ["mr"] = 595,
        },
        ["Green Dye"] = {
            ["H3220"] = 1676,
            ["mr"] = 1676,
        },
        ["Green Power Crystal"] = {
            ["H3220"] = 471,
            ["mr"] = 471,
        },
        ["Inscribed Leather Pants"] = {
            ["H3220"] = 854,
            ["mr"] = 854,
        },
        ["Heavy Linen Bandage"] = {
            ["H3220"] = 19,
            ["mr"] = 19,
        },
        ["Regal Cuffs of Intellect"] = {
            ["H3220"] = 14058,
            ["mr"] = 14058,
        },
        ["Decapitating Sword of the Tiger"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Renegade Circlet of the Gorilla"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Twilight Mantle of Arcane Wrath"] = {
            ["H3220"] = 6600,
            ["mr"] = 6600,
        },
        ["Essence of Water"] = {
            ["H3220"] = 98490,
            ["mr"] = 98490,
        },
        ["Ridge Cleaver of the Wolf"] = {
            ["H3220"] = 5358,
            ["mr"] = 5358,
        },
        ["Thick Scale Crown of Defense"] = {
            ["H3220"] = 15800,
            ["mr"] = 15800,
        },
        ["Aurora Boots"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Grunt's Pauldrons of the Gorilla"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Battleforge Wristguards of the Eagle"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Small Furry Paw"] = {
            ["mr"] = 97,
            ["cc"] = 15,
            ["L3220"] = 97,
            ["id"] = "5134:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 100,
        },
        ["Phalanx Boots of Stamina"] = {
            ["H3220"] = 7710,
            ["mr"] = 7710,
        },
        ["Twilight Mantle of Intellect"] = {
            ["H3220"] = 8247,
            ["mr"] = 8247,
        },
        ["Vendetta"] = {
            ["H3220"] = 59000,
            ["mr"] = 59000,
        },
        ["Ebonclaw Reaver of the Bear"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Rough Grinding Stone"] = {
            ["H3220"] = 74,
            ["mr"] = 74,
        },
        ["Cross Dagger of the Bear"] = {
            ["H3220"] = 7313,
            ["mr"] = 7313,
        },
        ["Righteous Cloak of Agility"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Serpent's Shoulders"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Willow Robe of Intellect"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["17 Pound Catfish"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Sage's Sash of Healing"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Pattern: Murloc Scale Bracers"] = {
            ["mr"] = 1400,
            ["cc"] = 9,
            ["id"] = "5789:0:0:0:0",
            ["sc"] = 1,
            ["H3227"] = 1400,
        },
        ["Bandit Jerkin of Intellect"] = {
            ["H3220"] = 2226,
            ["mr"] = 2226,
        },
        ["Mail Combat Spaulders"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["10 Pound Mud Snapper"] = {
            ["H3220"] = 714,
            ["mr"] = 714,
        },
        ["Headhunter's Woolies of the Eagle"] = {
            ["H3220"] = 10285,
            ["mr"] = 10285,
        },
        ["Grunt Axe of Strength"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Jazeraint Leggings of the Monkey"] = {
            ["mr"] = 19548,
            ["cc"] = 4,
            ["id"] = "9903:0:0:612:0",
            ["H3246"] = 19548,
            ["sc"] = 3,
        },
        ["Thick Murloc Scale"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Archer's Buckler of Strength"] = {
            ["H3220"] = 15071,
            ["mr"] = 15071,
        },
        ["Parrot Cage (Hyacinth Macaw)"] = {
            ["H3254"] = 3490000,
            ["mr"] = 3490000,
        },
        ["Embossed Plate Pauldrons of the Gorilla"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Knight's Girdle of the Monkey"] = {
            ["mr"] = 5832,
            ["cc"] = 4,
            ["id"] = "7462:0:0:603:0",
            ["H3246"] = 5832,
            ["sc"] = 3,
        },
        ["Trickster's Bindings of the Owl"] = {
            ["H3220"] = 5899,
            ["mr"] = 5899,
        },
        ["Cat Carrier (Black Tabby)"] = {
            ["H3220"] = 19300,
            ["mr"] = 19300,
        },
        ["Scaled Leather Leggings of the Falcon"] = {
            ["H3220"] = 9978,
            ["mr"] = 9978,
        },
        ["Pattern: Fine Leather Boots"] = {
            ["mr"] = 290,
            ["cc"] = 9,
            ["id"] = "2406:0:0:0:0",
            ["L3229"] = 290,
            ["H3229"] = 298,
            ["sc"] = 1,
        },
        ["Stonecloth Robe"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Priest's Mace of Strength"] = {
            ["H3220"] = 599,
            ["mr"] = 599,
        },
        ["Jazeraint Chestguard of the Monkey"] = {
            ["mr"] = 15432,
            ["cc"] = 4,
            ["id"] = "9897:0:0:613:0",
            ["H3246"] = 15432,
            ["sc"] = 3,
        },
        ["Felcloth Bag"] = {
            ["mr"] = 538000,
            ["cc"] = 1,
            ["id"] = "21341:0:0:0:0",
            ["H3246"] = 538000,
            ["sc"] = 1,
        },
        ["Hacking Cleaver of the Wolf"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Bearded Boneaxe"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Watcher's Cap of the Whale"] = {
            ["H3220"] = 11087,
            ["mr"] = 11087,
        },
        ["Glimmering Cloak"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Watcher's Star of the Eagle"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Swashbuckler's Cape of the Monkey"] = {
            ["mr"] = 15500,
            ["cc"] = 4,
            ["id"] = "10185:0:0:603:0",
            ["H3246"] = 15500,
            ["sc"] = 1,
        },
        ["Pattern: Blue Dragonscale Shoulders"] = {
            ["mr"] = 370000,
            ["cc"] = 9,
            ["id"] = "15763:0:0:0:0",
            ["H3229"] = 370000,
            ["sc"] = 1,
        },
        ["Troll Tribal Necklace"] = {
            ["mr"] = 611,
            ["H3254"] = 611,
            ["H3220"] = 214,
        },
        ["Potent Armor of the Monkey"] = {
            ["mr"] = 79900,
            ["cc"] = 4,
            ["id"] = "15170:0:0:626:0",
            ["H3246"] = 79900,
            ["sc"] = 2,
        },
        ["Pattern: Wicked Leather Headband"] = {
            ["mr"] = 197000,
            ["cc"] = 9,
            ["id"] = "15744:0:0:0:0",
            ["sc"] = 1,
            ["H3229"] = 198000,
            ["L3229"] = 197000,
        },
        ["Elemental Water"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Durable Belt of the Monkey"] = {
            ["mr"] = 10000,
            ["cc"] = 4,
            ["id"] = "10404:0:0:596:0",
            ["H3246"] = 10000,
            ["sc"] = 1,
        },
        ["Dervish Buckler of the Monkey"] = {
            ["mr"] = 4000,
            ["L3246"] = 4000,
            ["id"] = "6598:0:0:592:0",
            ["sc"] = 6,
            ["H3246"] = 5000,
            ["cc"] = 4,
        },
        ["Rigid Cape of the Monkey"] = {
            ["mr"] = 3200,
            ["cc"] = 4,
            ["id"] = "15114:0:0:587:0",
            ["H3246"] = 3200,
            ["sc"] = 1,
        },
        ["Rockscale Cod"] = {
            ["H3220"] = 15,
            ["mr"] = 15,
        },
        ["War Torn Pants of Power"] = {
            ["H3220"] = 550,
            ["mr"] = 550,
        },
        ["Widow Blade of the Monkey"] = {
            ["mr"] = 75000,
            ["cc"] = 2,
            ["id"] = "15217:0:0:599:0",
            ["H3246"] = 75000,
            ["sc"] = 7,
        },
        ["Pattern: Frostweave Robe"] = {
            ["mr"] = 23800,
            ["sc"] = 2,
            ["id"] = "14467:0:0:0:0",
            ["H3229"] = 23800,
            ["cc"] = 9,
        },
        ["Soldier's Armor of the Monkey"] = {
            ["mr"] = 1999,
            ["cc"] = 4,
            ["id"] = "6545:0:0:590:0",
            ["H3246"] = 1999,
            ["sc"] = 3,
        },
        ["Captain's Cloak of the Monkey"] = {
            ["mr"] = 9045,
            ["cc"] = 4,
            ["id"] = "7492:0:0:596:0",
            ["H3246"] = 9045,
            ["sc"] = 1,
        },
        ["Tracker's Tunic of the Monkey"] = {
            ["H3220"] = 43999,
            ["mr"] = 43999,
        },
        ["Warmonger's Gauntlets of the Monkey"] = {
            ["mr"] = 30000,
            ["cc"] = 4,
            ["id"] = "9960:0:0:609:0",
            ["H3246"] = 30000,
            ["sc"] = 3,
        },
        ["Spidersilk Boots"] = {
            ["H3220"] = 14900,
            ["mr"] = 14900,
        },
        ["Infiltrator Shoulders of the Monkey"] = {
            ["H3220"] = 13228,
            ["mr"] = 13228,
        },
        ["Renegade Belt of the Monkey"] = {
            ["mr"] = 26700,
            ["cc"] = 4,
            ["id"] = "9869:0:0:600:0",
            ["H3246"] = 26700,
            ["sc"] = 3,
        },
        ["Ember Wand of the Wolf"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["Dimensional Blade of the Monkey"] = {
            ["mr"] = 99928,
            ["cc"] = 2,
            ["id"] = "15219:0:0:602:0",
            ["H3246"] = 99928,
            ["sc"] = 7,
        },
        ["Buccaneer's Orb of Fiery Wrath"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Jadefire Belt of the Monkey"] = {
            ["mr"] = 50000,
            ["cc"] = 4,
            ["id"] = "15388:0:0:614:0",
            ["H3246"] = 50000,
            ["sc"] = 2,
        },
        ["Large Red Sack"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Elder's Robe of the Owl"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Hillman's Leather Gloves"] = {
            ["H3220"] = 1299,
            ["mr"] = 1299,
        },
        ["Heavy Woolen Gloves"] = {
            ["H3220"] = 298,
            ["mr"] = 298,
        },
        ["Frostweave Tunic"] = {
            ["H3220"] = 98989,
            ["mr"] = 98989,
        },
        ["Durable Hat of the Whale"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Hawkeye's Helm"] = {
            ["mr"] = 10000,
            ["cc"] = 4,
            ["id"] = "14591:0:0:0:0",
            ["H3246"] = 10000,
            ["sc"] = 2,
        },
        ["Cutthroat's Pants of the Eagle"] = {
            ["H3220"] = 8742,
            ["mr"] = 8742,
        },
        ["Formidable Cape of the Owl"] = {
            ["H3220"] = 12200,
            ["mr"] = 12200,
        },
        ["Pattern: Spider Silk Slippers"] = {
            ["mr"] = 2438,
            ["sc"] = 2,
            ["id"] = "4350:0:0:0:0",
            ["H3229"] = 2438,
            ["cc"] = 9,
        },
        ["Impenetrable Cloak of the Monkey"] = {
            ["mr"] = 60000,
            ["cc"] = 4,
            ["id"] = "15661:0:0:606:0",
            ["H3246"] = 60000,
            ["sc"] = 1,
        },
        ["Sentinel Breastplate of Intellect"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Captain's Circlet of the Owl"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Staff of Jordan"] = {
            ["H3220"] = 1749900,
            ["mr"] = 1749900,
        },
        ["Recipe: Philosopher's Stone"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Praetorian Coif of the Monkey"] = {
            ["mr"] = 62029,
            ["cc"] = 4,
            ["id"] = "15185:0:0:628:0",
            ["H3246"] = 62029,
            ["sc"] = 2,
        },
        ["Grim Guzzler Key"] = {
            ["mr"] = 600000,
            ["cc"] = 13,
            ["id"] = "11602:0:0:0:0",
            ["H3246"] = 600000,
            ["sc"] = 0,
        },
        ["Small Brown Pouch"] = {
            ["mr"] = 249,
            ["cc"] = 1,
            ["L3220"] = 248,
            ["id"] = "4496:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 249,
        },
        ["Banded Helm of the Monkey"] = {
            ["mr"] = 28680,
            ["cc"] = 4,
            ["id"] = "10408:0:0:604:0",
            ["H3246"] = 28680,
            ["sc"] = 3,
        },
        ["Edged Bastard Sword of the Monkey"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Pattern: Fine Leather Gloves"] = {
            ["mr"] = 187,
            ["cc"] = 9,
            ["id"] = "2408:0:0:0:0",
            ["L3229"] = 187,
            ["H3229"] = 188,
            ["sc"] = 1,
        },
        ["Knight's Crest of the Monkey"] = {
            ["mr"] = 10000,
            ["cc"] = 4,
            ["id"] = "7465:0:0:597:0",
            ["H3246"] = 10000,
            ["sc"] = 6,
        },
        ["Sentinel Breastplate of the Monkey"] = {
            ["mr"] = 16274,
            ["cc"] = 4,
            ["id"] = "7439:0:0:614:0",
            ["H3246"] = 16274,
            ["sc"] = 2,
        },
        ["Notched Shortsword of Stamina"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Outrunner's Legguards of the Eagle"] = {
            ["H3220"] = 2591,
            ["mr"] = 2591,
        },
        ["Wanderer's Hat of the Monkey"] = {
            ["mr"] = 50000,
            ["cc"] = 4,
            ["id"] = "10111:0:0:627:0",
            ["H3246"] = 50000,
            ["sc"] = 2,
        },
        ["Schematic: Deadly Scope"] = {
            ["H3220"] = 24999,
            ["mr"] = 24999,
        },
        ["Dripping Spider Mandible"] = {
            ["mr"] = 1090,
            ["cc"] = 15,
            ["id"] = "4585:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 1090,
        },
        ["Durable Rod of the Eagle"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Thistlefur Belt of the Monkey"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Raider's Chestpiece of the Tiger"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Fortified Spaulders of the Bear"] = {
            ["H3220"] = 6480,
            ["mr"] = 6480,
        },
        ["Bandit Boots of the Owl"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Raincaller Mitts of the Owl"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Twilight Gloves of the Owl"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Acrobatic Staff of the Bear"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Plans: Iridescent Hammer"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Hawkeye's Cloak"] = {
            ["mr"] = 9800,
            ["cc"] = 4,
            ["id"] = "14593:0:0:0:0",
            ["H3246"] = 9800,
            ["sc"] = 1,
        },
        ["Healing Potion"] = {
            ["mr"] = 610,
            ["cc"] = 0,
            ["H3238"] = 610,
            ["id"] = "929:0:0:0:0",
            ["sc"] = 0,
        },
        ["Robust Bracers of the Monkey"] = {
            ["mr"] = 8003,
            ["cc"] = 4,
            ["id"] = "15122:0:0:590:0",
            ["H3246"] = 8003,
            ["sc"] = 2,
        },
        ["Deeprock Salt"] = {
            ["H3220"] = 250,
            ["mr"] = 250,
        },
        ["Aboriginal Footwraps of Arcane Wrath"] = {
            ["H3220"] = 835,
            ["mr"] = 835,
        },
        ["Pattern: Black Dragonscale Leggings"] = {
            ["mr"] = 250000,
            ["cc"] = 9,
            ["id"] = "15781:0:0:0:0",
            ["H3229"] = 250000,
            ["sc"] = 1,
        },
        ["Seer's Fine Stein"] = {
            ["H3220"] = 6533,
            ["mr"] = 6533,
        },
        ["Renegade Belt of the Whale"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Black Vitriol"] = {
            ["H3220"] = 1570,
            ["mr"] = 1570,
        },
        ["Iron Shield Spike"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Tangy Clam Meat"] = {
            ["H3220"] = 183,
            ["mr"] = 183,
        },
        ["Long Tail Feather"] = {
            ["H3220"] = 302,
            ["mr"] = 302,
        },
        ["Pattern: Runic Leather Belt"] = {
            ["mr"] = 48399,
            ["cc"] = 9,
            ["id"] = "15745:0:0:0:0",
            ["L3229"] = 48399,
            ["H3229"] = 48400,
            ["sc"] = 1,
        },
        ["Pattern: Barbaric Gloves"] = {
            ["mr"] = 1329,
            ["cc"] = 9,
            ["id"] = "4297:0:0:0:0",
            ["L3229"] = 1329,
            ["H3229"] = 1330,
            ["sc"] = 1,
        },
        ["Knight's Legguards of the Bear"] = {
            ["H3220"] = 8700,
            ["mr"] = 8700,
        },
        ["Greenweave Vest of the Owl"] = {
            ["H3220"] = 4025,
            ["mr"] = 4025,
        },
        ["Amber Hoop of Nature Resistance"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["The Silencer"] = {
            ["H3220"] = 80000,
            ["mr"] = 80000,
        },
        ["Banded Pauldrons of the Gorilla"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Greater Maul of the Whale"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Scouting Bracers of Spirit"] = {
            ["H3220"] = 1938,
            ["mr"] = 1938,
        },
        ["Fel Steed Saddlebags"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Birchwood Maul of the Eagle"] = {
            ["H3220"] = 1380,
            ["mr"] = 1380,
        },
        ["Pattern: Barbaric Belt"] = {
            ["mr"] = 14430,
            ["cc"] = 9,
            ["id"] = "4301:0:0:0:0",
            ["sc"] = 1,
            ["H3227"] = 14430,
        },
        ["Zircon Band of Frost Resistance"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Pattern: Gray Woolen Robe"] = {
            ["mr"] = 5169,
            ["cc"] = 9,
            ["id"] = "2601:0:0:0:0",
            ["L3229"] = 2800,
            ["H3229"] = 5169,
            ["sc"] = 2,
        },
        ["Blue Leather Bag"] = {
            ["H3220"] = 966,
            ["mr"] = 966,
        },
        ["Warmonger's Leggings of the Monkey"] = {
            ["mr"] = 158280,
            ["cc"] = 4,
            ["id"] = "9964:0:0:623:0",
            ["H3246"] = 158280,
            ["sc"] = 3,
        },
        ["Spiked Chain Shield of Strength"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Standard Scope"] = {
            ["H3220"] = 830,
            ["mr"] = 830,
        },
        ["Revenant Boots of the Bear"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Speedsteel Rapier"] = {
            ["H3220"] = 69999,
            ["mr"] = 69999,
        },
        ["Darkmist Handguards of Spirit"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Conjurer's Cinch of Intellect"] = {
            ["H3220"] = 3898,
            ["mr"] = 3898,
        },
        ["Forest Leather Belt"] = {
            ["H3220"] = 2098,
            ["mr"] = 2098,
        },
        ["Light Feather"] = {
            ["H3231"] = 48,
            ["mr"] = 48,
            ["cc"] = 15,
            ["id"] = "17056:0:0:0:0",
            ["sc"] = 0,
        },
        ["Soldier's Wristguards of Power"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Green Hills of Stranglethorn - Page 16"] = {
            ["mr"] = 775,
            ["H3254"] = 775,
            ["H3220"] = 527,
        },
        ["Iridescent Scale Leggings"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Stone Hammer of Strength"] = {
            ["H3220"] = 24200,
            ["mr"] = 24200,
        },
        ["Fortified Boots of the Bear"] = {
            ["H3220"] = 6399,
            ["mr"] = 6399,
        },
        ["Silvered Bronze Shoulders"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Spider Ichor"] = {
            ["mr"] = 23,
            ["cc"] = 7,
            ["id"] = "3174:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 23,
        },
        ["Greenweave Sash of the Monkey"] = {
            ["mr"] = 1999,
            ["cc"] = 4,
            ["id"] = "9766:0:0:593:0",
            ["H3246"] = 1999,
            ["sc"] = 1,
        },
        ["Recipe: Discolored Healing Potion"] = {
            ["H3220"] = 323,
            ["mr"] = 323,
        },
        ["Runecloth"] = {
            ["mr"] = 550,
            ["cc"] = 7,
            ["H3239"] = 554,
            ["id"] = "14047:0:0:0:0",
            ["sc"] = 0,
            ["L3239"] = 550,
        },
        ["Ancient Greaves of the Owl"] = {
            ["H3220"] = 12966,
            ["mr"] = 12966,
        },
        ["Gauntlets of Ogre Strength"] = {
            ["H3220"] = 2730,
            ["mr"] = 2730,
        },
        ["Aboriginal Footwraps of Stamina"] = {
            ["H3220"] = 1092,
            ["mr"] = 1092,
        },
        ["Kolkar Booty Key"] = {
            ["mr"] = 904,
            ["sc"] = 0,
            ["id"] = "5020:0:0:0:0",
            ["H3246"] = 904,
            ["cc"] = 13,
        },
        ["Elemental Air"] = {
            ["H3220"] = 599,
            ["mr"] = 599,
        },
        ["Prospector Axe"] = {
            ["H3220"] = 17854,
            ["mr"] = 17854,
        },
        ["Mystic's Robe"] = {
            ["H3220"] = 1545,
            ["mr"] = 1545,
        },
        ["Pattern: White Wedding Dress"] = {
            ["mr"] = 12500,
            ["cc"] = 9,
            ["id"] = "10325:0:0:0:0",
            ["H3229"] = 12500,
            ["sc"] = 2,
        },
        ["Overlord's Shield of the Monkey"] = {
            ["mr"] = 49999,
            ["cc"] = 4,
            ["id"] = "9974:0:0:605:0",
            ["H3246"] = 49999,
            ["sc"] = 6,
        },
        ["Phalanx Breastplate of Stamina"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Leaden Mace of Shadow Wrath"] = {
            ["H3220"] = 15288,
            ["mr"] = 15288,
        },
        ["Guillotine Axe"] = {
            ["H3220"] = 49900,
            ["mr"] = 49900,
        },
        ["Battlesmasher of Stamina"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Gorilla Fang"] = {
            ["mr"] = 448,
            ["H3254"] = 448,
            ["H3220"] = 96,
        },
        ["Blackforge Greaves"] = {
            ["H3220"] = 44306,
            ["mr"] = 44306,
        },
        ["Windchaser Cuffs"] = {
            ["H3220"] = 4996,
            ["mr"] = 4996,
        },
        ["Stonecutter Claymore of the Eagle"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Hanzo Sword"] = {
            ["H3220"] = 1160000,
            ["mr"] = 1160000,
        },
        ["Bard's Bracers of the Owl"] = {
            ["H3220"] = 299,
            ["mr"] = 299,
        },
        ["Barbaric Battle Axe of Stamina"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Bandit Pants of Spirit"] = {
            ["H3220"] = 2322,
            ["mr"] = 2322,
        },
        ["Twilight Belt of Healing"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Beaded Robe of Spirit"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Explosive Shotgun"] = {
            ["H3220"] = 7100,
            ["mr"] = 7100,
        },
        ["Conjurer's Hood of the Whale"] = {
            ["H3220"] = 8545,
            ["mr"] = 8545,
        },
        ["Linen Bandage"] = {
            ["H3220"] = 10,
            ["mr"] = 10,
        },
        ["Outrunner's Cloak of Defense"] = {
            ["H3220"] = 975,
            ["mr"] = 975,
        },
        ["Mistscape Wizard Hat"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Pugilist Bracers"] = {
            ["H3220"] = 25500,
            ["mr"] = 25500,
        },
        ["Burning Pitch"] = {
            ["H3220"] = 1979,
            ["mr"] = 1979,
        },
        ["Chief Brigadier Bracers"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Archer's Gloves of the Monkey"] = {
            ["H3220"] = 4200,
            ["mr"] = 4200,
        },
        ["Outrunner's Slippers of Stamina"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Keeper's Woolies"] = {
            ["H3220"] = 60788,
            ["mr"] = 60788,
        },
        ["Elixir of Greater Water Breathing"] = {
            ["H3220"] = 11999,
            ["mr"] = 11999,
        },
        ["Prospector's Chestpiece"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Raw Greater Sagefish"] = {
            ["H3220"] = 409,
            ["mr"] = 409,
        },
        ["Fine Leather Tunic"] = {
            ["H3220"] = 698,
            ["mr"] = 698,
        },
        ["Starsight Tunic"] = {
            ["H3220"] = 9998,
            ["mr"] = 9998,
        },
        ["Red Linen Shirt"] = {
            ["H3220"] = 90,
            ["mr"] = 90,
        },
        ["Blacksmith Hammer"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Worn Turtle Shell Shield"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Arctic Ring of the Monkey"] = {
            ["H3220"] = 29500,
            ["mr"] = 29500,
        },
        ["Archer's Belt of the Wolf"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Sentinel Gloves of the Whale"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Barbarian War Axe of Agility"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Cabalist Chestpiece of the Monkey"] = {
            ["mr"] = 57548,
            ["cc"] = 4,
            ["id"] = "7527:0:0:623:0",
            ["H3246"] = 57548,
            ["sc"] = 2,
        },
        ["Elixir of Demonslaying"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Nobles Brand of the Monkey"] = {
            ["H3220"] = 27000,
            ["mr"] = 27000,
        },
        ["Oily Blackmouth"] = {
            ["mr"] = 1349,
            ["H3254"] = 1349,
            ["H3220"] = 80,
        },
        ["Archer's Gloves of Healing"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Warden's Wraps"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Bear Meat"] = {
            ["H3220"] = 129,
            ["mr"] = 129,
        },
        ["Bloodforged Sabatons of the Tiger"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Iridescent Pearl"] = {
            ["mr"] = 2749,
            ["H3254"] = 2749,
            ["H3220"] = 1901,
        },
        ["Swim Speed Potion"] = {
            ["H3220"] = 488,
            ["mr"] = 488,
        },
        ["Twilight Robe of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Short Bastard Sword of the Whale"] = {
            ["H3220"] = 1485,
            ["mr"] = 1485,
        },
        ["Formula: Enchant Bracer - Minor Spirit"] = {
            ["H3220"] = 199,
            ["mr"] = 199,
        },
        ["Scroll of Strength II"] = {
            ["H3220"] = 223,
            ["mr"] = 223,
        },
        ["Medium Quiver"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Pattern: Big Voodoo Robe"] = {
            ["mr"] = 7800,
            ["cc"] = 9,
            ["id"] = "8386:0:0:0:0",
            ["H3228"] = 7800,
            ["sc"] = 1,
        },
        ["Deepwood Bracers"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Recipe: Limited Invulnerability Potion"] = {
            ["H3220"] = 199000,
            ["mr"] = 199000,
        },
        ["Raincaller Cord of the Monkey"] = {
            ["H3220"] = 2268,
            ["mr"] = 2268,
        },
        ["Pathfinder Footpads of the Monkey"] = {
            ["H3220"] = 3755,
            ["mr"] = 3755,
        },
        ["Champion's Leggings of the Eagle"] = {
            ["H3220"] = 37492,
            ["mr"] = 37492,
        },
        ["Outrunner's Cord of the Monkey"] = {
            ["H3220"] = 1335,
            ["mr"] = 1335,
        },
        ["Greenstone Circle of Strength"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Scaled Shield of Frozen Wrath"] = {
            ["H3220"] = 5379,
            ["mr"] = 5379,
        },
        ["Pattern: Barbaric Leggings"] = {
            ["mr"] = 2900,
            ["cc"] = 9,
            ["id"] = "5973:0:0:0:0",
            ["L3229"] = 2900,
            ["H3229"] = 3999,
            ["sc"] = 1,
        },
        ["Infiltrator Boots of the Owl"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Barbed Club of Power"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Fortified Cloak of the Bear"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Duskwoven Turban of the Whale"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Lambent Scale Shield"] = {
            ["H3220"] = 5048,
            ["mr"] = 5048,
        },
        ["Renegade Bracers of the Bear"] = {
            ["H3220"] = 10552,
            ["mr"] = 10552,
        },
        ["Pattern: Enchanter's Cowl"] = {
            ["mr"] = 8967,
            ["sc"] = 2,
            ["id"] = "14630:0:0:0:0",
            ["H3229"] = 8967,
            ["cc"] = 9,
        },
        ["Grunt's Bracers of the Eagle"] = {
            ["H3220"] = 965,
            ["mr"] = 965,
        },
        ["Jazeraint Gauntlets of the Falcon"] = {
            ["H3220"] = 6509,
            ["mr"] = 6509,
        },
        ["Elder's Padded Armor of Intellect"] = {
            ["H3220"] = 5994,
            ["mr"] = 5994,
        },
        ["Conjurer's Breeches of the Owl"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Burnished Pauldrons"] = {
            ["H3220"] = 2765,
            ["mr"] = 2765,
        },
        ["Headstriker Sword of the Monkey"] = {
            ["mr"] = 37500,
            ["sc"] = 8,
            ["id"] = "15251:0:0:615:0",
            ["H3246"] = 37500,
            ["cc"] = 2,
        },
        ["Ivycloth Pants of Spirit"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Ice Cold Milk"] = {
            ["H3220"] = 50,
            ["mr"] = 50,
        },
        ["Spider's Silk"] = {
            ["mr"] = 3465,
            ["cc"] = 7,
            ["id"] = "3182:0:0:0:0",
            ["sc"] = 0,
            ["H3250"] = 3465,
        },
        ["Shimmering Armor of the Eagle"] = {
            ["H3220"] = 14625,
            ["mr"] = 14625,
        },
        ["Green Iron Leggings"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Ogremind Ring"] = {
            ["H3220"] = 6399,
            ["mr"] = 6399,
        },
        ["Beguiler Robes"] = {
            ["H3220"] = 39800,
            ["mr"] = 39800,
        },
        ["Sniper Rifle of the Owl"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Severing Axe of the Boar"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Sage's Mantle of Frozen Wrath"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Buccaneer's Gloves of the Eagle"] = {
            ["H3220"] = 1053,
            ["mr"] = 1053,
        },
        ["Red Mageweave Bag"] = {
            ["mr"] = 9095,
            ["cc"] = 1,
            ["H3239"] = 9095,
            ["id"] = "10051:0:0:0:0",
            ["sc"] = 0,
        },
        ["Bandit Buckler of Agility"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Lodestone Hoop of Regeneration"] = {
            ["H3220"] = 15096,
            ["mr"] = 15096,
        },
        ["Searing Blade"] = {
            ["H3220"] = 9100,
            ["mr"] = 9100,
        },
        ["Medium Leather"] = {
            ["H3220"] = 58,
            ["mr"] = 58,
        },
        ["Boahn's Fang"] = {
            ["H3220"] = 15054,
            ["mr"] = 15054,
        },
        ["Bandit Pants of the Eagle"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Pattern: Frostweave Pants"] = {
            ["mr"] = 79998,
            ["sc"] = 2,
            ["id"] = "14489:0:0:0:0",
            ["H3229"] = 79998,
            ["cc"] = 9,
        },
        ["Glimmering Mail Pauldrons"] = {
            ["H3220"] = 3510,
            ["mr"] = 3510,
        },
        ["Brutish Breastplate of Power"] = {
            ["H3220"] = 49999,
            ["mr"] = 49999,
        },
        ["Outrunner's Cuffs of the Boar"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Nocturnal Sash of the Owl"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Medium Armor Kit"] = {
            ["H3220"] = 259,
            ["mr"] = 259,
        },
        ["Wild Leather Shoulders of Intellect"] = {
            ["H3220"] = 24000,
            ["mr"] = 24000,
        },
        ["Nightsky Trousers"] = {
            ["H3220"] = 4300,
            ["mr"] = 4300,
        },
        ["Quillshooter"] = {
            ["H3220"] = 199999,
            ["mr"] = 199999,
        },
        ["Harpy Needler of the Monkey"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Sentry's Armsplints of Stamina"] = {
            ["H3220"] = 1990,
            ["mr"] = 1990,
        },
        ["Lesser Moonstone"] = {
            ["mr"] = 9600,
            ["sc"] = 0,
            ["id"] = "1705:0:0:0:0",
            ["cc"] = 7,
            ["H3254"] = 9600,
            ["H3230"] = 700,
        },
        ["Raincaller Pants of the Whale"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Interlaced Cowl"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Outrunner's Slippers of the Whale"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Headhunter's Slippers of the Monkey"] = {
            ["H3220"] = 10500,
            ["mr"] = 10500,
        },
        ["Huntsman's Boots of the Monkey"] = {
            ["mr"] = 10000,
            ["cc"] = 4,
            ["id"] = "9885:0:0:605:0",
            ["H3246"] = 10000,
            ["sc"] = 2,
        },
        ["Heavy Mageweave Bandage"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Light Leather"] = {
            ["H3220"] = 16,
            ["mr"] = 16,
        },
        ["Glyphed Mitts"] = {
            ["H3220"] = 5200,
            ["mr"] = 5200,
        },
        ["Elixir of Minor Defense"] = {
            ["H3220"] = 17,
            ["mr"] = 17,
        },
        ["Durable Shoulders of the Whale"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Umbral Wand of the Boar"] = {
            ["H3220"] = 61932,
            ["mr"] = 61932,
        },
        ["Sentinel Gloves of the Owl"] = {
            ["H3220"] = 6729,
            ["mr"] = 6729,
        },
        ["Stone Hammer of the Tiger"] = {
            ["H3220"] = 25800,
            ["mr"] = 25800,
        },
        ["Kovork's Rattle"] = {
            ["H3220"] = 16621,
            ["mr"] = 16621,
        },
        ["Lupine Leggings of the Wolf"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Rough Sharpening Stone"] = {
            ["H3220"] = 11,
            ["mr"] = 11,
        },
        ["Brigade Cloak of the Monkey"] = {
            ["mr"] = 9577,
            ["cc"] = 4,
            ["id"] = "9929:0:0:599:0",
            ["H3246"] = 9577,
            ["sc"] = 1,
        },
        ["Battleforge Armor of the Bear"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Raider Shortsword of Power"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Recipe: Elixir of Ogre's Strength"] = {
            ["H3220"] = 5200,
            ["mr"] = 5200,
        },
        ["Handstitched Leather Belt"] = {
            ["H3220"] = 299,
            ["mr"] = 299,
        },
        ["Banded Leggings of the Monkey"] = {
            ["mr"] = 9000,
            ["cc"] = 4,
            ["id"] = "9841:0:0:605:0",
            ["H3246"] = 9000,
            ["sc"] = 3,
        },
        ["Superior Cloak of Defense"] = {
            ["H3220"] = 1957,
            ["mr"] = 1957,
        },
        ["Blindweed"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Chief Brigadier Shield"] = {
            ["H3220"] = 10129,
            ["mr"] = 10129,
        },
        ["Glimmering Mail Coif"] = {
            ["H3220"] = 5499,
            ["mr"] = 5499,
        },
        ["Staunch Hammer of Power"] = {
            ["H3220"] = 1199,
            ["mr"] = 1199,
        },
        ["Black Diamond"] = {
            ["H3220"] = 438,
            ["mr"] = 438,
        },
        ["Merc Sword of Stamina"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Twilight Cape of the Eagle"] = {
            ["H3220"] = 16000,
            ["mr"] = 16000,
        },
        ["Feral Shoes of the Monkey"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Polished Zweihander of the Wolf"] = {
            ["H3220"] = 5733,
            ["mr"] = 5733,
        },
        ["Opaque Wand"] = {
            ["H3220"] = 1593,
            ["mr"] = 1593,
        },
        ["Shiver Blade"] = {
            ["mr"] = 4500,
            ["H3254"] = 4500,
            ["H3220"] = 3000,
        },
        ["Night Reaver"] = {
            ["H3220"] = 300000,
            ["mr"] = 300000,
        },
        ["Dwarven Mild"] = {
            ["H3220"] = 117,
            ["mr"] = 117,
        },
        ["Wrangler's Leggings of Spirit"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Pattern: Big Voodoo Mask"] = {
            ["mr"] = 1400,
            ["cc"] = 9,
            ["id"] = "8387:0:0:0:0",
            ["L3229"] = 1399,
            ["H3229"] = 1400,
            ["sc"] = 1,
        },
        ["Cavalier Two-hander of the Bear"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Grunt's Pauldrons of the Tiger"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Phalanx Shield of Strength"] = {
            ["H3220"] = 10366,
            ["mr"] = 10366,
        },
        ["Honed Stiletto of the Monkey"] = {
            ["mr"] = 8984,
            ["sc"] = 15,
            ["id"] = "15242:0:0:588:0",
            ["H3246"] = 8984,
            ["cc"] = 2,
        },
        ["Conjurer's Gloves of Healing"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Green Whelp Scale"] = {
            ["H3220"] = 387,
            ["mr"] = 387,
        },
        ["War Torn Tunic of the Bear"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Patched Leather Shoulderpads"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Hero's Band"] = {
            ["H3220"] = 56109,
            ["mr"] = 56109,
        },
        ["Crusader Bow of the Monkey"] = {
            ["mr"] = 30361,
            ["cc"] = 2,
            ["id"] = "15287:0:0:592:0",
            ["H3246"] = 30361,
            ["sc"] = 2,
        },
        ["Training Sword of Stamina"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Lupine Vest of Stamina"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Azure Silk Vest"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Meaty Bat Wing"] = {
            ["H3220"] = 110,
            ["mr"] = 110,
        },
        ["Rigid Shoulders of the Eagle"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Embersilk Mitts of Spirit"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Dervish Tunic of the Eagle"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Resilient Leggings"] = {
            ["H3220"] = 6899,
            ["mr"] = 6899,
        },
        ["Severing Axe of the Monkey"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Enduring Belt"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Conjurer's Mantle of Intellect"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Selenium Chain of the Eagle"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Indurium Ore"] = {
            ["H3220"] = 895,
            ["mr"] = 895,
        },
        ["Chimera Leather"] = {
            ["H3220"] = 9300,
            ["mr"] = 9300,
        },
        ["War Torn Tunic of the Tiger"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Shimmering Sash of the Whale"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Twilight Cape of Arcane Wrath"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Silk Cloth"] = {
            ["mr"] = 199,
            ["cc"] = 7,
            ["sc"] = 0,
            ["id"] = "4306:0:0:0:0",
            ["H3254"] = 199,
            ["L3250"] = 190,
            ["H3250"] = 200,
        },
        ["Blood Ring"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Hawkeye's Buckler"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Silver Skeleton Key"] = {
            ["H3220"] = 100,
            ["mr"] = 100,
        },
        ["Mystic's Belt"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Basalt Ring of the Gorilla"] = {
            ["H3220"] = 4200,
            ["mr"] = 4200,
        },
        ["Brackwater Leggings"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Wrangler's Mantle of Stamina"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Gothic Plate Spaulders of the Bear"] = {
            ["H3220"] = 24506,
            ["mr"] = 24506,
        },
        ["Ceremonial Cloak"] = {
            ["H3220"] = 127,
            ["mr"] = 127,
        },
        ["Knight's Cloak of the Bear"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Lodestone Hoop of Strength"] = {
            ["H3220"] = 48500,
            ["mr"] = 48500,
        },
        ["White Linen Robe"] = {
            ["H3220"] = 594,
            ["mr"] = 594,
        },
        ["Formula: Enchant Weapon - Crusader"] = {
            ["H3220"] = 2000000,
            ["mr"] = 2000000,
        },
        ["Outrunner's Cord of the Whale"] = {
            ["H3220"] = 701,
            ["mr"] = 701,
        },
        ["Black Mageweave Leggings"] = {
            ["H3220"] = 7599,
            ["mr"] = 7599,
        },
        ["Invisibility Potion"] = {
            ["H3220"] = 13500,
            ["mr"] = 13500,
        },
        ["Schematic: Gnomish Universal Remote"] = {
            ["H3220"] = 3499,
            ["mr"] = 3499,
        },
        ["Jagged Star of Power"] = {
            ["H3220"] = 4114,
            ["mr"] = 4114,
        },
        ["Archer's Trousers of the Eagle"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Pattern: Enchanted Mageweave Pouch"] = {
            ["mr"] = 11800,
            ["cc"] = 9,
            ["id"] = "22307:0:0:0:0",
            ["L3229"] = 11799,
            ["H3229"] = 11800,
            ["sc"] = 2,
        },
        ["Captain's Waistguard of Defense"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Runecloth Gloves"] = {
            ["mr"] = 10000,
            ["cc"] = 4,
            ["H3239"] = 10000,
            ["id"] = "13863:0:0:0:0",
            ["sc"] = 1,
        },
        ["Shimmering Stave of Arcane Wrath"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Headhunter's Belt of the Owl"] = {
            ["H3220"] = 3380,
            ["mr"] = 3380,
        },
        ["Shimmering Trousers of the Whale"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Robust Boots of Power"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Gossamer Headpiece of Fiery Wrath"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Scroll of Strength III"] = {
            ["H3220"] = 3699,
            ["mr"] = 3699,
        },
        ["Buccaneer's Orb of the Falcon"] = {
            ["H3220"] = 2044,
            ["mr"] = 2044,
        },
        ["Thistlefur Robe of Stamina"] = {
            ["H3220"] = 5600,
            ["mr"] = 5600,
        },
        ["Enduring Boots"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Vital Leggings of the Eagle"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Fortified Belt of the Whale"] = {
            ["H3220"] = 2199,
            ["mr"] = 2199,
        },
        ["Devilsaur Leather"] = {
            ["mr"] = 103899,
            ["sc"] = 0,
            ["id"] = "15417:0:0:0:0",
            ["H3246"] = 103899,
            ["cc"] = 7,
        },
        ["Nightscape Boots"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Gloom Reaper of the Tiger"] = {
            ["H3220"] = 13333,
            ["mr"] = 13333,
        },
        ["Recipe: Free Action Potion"] = {
            ["H3220"] = 14800,
            ["mr"] = 14800,
        },
        ["Cross-stitched Shoulderpads"] = {
            ["H3220"] = 1490,
            ["mr"] = 1490,
        },
        ["Heraldic Boots"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Superior Leggings of Spirit"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Embersilk Mitts of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Jagged Star of the Bear"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Infiltrator Cap of the Wolf"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Monk's Staff of the Boar"] = {
            ["H3220"] = 40819,
            ["mr"] = 40819,
        },
        ["Robust Buckler of the Wolf"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Heavy Grinding Stone"] = {
            ["H3220"] = 1875,
            ["mr"] = 1875,
        },
        ["Sunblaze Coif"] = {
            ["H3220"] = 10200,
            ["mr"] = 10200,
        },
        ["Arctic Pendant of Agility"] = {
            ["H3220"] = 64500,
            ["mr"] = 64500,
        },
        ["Raw Spotted Yellowtail"] = {
            ["H3220"] = 85,
            ["mr"] = 85,
        },
        ["Recipe: Magic Resistance Potion"] = {
            ["H3220"] = 7300,
            ["mr"] = 7300,
        },
        ["Raincaller Cuffs of Arcane Wrath"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Elder's Padded Armor of Stamina"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Conjurer's Vest of the Owl"] = {
            ["H3220"] = 6906,
            ["mr"] = 6906,
        },
        ["Oak Mallet of the Eagle"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Watcher's Mantle of the Whale"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Captain's Gauntlets of the Monkey"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Grizzly Jerkin of the Eagle"] = {
            ["H3220"] = 858,
            ["mr"] = 858,
        },
        ["Infiltrator Armor of the Monkey"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Feral Buckler of the Bear"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Elemental Fire"] = {
            ["H3220"] = 7227,
            ["mr"] = 7227,
        },
        ["Glimmering Flamberge of the Monkey"] = {
            ["H3220"] = 11599,
            ["mr"] = 11599,
        },
        ["Cerulean Ring of the Owl"] = {
            ["H3220"] = 6432,
            ["mr"] = 6432,
        },
        ["Sentry's Gloves of the Whale"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Banded Armor of the Tiger"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Lupine Mantle"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Tracker's Wristguards of the Eagle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Quillfire Bow of the Falcon"] = {
            ["H3220"] = 99900,
            ["mr"] = 99900,
        },
        ["Formula: Enchant Bracer - Greater Stamina"] = {
            ["H3220"] = 27000,
            ["mr"] = 27000,
        },
        ["Phalanx Shield of Defense"] = {
            ["H3220"] = 10900,
            ["mr"] = 10900,
        },
        ["Cerulean Ring of Concentration"] = {
            ["H3220"] = 6300,
            ["mr"] = 6300,
        },
        ["Phalanx Boots of Defense"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Emblazoned Shoulders"] = {
            ["H3220"] = 2017,
            ["mr"] = 2017,
        },
        ["Wolf Rider's Leggings of the Whale"] = {
            ["H3220"] = 23900,
            ["mr"] = 23900,
        },
        ["Jouster's Gauntlets"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Willow Vest of the Whale"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Soft Bushy Tail"] = {
            ["H3220"] = 749,
            ["mr"] = 749,
        },
        ["Flurry Axe"] = {
            ["H3220"] = 2200000,
            ["mr"] = 2200000,
        },
        ["Arena Vambraces"] = {
            ["H3220"] = 120000,
            ["mr"] = 120000,
        },
        ["Thunderbrow Ring"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Wand of Eventide"] = {
            ["H3220"] = 4200,
            ["mr"] = 4200,
        },
        ["Amethyst Band of Nature Resistance"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Sequoia Branch of Stamina"] = {
            ["H3220"] = 41000,
            ["mr"] = 41000,
        },
        ["Elder's Pants of the Owl"] = {
            ["H3220"] = 12400,
            ["mr"] = 12400,
        },
        ["Thick Scale Sabatons of the Gorilla"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Burnt Leather Vest"] = {
            ["H3220"] = 398,
            ["mr"] = 398,
        },
        ["Discolored Healing Potion"] = {
            ["H3220"] = 74,
            ["mr"] = 74,
        },
        ["Swift Boots"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Viking Sword of the Tiger"] = {
            ["H3220"] = 6969,
            ["mr"] = 6969,
        },
        ["Warden's Wizard Hat"] = {
            ["H3220"] = 15800,
            ["mr"] = 15800,
        },
        ["Pagan Cape of the Whale"] = {
            ["H3220"] = 995,
            ["mr"] = 995,
        },
        ["Pressed Felt Robe"] = {
            ["H3220"] = 5610,
            ["mr"] = 5610,
        },
        ["Tyrant's Armguards"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Captain's Waistguard of the Eagle"] = {
            ["H3220"] = 10100,
            ["mr"] = 10100,
        },
        ["Dervish Cape of the Eagle"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Primal Leggings of the Eagle"] = {
            ["H3220"] = 790,
            ["mr"] = 790,
        },
        ["Bloodspattered Sash of the Bear"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Twilight Belt of the Whale"] = {
            ["H3220"] = 3123,
            ["mr"] = 3123,
        },
        ["Champion's Girdle of the Monkey"] = {
            ["mr"] = 18000,
            ["cc"] = 4,
            ["id"] = "7546:0:0:611:0",
            ["H3246"] = 18000,
            ["sc"] = 3,
        },
        ["Tracker's Wristguards of Nature's Wrath"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Buccaneer's Vest of the Whale"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Jazeraint Bracers of Healing"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Arctic Ring of the Tiger"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Rakzur Club"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Thick Scale Gauntlets of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pillager's Cloak of Defense"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Fire Oil"] = {
            ["H3220"] = 389,
            ["mr"] = 389,
        },
        ["Sanguine Mantle"] = {
            ["H3220"] = 2499,
            ["mr"] = 2499,
        },
        ["Conjurer's Cinch of Healing"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Recipe: Major Healing Potion"] = {
            ["H3220"] = 97000,
            ["mr"] = 97000,
        },
        ["Handstitched Leather Vest"] = {
            ["H3220"] = 188,
            ["mr"] = 188,
        },
        ["Thistle Tea"] = {
            ["H3220"] = 740,
            ["mr"] = 740,
        },
        ["Dervish Belt of the Monkey"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Lunar Belt of the Whale"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Warmonger's Chestpiece of the Monkey"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Scroll of Stamina IV"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Valorous Wristguards"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Frenzied Striker"] = {
            ["H3220"] = 500000,
            ["mr"] = 500000,
        },
        ["Moccasins of the White Hare"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Phalanx Gauntlets of the Bear"] = {
            ["H3220"] = 3350,
            ["mr"] = 3350,
        },
        ["Ranger Leggings of the Monkey"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Burnished Bracers"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Howling Blade"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Sapphiron's Scale Boots"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Wicked Chain Cloak of Strength"] = {
            ["H3220"] = 4999,
            ["mr"] = 4999,
        },
        ["Vital Leggings of the Owl"] = {
            ["H3220"] = 5499,
            ["mr"] = 5499,
        },
        ["Banded Pauldrons of the Monkey"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Combat Shield"] = {
            ["H3220"] = 6435,
            ["mr"] = 6435,
        },
        ["Plans: Deadly Bronze Poniard"] = {
            ["H3220"] = 442,
            ["mr"] = 442,
        },
        ["Robust Girdle of the Whale"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Recipe: Elixir of Detect Lesser Invisibility"] = {
            ["H3220"] = 4700,
            ["mr"] = 4700,
        },
        ["Dense Stone"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Wild Leather Shoulders of the Monkey"] = {
            ["H3220"] = 24999,
            ["mr"] = 24999,
        },
        ["Morrowgrain"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Greater Mana Potion"] = {
            ["mr"] = 2400,
            ["H3254"] = 2400,
            ["H3220"] = 575,
        },
        ["Orb of Mistmantle"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Essence of Earth"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Schematic: Mithril Mechanical Dragonling"] = {
            ["H3220"] = 155000,
            ["mr"] = 155000,
        },
        ["Gleaming Claymore of the Wolf"] = {
            ["H3220"] = 2322,
            ["mr"] = 2322,
        },
        ["Linked Chain Shoulderpads"] = {
            ["H3220"] = 1095,
            ["mr"] = 1095,
        },
        ["Dwarven Magestaff of the Boar"] = {
            ["mr"] = 9000,
            ["sc"] = 10,
            ["id"] = "2072:0:0:1109:0",
            ["cc"] = 2,
            ["H3226"] = 9000,
        },
        ["Thick Scale Breastplate of Power"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Vital Orb of Arcane Wrath"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Golden Sansam"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Hulking Leggings"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Fortified Spaulders of the Boar"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Lupine Vest of the Whale"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Thunder Lizard Tail"] = {
            ["H3220"] = 188,
            ["mr"] = 188,
        },
        ["Twilight Pants of Fiery Wrath"] = {
            ["H3220"] = 12177,
            ["mr"] = 12177,
        },
        ["Exquisite Flamberge of the Monkey"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Cobalt Ring of the Whale"] = {
            ["H3220"] = 4499,
            ["mr"] = 4499,
        },
        ["Iron Strut"] = {
            ["H3220"] = 1799,
            ["mr"] = 1799,
        },
        ["Ivycloth Sash of the Owl"] = {
            ["H3220"] = 2159,
            ["mr"] = 2159,
        },
        ["Sentry's Slippers of the Bear"] = {
            ["H3220"] = 5399,
            ["mr"] = 5399,
        },
        ["Wrangler's Cloak of the Monkey"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Outrunner's Gloves of Healing"] = {
            ["H3220"] = 2890,
            ["mr"] = 2890,
        },
        ["Tin Ore"] = {
            ["H3220"] = 295,
            ["mr"] = 295,
        },
        ["Symbolic Legplates"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Elder's Bracers of the Eagle"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Cavalier Two-hander of the Boar"] = {
            ["H3220"] = 14000,
            ["mr"] = 14000,
        },
        ["19 Pound Catfish"] = {
            ["H3220"] = 434,
            ["mr"] = 434,
        },
        ["Wicked Spiked Mace"] = {
            ["H3220"] = 4800,
            ["mr"] = 4800,
        },
        ["Battle Knife of the Monkey"] = {
            ["mr"] = 8317,
            ["sc"] = 15,
            ["id"] = "15241:0:0:587:0",
            ["H3246"] = 8317,
            ["cc"] = 2,
        },
        ["Pagan Rod of the Whale"] = {
            ["H3220"] = 4407,
            ["mr"] = 4407,
        },
        ["Spiked Chain Shoulder Pads of the Monkey"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Wrangler's Mantle of the Owl"] = {
            ["H3220"] = 9262,
            ["mr"] = 9262,
        },
        ["Elixir of Defense"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Buccaneer's Pants of the Eagle"] = {
            ["mr"] = 4800,
            ["cc"] = 4,
            ["id"] = "14171:0:0:851:0",
            ["sc"] = 1,
            ["H3224"] = 4800,
        },
        ["Pattern: Runic Leather Pants"] = {
            ["mr"] = 15000,
            ["cc"] = 9,
            ["id"] = "15765:0:0:0:0",
            ["L3229"] = 15000,
            ["H3229"] = 18899,
            ["sc"] = 1,
        },
        ["Shredder Operating Manual - Page 8"] = {
            ["mr"] = 150,
            ["cc"] = 15,
            ["id"] = "16652:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 150,
        },
        ["Bloodspattered Gloves of the Gorilla"] = {
            ["H3220"] = 1065,
            ["mr"] = 1065,
        },
        ["Native Pants of the Monkey"] = {
            ["mr"] = 3000,
            ["sc"] = 1,
            ["id"] = "14097:0:0:586:0",
            ["H3246"] = 3000,
            ["cc"] = 4,
        },
        ["Formula: Smoking Heart of the Mountain"] = {
            ["H3220"] = 17200,
            ["mr"] = 17200,
        },
        ["Basalt Ring of the Bear"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Crystal Starfire Medallion"] = {
            ["H3220"] = 21800,
            ["mr"] = 21800,
        },
        ["Disciple's Stein of Fiery Wrath"] = {
            ["H3220"] = 790,
            ["mr"] = 790,
        },
        ["Green Hills of Stranglethorn - Page 14"] = {
            ["mr"] = 995,
            ["H3254"] = 995,
            ["H3220"] = 1116,
        },
        ["Severing Axe of the Whale"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Aboriginal Rod of the Owl"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["War Torn Shield of the Bear"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Crimson Lotus"] = {
            ["H3220"] = 294,
            ["mr"] = 294,
        },
        ["Fortified Spaulders of the Whale"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Edged Bastard Sword of Stamina"] = {
            ["H3220"] = 2754,
            ["mr"] = 2754,
        },
        ["Wildvine"] = {
            ["mr"] = 5654,
            ["cc"] = 7,
            ["H3226"] = 5654,
            ["id"] = "8153:0:0:0:0",
            ["sc"] = 0,
        },
        ["Stalking Pants"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Mageweave Cloth"] = {
            ["mr"] = 600,
            ["cc"] = 7,
            ["id"] = "4338:0:0:0:0",
            ["sc"] = 0,
            ["H3254"] = 600,
            ["H3246"] = 412,
        },
        ["Northern Shortsword of Strength"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Mindbender Loop"] = {
            ["H3220"] = 6200,
            ["mr"] = 6200,
        },
        ["Superior Cloak of the Monkey"] = {
            ["mr"] = 4882,
            ["cc"] = 4,
            ["id"] = "9805:0:0:589:0",
            ["H3246"] = 4882,
            ["sc"] = 1,
        },
        ["Turtle Scale"] = {
            ["H3220"] = 7499,
            ["mr"] = 7499,
        },
        ["Plans: Silvered Bronze Leggings"] = {
            ["H3220"] = 1945,
            ["mr"] = 1945,
        },
        ["Ritual Bands of the Owl"] = {
            ["H3220"] = 2050,
            ["mr"] = 2050,
        },
        ["Tender Wolf Meat"] = {
            ["H3220"] = 473,
            ["mr"] = 473,
        },
        ["Archer's Belt of the Eagle"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Raider's Chestpiece of the Bear"] = {
            ["H3220"] = 1390,
            ["mr"] = 1390,
        },
        ["Native Branch of the Owl"] = {
            ["H3220"] = 3522,
            ["mr"] = 3522,
        },
        ["Grunt's Cape of the Bear"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Silver Bar"] = {
            ["mr"] = 201,
            ["cc"] = 7,
            ["id"] = "2842:0:0:0:0",
            ["sc"] = 0,
            ["H3225"] = 201,
        },
        ["Bonelink Cape of the Monkey"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Basilisk Hide Pants"] = {
            ["H3220"] = 89500,
            ["mr"] = 89500,
        },
        ["Breath of Wind"] = {
            ["H3220"] = 580,
            ["mr"] = 580,
        },
        ["Conjurer's Cinch of the Owl"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Elder's Amber Stave of the Falcon"] = {
            ["H3220"] = 4700,
            ["mr"] = 4700,
        },
        ["Iridium Chain of the Eagle"] = {
            ["H3220"] = 19500,
            ["mr"] = 19500,
        },
        ["Prospector's Pads"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Formidable Circlet of the Monkey"] = {
            ["mr"] = 80000,
            ["cc"] = 4,
            ["id"] = "15634:0:0:626:0",
            ["H3246"] = 80000,
            ["sc"] = 3,
        },
        ["Embossed Plate Girdle of Strength"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Northern Shortsword of Stamina"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Cross Dagger of Power"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Feral Leggings of Healing"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Imperial Red Cloak"] = {
            ["H3220"] = 23100,
            ["mr"] = 23100,
        },
        ["Gaea's Cuffs of Shadow Wrath"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Vital Shoulders of Frozen Wrath"] = {
            ["H3220"] = 5693,
            ["mr"] = 5693,
        },
        ["Major Healing Potion"] = {
            ["H3220"] = 9400,
            ["mr"] = 9400,
        },
        ["Chief Brigadier Girdle"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Rigid Cape of the Owl"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Battle Slayer of the Monkey"] = {
            ["H3220"] = 9899,
            ["mr"] = 9899,
        },
        ["Lord's Cape of the Falcon"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Embersilk Coronet of the Whale"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Lesser Magic Essence"] = {
            ["H3220"] = 489,
            ["mr"] = 489,
        },
        ["Black Dragonscale"] = {
            ["H3220"] = 19600,
            ["mr"] = 19600,
        },
        ["Plans: Heavy Mithril Helm"] = {
            ["H3220"] = 6999,
            ["mr"] = 6999,
        },
        ["Medicine Staff of Healing"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Regal Sash of the Eagle"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pillager's Girdle of Strength"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Acrobatic Staff of Nature's Wrath"] = {
            ["H3220"] = 24264,
            ["mr"] = 24264,
        },
        ["Archer's Gloves of the Whale"] = {
            ["H3220"] = 5823,
            ["mr"] = 5823,
        },
        ["Bloodforged Gauntlets of the Bear"] = {
            ["H3220"] = 29999,
            ["mr"] = 29999,
        },
        ["Spiked Club of the Monkey"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Giant Club of Healing"] = {
            ["H3220"] = 18888,
            ["mr"] = 18888,
        },
        ["Field Plate Armor of the Monkey"] = {
            ["mr"] = 20585,
            ["cc"] = 4,
            ["id"] = "9286:0:0:617:0",
            ["H3246"] = 20585,
            ["sc"] = 4,
        },
        ["Raider Shortsword of Stamina"] = {
            ["H3220"] = 2475,
            ["mr"] = 2475,
        },
        ["Imposing Shoulders of the Monkey"] = {
            ["mr"] = 9500,
            ["sc"] = 2,
            ["id"] = "15169:0:0:608:0",
            ["H3246"] = 9500,
            ["cc"] = 4,
        },
        ["Hatchet"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Cured Thick Hide"] = {
            ["mr"] = 10020,
            ["H3254"] = 10020,
            ["H3220"] = 5900,
        },
        ["Razor's Edge"] = {
            ["H3220"] = 17400,
            ["mr"] = 17400,
        },
        ["Embossed Plate Armor of the Bear"] = {
            ["H3220"] = 15015,
            ["mr"] = 15015,
        },
        ["Watcher's Handwraps of the Whale"] = {
            ["H3220"] = 2325,
            ["mr"] = 2325,
        },
        ["Scroll of Intellect"] = {
            ["H3220"] = 50,
            ["mr"] = 50,
        },
        ["Fortified Leggings of the Boar"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Twin-bladed Axe of Agility"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Raider's Belt of the Whale"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Geomancer's Spaulders of the Whale"] = {
            ["H3220"] = 8632,
            ["mr"] = 8632,
        },
        ["Seaweed"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Magister's Bindings"] = {
            ["H3220"] = 249999,
            ["mr"] = 249999,
        },
        ["Stone Hammer of the Monkey"] = {
            ["mr"] = 37800,
            ["cc"] = 2,
            ["id"] = "15260:0:0:611:0",
            ["H3246"] = 37800,
            ["sc"] = 5,
        },
        ["Sorcerer Hat of Stamina"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Raincaller Robes of the Whale"] = {
            ["H3220"] = 5838,
            ["mr"] = 5838,
        },
        ["Scouting Cloak of the Owl"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Wrangler's Boots of the Owl"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Elixir of Minor Agility"] = {
            ["H3220"] = 665,
            ["mr"] = 665,
        },
        ["Large Brown Sack"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Wrangler's Buckler of the Eagle"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Cutthroat's Buckler of the Whale"] = {
            ["H3220"] = 15078,
            ["mr"] = 15078,
        },
        ["Elixir of Greater Intellect"] = {
            ["H3220"] = 1595,
            ["mr"] = 1595,
        },
        ["Greater Maul of the Bear"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Field Plate Vambraces of Strength"] = {
            ["H3220"] = 13900,
            ["mr"] = 13900,
        },
        ["Battle Knife of Power"] = {
            ["H3220"] = 5800,
            ["mr"] = 5800,
        },
        ["Silver Rod"] = {
            ["H3220"] = 224,
            ["mr"] = 224,
        },
        ["Aboriginal Footwraps of the Owl"] = {
            ["H3220"] = 660,
            ["mr"] = 660,
        },
        ["Oil of Olaf"] = {
            ["H3220"] = 6099,
            ["mr"] = 6099,
        },
        ["Pattern: Phoenix Pants"] = {
            ["mr"] = 2025,
            ["sc"] = 2,
            ["id"] = "4349:0:0:0:0",
            ["H3229"] = 2025,
            ["cc"] = 9,
        },
        ["Rune of Opening"] = {
            ["H3220"] = 48,
            ["mr"] = 48,
        },
        ["Rigid Tunic of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Green Hills of Stranglethorn - Page 20"] = {
            ["mr"] = 975,
            ["H3254"] = 975,
            ["H3220"] = 765,
        },
        ["Imposing Pants of the Monkey"] = {
            ["H3220"] = 23700,
            ["mr"] = 23700,
        },
        ["Thallium Choker of Concentration"] = {
            ["H3220"] = 49999,
            ["mr"] = 49999,
        },
        ["Sorcerer Pants of Healing"] = {
            ["H3220"] = 11299,
            ["mr"] = 11299,
        },
        ["Embossed Plate Pauldrons of the Bear"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Ivycloth Tunic of Frozen Wrath"] = {
            ["H3220"] = 16400,
            ["mr"] = 16400,
        },
        ["Infiltrator Armor of Agility"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Mercenary Blade of the Tiger"] = {
            ["H3220"] = 13300,
            ["mr"] = 13300,
        },
        ["Ichor of Undeath"] = {
            ["H3220"] = 407,
            ["mr"] = 407,
        },
        ["Essence of Air"] = {
            ["H3220"] = 92527,
            ["mr"] = 92527,
        },
        ["Phalanx Girdle of the Bear"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Braincage"] = {
            ["H3220"] = 154842,
            ["mr"] = 154842,
        },
        ["Renegade Belt of Power"] = {
            ["H3220"] = 4949,
            ["mr"] = 4949,
        },
        ["Gloves of the Fang"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Light Bow"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Keller's Girdle"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Pattern: Frostsaber Gloves"] = {
            ["mr"] = 9300,
            ["sc"] = 1,
            ["id"] = "15761:0:0:0:0",
            ["H3229"] = 9300,
            ["cc"] = 9,
        },
        ["Thick Leather"] = {
            ["mr"] = 723,
            ["H3254"] = 723,
            ["H3220"] = 633,
        },
        ["Soldier's Boots of the Bear"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Renegade Boots of the Bear"] = {
            ["H3220"] = 7199,
            ["mr"] = 7199,
        },
        ["Bolt of Linen Cloth"] = {
            ["mr"] = 76,
            ["sc"] = 0,
            ["id"] = "2996:0:0:0:0",
            ["H3246"] = 76,
            ["cc"] = 7,
        },
        ["Raincaller Mitts of the Falcon"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Crushridge Bindings"] = {
            ["H3220"] = 37500,
            ["mr"] = 37500,
        },
        ["Jungle Necklace of the Monkey"] = {
            ["H3220"] = 131000,
            ["mr"] = 131000,
        },
        ["Azure Silk Hood"] = {
            ["H3220"] = 1495,
            ["mr"] = 1495,
        },
        ["Dark Iron Residue"] = {
            ["H3220"] = 151,
            ["mr"] = 151,
        },
        ["Raincaller Cloak of Arcane Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Conjurer's Gloves of the Owl"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Plans: Shadow Crescent Axe"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Wanderer's Armor of the Monkey"] = {
            ["mr"] = 150000,
            ["sc"] = 2,
            ["id"] = "10105:0:0:632:0",
            ["L3246"] = 150000,
            ["H3246"] = 229999,
            ["cc"] = 4,
        },
        ["Plans: Thorium Shield Spike"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Greater Adept's Robe"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Embossed Plate Helmet of the Bear"] = {
            ["H3220"] = 20300,
            ["mr"] = 20300,
        },
        ["Murphstar of the Monkey"] = {
            ["mr"] = 29200,
            ["cc"] = 2,
            ["id"] = "1207:0:0:593:0",
            ["H3246"] = 29200,
            ["sc"] = 4,
        },
        ["Chieftain's Boots of the Monkey"] = {
            ["mr"] = 16537,
            ["cc"] = 4,
            ["id"] = "9948:0:0:611:0",
            ["H3246"] = 16537,
            ["sc"] = 2,
        },
        ["Stringy Vulture Meat"] = {
            ["H3220"] = 18,
            ["mr"] = 18,
        },
        ["Nocturnal Tunic of the Eagle"] = {
            ["H3220"] = 14235,
            ["mr"] = 14235,
        },
        ["Pulsating Hydra Heart"] = {
            ["mr"] = 35384,
            ["H3254"] = 35384,
            ["H3220"] = 4400,
        },
        ["Pattern: Tough Scorpid Shoulders"] = {
            ["mr"] = 2200,
            ["cc"] = 9,
            ["id"] = "8400:0:0:0:0",
            ["L3229"] = 2200,
            ["H3229"] = 2300,
            ["sc"] = 1,
        },
        ["Spiked Chain Wristbands of the Bear"] = {
            ["H3220"] = 19800,
            ["mr"] = 19800,
        },
        ["Bard's Gloves of the Bear"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Sniper Rifle of the Eagle"] = {
            ["H3220"] = 17000,
            ["mr"] = 17000,
        },
        ["Libram of Tenacity"] = {
            ["H3220"] = 59999,
            ["mr"] = 59999,
        },
        ["Bronze Framework"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Shadowblade"] = {
            ["H3220"] = 499000,
            ["mr"] = 499000,
        },
        ["Knight's Cloak of Defense"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Large Radiant Shard"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Defender Gauntlets of the Bear"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Gargoyle's Bite"] = {
            ["H3220"] = 5600,
            ["mr"] = 5600,
        },
        ["Larval Acid"] = {
            ["H3220"] = 19789,
            ["mr"] = 19789,
        },
        ["Pattern: Truefaith Vestments"] = {
            ["mr"] = 3250000,
            ["cc"] = 9,
            ["id"] = "14512:0:0:0:0",
            ["H3229"] = 3250000,
            ["sc"] = 2,
        },
        ["Hillborne Axe of the Bear"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Phalanx Breastplate of the Bear"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Shredder Operating Manual - Page 9"] = {
            ["mr"] = 195,
            ["cc"] = 15,
            ["id"] = "16653:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 195,
        },
        ["Durable Rod of the Owl"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Jazeraint Belt of the Boar"] = {
            ["H3220"] = 4835,
            ["mr"] = 4835,
        },
        ["Toughened Leather Gloves"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Turtle Meat"] = {
            ["H3220"] = 175,
            ["mr"] = 175,
        },
        ["Renegade Gauntlets of Healing"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Tyrant's Belt"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Mithril Gyro-Shot"] = {
            ["H3220"] = 8,
            ["mr"] = 8,
        },
        ["Lesser Nether Essence"] = {
            ["H3220"] = 4100,
            ["mr"] = 4100,
        },
        ["Gemmed Copper Gauntlets of the Monkey"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Robust Shoulders of Defense"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Grim Reaper"] = {
            ["H3220"] = 55000,
            ["mr"] = 55000,
        },
        ["Plans: Golden Scale Shoulders"] = {
            ["mr"] = 19602,
            ["sc"] = 4,
            ["L3251"] = 19404,
            ["id"] = "3871:0:0:0:0",
            ["H3253"] = 19602,
            ["H3251"] = 19800,
            ["cc"] = 9,
        },
        ["Conjurer's Cloak of the Whale"] = {
            ["H3220"] = 3099,
            ["mr"] = 3099,
        },
        ["Feathered Headdress"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Lupine Slippers of the Owl"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Gut Ripper"] = {
            ["H3220"] = 350000,
            ["mr"] = 350000,
        },
        ["Renegade Cloak of Defense"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Felcloth Pants"] = {
            ["mr"] = 246000,
            ["sc"] = 1,
            ["id"] = "14107:0:0:0:0",
            ["H3246"] = 246000,
            ["cc"] = 4,
        },
        ["Spinel Ring of Fire Resistance"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Fighter Broadsword of the Monkey"] = {
            ["mr"] = 41400,
            ["sc"] = 7,
            ["id"] = "15212:0:0:587:0",
            ["H3246"] = 41400,
            ["cc"] = 2,
        },
        ["Heraldic Spaulders"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Imposing Boots of Power"] = {
            ["H3220"] = 11100,
            ["mr"] = 11100,
        },
        ["Formula: Enchant Boots - Minor Agility"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Bloodspattered Gloves of the Eagle"] = {
            ["H3220"] = 907,
            ["mr"] = 907,
        },
        ["Bristlebark Gloves"] = {
            ["mr"] = 37407,
            ["H3254"] = 37407,
            ["H3220"] = 1258,
        },
        ["Outrunner's Slippers of the Boar"] = {
            ["H3220"] = 1767,
            ["mr"] = 1767,
        },
        ["Shadoweave Robe"] = {
            ["H3220"] = 61497,
            ["mr"] = 61497,
        },
        ["Uther's Strength"] = {
            ["H3220"] = 69999,
            ["mr"] = 69999,
        },
        ["Shiny Fish Scales"] = {
            ["mr"] = 12,
            ["cc"] = 15,
            ["id"] = "17057:0:0:0:0",
            ["sc"] = 0,
            ["H3224"] = 12,
        },
        ["Compact Shotgun"] = {
            ["H3220"] = 497,
            ["mr"] = 497,
        },
        ["Sorcerer Cloak of the Whale"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Bolt of Mageweave"] = {
            ["H3220"] = 2399,
            ["mr"] = 2399,
        },
        ["Schematic: EZ-Thro Dynamite"] = {
            ["H3220"] = 269,
            ["mr"] = 269,
        },
        ["Sentry's Gloves of the Bear"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Worn Mail Boots"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Deviate Scale Belt"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Oak Mallet of Stamina"] = {
            ["H3220"] = 4895,
            ["mr"] = 4895,
        },
        ["Green Lens of Shadow Wrath"] = {
            ["H3220"] = 240000,
            ["mr"] = 240000,
        },
        ["Hawkeye's Gloves"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Vision Dust"] = {
            ["mr"] = 2500,
            ["H3254"] = 2500,
            ["H3220"] = 1705,
        },
        ["Hawkeye's Epaulets"] = {
            ["mr"] = 19000,
            ["cc"] = 4,
            ["id"] = "14596:0:0:0:0",
            ["H3246"] = 19000,
            ["sc"] = 2,
        },
        ["Magician's Mantle"] = {
            ["H3220"] = 27900,
            ["mr"] = 27900,
        },
        ["Willow Belt of the Monkey"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Swiftwind"] = {
            ["H3220"] = 77500,
            ["mr"] = 77500,
        },
        ["Formula: Enchant Chest - Lesser Mana"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Steadfast Cloak of the Bear"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Nightsky Wristbands"] = {
            ["H3220"] = 4599,
            ["mr"] = 4599,
        },
        ["Tigerstrike Mantle"] = {
            ["H3220"] = 85000,
            ["mr"] = 85000,
        },
        ["Barbaric Bracers"] = {
            ["H3220"] = 13800,
            ["mr"] = 13800,
        },
        ["Lupine Slippers of the Gorilla"] = {
            ["H3220"] = 888,
            ["mr"] = 888,
        },
        ["Huntsman's Belt of the Falcon"] = {
            ["H3220"] = 12554,
            ["mr"] = 12554,
        },
        ["Polished Zweihander of the Whale"] = {
            ["H3220"] = 5876,
            ["mr"] = 5876,
        },
        ["Barbarian War Axe of the Monkey"] = {
            ["H3224"] = 13200,
            ["sc"] = 1,
            ["id"] = "3201:0:0:601:0",
            ["mr"] = 13200,
            ["cc"] = 2,
        },
        ["Grunt's Pauldrons of the Bear"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Sorcerer Cloak of Intellect"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Superior Gloves of the Bear"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Gleaming Claymore of the Eagle"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Heavy Shortbow"] = {
            ["H3220"] = 1159,
            ["mr"] = 1159,
        },
        ["Delicious Cave Mold"] = {
            ["H3220"] = 241,
            ["mr"] = 241,
        },
        ["Dwarven Magestaff of Shadow Wrath"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Gleaming Claymore of Power"] = {
            ["H3220"] = 1888,
            ["mr"] = 1888,
        },
        ["White Leather Bag"] = {
            ["H3220"] = 877,
            ["mr"] = 877,
        },
        ["Councillor's Cloak of the Owl"] = {
            ["H3220"] = 25500,
            ["mr"] = 25500,
        },
        ["Decapitating Sword of Stamina"] = {
            ["H3220"] = 6999,
            ["mr"] = 6999,
        },
        ["Stonecutter Claymore of the Bear"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Hematite Link of Shadow Resistance"] = {
            ["H3220"] = 11912,
            ["mr"] = 11912,
        },
        ["Wrangler's Gloves of the Owl"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Mana Potion"] = {
            ["H3220"] = 387,
            ["mr"] = 387,
        },
        ["Willow Vest of the Owl"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Aurora Sash"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Scaled Leather Belt of the Bear"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Spiked Chain Leggings of Strength"] = {
            ["H3220"] = 7800,
            ["mr"] = 7800,
        },
        ["Shredder Operating Manual - Page 5"] = {
            ["mr"] = 399,
            ["cc"] = 15,
            ["id"] = "16649:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 399,
        },
        ["Royal Trousers of the Eagle"] = {
            ["H3220"] = 49600,
            ["mr"] = 49600,
        },
        ["Quartz Ring of Frost Resistance"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Double-barreled Shotgun"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Shimmering Robe of the Owl"] = {
            ["H3220"] = 14040,
            ["mr"] = 14040,
        },
        ["Earthroot"] = {
            ["mr"] = 615,
            ["H3254"] = 615,
            ["H3220"] = 25,
        },
        ["Forest Leather Chestpiece"] = {
            ["mr"] = 2100,
            ["cc"] = 4,
            ["id"] = "3055:0:0:0:0",
            ["sc"] = 2,
            ["H3232"] = 2100,
        },
        ["Pagan Cape of Fiery Wrath"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Green Lens of Nature's Wrath"] = {
            ["H3220"] = 43200,
            ["mr"] = 43200,
        },
        ["Glimmering Flamberge of Strength"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Sorcerer Slippers of Spirit"] = {
            ["H3220"] = 12164,
            ["mr"] = 12164,
        },
        ["Starfaller"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Shimmering Cloak of the Eagle"] = {
            ["H3220"] = 1536,
            ["mr"] = 1536,
        },
        ["Medicine Staff of Intellect"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Iron Grenade"] = {
            ["H3220"] = 899,
            ["mr"] = 899,
        },
        ["Sagefish Delight"] = {
            ["mr"] = 1017,
            ["H3254"] = 1017,
            ["H3220"] = 447,
        },
        ["Rigid Bracelets of the Monkey"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Elder's Mantle of Frozen Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Tromping Miner's Boots"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Gyrochronatom"] = {
            ["H3220"] = 4778,
            ["mr"] = 4778,
        },
        ["Sentry's Armsplints of Strength"] = {
            ["H3220"] = 3800,
            ["mr"] = 3800,
        },
        ["Scaled Leather Headband of the Bear"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Watcher's Handwraps of Fiery Wrath"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Hunter's Muzzle Loader"] = {
            ["H3220"] = 1495,
            ["mr"] = 1495,
        },
        ["Thallium Choker of Strength"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Grunt's Bracers of the Bear"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Schematic: Mechanical Dragonling"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Warrior's Tunic"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Fortified Bracers of the Bear"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Lupine Handwraps of the Eagle"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Pattern: Green Silk Pack"] = {
            ["mr"] = 2999,
            ["sc"] = 2,
            ["id"] = "5774:0:0:0:0",
            ["H3229"] = 2999,
            ["cc"] = 9,
        },
        ["Conjurer's Mantle of Shadow Wrath"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Banded Helm of the Bear"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Relic Coffer Key"] = {
            ["mr"] = 2494,
            ["sc"] = 0,
            ["id"] = "11078:0:0:0:0",
            ["H3246"] = 2494,
            ["cc"] = 13,
        },
        ["Thornspike"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Bloodspattered Shield of Strength"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Merc Sword of Strength"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Dark Leather Cloak"] = {
            ["H3220"] = 683,
            ["mr"] = 683,
        },
        ["Red Rose"] = {
            ["H3220"] = 8483,
            ["mr"] = 8483,
        },
        ["Raincaller Cord of the Owl"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Sturdy Quarterstaff of the Wolf"] = {
            ["H3220"] = 1455,
            ["mr"] = 1455,
        },
        ["Willow Robe of the Owl"] = {
            ["H3220"] = 3700,
            ["mr"] = 3700,
        },
        ["Small Glimmering Shard"] = {
            ["mr"] = 198,
            ["cc"] = 7,
            ["L3220"] = 198,
            ["id"] = "10978:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 199,
        },
        ["Infiltrator Gloves of the Eagle"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Spiked Chain Cloak of the Bear"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Free Action Potion"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Lambent Scale Boots"] = {
            ["H3220"] = 7865,
            ["mr"] = 7865,
        },
        ["Spiritchaser Staff of Fiery Wrath"] = {
            ["H3220"] = 68900,
            ["mr"] = 68900,
        },
        ["Formula: Enchant Cloak - Lesser Agility"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Sentry's Surcoat of the Tiger"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Huge Venom Sac"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Massive Battle Axe of the Whale"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Lupine Leggings of the Eagle"] = {
            ["H3220"] = 1459,
            ["mr"] = 1459,
        },
        ["Pattern: Swift Boots"] = {
            ["mr"] = 2399,
            ["cc"] = 9,
            ["id"] = "7453:0:0:0:0",
            ["L3229"] = 2399,
            ["H3229"] = 5700,
            ["sc"] = 1,
        },
        ["Elder's Bracers of Fiery Wrath"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Deadly Kris of Stamina"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Recipe: Transmute Mithril to Truesilver"] = {
            ["H3220"] = 8999,
            ["mr"] = 8999,
        },
        ["Scouting Buckler of Stamina"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Devilsaur Leggings"] = {
            ["mr"] = 1800000,
            ["cc"] = 4,
            ["id"] = "15062:0:0:0:0",
            ["H3246"] = 1800000,
            ["sc"] = 2,
        },
        ["Pattern: Runecloth Bag"] = {
            ["mr"] = 286969,
            ["sc"] = 2,
            ["H3239"] = 286969,
            ["id"] = "14468:0:0:0:0",
            ["cc"] = 9,
        },
        ["Grizzly Buckler of the Whale"] = {
            ["H3220"] = 915,
            ["mr"] = 915,
        },
        ["Hook Dagger of Frozen Wrath"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Heavy Blasting Powder"] = {
            ["mr"] = 800,
            ["H3254"] = 800,
            ["H3220"] = 319,
        },
        ["Sanguine Cuffs"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Acrobatic Staff of the Monkey"] = {
            ["H3220"] = 13772,
            ["mr"] = 13772,
        },
        ["Aboriginal Footwraps of the Falcon"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Tainted Pierce"] = {
            ["H3220"] = 55500,
            ["mr"] = 55500,
        },
        ["Lupine Buckler of the Owl"] = {
            ["H3220"] = 794,
            ["mr"] = 794,
        },
        ["Wicked Chain Shield of the Gorilla"] = {
            ["H3220"] = 6300,
            ["mr"] = 6300,
        },
        ["Phalanx Boots of the Bear"] = {
            ["mr"] = 4500,
            ["cc"] = 4,
            ["id"] = "7417:0:0:1196:0",
            ["sc"] = 3,
            ["H3252"] = 4500,
        },
        ["Scholarly Robes"] = {
            ["H3220"] = 2089,
            ["mr"] = 2089,
        },
        ["Phalanx Boots of the Tiger"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Grunt's AnkleWraps of the Bear"] = {
            ["H3220"] = 1120,
            ["mr"] = 1120,
        },
        ["Witchfury"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Infiltrator Boots of Agility"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Spiked Chain Cloak of Stamina"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Shimmering Stave of the Eagle"] = {
            ["H3220"] = 4827,
            ["mr"] = 4827,
        },
        ["Recipe: Crispy Lizard Tail"] = {
            ["H3220"] = 3564,
            ["mr"] = 3564,
        },
        ["Bright Boots"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Recipe: Nightfin Soup"] = {
            ["H3220"] = 39600,
            ["mr"] = 39600,
        },
        ["Elixir of Minor Fortitude"] = {
            ["H3220"] = 22,
            ["mr"] = 22,
        },
        ["Formula: Enchant Bracer - Superior Strength"] = {
            ["H3254"] = 2350000,
            ["mr"] = 2350000,
        },
        ["Worn Mail Vest"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Dwarven Magestaff of the Bear"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Grunt's Legguards of the Whale"] = {
            ["H3220"] = 6280,
            ["mr"] = 6280,
        },
        ["Mild Spices"] = {
            ["H3220"] = 5,
            ["mr"] = 5,
        },
        ["Hulking Gauntlets"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Precision Arrow"] = {
            ["H3220"] = 22,
            ["mr"] = 22,
        },
        ["Sentinel Cloak of the Owl"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Feral Buckler of Stamina"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Burnished Leggings"] = {
            ["H3220"] = 1266,
            ["mr"] = 1266,
        },
        ["Ravager's Armor"] = {
            ["H3220"] = 19899,
            ["mr"] = 19899,
        },
        ["Wicked Chain Cloak of the Bear"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Melon Juice"] = {
            ["H3220"] = 41,
            ["mr"] = 41,
        },
        ["Tender Crab Meat"] = {
            ["H3220"] = 207,
            ["mr"] = 207,
        },
        ["Bandit Gloves of the Monkey"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Ivory Band of the Gorilla"] = {
            ["H3220"] = 4422,
            ["mr"] = 4422,
        },
        ["Archer's Belt of Healing"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Shimmering Trousers of Fiery Wrath"] = {
            ["H3220"] = 6043,
            ["mr"] = 6043,
        },
        ["Vital Sash of Intellect"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Sergeant's Warhammer of Healing"] = {
            ["H3220"] = 1700,
            ["mr"] = 1700,
        },
        ["Training Sword of the Eagle"] = {
            ["H3220"] = 599,
            ["mr"] = 599,
        },
        ["Ranger Helm of the Monkey"] = {
            ["mr"] = 9900,
            ["sc"] = 2,
            ["id"] = "7479:0:0:613:0",
            ["H3246"] = 9900,
            ["cc"] = 4,
        },
        ["Dervish Boots of the Monkey"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Twilight Mantle of the Owl"] = {
            ["H3220"] = 14400,
            ["mr"] = 14400,
        },
        ["Hunting Pants"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Green Woolen Bag"] = {
            ["mr"] = 1185,
            ["cc"] = 1,
            ["id"] = "4241:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 1185,
        },
        ["Face Smasher"] = {
            ["H3220"] = 17600,
            ["mr"] = 17600,
        },
        ["Battle Knife of Strength"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Pillager's Girdle of the Monkey"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Huntsman's Cap of the Monkey"] = {
            ["mr"] = 15600,
            ["cc"] = 4,
            ["id"] = "9889:0:0:613:0",
            ["H3246"] = 15600,
            ["sc"] = 2,
        },
        ["Sorcerer Hat of the Eagle"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Elder's Gloves of the Falcon"] = {
            ["H3220"] = 2499,
            ["mr"] = 2499,
        },
        ["Renegade Leggings of the Bear"] = {
            ["H3220"] = 9628,
            ["mr"] = 9628,
        },
        ["Short Bastard Sword of the Eagle"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Resilient Robe"] = {
            ["H3220"] = 4599,
            ["mr"] = 4599,
        },
        ["Bloodspattered Sabatons of Stamina"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Shadow Goggles"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Silvered Bronze Leggings"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Greenstone Circle of the Gorilla"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Minor Magic Resistance Potion"] = {
            ["H3220"] = 212,
            ["mr"] = 212,
        },
        ["Elder's Bracers of Arcane Wrath"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Forest Leather Bracers"] = {
            ["H3220"] = 12999,
            ["mr"] = 12999,
        },
        ["Dream Dust"] = {
            ["H3220"] = 1915,
            ["mr"] = 1915,
        },
        ["Greenweave Sandals of the Whale"] = {
            ["H3220"] = 4600,
            ["mr"] = 4600,
        },
        ["Merc Sword of the Eagle"] = {
            ["H3220"] = 9750,
            ["mr"] = 9750,
        },
        ["Priest's Mace of Stamina"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Banded Armor of the Bear"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Phalanx Cloak of the Gorilla"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Blisterbane Wrap"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Mithril Blunderbuss"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Bandit Cloak of Spirit"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Rigid Buckler of the Bear"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Stonecloth Britches"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Sergeant's Warhammer of Strength"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Recipe: Elixir of the Sages"] = {
            ["H3220"] = 200500,
            ["mr"] = 200500,
        },
        ["Pagan Mitts of the Eagle"] = {
            ["H3220"] = 3499,
            ["mr"] = 3499,
        },
        ["Splitting Hatchet of the Tiger"] = {
            ["H3220"] = 8728,
            ["mr"] = 8728,
        },
        ["Saltstone Girdle of the Tiger"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Hacking Cleaver of Stamina"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Wintersbite"] = {
            ["H3220"] = 1455,
            ["mr"] = 1455,
        },
        ["Pathfinder Hat of the Eagle"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Green Hills of Stranglethorn - Page 26"] = {
            ["mr"] = 645,
            ["H3254"] = 645,
            ["H3220"] = 493,
        },
        ["Scouting Gloves of the Whale"] = {
            ["H3220"] = 1298,
            ["mr"] = 1298,
        },
        ["Durable Gloves of the Eagle"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Pillager's Bracers of the Bear"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["The Black Knight"] = {
            ["H3220"] = 16899,
            ["mr"] = 16899,
        },
        ["Windchaser Cinch"] = {
            ["H3220"] = 12200,
            ["mr"] = 12200,
        },
        ["Pattern: Heavy Woolen Cloak"] = {
            ["mr"] = 800,
            ["sc"] = 2,
            ["id"] = "4346:0:0:0:0",
            ["H3229"] = 800,
            ["cc"] = 9,
        },
        ["Warbringer's Sabatons  of the Bear"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Formula: Enchant Boots - Greater Stamina"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Skullcrusher Mace of the Monkey"] = {
            ["mr"] = 59000,
            ["sc"] = 4,
            ["id"] = "1608:0:0:596:0",
            ["L3246"] = 59000,
            ["H3246"] = 227280,
            ["cc"] = 2,
        },
        ["The Horde's Hellscream"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Rusty Hatchet"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Medium Hide"] = {
            ["H3220"] = 299,
            ["mr"] = 299,
        },
        ["Ivycloth Gloves of Spirit"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Mercurial Gauntlets of Healing"] = {
            ["H3220"] = 45000,
            ["mr"] = 45000,
        },
        ["Lesser Wizard's Robe"] = {
            ["H3220"] = 8700,
            ["mr"] = 8700,
        },
        ["Pathfinder Hat of the Whale"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Wicked Chain Gauntlets of the Bear"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Hibernal Mantle"] = {
            ["H3220"] = 17670,
            ["mr"] = 17670,
        },
        ["Silk Bandage"] = {
            ["H3220"] = 215,
            ["mr"] = 215,
        },
        ["Robust Leggings of the Whale"] = {
            ["H3220"] = 3025,
            ["mr"] = 3025,
        },
        ["Thistlefur Bands of Stamina"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Soft-soled Linen Boots"] = {
            ["H3220"] = 695,
            ["mr"] = 695,
        },
        ["Chromite Bracers"] = {
            ["H3220"] = 11998,
            ["mr"] = 11998,
        },
        ["Acrobatic Staff of Healing"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Chief Brigadier Pauldrons"] = {
            ["H3220"] = 8699,
            ["mr"] = 8699,
        },
        ["Shadow Hood"] = {
            ["H3220"] = 17000,
            ["mr"] = 17000,
        },
        ["Formula: Enchant Chest - Minor Mana"] = {
            ["H3220"] = 100,
            ["mr"] = 100,
        },
        ["Opulent Cape of Frozen Wrath"] = {
            ["H3220"] = 38878,
            ["mr"] = 38878,
        },
        ["Light Leather Quiver"] = {
            ["H3220"] = 94,
            ["mr"] = 94,
        },
        ["Coarse Grinding Stone"] = {
            ["mr"] = 495,
            ["cc"] = 7,
            ["id"] = "3478:0:0:0:0",
            ["sc"] = 0,
            ["H3225"] = 495,
        },
        ["Formula: Enchant Bracer - Lesser Spirit"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Warchief Kilt"] = {
            ["H3220"] = 19143,
            ["mr"] = 19143,
        },
        ["Renegade Pauldrons of the Bear"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Vital Headband of Intellect"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Green Hills of Stranglethorn - Page 11"] = {
            ["mr"] = 790,
            ["H3254"] = 790,
            ["H3220"] = 497,
        },
        ["Nightsky Cloak"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Pathfinder Footpads of the Whale"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Nightsky Sash"] = {
            ["H3220"] = 2499,
            ["mr"] = 2499,
        },
        ["Enchanted Kodo Bracers"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Phalanx Leggings of the Whale"] = {
            ["H3220"] = 11823,
            ["mr"] = 11823,
        },
        ["Plans: Golden Scale Leggings"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Dokebi Leggings"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Spiked Chain Leggings of Power"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Wrangler's Gloves of Spirit"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Renegade Gauntlets of the Bear"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Plans: Mighty Iron Hammer"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Willow Pants of the Whale"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Cord of Elements"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Barbaric Leggings"] = {
            ["H3220"] = 5948,
            ["mr"] = 5948,
        },
        ["Green Hills of Stranglethorn - Page 21"] = {
            ["mr"] = 1070,
            ["H3254"] = 1070,
            ["H3220"] = 482,
        },
        ["Grunt's Belt of the Bear"] = {
            ["H3220"] = 1865,
            ["mr"] = 1865,
        },
        ["Aurora Gloves"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Patterned Bronze Bracers"] = {
            ["mr"] = 8399,
            ["H3254"] = 8399,
            ["H3220"] = 3500,
        },
        ["Staff of the Friar"] = {
            ["H3220"] = 36800,
            ["mr"] = 36800,
        },
        ["Basalt Necklace of the Gorilla"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Twin-bladed Axe of the Monkey"] = {
            ["mr"] = 160000,
            ["sc"] = 1,
            ["id"] = "15268:0:0:590:0",
            ["L3246"] = 160000,
            ["H3246"] = 320000,
            ["cc"] = 2,
        },
        ["Ivycloth Gloves of the Eagle"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Durable Belt of Fiery Wrath"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Barbaric Linen Vest"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Pattern: Pilferer's Gloves"] = {
            ["mr"] = 975,
            ["sc"] = 1,
            ["id"] = "7363:0:0:0:0",
            ["H3229"] = 975,
            ["cc"] = 9,
        },
        ["Robust Gloves of the Wolf"] = {
            ["H3220"] = 11880,
            ["mr"] = 11880,
        },
        ["Giant Club of Power"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Greenweave Branch of Frozen Wrath"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Massive Battle Axe of the Wolf"] = {
            ["H3220"] = 5200,
            ["mr"] = 5200,
        },
        ["Fortified Bracers of the Gorilla"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Bloodscalp Channeling Staff"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Cutthroat's Pants of the Owl"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Conjurer's Gloves of Fiery Wrath"] = {
            ["H3220"] = 4804,
            ["mr"] = 4804,
        },
        ["Bloodspattered Sabatons of the Bear"] = {
            ["H3220"] = 1200,
            ["mr"] = 1200,
        },
        ["Recipe: Elixir of Poison Resistance"] = {
            ["H3220"] = 1985,
            ["mr"] = 1985,
        },
        ["Conjurer's Robe of the Eagle"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Sentinel Shoulders of the Monkey"] = {
            ["H3220"] = 10700,
            ["mr"] = 10700,
        },
        ["Outrunner's Slippers of Defense"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Brutal War Axe of the Eagle"] = {
            ["H3220"] = 12222,
            ["mr"] = 12222,
        },
        ["Swiftthistle"] = {
            ["H3220"] = 370,
            ["mr"] = 370,
        },
        ["Recipe: Elixir of Superior Defense"] = {
            ["H3220"] = 24900,
            ["mr"] = 24900,
        },
        ["Greenweave Sandals of the Eagle"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Scouting Cloak of the Monkey"] = {
            ["mr"] = 3000,
            ["cc"] = 4,
            ["id"] = "6585:0:0:587:0",
            ["H3246"] = 3000,
            ["sc"] = 1,
        },
        ["Silksand Gloves"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Firefin Snapper"] = {
            ["mr"] = 744,
            ["H3254"] = 744,
            ["H3220"] = 12,
        },
        ["Sage's Pants of the Eagle"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Glimmering Flamberge of the Eagle"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Conjurer's Sphere of the Owl"] = {
            ["H3220"] = 13100,
            ["mr"] = 13100,
        },
        ["Knight's Boots of Defense"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Magefist Gloves"] = {
            ["H3220"] = 59900,
            ["mr"] = 59900,
        },
        ["Swamp Pendant of the Monkey"] = {
            ["mr"] = 39999,
            ["L3246"] = 39999,
            ["id"] = "12045:0:0:607:0",
            ["sc"] = 0,
            ["H3246"] = 47588,
            ["cc"] = 4,
        },
        ["Raptor Hide"] = {
            ["H3220"] = 206,
            ["mr"] = 206,
        },
        ["Sentinel Girdle of the Owl"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Fortified Chain of the Whale"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Magician Staff of the Whale"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Jagged Piece of Stone"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Cuergo's Treasure Map"] = {
            ["H3220"] = 11500,
            ["mr"] = 11500,
        },
        ["Furious Falchion of the Tiger"] = {
            ["H3220"] = 38000,
            ["mr"] = 38000,
        },
        ["Sentry's Gloves of the Gorilla"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Plans: Runed Copper Breastplate"] = {
            ["H3220"] = 249,
            ["mr"] = 249,
        },
        ["Robust Tunic of the Monkey"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Spiked Chain Shield of the Tiger"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Bloodwoven Mitts of Healing"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Dokebi Cord"] = {
            ["H3220"] = 1199,
            ["mr"] = 1199,
        },
        ["Mystical Headwrap of the Owl"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Battleforge Shield of Strength"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Diamond-Tip Bludgeon of Power"] = {
            ["H3220"] = 30562,
            ["mr"] = 30562,
        },
        ["Symbolic Pauldrons"] = {
            ["H3220"] = 37578,
            ["mr"] = 37578,
        },
        ["Seer's Gloves"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Ancona Chicken"] = {
            ["H3220"] = 17500,
            ["mr"] = 17500,
        },
        ["Cresting Charm"] = {
            ["H3220"] = 3413,
            ["mr"] = 3413,
        },
        ["Nature Protection Potion"] = {
            ["H3220"] = 1483,
            ["mr"] = 1483,
        },
        ["Phalanx Gauntlets of the Boar"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Plans: Dark Iron Plate"] = {
            ["H3220"] = 125000,
            ["mr"] = 125000,
        },
        ["Shimmering Boots of the Whale"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Charger's Armor of the Tiger"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Jazeraint Belt of the Falcon"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Plans: Iron Shield Spike"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Pagan Cape of the Owl"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Ritual Leggings of Spirit"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Heavy Marauder Scimitar"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Ghostwalker Belt of Healing"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Captain's Bracers of the Wolf"] = {
            ["H3220"] = 39216,
            ["mr"] = 39216,
        },
        ["Battlecaller Gauntlets"] = {
            ["H3220"] = 200000,
            ["mr"] = 200000,
        },
        ["Sentinel Breastplate of the Eagle"] = {
            ["H3220"] = 11000,
            ["mr"] = 11000,
        },
        ["War Torn Pants of the Bear"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["War Torn Pants of the Tiger"] = {
            ["H3220"] = 714,
            ["mr"] = 714,
        },
        ["Antipodean Rod"] = {
            ["H3220"] = 48977,
            ["mr"] = 48977,
        },
        ["Sniper Rifle of the Wolf"] = {
            ["H3220"] = 45988,
            ["mr"] = 45988,
        },
        ["Scaled Cloak of Defense"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Thistlefur Branch of the Owl"] = {
            ["H3220"] = 6900,
            ["mr"] = 6900,
        },
        ["Viking Sword of Stamina"] = {
            ["H3220"] = 8542,
            ["mr"] = 8542,
        },
        ["Amber Hoop of Fire Resistance"] = {
            ["H3220"] = 1669,
            ["mr"] = 1669,
        },
        ["Native Branch of the Wolf"] = {
            ["H3220"] = 1717,
            ["mr"] = 1717,
        },
        ["Infiltrator Pants of the Whale"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Archer's Gloves of the Owl"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Raider's Legguards of the Bear"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Slayer's Shoulder Pads"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["Wolffear Harness"] = {
            ["H3220"] = 73300,
            ["mr"] = 73300,
        },
        ["Banded Pauldrons of the Eagle"] = {
            ["H3220"] = 4699,
            ["mr"] = 4699,
        },
        ["Devilsaur Gauntlets"] = {
            ["mr"] = 990000,
            ["cc"] = 4,
            ["id"] = "15063:0:0:0:0",
            ["H3246"] = 990000,
            ["sc"] = 2,
        },
        ["Scouting Cloak of the Whale"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Bloody Brass Knuckles"] = {
            ["mr"] = 6999,
            ["L3246"] = 6999,
            ["id"] = "7683:0:0:0:0",
            ["sc"] = 13,
            ["H3246"] = 7000,
            ["cc"] = 2,
        },
        ["Raider's Gauntlets of Strength"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Pattern: Green Dragonscale Breastplate"] = {
            ["mr"] = 50000,
            ["cc"] = 9,
            ["id"] = "15726:0:0:0:0",
            ["sc"] = 1,
            ["H3227"] = 50000,
        },
        ["Wildvine Potion"] = {
            ["mr"] = 2900,
            ["cc"] = 0,
            ["H3226"] = 2900,
            ["id"] = "9144:0:0:0:0",
            ["sc"] = 0,
        },
        ["Huntsman's Cape of Defense"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Infiltrator Armor of Stamina"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Pattern: Hands of Darkness"] = {
            ["mr"] = 60000,
            ["sc"] = 2,
            ["id"] = "7092:0:0:0:0",
            ["H3229"] = 60000,
            ["cc"] = 9,
        },
        ["Fire Protection Potion"] = {
            ["H3220"] = 1980,
            ["mr"] = 1980,
        },
        ["Shimmering Gloves of Healing"] = {
            ["H3220"] = 1595,
            ["mr"] = 1595,
        },
        ["Short Bastard Sword of the Monkey"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Conjurer's Cloak of Intellect"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Deviate Scale"] = {
            ["H3220"] = 20,
            ["mr"] = 20,
        },
        ["Barbarian War Axe of the Eagle"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Grunt's Shield of the Bear"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Raptor Punch"] = {
            ["H3220"] = 88,
            ["mr"] = 88,
        },
        ["Jungle Remedy"] = {
            ["H3220"] = 550,
            ["mr"] = 550,
        },
        ["Brigade Circlet of Healing"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Shredder Operating Manual - Page 6"] = {
            ["mr"] = 165,
            ["cc"] = 15,
            ["id"] = "16650:0:0:0:0",
            ["sc"] = 0,
            ["H3227"] = 165,
        },
        ["Soldier's Leggings of the Bear"] = {
            ["H3220"] = 898,
            ["mr"] = 898,
        },
        ["Raw Summer Bass"] = {
            ["H3220"] = 1428,
            ["mr"] = 1428,
        },
        ["Elixir of Poison Resistance"] = {
            ["H3220"] = 1490,
            ["mr"] = 1490,
        },
        ["Soul Dust"] = {
            ["mr"] = 812,
            ["H3254"] = 812,
            ["H3220"] = 405,
        },
        ["Robust Gloves of the Whale"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["River Pride Choker"] = {
            ["H3220"] = 43875,
            ["mr"] = 43875,
        },
        ["Wolf Rider's Leggings of the Bear"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Blade of the Titans"] = {
            ["H3220"] = 99999,
            ["mr"] = 99999,
        },
        ["Sickly Looking Fish"] = {
            ["H3220"] = 44,
            ["mr"] = 44,
        },
        ["Stonecloth Cape"] = {
            ["H3220"] = 6300,
            ["mr"] = 6300,
        },
        ["Scroll of Strength"] = {
            ["mr"] = 875,
            ["H3254"] = 875,
            ["H3220"] = 90,
        },
        ["Dreamweave Circlet"] = {
            ["H3220"] = 84500,
            ["mr"] = 84500,
        },
        ["Bard's Trousers of Power"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Spiced Wolf Meat"] = {
            ["H3220"] = 73,
            ["mr"] = 73,
        },
        ["Vital Tunic of the Eagle"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Large Opal"] = {
            ["H3220"] = 69499,
            ["mr"] = 69499,
        },
        ["Pathfinder Cloak of the Monkey"] = {
            ["H3220"] = 2903,
            ["mr"] = 2903,
        },
        ["Brigade Pauldrons of Stamina"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Simple Branch of the Eagle"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Liferoot"] = {
            ["H3220"] = 499,
            ["mr"] = 499,
        },
        ["Pattern: Brightcloth Robe"] = {
            ["mr"] = 10500,
            ["cc"] = 9,
            ["id"] = "14478:0:0:0:0",
            ["sc"] = 2,
            ["H3227"] = 10500,
        },
        ["Elixir of Wisdom"] = {
            ["H3220"] = 149,
            ["mr"] = 149,
        },
        ["Buccaneer's Boots of Stamina"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Keeper's Cloak"] = {
            ["H3220"] = 18700,
            ["mr"] = 18700,
        },
        ["Dwarven Magestaff of Frozen Wrath"] = {
            ["H3220"] = 15102,
            ["mr"] = 15102,
        },
        ["Pillager's Girdle of the Bear"] = {
            ["H3220"] = 10300,
            ["mr"] = 10300,
        },
        ["Green Silk Pack"] = {
            ["H3220"] = 4899,
            ["mr"] = 4899,
        },
        ["Durable Cape of the Owl"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Whipwood Recurve Bow"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Pioneer Trousers of Power"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Conjurer's Shoes of the Owl"] = {
            ["H3220"] = 8200,
            ["mr"] = 8200,
        },
        ["Poisoned Spider Fang"] = {
            ["mr"] = 587,
            ["cc"] = 15,
            ["id"] = "3931:0:0:0:0",
            ["sc"] = 0,
            ["H3232"] = 587,
        },
        ["Scaled Leather Leggings of Spirit"] = {
            ["H3220"] = 9093,
            ["mr"] = 9093,
        },
        ["Slayer's Pants"] = {
            ["H3220"] = 5049,
            ["mr"] = 5049,
        },
        ["Rigid Buckler of the Boar"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Boulder Pads"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Elixir of Lion's Strength"] = {
            ["H3220"] = 31,
            ["mr"] = 31,
        },
        ["Greenweave Gloves of the Whale"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Heraldic Gloves"] = {
            ["H3220"] = 34800,
            ["mr"] = 34800,
        },
        ["Pattern: Guardian Leather Bracers"] = {
            ["mr"] = 4680,
            ["cc"] = 9,
            ["id"] = "4300:0:0:0:0",
            ["L3229"] = 4680,
            ["H3229"] = 4900,
            ["sc"] = 1,
        },
        ["Clamlette Surprise"] = {
            ["H3220"] = 450,
            ["mr"] = 450,
        },
        ["Scarlet Gauntlets"] = {
            ["H3220"] = 9277,
            ["mr"] = 9277,
        },
        ["Jazeraint Cloak of Stamina"] = {
            ["H3220"] = 4555,
            ["mr"] = 4555,
        },
        ["Battleforge Boots of the Boar"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Accurate Scope"] = {
            ["H3220"] = 4844,
            ["mr"] = 4844,
        },
        ["Gossamer Robe of Spirit"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Greenweave Sandals of Stamina"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Rockshard Pellets"] = {
            ["H3220"] = 90,
            ["mr"] = 90,
        },
        ["Seer's Mantle"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Stealthblade"] = {
            ["H3220"] = 85000,
            ["mr"] = 85000,
        },
        ["Recipe: Brilliant Smallfish"] = {
            ["H3220"] = 10567,
            ["mr"] = 10567,
        },
        ["Gray Woolen Shirt"] = {
            ["H3220"] = 2203,
            ["mr"] = 2203,
        },
        ["Infiltrator Cap of the Eagle"] = {
            ["H3220"] = 12200,
            ["mr"] = 12200,
        },
        ["Staunch Hammer of Stamina"] = {
            ["H3220"] = 955,
            ["mr"] = 955,
        },
        ["Ritual Stein of the Owl"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Slaghammer"] = {
            ["H3220"] = 58000,
            ["mr"] = 58000,
        },
        ["Rageclaw Shoulder Pads of the Monkey"] = {
            ["H3220"] = 192343,
            ["mr"] = 192343,
        },
        ["Recipe: Poached Sunscale Salmon"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Short Bastard Sword of Agility"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Overlord's Crown of the Bear"] = {
            ["H3220"] = 20600,
            ["mr"] = 20600,
        },
        ["Recipe: Elixir of Shadow Power"] = {
            ["H3220"] = 69000,
            ["mr"] = 69000,
        },
        ["Thallium Choker of Stamina"] = {
            ["H3220"] = 10100,
            ["mr"] = 10100,
        },
        ["Red Leather Bag"] = {
            ["H3220"] = 899,
            ["mr"] = 899,
        },
        ["Dwarven Magestaff of the Wolf"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Ghostwalker Gloves of the Owl"] = {
            ["H3220"] = 9133,
            ["mr"] = 9133,
        },
        ["Acrobatic Staff of the Whale"] = {
            ["H3220"] = 9400,
            ["mr"] = 9400,
        },
        ["Gossamer Pants of Spirit"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Feral Buckler of the Monkey"] = {
            ["mr"] = 4999,
            ["cc"] = 4,
            ["id"] = "15307:0:0:587:0",
            ["H3246"] = 4999,
            ["sc"] = 6,
        },
        ["Banded Leggings of the Gorilla"] = {
            ["H3220"] = 37956,
            ["mr"] = 37956,
        },
        ["Topaz Ring of Fire Resistance"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Knight's Breastplate of Strength"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Sorcerer Pants of the Owl"] = {
            ["H3220"] = 31000,
            ["mr"] = 31000,
        },
        ["Steel Bar"] = {
            ["H3220"] = 2100,
            ["mr"] = 2100,
        },
        ["Plans: Inlaid Mithril Cylinder"] = {
            ["H3220"] = 14800,
            ["mr"] = 14800,
        },
        ["Tyrant's Chestpiece"] = {
            ["mr"] = 70000,
            ["H3254"] = 70000,
            ["H3220"] = 69900,
        },
        ["Infiltrator Cord of the Owl"] = {
            ["H3220"] = 4498,
            ["mr"] = 4498,
        },
        ["Headhunter's Headdress of the Monkey"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Pagan Rod of the Wolf"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Raincaller Mantle of the Wolf"] = {
            ["H3220"] = 3599,
            ["mr"] = 3599,
        },
        ["Superior Tunic of the Whale"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Aquamarine"] = {
            ["mr"] = 3413,
            ["H3254"] = 3413,
            ["H3220"] = 1540,
        },
        ["Sentry's Shoulderguards of the Bear"] = {
            ["H3231"] = 24912,
            ["mr"] = 24912,
            ["cc"] = 4,
            ["id"] = "15531:0:0:1191:0",
            ["sc"] = 3,
        },
        ["Webwing Cloak"] = {
            ["H3220"] = 1480,
            ["mr"] = 1480,
        },
        ["Tellurium Necklace of the Owl"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Nightscape Headband"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Sentinel Trousers of the Owl"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Viking Sword of the Monkey"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Goblin Rocket Fuel"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Nocturnal Tunic of Stamina"] = {
            ["H3220"] = 10200,
            ["mr"] = 10200,
        },
        ["Embossed Leather Boots"] = {
            ["H3220"] = 421,
            ["mr"] = 421,
        },
        ["Renegade Gauntlets of the Boar"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Cutthroat's Belt of the Owl"] = {
            ["H3220"] = 3744,
            ["mr"] = 3744,
        },
        ["Grizzly Pants of the Monkey"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Green Linen Shirt"] = {
            ["H3220"] = 98,
            ["mr"] = 98,
        },
        ["Huntsman's Gloves of Defense"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Pattern: Green Silk Armor"] = {
            ["mr"] = 944,
            ["cc"] = 9,
            ["id"] = "7090:0:0:0:0",
            ["L3229"] = 944,
            ["H3229"] = 2000,
            ["sc"] = 2,
        },
        ["Durable Tunic of Intellect"] = {
            ["H3220"] = 5700,
            ["mr"] = 5700,
        },
        ["Mechanical Chicken"] = {
            ["H3220"] = 7200,
            ["mr"] = 7200,
        },
        ["Superior Gloves of the Monkey"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Worn Leather Boots"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Dreamfoil"] = {
            ["H3220"] = 5895,
            ["mr"] = 5895,
        },
        ["Harpy Needler of the Wolf"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Phalanx Spaulders of the Boar"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Grizzly Pants of Spirit"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Wicked Claw"] = {
            ["H3220"] = 604,
            ["mr"] = 604,
        },
        ["Prairie Dog Whistle"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Ancient Greaves of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Renegade Shield of the Bear"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Raw Longjaw Mud Snapper"] = {
            ["H3220"] = 2,
            ["mr"] = 2,
        },
        ["Feral Cloak of Nature's Wrath"] = {
            ["H3220"] = 830,
            ["mr"] = 830,
        },
        ["Buccaneer's Orb of Healing"] = {
            ["H3220"] = 14900,
            ["mr"] = 14900,
        },
        ["Insignia Mantle"] = {
            ["H3220"] = 8499,
            ["mr"] = 8499,
        },
        ["Battlefield Destroyer of the Tiger"] = {
            ["H3220"] = 35000,
            ["mr"] = 35000,
        },
        ["Grunt's Shield of the Boar"] = {
            ["H3220"] = 3905,
            ["mr"] = 3905,
        },
        ["Elder's Mantle of the Wolf"] = {
            ["H3220"] = 21816,
            ["mr"] = 21816,
        },
        ["Stone Hammer of the Bear"] = {
            ["H3220"] = 23000,
            ["mr"] = 23000,
        },
        ["Superior Belt of the Monkey"] = {
            ["H3220"] = 3600,
            ["mr"] = 3600,
        },
        ["Hook Dagger of Stamina"] = {
            ["H3220"] = 3300,
            ["mr"] = 3300,
        },
        ["Thistlefur Belt of the Owl"] = {
            ["H3220"] = 2399,
            ["mr"] = 2399,
        },
        ["Mistscape Mantle"] = {
            ["H3220"] = 9000,
            ["mr"] = 9000,
        },
        ["Charged Scale"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Grizzly Jerkin of Nature's Wrath"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Aboriginal Loincloth of Spirit"] = {
            ["H3220"] = 570,
            ["mr"] = 570,
        },
        ["Thistlefur Branch of the Eagle"] = {
            ["H3220"] = 5555,
            ["mr"] = 5555,
        },
        ["Medicine Staff of the Boar"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Heavy Hide"] = {
            ["H3220"] = 270,
            ["mr"] = 270,
        },
        ["Parrot Cage (Cockatiel)"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Renegade Boots of the Tiger"] = {
            ["H3220"] = 8811,
            ["mr"] = 8811,
        },
        ["Pattern: Brightcloth Gloves"] = {
            ["mr"] = 15000,
            ["sc"] = 2,
            ["id"] = "14479:0:0:0:0",
            ["H3229"] = 15000,
            ["cc"] = 9,
        },
        ["Knight's Gauntlets of the Bear"] = {
            ["H3220"] = 6700,
            ["mr"] = 6700,
        },
        ["Colorful Kilt"] = {
            ["H3220"] = 1598,
            ["mr"] = 1598,
        },
        ["Pattern: Volcanic Breastplate"] = {
            ["mr"] = 15000,
            ["cc"] = 9,
            ["id"] = "15749:0:0:0:0",
            ["sc"] = 1,
            ["H3227"] = 15000,
        },
        ["Native Branch of the Whale"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Book: Gift of the Wild II"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Battering Hammer of the Gorilla"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Huntsman's Boots of Agility"] = {
            ["H3220"] = 12537,
            ["mr"] = 12537,
        },
        ["Pattern: Truefaith Gloves"] = {
            ["mr"] = 5400,
            ["cc"] = 9,
            ["id"] = "7091:0:0:0:0",
            ["L3229"] = 5400,
            ["H3229"] = 5500,
            ["sc"] = 2,
        },
        ["Outrunner's Gloves of the Eagle"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Field Plate Leggings of the Bear"] = {
            ["H3220"] = 24500,
            ["mr"] = 24500,
        },
        ["Simple Wood"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Captain's Circlet of Intellect"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Battering Hammer of the Boar"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Ghostwalker Legguards of the Eagle"] = {
            ["H3220"] = 12500,
            ["mr"] = 12500,
        },
        ["Brutish Belt of the Bear"] = {
            ["H3220"] = 15067,
            ["mr"] = 15067,
        },
        ["Sentry's Slippers of the Gorilla"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Ritual Shroud of the Owl"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Pattern: Frostsaber Leggings"] = {
            ["mr"] = 5300,
            ["sc"] = 1,
            ["id"] = "15747:0:0:0:0",
            ["H3229"] = 5300,
            ["cc"] = 9,
        },
        ["Resilient Cape"] = {
            ["H3220"] = 1940,
            ["mr"] = 1940,
        },
        ["Raincaller Mantle of Arcane Wrath"] = {
            ["H3220"] = 1899,
            ["mr"] = 1899,
        },
        ["Shiny Bauble"] = {
            ["H3220"] = 430,
            ["mr"] = 430,
        },
        ["Worn Dragonscale"] = {
            ["H3220"] = 975,
            ["mr"] = 975,
        },
        ["Rigid Cape of Agility"] = {
            ["H3220"] = 1698,
            ["mr"] = 1698,
        },
        ["Spiked Chain Gauntlets of the Boar"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Medicine Staff of the Monkey"] = {
            ["H3220"] = 2600,
            ["mr"] = 2600,
        },
        ["Formidable Sabatons of the Whale"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Brutish Shoulders of the Tiger"] = {
            ["H3220"] = 12018,
            ["mr"] = 12018,
        },
        ["Archon Chestpiece"] = {
            ["H3220"] = 22000,
            ["mr"] = 22000,
        },
        ["Schematic: Gyrofreeze Ice Reflector"] = {
            ["H3220"] = 68900,
            ["mr"] = 68900,
        },
        ["Militant Shortsword of the Tiger"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Heavy Woolen Pants"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Training Sword of the Bear"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Glimmering Shield"] = {
            ["H3220"] = 3518,
            ["mr"] = 3518,
        },
        ["Swashbuckler's Belt of the Monkey"] = {
            ["mr"] = 200000,
            ["cc"] = 4,
            ["id"] = "10190:0:0:616:0",
            ["H3246"] = 200000,
            ["sc"] = 2,
        },
        ["Jungle Ring of the Monkey"] = {
            ["mr"] = 126054,
            ["cc"] = 4,
            ["id"] = "12016:0:0:611:0",
            ["H3246"] = 126054,
            ["sc"] = 0,
        },
        ["Pattern: Greater Adept's Robe"] = {
            ["mr"] = 2400,
            ["cc"] = 9,
            ["id"] = "6275:0:0:0:0",
            ["sc"] = 2,
            ["H3227"] = 2400,
        },
        ["Sequoia Branch of the Boar"] = {
            ["H3220"] = 24000,
            ["mr"] = 24000,
        },
        ["Curved Dagger of Arcane Wrath"] = {
            ["H3220"] = 907,
            ["mr"] = 907,
        },
        ["Raven's Claws"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Rigid Leggings of the Monkey"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Spiked Chain Leggings of the Gorilla"] = {
            ["H3220"] = 4724,
            ["mr"] = 4724,
        },
        ["Schematic: Thorium Widget"] = {
            ["H3220"] = 17900,
            ["mr"] = 17900,
        },
        ["Bard's Tunic of Intellect"] = {
            ["H3220"] = 2098,
            ["mr"] = 2098,
        },
        ["Cadet Leggings of the Bear"] = {
            ["H3220"] = 1550,
            ["mr"] = 1550,
        },
        ["Wrangler's Belt of the Whale"] = {
            ["H3220"] = 2356,
            ["mr"] = 2356,
        },
        ["Elixir of Lesser Agility"] = {
            ["mr"] = 15783,
            ["H3254"] = 15783,
            ["H3220"] = 1500,
        },
        ["Crude Scope"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Golden Rod"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Sniper Rifle of the Tiger"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Headstriker Sword of Power"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Buccaneer's Pants of Fiery Wrath"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Sorcerer Cloak of Stamina"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Blackfang"] = {
            ["H3220"] = 11100,
            ["mr"] = 11100,
        },
        ["Scarlet Wristguards"] = {
            ["H3220"] = 2827,
            ["mr"] = 2827,
        },
        ["Grizzly Pants of Power"] = {
            ["H3220"] = 1050,
            ["mr"] = 1050,
        },
        ["Recipe: Curiously Tasty Omelet"] = {
            ["H3220"] = 5168,
            ["mr"] = 5168,
        },
        ["Mail Combat Armguards"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["Gloom Reaper of the Boar"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Vital Cape of the Eagle"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Runecloth Bag"] = {
            ["mr"] = 21000,
            ["cc"] = 1,
            ["H3239"] = 21000,
            ["id"] = "14046:0:0:0:0",
            ["sc"] = 0,
        },
        ["Elder's Mantle of Stamina"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Defender Girdle of Strength"] = {
            ["H3220"] = 3250,
            ["mr"] = 3250,
        },
        ["Rigid Leggings of the Owl"] = {
            ["H3220"] = 2276,
            ["mr"] = 2276,
        },
        ["Formula: Enchant Bracer - Lesser Strength"] = {
            ["H3220"] = 1790,
            ["mr"] = 1790,
        },
        ["Felcloth Robe"] = {
            ["mr"] = 396543,
            ["cc"] = 4,
            ["id"] = "14106:0:0:0:0",
            ["H3246"] = 396543,
            ["sc"] = 1,
        },
        ["Ring of Saviors"] = {
            ["H3220"] = 310000,
            ["mr"] = 310000,
        },
        ["Sentinel Girdle of the Eagle"] = {
            ["H3220"] = 6228,
            ["mr"] = 6228,
        },
        ["Bruiseweed"] = {
            ["H3220"] = 37,
            ["mr"] = 37,
        },
        ["Spiked Chain Breastplate of the Eagle"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Battle Knife of Fiery Wrath"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Truesilver Ore"] = {
            ["H3220"] = 1349,
            ["mr"] = 1349,
        },
        ["Darnassian Bleu"] = {
            ["H3220"] = 10,
            ["mr"] = 10,
        },
        ["Sentinel Trousers of the Whale"] = {
            ["H3220"] = 18135,
            ["mr"] = 18135,
        },
        ["Slayer's Slippers"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Ranger Boots of Power"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Insignia Buckler"] = {
            ["H3220"] = 5690,
            ["mr"] = 5690,
        },
        ["Plans: Annihilator"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Huntsman's Belt of the Monkey"] = {
            ["H3220"] = 12555,
            ["mr"] = 12555,
        },
        ["Pattern: Red Mageweave Gloves"] = {
            ["mr"] = 4778,
            ["cc"] = 9,
            ["id"] = "10312:0:0:0:0",
            ["L3229"] = 4778,
            ["H3229"] = 4900,
            ["sc"] = 2,
        },
        ["Stout Battlehammer of Power"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Strong Fishing Pole"] = {
            ["H3220"] = 1395,
            ["mr"] = 1395,
        },
        ["Copper Bar"] = {
            ["H3220"] = 117,
            ["mr"] = 117,
        },
        ["Dervish Tunic of Power"] = {
            ["H3220"] = 8081,
            ["mr"] = 8081,
        },
        ["Infiltrator Shoulders of Stamina"] = {
            ["H3220"] = 6999,
            ["mr"] = 6999,
        },
        ["Stonescale Oil"] = {
            ["H3220"] = 11800,
            ["mr"] = 11800,
        },
        ["Staff of the Shade"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Jagged Star of the Tiger"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Formula: Enchant Bracer - Superior Spirit"] = {
            ["H3220"] = 61500,
            ["mr"] = 61500,
        },
        ["Durable Hat of the Owl"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Resilient Boots"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Pattern: Rich Purple Silk Shirt"] = {
            ["mr"] = 290000,
            ["sc"] = 2,
            ["id"] = "4354:0:0:0:0",
            ["H3229"] = 290000,
            ["cc"] = 9,
        },
        ["Banded Boots of the Bear"] = {
            ["H3220"] = 3399,
            ["mr"] = 3399,
        },
        ["Hefty Battlehammer of the Tiger"] = {
            ["H3220"] = 5050,
            ["mr"] = 5050,
        },
        ["Dark Leather Shoulders"] = {
            ["H3220"] = 7300,
            ["mr"] = 7300,
        },
        ["Archer's Bracers of Stamina"] = {
            ["H3220"] = 2700,
            ["mr"] = 2700,
        },
        ["Potent Belt of the Monkey"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Potent Gloves of the Monkey"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Sanguine Belt"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Chesterfall Musket"] = {
            ["H3220"] = 18600,
            ["mr"] = 18600,
        },
        ["Blue Pearl"] = {
            ["mr"] = 3500,
            ["H3254"] = 3500,
            ["H3220"] = 7900,
        },
        ["Zesty Clam Meat"] = {
            ["mr"] = 645,
            ["H3254"] = 645,
            ["H3220"] = 96,
        },
        ["Twilight Pants of the Owl"] = {
            ["H3220"] = 18500,
            ["mr"] = 18500,
        },
        ["Sentry's Shoulderguards of the Boar"] = {
            ["H3220"] = 6228,
            ["mr"] = 6228,
        },
        ["Elder's Hat of the Owl"] = {
            ["H3220"] = 9800,
            ["mr"] = 9800,
        },
        ["Defender Leggings of Strength"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Pattern: Red Mageweave Headband"] = {
            ["mr"] = 4999,
            ["sc"] = 2,
            ["id"] = "10320:0:0:0:0",
            ["H3229"] = 4999,
            ["cc"] = 9,
        },
        ["Infiltrator Pants of the Monkey"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Scaled Leather Shoulders of Intellect"] = {
            ["H3220"] = 4799,
            ["mr"] = 4799,
        },
        ["Huntsman's Leggings of the Bear"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Barbaric Cloth Vest"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Pattern: Heavy Earthen Gloves"] = {
            ["mr"] = 905,
            ["cc"] = 9,
            ["id"] = "7364:0:0:0:0",
            ["L3229"] = 905,
            ["H3229"] = 909,
            ["sc"] = 1,
        },
        ["Polished Zweihander of Strength"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Cutthroat's Belt of the Whale"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Schematic: Mithril Heavy-bore Rifle"] = {
            ["H3220"] = 4925,
            ["mr"] = 4925,
        },
        ["Scaled Cloak of the Wolf"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Pattern: Star Belt"] = {
            ["mr"] = 800000,
            ["sc"] = 2,
            ["id"] = "4356:0:0:0:0",
            ["H3229"] = 800000,
            ["cc"] = 9,
        },
        ["Bandit Jerkin of the Whale"] = {
            ["H3220"] = 3421,
            ["mr"] = 3421,
        },
        ["Sage's Pants of the Whale"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Sage's Stave of Frozen Wrath"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Barbaric Battle Axe of the Monkey"] = {
            ["mr"] = 6999,
            ["sc"] = 1,
            ["id"] = "3195:0:0:590:0",
            ["H3246"] = 6999,
            ["cc"] = 2,
        },
        ["Archer's Shoulderpads of the Falcon"] = {
            ["H3220"] = 10501,
            ["mr"] = 10501,
        },
        ["Mercenary Blade of Power"] = {
            ["H3220"] = 15000,
            ["mr"] = 15000,
        },
        ["Ivycloth Sash of the Whale"] = {
            ["H3220"] = 2844,
            ["mr"] = 2844,
        },
        ["Scaled Leather Bracers of the Whale"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Severing Axe of the Wolf"] = {
            ["H3220"] = 1050,
            ["mr"] = 1050,
        },
        ["Stonecutter Claymore of the Whale"] = {
            ["H3220"] = 27885,
            ["mr"] = 27885,
        },
        ["Formula: Enchant Shield - Lesser Block"] = {
            ["H3220"] = 2425,
            ["mr"] = 2425,
        },
        ["Stonecutter Claymore of the Monkey"] = {
            ["mr"] = 9900,
            ["sc"] = 8,
            ["id"] = "3197:0:0:607:0",
            ["L3246"] = 9900,
            ["H3246"] = 14000,
            ["cc"] = 2,
        },
        ["Elder's Gloves of the Owl"] = {
            ["H3220"] = 4777,
            ["mr"] = 4777,
        },
        ["Grunt's Cape of the Monkey"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Schematic: Portable Bronze Mortar"] = {
            ["H3220"] = 800,
            ["mr"] = 800,
        },
        ["Beazel's Basher"] = {
            ["H3220"] = 14849,
            ["mr"] = 14849,
        },
        ["Tracker's Tunic of Intellect"] = {
            ["H3220"] = 30000,
            ["mr"] = 30000,
        },
        ["Ritual Tunic of Arcane Wrath"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Formula: Enchant Weapon - Lesser Elemental Slayer"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Recipe: Rockscale Cod"] = {
            ["H3220"] = 3799,
            ["mr"] = 3799,
        },
        ["Magician Staff of the Boar"] = {
            ["H3220"] = 7600,
            ["mr"] = 7600,
        },
        ["Pathfinder Vest of the Owl"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Nocturnal Gloves of the Eagle"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Cutthroat's Boots of Power"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Greenweave Sash of Healing"] = {
            ["H3220"] = 2166,
            ["mr"] = 2166,
        },
        ["Aurora Cloak"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Scaled Cloak of the Monkey"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Hot Lion Chops"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Neophyte's Shirt"] = {
            ["H3220"] = 299,
            ["mr"] = 299,
        },
        ["Pattern: Black Silk Pack"] = {
            ["mr"] = 2500,
            ["cc"] = 9,
            ["id"] = "5775:0:0:0:0",
            ["L3229"] = 2500,
            ["H3229"] = 2968,
            ["sc"] = 2,
        },
        ["Righteous Orb"] = {
            ["H3220"] = 990000,
            ["mr"] = 990000,
        },
        ["Pattern: Colorful Kilt"] = {
            ["mr"] = 1100,
            ["cc"] = 9,
            ["id"] = "10316:0:0:0:0",
            ["L3229"] = 1100,
            ["H3229"] = 4200,
            ["sc"] = 2,
        },
        ["Pattern: Blue Overalls"] = {
            ["mr"] = 2400,
            ["cc"] = 9,
            ["id"] = "6274:0:0:0:0",
            ["L3229"] = 2400,
            ["H3229"] = 2500,
            ["sc"] = 2,
        },
        ["Plans: Wildthorn Mail"] = {
            ["H3220"] = 29998,
            ["mr"] = 29998,
        },
        ["Scorpashi Leggings"] = {
            ["H3220"] = 20000,
            ["mr"] = 20000,
        },
        ["Polished Zweihander of the Bear"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Sequoia Hammer of the Monkey"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Jagged Star of Shadow Wrath"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["Defender Tunic of the Bear"] = {
            ["H3220"] = 1999,
            ["mr"] = 1999,
        },
        ["Thistlefur Mantle of Frozen Wrath"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Pattern: Cindercloth Gloves"] = {
            ["H3220"] = 14925,
            ["mr"] = 14925,
        },
        ["Huntsman's Boots of the Bear"] = {
            ["H3220"] = 12537,
            ["mr"] = 12537,
        },
        ["Formal White Shirt"] = {
            ["H3220"] = 700,
            ["mr"] = 700,
        },
        ["Phalanx Gauntlets of Strength"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Red Woolen Bag"] = {
            ["mr"] = 990,
            ["cc"] = 1,
            ["id"] = "5763:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 990,
        },
        ["Bottom Half of Advanced Armorsmithing: Volume I"] = {
            ["H3220"] = 450000,
            ["mr"] = 450000,
        },
        ["Banded Shield of Strength"] = {
            ["H3220"] = 44868,
            ["mr"] = 44868,
        },
        ["Councillor's Cloak of Arcane Wrath"] = {
            ["H3220"] = 33400,
            ["mr"] = 33400,
        },
        ["Ancestral Tunic"] = {
            ["H3220"] = 300,
            ["mr"] = 300,
        },
        ["Pattern: Dark Leather Shoulders"] = {
            ["mr"] = 1195,
            ["sc"] = 1,
            ["id"] = "4296:0:0:0:0",
            ["H3229"] = 1195,
            ["cc"] = 9,
        },
        ["Twilight Cuffs of the Eagle"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Staunch Hammer of Arcane Wrath"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Thick Scale Shoulder Pads of the Bear"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["War Torn Pants of Strength"] = {
            ["H3220"] = 599,
            ["mr"] = 599,
        },
        ["Pattern: Runecloth Pants"] = {
            ["mr"] = 79900,
            ["sc"] = 2,
            ["id"] = "14491:0:0:0:0",
            ["H3229"] = 79900,
            ["cc"] = 9,
        },
        ["Soldier's Gauntlets of the Eagle"] = {
            ["H3220"] = 815,
            ["mr"] = 815,
        },
        ["Elder's Padded Armor of the Owl"] = {
            ["H3220"] = 8956,
            ["mr"] = 8956,
        },
        ["Silksand Cape"] = {
            ["H3220"] = 6969,
            ["mr"] = 6969,
        },
        ["Cutthroat's Vest of the Owl"] = {
            ["H3220"] = 7700,
            ["mr"] = 7700,
        },
        ["War Torn Handgrips"] = {
            ["H3220"] = 205,
            ["mr"] = 205,
        },
        ["Arcane Crystal"] = {
            ["H3220"] = 299999,
            ["mr"] = 299999,
        },
        ["Gryphon Mail Gauntlets of the Bear"] = {
            ["H3220"] = 15500,
            ["mr"] = 15500,
        },
        ["Impenetrable Bindings of the Monkey"] = {
            ["mr"] = 30000,
            ["cc"] = 4,
            ["id"] = "15659:0:0:608:0",
            ["H3246"] = 30000,
            ["sc"] = 3,
        },
        ["Durable Cape of the Whale"] = {
            ["H3220"] = 2999,
            ["mr"] = 2999,
        },
        ["Spiked Chain Slippers of the Gorilla"] = {
            ["H3220"] = 2900,
            ["mr"] = 2900,
        },
        ["Patchwork Belt"] = {
            ["H3220"] = 450,
            ["mr"] = 450,
        },
        ["Light Leather Bracers"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Scroll of Agility II"] = {
            ["H3220"] = 359,
            ["mr"] = 359,
        },
        ["Shiny War Axe"] = {
            ["H3220"] = 2250,
            ["mr"] = 2250,
        },
        ["Wrangler's Boots of Agility"] = {
            ["H3220"] = 4208,
            ["mr"] = 4208,
        },
        ["Conjurer's Mantle of the Eagle"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Plans: Golden Scale Cuirass"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Resplendent Guardian"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Twisted Sabre"] = {
            ["H3220"] = 7800,
            ["mr"] = 7800,
        },
        ["Azure Silk Belt"] = {
            ["H3220"] = 7799,
            ["mr"] = 7799,
        },
        ["Pattern: Wicked Leather Bracers"] = {
            ["mr"] = 35400,
            ["cc"] = 9,
            ["id"] = "15728:0:0:0:0",
            ["L3229"] = 29800,
            ["H3229"] = 35400,
            ["sc"] = 1,
        },
        ["Seer's Boots"] = {
            ["H3220"] = 1900,
            ["mr"] = 1900,
        },
        ["Long Elegant Feather"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Dazzling Longsword"] = {
            ["H3220"] = 219999,
            ["mr"] = 219999,
        },
        ["Mithril Spurs"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Formula: Enchant Gloves - Skinning"] = {
            ["mr"] = 5000,
            ["H3254"] = 5000,
            ["H3220"] = 17800,
        },
        ["Giant Clam Meat"] = {
            ["mr"] = 2500,
            ["H3254"] = 2500,
            ["H3220"] = 310,
        },
        ["Wrangler's Boots of the Wolf"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Battle Knife of the Tiger"] = {
            ["H3220"] = 4500,
            ["mr"] = 4500,
        },
        ["Barbaric Loincloth"] = {
            ["H3220"] = 498,
            ["mr"] = 498,
        },
        ["Hillborne Axe of the Wolf"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Heavy Scorpid Belt"] = {
            ["mr"] = 24197,
            ["cc"] = 9,
            ["id"] = "15743:0:0:0:0",
            ["sc"] = 1,
            ["H3227"] = 24197,
        },
        ["Infiltrator Cord of Intellect"] = {
            ["H3220"] = 3399,
            ["mr"] = 3399,
        },
        ["Insignia Bracers"] = {
            ["H3220"] = 9899,
            ["mr"] = 9899,
        },
        ["Wrangler's Mantle of the Eagle"] = {
            ["H3220"] = 18000,
            ["mr"] = 18000,
        },
        ["Hillman's Belt"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Pagan Belt of the Whale"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Raptor Egg"] = {
            ["mr"] = 666,
            ["H3254"] = 666,
            ["H3220"] = 129,
        },
        ["Felcloth Shoulders"] = {
            ["mr"] = 441000,
            ["cc"] = 4,
            ["id"] = "14112:0:0:0:0",
            ["H3246"] = 441000,
            ["sc"] = 1,
        },
        ["Burnished Shield"] = {
            ["H3220"] = 4754,
            ["mr"] = 4754,
        },
        ["Native Pants of Healing"] = {
            ["H3220"] = 2599,
            ["mr"] = 2599,
        },
        ["Tundra Ring of the Falcon"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Infiltrator Armor of the Eagle"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Fighter Broadsword of the Bear"] = {
            ["H3220"] = 3999,
            ["mr"] = 3999,
        },
        ["First Mate Hat"] = {
            ["mr"] = 1986827,
            ["H3254"] = 1986827,
            ["H3220"] = 500000,
        },
        ["Embersilk Mantle of the Eagle"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Plans: Polished Steel Boots"] = {
            ["H3220"] = 3200,
            ["mr"] = 3200,
        },
        ["Durable Boots of Stamina"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Hefty Battlehammer of the Bear"] = {
            ["H3220"] = 8900,
            ["mr"] = 8900,
        },
        ["Infiltrator Gloves of Healing"] = {
            ["H3220"] = 4983,
            ["mr"] = 4983,
        },
        ["Archer's Belt of the Whale"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Marauder's Cloak of Stamina"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Owl Bracers"] = {
            ["H3220"] = 5027,
            ["mr"] = 5027,
        },
        ["Pattern: Frostweave Gloves"] = {
            ["mr"] = 40000,
            ["sc"] = 2,
            ["id"] = "14474:0:0:0:0",
            ["H3229"] = 40000,
            ["cc"] = 9,
        },
        ["Sage's Bracers of Healing"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Cutthroat's Cape of the Wolf"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Edged Bastard Sword of the Boar"] = {
            ["H3220"] = 1995,
            ["mr"] = 1995,
        },
        ["Elder's Hat of Healing"] = {
            ["H3220"] = 11520,
            ["mr"] = 11520,
        },
        ["Aboriginal Shoulder Pads"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Barbaric Cloth Robe"] = {
            ["H3220"] = 1251,
            ["mr"] = 1251,
        },
        ["Ravager's Handwraps"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Middle Map Fragment"] = {
            ["mr"] = 12003,
            ["H3254"] = 12003,
            ["H3220"] = 2295,
        },
        ["Deadwood Sledge"] = {
            ["H3220"] = 24000,
            ["mr"] = 24000,
        },
        ["Elder's Sash of Fiery Wrath"] = {
            ["H3220"] = 6468,
            ["mr"] = 6468,
        },
        ["Plans: Silvered Bronze Breastplate"] = {
            ["H3220"] = 448,
            ["mr"] = 448,
        },
        ["Pattern: Murloc Scale Belt"] = {
            ["mr"] = 414,
            ["cc"] = 9,
            ["id"] = "5786:0:0:0:0",
            ["L3229"] = 414,
            ["H3229"] = 415,
            ["sc"] = 1,
        },
        ["Darkmist Pants of the Owl"] = {
            ["H3220"] = 16239,
            ["mr"] = 16239,
        },
        ["Silvered Bronze Gauntlets"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Zircon Band of Nature Resistance"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["Greater Astral Essence"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Fused Wiring"] = {
            ["mr"] = 32904,
            ["sc"] = 1,
            ["id"] = "7191:0:0:0:0",
            ["H3229"] = 32904,
            ["cc"] = 7,
        },
        ["Willow Cape of Fiery Wrath"] = {
            ["H3220"] = 400,
            ["mr"] = 400,
        },
        ["Scroll of Intellect II"] = {
            ["H3220"] = 247,
            ["mr"] = 247,
        },
        ["Lambent Scale Girdle"] = {
            ["H3220"] = 3298,
            ["mr"] = 3298,
        },
        ["Pattern: Deviate Scale Belt"] = {
            ["mr"] = 965,
            ["sc"] = 1,
            ["id"] = "6476:0:0:0:0",
            ["H3229"] = 965,
            ["cc"] = 9,
        },
        ["Banded Armor of Strength"] = {
            ["H3220"] = 10845,
            ["mr"] = 10845,
        },
        ["Combat Cloak"] = {
            ["H3220"] = 2300,
            ["mr"] = 2300,
        },
        ["Vital Sash of the Eagle"] = {
            ["H3220"] = 6500,
            ["mr"] = 6500,
        },
        ["Stalvan's Reaper"] = {
            ["H3220"] = 25432,
            ["mr"] = 25432,
        },
        ["Pattern: Frostsaber Boots"] = {
            ["mr"] = 8600,
            ["cc"] = 9,
            ["id"] = "15740:0:0:0:0",
            ["L3229"] = 8600,
            ["H3229"] = 50000,
            ["sc"] = 1,
        },
        ["Mail Combat Leggings"] = {
            ["H3220"] = 6998,
            ["mr"] = 6998,
        },
        ["Hematite Link of Fire Resistance"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Raincaller Mitts of Spirit"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Heavy Bronze Mace"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Raider's Belt of Strength"] = {
            ["H3220"] = 1300,
            ["mr"] = 1300,
        },
        ["Barbed Club of Stamina"] = {
            ["H3220"] = 1992,
            ["mr"] = 1992,
        },
        ["Chief Brigadier Gauntlets"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Rigid Gloves of the Bear"] = {
            ["H3220"] = 5219,
            ["mr"] = 5219,
        },
        ["Handstitched Leather Cloak"] = {
            ["H3220"] = 169,
            ["mr"] = 169,
        },
        ["Vilerend Slicer"] = {
            ["mr"] = 90863,
            ["H3254"] = 90863,
            ["H3220"] = 160000,
        },
        ["Hook Dagger of Agility"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Frayed Robe"] = {
            ["H3220"] = 600,
            ["mr"] = 600,
        },
        ["Clay Ring of Strength"] = {
            ["H3220"] = 17948,
            ["mr"] = 17948,
        },
        ["Vibrant Plume"] = {
            ["H3231"] = 825,
            ["mr"] = 825,
            ["cc"] = 15,
            ["id"] = "5117:0:0:0:0",
            ["sc"] = 0,
        },
        ["Scaled Leather Leggings of the Monkey"] = {
            ["mr"] = 7900,
            ["cc"] = 4,
            ["id"] = "9833:0:0:604:0",
            ["H3246"] = 7900,
            ["sc"] = 2,
        },
        ["Raw Sunscale Salmon"] = {
            ["H3220"] = 242,
            ["mr"] = 242,
        },
        ["Bloodspattered Gloves of the Bear"] = {
            ["H3220"] = 548,
            ["mr"] = 548,
        },
        ["Scouting Trousers of the Owl"] = {
            ["H3220"] = 1600,
            ["mr"] = 1600,
        },
        ["Conjurer's Vest of the Eagle"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Codex: Prayer of Fortitude II"] = {
            ["H3220"] = 89900,
            ["mr"] = 89900,
        },
        ["Battle Slayer of Strength"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Wicked Chain Chestpiece of the Bear"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Embossed Leather Vest"] = {
            ["H3220"] = 698,
            ["mr"] = 698,
        },
        ["Brutal War Axe of the Monkey"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Huntsman's Cap of the Wolf"] = {
            ["H3220"] = 8600,
            ["mr"] = 8600,
        },
        ["Tellurium Necklace of Stamina"] = {
            ["H3220"] = 25000,
            ["mr"] = 25000,
        },
        ["Red Linen Robe"] = {
            ["H3220"] = 1495,
            ["mr"] = 1495,
        },
        ["War Torn Tunic of Stamina"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Curve-bladed Ripper"] = {
            ["H3220"] = 79000,
            ["mr"] = 79000,
        },
        ["Pattern: Dusky Leather Leggings"] = {
            ["mr"] = 1775,
            ["sc"] = 1,
            ["id"] = "7449:0:0:0:0",
            ["H3229"] = 1775,
            ["cc"] = 9,
        },
        ["Sentinel Trousers of the Bear"] = {
            ["H3220"] = 14999,
            ["mr"] = 14999,
        },
        ["Ranger Shoulders of the Monkey"] = {
            ["H3220"] = 13500,
            ["mr"] = 13500,
        },
        ["Ridge Cleaver of the Boar"] = {
            ["H3220"] = 3400,
            ["mr"] = 3400,
        },
        ["Sergeant's Warhammer of Nature's Wrath"] = {
            ["H3220"] = 1702,
            ["mr"] = 1702,
        },
        ["Outrunner's Slippers of the Bear"] = {
            ["H3220"] = 1800,
            ["mr"] = 1800,
        },
        ["Conjurer's Bracers of Spirit"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Hillman's Leather Vest"] = {
            ["H3220"] = 1598,
            ["mr"] = 1598,
        },
        ["Battle Knife of the Bear"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Red Power Crystal"] = {
            ["H3220"] = 471,
            ["mr"] = 471,
        },
        ["Wolf Rider's Padded Armor of the Monkey"] = {
            ["H3220"] = 55959,
            ["mr"] = 55959,
        },
        ["Fortified Gauntlets of Power"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Basalt Necklace of the Bear"] = {
            ["H3220"] = 19900,
            ["mr"] = 19900,
        },
        ["Pattern: Volcanic Leggings"] = {
            ["mr"] = 6699,
            ["cc"] = 9,
            ["id"] = "15732:0:0:0:0",
            ["L3229"] = 6699,
            ["H3229"] = 6825,
            ["sc"] = 1,
        },
        ["Carving Knife of Stamina"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Greenweave Mantle of the Whale"] = {
            ["H3220"] = 6000,
            ["mr"] = 6000,
        },
        ["Bonelink Legplates of the Monkey"] = {
            ["H3220"] = 19999,
            ["mr"] = 19999,
        },
        ["Fortified Bracers of the Boar"] = {
            ["H3220"] = 7911,
            ["mr"] = 7911,
        },
        ["Symbolic Crest"] = {
            ["H3220"] = 19800,
            ["mr"] = 19800,
        },
        ["Grizzly Buckler of Shadow Wrath"] = {
            ["H3220"] = 1001,
            ["mr"] = 1001,
        },
        ["Pattern: Frostweave Tunic"] = {
            ["mr"] = 58013,
            ["sc"] = 2,
            ["id"] = "14466:0:0:0:0",
            ["H3229"] = 58013,
            ["cc"] = 9,
        },
        ["The Green Tower"] = {
            ["H3220"] = 458000,
            ["mr"] = 458000,
        },
        ["Recipe: Ghost Dye"] = {
            ["H3220"] = 70000,
            ["mr"] = 70000,
        },
        ["Lesser Mana Potion"] = {
            ["H3220"] = 38,
            ["mr"] = 38,
        },
        ["Runed Copper Breastplate"] = {
            ["H3220"] = 999,
            ["mr"] = 999,
        },
        ["Scaled Leather Headband of Intellect"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Knightly Longsword of Stamina"] = {
            ["H3220"] = 14100,
            ["mr"] = 14100,
        },
        ["Silver-thread Armor"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Pattern: Barbaric Bracers"] = {
            ["mr"] = 5200,
            ["sc"] = 1,
            ["id"] = "18949:0:0:0:0",
            ["H3229"] = 5200,
            ["cc"] = 9,
        },
        ["Metal Stave"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Cerulean Ring of Spirit"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Scroll of Agility III"] = {
            ["H3220"] = 4826,
            ["mr"] = 4826,
        },
        ["Bard's Boots of the Gorilla"] = {
            ["H3220"] = 2000,
            ["mr"] = 2000,
        },
        ["Durable Robe of the Whale"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Ravager's Woolies"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Pattern: Rugged Leather Pants"] = {
            ["mr"] = 399,
            ["sc"] = 1,
            ["id"] = "7288:0:0:0:0",
            ["H3229"] = 399,
            ["cc"] = 9,
        },
        ["Looming Gavel"] = {
            ["H3220"] = 10909,
            ["mr"] = 10909,
        },
        ["Elixir of Agility"] = {
            ["H3220"] = 680,
            ["mr"] = 680,
        },
        ["Watcher's Robes of the Owl"] = {
            ["H3220"] = 8800,
            ["mr"] = 8800,
        },
        ["Firebloom"] = {
            ["H3220"] = 370,
            ["mr"] = 370,
        },
        ["Thistlefur Sandals of Spirit"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Battle Chain Tunic"] = {
            ["H3220"] = 450,
            ["mr"] = 450,
        },
        ["Gloom Reaper of the Whale"] = {
            ["H3220"] = 17093,
            ["mr"] = 17093,
        },
        ["Nobles Brand of the Tiger"] = {
            ["H3254"] = 29000,
            ["mr"] = 29000,
        },
        ["Crocolisk Meat"] = {
            ["H3220"] = 38,
            ["mr"] = 38,
        },
        ["Frog Leg Stew"] = {
            ["H3220"] = 1050,
            ["mr"] = 1050,
        },
        ["Fadeleaf"] = {
            ["H3220"] = 620,
            ["mr"] = 620,
        },
        ["Pattern: Gem-studded Leather Belt"] = {
            ["mr"] = 16929,
            ["sc"] = 1,
            ["id"] = "14635:0:0:0:0",
            ["H3229"] = 16929,
            ["cc"] = 9,
        },
        ["Target Dummy"] = {
            ["H3220"] = 839,
            ["mr"] = 839,
        },
        ["Embossed Plate Boots of Defense"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Headstriker Sword of Agility"] = {
            ["H3220"] = 23000,
            ["mr"] = 23000,
        },
        ["Small Black Pouch"] = {
            ["mr"] = 410,
            ["cc"] = 1,
            ["id"] = "5571:0:0:0:0",
            ["sc"] = 0,
            ["H3220"] = 410,
        },
        ["Battleforge Boots of the Bear"] = {
            ["H3220"] = 4900,
            ["mr"] = 4900,
        },
        ["Sentinel Shoulders of Defense"] = {
            ["H3220"] = 8000,
            ["mr"] = 8000,
        },
        ["Jacinth Circle of Nature Resistance"] = {
            ["H3220"] = 4398,
            ["mr"] = 4398,
        },
        ["Greenweave Branch of Shadow Wrath"] = {
            ["H3220"] = 3500,
            ["mr"] = 3500,
        },
        ["Regal Sash of Fiery Wrath"] = {
            ["H3220"] = 15776,
            ["mr"] = 15776,
        },
        ["Dokebi Gloves"] = {
            ["H3220"] = 2800,
            ["mr"] = 2800,
        },
        ["Jacinth Circle of Frost Resistance"] = {
            ["H3220"] = 5000,
            ["mr"] = 5000,
        },
        ["Buccaneer's Orb of the Owl"] = {
            ["H3220"] = 2500,
            ["mr"] = 2500,
        },
        ["Raincaller Cloak of Stamina"] = {
            ["H3220"] = 2925,
            ["mr"] = 2925,
        },
        ["Swashbuckler's Shoulderpads of the Monkey"] = {
            ["mr"] = 107306,
            ["cc"] = 4,
            ["id"] = "10189:0:0:617:0",
            ["H3246"] = 107306,
            ["sc"] = 2,
        },
        ["Scouting Trousers of the Eagle"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Runed Copper Pants"] = {
            ["H3220"] = 799,
            ["mr"] = 799,
        },
        ["Durable Belt of the Eagle"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Dark Leather Pants"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Greater Mystic Essence"] = {
            ["H3220"] = 2400,
            ["mr"] = 2400,
        },
        ["Mutton Chop"] = {
            ["H3220"] = 40,
            ["mr"] = 40,
        },
        ["Hammer of the Northern Wind"] = {
            ["H3220"] = 150000,
            ["mr"] = 150000,
        },
        ["Outrunner's Cuffs of the Gorilla"] = {
            ["H3220"] = 500,
            ["mr"] = 500,
        },
        ["Warped Blade"] = {
            ["H3220"] = 2200,
            ["mr"] = 2200,
        },
        ["Thistlefur Mantle of the Eagle"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Advanced Target Dummy"] = {
            ["H3220"] = 17200,
            ["mr"] = 17200,
        },
        ["Scouting Cloak of the Wolf"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Ornate Mithril Helm"] = {
            ["H3220"] = 60000,
            ["mr"] = 60000,
        },
        ["Phalanx Breastplate of the Whale"] = {
            ["H3220"] = 6415,
            ["mr"] = 6415,
        },
        ["Crispy Bat Wing"] = {
            ["H3220"] = 133,
            ["mr"] = 133,
        },
        ["Thistlefur Cap of the Whale"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Sage's Robe of Stamina"] = {
            ["H3220"] = 4400,
            ["mr"] = 4400,
        },
        ["Wrangler's Boots of Spirit"] = {
            ["H3220"] = 3177,
            ["mr"] = 3177,
        },
        ["Mail Combat Armor"] = {
            ["H3220"] = 7900,
            ["mr"] = 7900,
        },
        ["Big Iron Fishing Pole"] = {
            ["H3220"] = 7799,
            ["mr"] = 7799,
        },
        ["Enormous Ogre Belt"] = {
            ["H3220"] = 19499,
            ["mr"] = 19499,
        },
        ["Bloodbelly Fish"] = {
            ["H3220"] = 200,
            ["mr"] = 200,
        },
        ["Infantry Tunic of the Tiger"] = {
            ["H3220"] = 1100,
            ["mr"] = 1100,
        },
        ["Robe of the Magi"] = {
            ["H3220"] = 99999,
            ["mr"] = 99999,
        },
        ["Pathfinder Bracers of Spirit"] = {
            ["H3220"] = 1500,
            ["mr"] = 1500,
        },
        ["Honed Stiletto of Power"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Outrunner's Cloak of the Monkey"] = {
            ["mr"] = 1985,
            ["cc"] = 4,
            ["id"] = "15501:0:0:586:0",
            ["H3246"] = 1985,
            ["sc"] = 1,
        },
        ["Honed Stiletto of the Tiger"] = {
            ["H3220"] = 7899,
            ["mr"] = 7899,
        },
        ["Pattern: Runecloth Headband"] = {
            ["mr"] = 30000,
            ["cc"] = 9,
            ["H3239"] = 30000,
            ["id"] = "14498:0:0:0:0",
            ["sc"] = 2,
        },
        ["Archer's Buckler of the Monkey"] = {
            ["H3220"] = 7500,
            ["mr"] = 7500,
        },
        ["Imperial Cloak"] = {
            ["H3220"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Chimeric Vest"] = {
            ["mr"] = 11900,
            ["cc"] = 9,
            ["id"] = "15755:0:0:0:0",
            ["H3229"] = 11900,
            ["sc"] = 1,
        },
        ["Knight's Headguard of the Eagle"] = {
            ["H3220"] = 10508,
            ["mr"] = 10508,
        },
        ["Twin-bladed Axe of Power"] = {
            ["H3220"] = 3072,
            ["mr"] = 3072,
        },
        ["Aurora Pants"] = {
            ["H3220"] = 8500,
            ["mr"] = 8500,
        },
        ["Heavy Runecloth Bandage"] = {
            ["mr"] = 1485,
            ["cc"] = 0,
            ["H3239"] = 1485,
            ["id"] = "14530:0:0:0:0",
            ["sc"] = 0,
        },
        ["Solid Blasting Powder"] = {
            ["H3220"] = 745,
            ["mr"] = 745,
        },
        ["Superior Healing Potion"] = {
            ["mr"] = 1725,
            ["H3254"] = 1725,
            ["H3220"] = 894,
        },
        ["Pattern: Stormshroud Pants"] = {
            ["mr"] = 14200,
            ["cc"] = 9,
            ["id"] = "15741:0:0:0:0",
            ["L3229"] = 14200,
            ["H3229"] = 14800,
            ["sc"] = 1,
        },
        ["Pattern: Red Whelp Gloves"] = {
            ["mr"] = 200000,
            ["sc"] = 1,
            ["id"] = "7290:0:0:0:0",
            ["H3229"] = 200000,
            ["cc"] = 9,
        },
        ["Ricochet Blunderbuss of the Monkey"] = {
            ["mr"] = 37200,
            ["cc"] = 2,
            ["id"] = "4089:0:0:593:0",
            ["H3246"] = 37200,
            ["sc"] = 3,
        },
        ["Lunar Handwraps of the Owl"] = {
            ["H3220"] = 13000,
            ["mr"] = 13000,
        },
        ["Tuxedo Jacket"] = {
            ["H3220"] = 50000,
            ["mr"] = 50000,
        },
        ["Hulking Belt"] = {
            ["H3220"] = 1473,
            ["mr"] = 1473,
        },
        ["Enduring Bracers"] = {
            ["H3220"] = 4600,
            ["mr"] = 4600,
        },
        ["Pattern: Runic Leather Bracers"] = {
            ["mr"] = 16600,
            ["cc"] = 9,
            ["id"] = "15739:0:0:0:0",
            ["L3229"] = 15000,
            ["H3229"] = 16600,
            ["sc"] = 1,
        },
        ["Illusion Dust"] = {
            ["H3220"] = 12187,
            ["mr"] = 12187,
        },
        ["Hibernal Robe"] = {
            ["H3220"] = 29800,
            ["mr"] = 29800,
        },
        ["Emerald Vambraces of the Bear"] = {
            ["H3220"] = 40000,
            ["mr"] = 40000,
        },
        ["Captain's Cloak of the Whale"] = {
            ["H3220"] = 3900,
            ["mr"] = 3900,
        },
        ["Sentry's Cape of the Bear"] = {
            ["H3220"] = 900,
            ["mr"] = 900,
        },
        ["Rigid Tunic of Intellect"] = {
            ["H3220"] = 7000,
            ["mr"] = 7000,
        },
        ["Stringy Wolf Meat"] = {
            ["H3220"] = 562,
            ["mr"] = 562,
        },
        ["War Torn Shield of the Boar"] = {
            ["H3220"] = 4000,
            ["mr"] = 4000,
        },
        ["Pattern: Robe of the Void"] = {
            ["mr"] = 1950000,
            ["sc"] = 2,
            ["id"] = "14514:0:0:0:0",
            ["H3229"] = 1950000,
            ["cc"] = 9,
        },
        ["Elder's Bracers of the Owl"] = {
            ["H3220"] = 4566,
            ["mr"] = 4566,
        },
        ["Wispy Cloak"] = {
            ["H3220"] = 100,
            ["mr"] = 100,
        },
        ["Smooth Stone Chip"] = {
            ["H3220"] = 1570,
            ["mr"] = 1570,
        },
        ["Pattern: Tuxedo Shirt"] = {
            ["mr"] = 39799,
            ["sc"] = 2,
            ["id"] = "10321:0:0:0:0",
            ["H3229"] = 39799,
            ["cc"] = 9,
        },
        ["Coarse Sharpening Stone"] = {
            ["H3220"] = 415,
            ["mr"] = 415,
        },
        ["Pattern: Herbalist's Gloves"] = {
            ["mr"] = 200000,
            ["sc"] = 1,
            ["id"] = "7361:0:0:0:0",
            ["H3229"] = 200000,
            ["cc"] = 9,
        },
        ["Elder's Hat of the Whale"] = {
            ["H3220"] = 9500,
            ["mr"] = 9500,
        },
        ["Defender Tunic of the Monkey"] = {
            ["mr"] = 15744,
            ["cc"] = 4,
            ["id"] = "6580:0:0:595:0",
            ["H3246"] = 15744,
            ["sc"] = 3,
        },
        ["Precisely Calibrated Boomstick"] = {
            ["H3220"] = 1800000,
            ["mr"] = 1800000,
        },
        ["Black Mageweave Headband"] = {
            ["H3220"] = 7399,
            ["mr"] = 7399,
        },
        ["Renegade Gauntlets of Strength"] = {
            ["H3220"] = 5900,
            ["mr"] = 5900,
        },
        ["Pattern: Tuxedo Pants"] = {
            ["mr"] = 39799,
            ["sc"] = 2,
            ["id"] = "10323:0:0:0:0",
            ["H3229"] = 39799,
            ["cc"] = 9,
        },
        ["Swamp Ring of the Monkey"] = {
            ["mr"] = 90000,
            ["cc"] = 4,
            ["id"] = "12015:0:0:606:0",
            ["H3246"] = 90000,
            ["sc"] = 0,
        },
        ["Pattern: Cindercloth Pants"] = {
            ["mr"] = 200000,
            ["cc"] = 9,
            ["id"] = "14490:0:0:0:0",
            ["H3229"] = 200000,
            ["sc"] = 2,
        },
        ["Cavalier Two-hander of the Wolf"] = {
            ["H3220"] = 5499,
            ["mr"] = 5499,
        },
        ["Soldier's Gauntlets of the Monkey"] = {
            ["mr"] = 3600,
            ["cc"] = 4,
            ["id"] = "6547:0:0:589:0",
            ["H3246"] = 3600,
            ["sc"] = 3,
        },
        ["Formula: Enchant 2H Weapon - Lesser Spirit"] = {
            ["H3220"] = 975,
            ["mr"] = 975,
        },
        ["Merc Sword of the Whale"] = {
            ["H3220"] = 1776,
            ["mr"] = 1776,
        },
        ["Glimmering Mail Legguards"] = {
            ["H3220"] = 3100,
            ["mr"] = 3100,
        },
        ["Watcher's Cinch of the Monkey"] = {
            ["mr"] = 3500,
            ["cc"] = 4,
            ["id"] = "14185:0:0:594:0",
            ["H3246"] = 3500,
            ["sc"] = 1,
        },
        ["Razor Blade of the Monkey"] = {
            ["mr"] = 19999,
            ["cc"] = 2,
            ["id"] = "15244:0:0:595:0",
            ["H3246"] = 19999,
            ["sc"] = 15,
        },
        ["Nightscape Tunic"] = {
            ["H3220"] = 7999,
            ["mr"] = 7999,
        },
        ["Pattern: Runic Leather Armor"] = {
            ["mr"] = 30000,
            ["cc"] = 9,
            ["id"] = "15776:0:0:0:0",
            ["H3229"] = 30000,
            ["sc"] = 1,
        },
        ["Thistlefur Belt of Healing"] = {
            ["H3220"] = 3000,
            ["mr"] = 3000,
        },
        ["Robust Cloak of Nature's Wrath"] = {
            ["H3220"] = 1400,
            ["mr"] = 1400,
        },
        ["Rune of Portals"] = {
            ["H3254"] = 250000,
            ["mr"] = 250000,
        },
        ["Conjurer's Cloak of Stamina"] = {
            ["H3220"] = 5500,
            ["mr"] = 5500,
        },
        ["Grunt's Handwraps of Power"] = {
            ["H3220"] = 1000,
            ["mr"] = 1000,
        },
        ["Orb of Power"] = {
            ["H3220"] = 11900,
            ["mr"] = 11900,
        },
        ["Renegade Chestguard of the Tiger"] = {
            ["H3220"] = 19000,
            ["mr"] = 19000,
        },
        ["Symbolic Crown"] = {
            ["H3220"] = 12000,
            ["mr"] = 12000,
        },
        ["Thick Scale Legguards of the Bear"] = {
            ["H3220"] = 9999,
            ["mr"] = 9999,
        },
        ["Abjurer's Crystal of Healing"] = {
            ["H3254"] = 150000,
            ["mr"] = 150000,
        },
        ["Elixir of Shadow Power"] = {
            ["H3220"] = 9900,
            ["mr"] = 9900,
        },
        ["Protective Pavise"] = {
            ["H3254"] = 15996,
            ["mr"] = 15996,
        },
        ["Pattern: Mooncloth"] = {
            ["mr"] = 50000,
            ["sc"] = 2,
            ["id"] = "14526:0:0:0:0",
            ["H3229"] = 50000,
            ["cc"] = 9,
        },
    },
    ["Ashbringer_Alliance"] = {
        ["Oak Mallet of the Tiger"] = {
            ["H3254"] = 9580,
            ["mr"] = 9580,
        },
        ["Arcane Elixir"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Stone Hammer of Strength"] = {
            ["H3254"] = 21469,
            ["mr"] = 21469,
        },
        ["Mithril Shield Spike"] = {
            ["H3254"] = 19799,
            ["mr"] = 19799,
        },
        ["The Ziggler"] = {
            ["H3254"] = 58444,
            ["mr"] = 58444,
        },
        ["Steadfast Buckler of the Wolf"] = {
            ["H3254"] = 22200,
            ["mr"] = 22200,
        },
        ["Swashbuckler's Leggings of Power"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Twilight Armor of the Owl"] = {
            ["H3254"] = 12500,
            ["mr"] = 12500,
        },
        ["Furious Falchion of the Bear"] = {
            ["H3254"] = 26500,
            ["mr"] = 26500,
        },
        ["Lesser Mana Potion"] = {
            ["H3254"] = 60,
            ["mr"] = 60,
        },
        ["Eye of Flame"] = {
            ["H3254"] = 457998,
            ["mr"] = 457998,
        },
        ["Chillwind E'ko"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Blush Ember Ring"] = {
            ["H3254"] = 33600,
            ["mr"] = 33600,
        },
        ["Formula: Enchant 2H Weapon - Superior Impact"] = {
            ["H3254"] = 438750,
            ["mr"] = 438750,
        },
        ["Flask of Big Mojo"] = {
            ["H3254"] = 1970,
            ["mr"] = 1970,
        },
        ["Pattern: Devilsaur Leggings"] = {
            ["H3254"] = 499999,
            ["mr"] = 499999,
        },
        ["Crescent Edge of Agility"] = {
            ["H3254"] = 34999,
            ["mr"] = 34999,
        },
        ["Raw Sagefish"] = {
            ["H3254"] = 398,
            ["mr"] = 398,
        },
        ["Shadowcat Hide"] = {
            ["H3254"] = 301,
            ["mr"] = 301,
        },
        ["Goblin Nutcracker of Power"] = {
            ["H3254"] = 25349,
            ["mr"] = 25349,
        },
        ["Firebloom"] = {
            ["H3254"] = 298,
            ["mr"] = 298,
        },
        ["Green Hills of Stranglethorn - Page 18"] = {
            ["H3254"] = 672,
            ["mr"] = 672,
        },
        ["Underworld Band"] = {
            ["H3254"] = 1090000,
            ["mr"] = 1090000,
        },
        ["Recipe: Elixir of Giants"] = {
            ["H3254"] = 33200,
            ["mr"] = 33200,
        },
        ["Tuxedo Jacket"] = {
            ["H3254"] = 17999,
            ["mr"] = 17999,
        },
        ["Standard Scope"] = {
            ["H3254"] = 1478,
            ["mr"] = 1478,
        },
        ["Felcloth Hood"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Black Mageweave Gloves"] = {
            ["H3254"] = 4250,
            ["mr"] = 4250,
        },
        ["Elemental Air"] = {
            ["H3254"] = 1057,
            ["mr"] = 1057,
        },
        ["Plated Armorfish"] = {
            ["H3254"] = 1320,
            ["mr"] = 1320,
        },
        ["Weak Troll's Blood Potion"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Swashbuckler's Leggings of the Monkey"] = {
            ["H3254"] = 48263,
            ["mr"] = 48263,
        },
        ["Mageweave Bandage"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Councillor's Shoulders of the Whale"] = {
            ["H3254"] = 36660,
            ["mr"] = 36660,
        },
        ["Dwarven Magestaff of Healing"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Pattern: Truefaith Gloves"] = {
            ["H3254"] = 5363,
            ["mr"] = 5363,
        },
        ["Lightforge Gauntlets"] = {
            ["H3254"] = 98900,
            ["mr"] = 98900,
        },
        ["Bronze Bar"] = {
            ["H3254"] = 160,
            ["mr"] = 160,
        },
        ["Heavy Lamellar Gauntlets of the Bear"] = {
            ["H3254"] = 24200,
            ["mr"] = 24200,
        },
        ["Naga Scale"] = {
            ["H3254"] = 170,
            ["mr"] = 170,
        },
        ["Wanderer's Belt of the Eagle"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Coarse Gorilla Hair"] = {
            ["H3254"] = 610,
            ["mr"] = 610,
        },
        ["Greater Fire Protection Potion"] = {
            ["H3254"] = 34368,
            ["mr"] = 34368,
        },
        ["Hacking Cleaver of the Tiger"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Silverleaf"] = {
            ["H3254"] = 2,
            ["mr"] = 2,
        },
        ["Lesser Bloodstone Ore"] = {
            ["H3254"] = 165,
            ["mr"] = 165,
        },
        ["Captain's Circlet of the Wolf"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Small Green Pouch"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Hunting Buckler"] = {
            ["H3254"] = 1998,
            ["mr"] = 1998,
        },
        ["Dusky Leather Leggings"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Pioneer Trousers of the Falcon"] = {
            ["H3254"] = 400000,
            ["mr"] = 400000,
        },
        ["Pattern: Spider Belt"] = {
            ["H3254"] = 9300,
            ["mr"] = 9300,
        },
        ["Lobster Stew"] = {
            ["H3254"] = 172,
            ["mr"] = 172,
        },
        ["Big Iron Fishing Pole"] = {
            ["H3254"] = 72799,
            ["mr"] = 72799,
        },
        ["Archer's Cap of Healing"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Recipe: Frost Protection Potion"] = {
            ["H3254"] = 10600,
            ["mr"] = 10600,
        },
        ["Long Silken Cloak"] = {
            ["H3254"] = 4050,
            ["mr"] = 4050,
        },
        ["Goldenbark Apple"] = {
            ["H3254"] = 1267,
            ["mr"] = 1267,
        },
        ["Ember Wand of the Owl"] = {
            ["H3254"] = 24900,
            ["mr"] = 24900,
        },
        ["Gossamer Headpiece of Arcane Wrath"] = {
            ["H3254"] = 29397,
            ["mr"] = 29397,
        },
        ["Burnside Rifle of the Eagle"] = {
            ["H3254"] = 60131,
            ["mr"] = 60131,
        },
        ["Conjurer's Robe of the Owl"] = {
            ["H3254"] = 11500,
            ["mr"] = 11500,
        },
        ["Raw Nightfin Snapper"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Windchaser Orb"] = {
            ["H3254"] = 22000,
            ["mr"] = 22000,
        },
        ["Pattern: Hillman's Belt"] = {
            ["H3254"] = 990,
            ["mr"] = 990,
        },
        ["Imposing Pants of the Eagle"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Strong Troll's Blood Potion"] = {
            ["H3254"] = 505,
            ["mr"] = 505,
        },
        ["Green Lens of Regeneration"] = {
            ["H3254"] = 21400,
            ["mr"] = 21400,
        },
        ["Gnomish Mind Control Cap"] = {
            ["H3254"] = 124058,
            ["mr"] = 124058,
        },
        ["Field Repair Bot 74A"] = {
            ["H3254"] = 371000,
            ["mr"] = 371000,
        },
        ["Keeper's Woolies"] = {
            ["H3254"] = 29250,
            ["mr"] = 29250,
        },
        ["Barbaric Battle Axe of the Whale"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Traveler's Leggings"] = {
            ["H3254"] = 59000,
            ["mr"] = 59000,
        },
        ["Bloodpike"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Bonelink Bracers of Spirit"] = {
            ["H3254"] = 14963,
            ["mr"] = 14963,
        },
        ["Recipe: Smoked Sagefish"] = {
            ["H3254"] = 1765,
            ["mr"] = 1765,
        },
        ["Pattern: Black Dragonscale Shoulders"] = {
            ["H3254"] = 28000,
            ["mr"] = 28000,
        },
        ["Fortified Cloak of the Bear"] = {
            ["H3254"] = 2780,
            ["mr"] = 2780,
        },
        ["Bloodwoven Pants of the Whale"] = {
            ["H3254"] = 21000,
            ["mr"] = 21000,
        },
        ["Azure Silk Gloves"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Soldier's Armor of the Bear"] = {
            ["H3254"] = 2600,
            ["mr"] = 2600,
        },
        ["Infiltrator Cloak of the Whale"] = {
            ["H3254"] = 7532,
            ["mr"] = 7532,
        },
        ["Crocolisk Meat"] = {
            ["H3254"] = 148,
            ["mr"] = 148,
        },
        ["Scouting Trousers of the Eagle"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["47 Pound Grouper"] = {
            ["H3254"] = 27000,
            ["mr"] = 27000,
        },
        ["Scroll of Spirit"] = {
            ["H3254"] = 150,
            ["mr"] = 150,
        },
        ["Nightshade Spaulders of the Monkey"] = {
            ["H3254"] = 191280,
            ["mr"] = 191280,
        },
        ["Shadowgem"] = {
            ["H3254"] = 268,
            ["mr"] = 268,
        },
        ["Mercurial Cloak of Agility"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Murloc Fin"] = {
            ["H3254"] = 224,
            ["mr"] = 224,
        },
        ["Scroll of Spirit II"] = {
            ["H3254"] = 448,
            ["mr"] = 448,
        },
        ["Firemane Leggings"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Runecloth"] = {
            ["H3254"] = 508,
            ["mr"] = 508,
        },
        ["Skinning Knife"] = {
            ["H3254"] = 299,
            ["mr"] = 299,
        },
        ["Buccaneer's Pants of Healing"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Ornate Circlet of the Wolf"] = {
            ["H3254"] = 182868,
            ["mr"] = 182868,
        },
        ["Disciple's Stein of the Owl"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Regal Armor of the Owl"] = {
            ["H3254"] = 10500,
            ["mr"] = 10500,
        },
        ["Mageflame Cloak"] = {
            ["H3254"] = 37999,
            ["mr"] = 37999,
        },
        ["Heavy Quiver"] = {
            ["H3254"] = 7605,
            ["mr"] = 7605,
        },
        ["Sequoia Branch of the Monkey"] = {
            ["H3254"] = 37593,
            ["mr"] = 37593,
        },
        ["Regal Gloves of the Owl"] = {
            ["H3254"] = 9930,
            ["mr"] = 9930,
        },
        ["Dervish Gloves of the Gorilla"] = {
            ["H3254"] = 6357,
            ["mr"] = 6357,
        },
        ["Starsight Tunic"] = {
            ["H3254"] = 19013,
            ["mr"] = 19013,
        },
        ["Skeletal Club"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Sequoia Branch of the Eagle"] = {
            ["H3254"] = 24999,
            ["mr"] = 24999,
        },
        ["Formula: Enchant Weapon - Icy Chill"] = {
            ["H3254"] = 99898,
            ["mr"] = 99898,
        },
        ["Willow Pants of the Falcon"] = {
            ["H3254"] = 1200,
            ["mr"] = 1200,
        },
        ["Smashing Star of the Monkey"] = {
            ["H3254"] = 59500,
            ["mr"] = 59500,
        },
        ["Stonerender Gauntlets"] = {
            ["H3254"] = 82000,
            ["mr"] = 82000,
        },
        ["Okra"] = {
            ["H3254"] = 15,
            ["mr"] = 15,
        },
        ["Light Leather"] = {
            ["H3254"] = 33,
            ["mr"] = 33,
        },
        ["Greater Scythe of the Wolf"] = {
            ["H3254"] = 34062,
            ["mr"] = 34062,
        },
        ["Decapitating Sword of the Bear"] = {
            ["H3254"] = 7400,
            ["mr"] = 7400,
        },
        ["Swim Speed Potion"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Bard's Trousers of the Bear"] = {
            ["H3254"] = 1743,
            ["mr"] = 1743,
        },
        ["Burnished Girdle"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Scaled Cloak of the Owl"] = {
            ["H3254"] = 2999,
            ["mr"] = 2999,
        },
        ["Azerothian Diamond"] = {
            ["H3254"] = 94999,
            ["mr"] = 94999,
        },
        ["Heavy Sharpening Stone"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Dervish Buckler of the Bear"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Nobles Brand of the Bear"] = {
            ["H3254"] = 22763,
            ["mr"] = 22763,
        },
        ["Conjurer's Breeches of the Falcon"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Flask of Supreme Power"] = {
            ["H3254"] = 660000,
            ["mr"] = 660000,
        },
        ["Pattern: Mooncloth Bag"] = {
            ["H3254"] = 459998,
            ["mr"] = 459998,
        },
        ["Wicked Blackjack"] = {
            ["H3254"] = 2498,
            ["mr"] = 2498,
        },
        ["Bandit Jerkin of Spirit"] = {
            ["H3254"] = 6600,
            ["mr"] = 6600,
        },
        ["Looming Gavel"] = {
            ["H3254"] = 25935,
            ["mr"] = 25935,
        },
        ["Royal Headband of Intellect"] = {
            ["H3254"] = 19500,
            ["mr"] = 19500,
        },
        ["Blesswind Hammer of Power"] = {
            ["H3254"] = 67116,
            ["mr"] = 67116,
        },
        ["Pattern: Robe of the Void"] = {
            ["H3254"] = 199500,
            ["mr"] = 199500,
        },
        ["Tracker's Headband of the Boar"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Mooncloth"] = {
            ["H3254"] = 210000,
            ["mr"] = 210000,
        },
        ["Dwarven Hatchet of Stamina"] = {
            ["H3254"] = 1200,
            ["mr"] = 1200,
        },
        ["Imperfect Draenethyst Fragment"] = {
            ["H3254"] = 8799,
            ["mr"] = 8799,
        },
        ["Salt Shaker"] = {
            ["H3254"] = 48500,
            ["mr"] = 48500,
        },
        ["Defender Shield of Agility"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Tender Wolf Steak"] = {
            ["H3254"] = 860,
            ["mr"] = 860,
        },
        ["Powerful Mojo"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Councillor's Sash of Fiery Wrath"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Pattern: Barbaric Leggings"] = {
            ["H3254"] = 4099,
            ["mr"] = 4099,
        },
        ["Insignia Cap"] = {
            ["H3254"] = 6633,
            ["mr"] = 6633,
        },
        ["Knight's Headguard of the Bear"] = {
            ["H3254"] = 9500,
            ["mr"] = 9500,
        },
        ["Royal Amice of Intellect"] = {
            ["H3254"] = 11354,
            ["mr"] = 11354,
        },
        ["Stone Hammer of the Tiger"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Raw Loch Frenzy"] = {
            ["H3254"] = 74,
            ["mr"] = 74,
        },
        ["Superior Gloves of the Falcon"] = {
            ["H3254"] = 2600,
            ["mr"] = 2600,
        },
        ["Mercurial Cloak of the Owl"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Ivycloth Mantle of Stamina"] = {
            ["H3254"] = 3114,
            ["mr"] = 3114,
        },
        ["Jouster's Wristguards"] = {
            ["H3254"] = 21978,
            ["mr"] = 21978,
        },
        ["Unearthed Bands of Power"] = {
            ["H3254"] = 199999,
            ["mr"] = 199999,
        },
        ["Shadowcraft Belt"] = {
            ["H3254"] = 140000,
            ["mr"] = 140000,
        },
        ["Soothing Spices"] = {
            ["H3254"] = 1335,
            ["mr"] = 1335,
        },
        ["Infiltrator Pants of the Bear"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Lunar Slippers of the Eagle"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Buccaneer's Cape of Stamina"] = {
            ["H3254"] = 1900,
            ["mr"] = 1900,
        },
        ["Star Ruby"] = {
            ["H3254"] = 5069,
            ["mr"] = 5069,
        },
        ["Blesswind Hammer of the Monkey"] = {
            ["H3254"] = 48998,
            ["mr"] = 48998,
        },
        ["Bard's Trousers of the Eagle"] = {
            ["H3254"] = 6200,
            ["mr"] = 6200,
        },
        ["Embossed Plate Gauntlets of Strength"] = {
            ["H3254"] = 8419,
            ["mr"] = 8419,
        },
        ["Recipe: Elixir of Fortitude"] = {
            ["H3254"] = 5400,
            ["mr"] = 5400,
        },
        ["Pattern: Felcloth Hood"] = {
            ["H3254"] = 299999,
            ["mr"] = 299999,
        },
        ["Ridge Cleaver of the Whale"] = {
            ["H3254"] = 4800,
            ["mr"] = 4800,
        },
        ["Dreadmist Wraps"] = {
            ["H3254"] = 25999,
            ["mr"] = 25999,
        },
        ["Conjurer's Breeches of the Eagle"] = {
            ["H3254"] = 18281,
            ["mr"] = 18281,
        },
        ["Searing Blade"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Sorcerer Mantle of the Owl"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Green Iron Leggings"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Plans: Heavy Mithril Pants"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Recipe: Murloc Fin Soup"] = {
            ["H3254"] = 590,
            ["mr"] = 590,
        },
        ["Pattern: Runic Leather Gauntlets"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Band of Thorns"] = {
            ["H3254"] = 4099,
            ["mr"] = 4099,
        },
        ["Dreamless Sleep Potion"] = {
            ["H3254"] = 580,
            ["mr"] = 580,
        },
        ["Red Wolf Meat"] = {
            ["H3254"] = 150,
            ["mr"] = 150,
        },
        ["Glimmering Flamberge of the Wolf"] = {
            ["H3254"] = 11200,
            ["mr"] = 11200,
        },
        ["Gold Power Core"] = {
            ["H3254"] = 478,
            ["mr"] = 478,
        },
        ["Sandals of the Insurgent"] = {
            ["H3254"] = 19600,
            ["mr"] = 19600,
        },
        ["Traveler's Backpack"] = {
            ["H3254"] = 261300,
            ["mr"] = 261300,
        },
        ["Buccaneer's Gloves of the Owl"] = {
            ["H3254"] = 2354,
            ["mr"] = 2354,
        },
        ["Crystalpine Stinger"] = {
            ["H3254"] = 41500,
            ["mr"] = 41500,
        },
        ["Skullcrusher Mace of Healing"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Scorpion Sting"] = {
            ["H3254"] = 58013,
            ["mr"] = 58013,
        },
        ["Sentry Cloak"] = {
            ["H3254"] = 440000,
            ["mr"] = 440000,
        },
        ["Imperial Leather Spaulders"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Monstrous War Axe of the Whale"] = {
            ["H3254"] = 25463,
            ["mr"] = 25463,
        },
        ["Bindings of Elements"] = {
            ["H3254"] = 47105,
            ["mr"] = 47105,
        },
        ["Twilight Cape of Healing"] = {
            ["H3254"] = 6153,
            ["mr"] = 6153,
        },
        ["Glowstar Rod of Spirit"] = {
            ["H3254"] = 157500,
            ["mr"] = 157500,
        },
        ["Plans: Dazzling Mithril Rapier"] = {
            ["H3254"] = 3800,
            ["mr"] = 3800,
        },
        ["Captain's Key"] = {
            ["H3254"] = 7800,
            ["mr"] = 7800,
        },
        ["Scroll of Protection III"] = {
            ["H3254"] = 1677,
            ["mr"] = 1677,
        },
        ["Elixir of Detect Demon"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Ivycloth Sash of the Monkey"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Dark Silk Shirt"] = {
            ["H3254"] = 12683,
            ["mr"] = 12683,
        },
        ["Cerulean Ring of the Whale"] = {
            ["H3254"] = 6432,
            ["mr"] = 6432,
        },
        ["Hibernal Pants"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Mail Combat Leggings"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Knight's Crest of the Bear"] = {
            ["H3254"] = 10800,
            ["mr"] = 10800,
        },
        ["Formula: Enchant Gloves - Advanced Herbalism"] = {
            ["H3254"] = 7488,
            ["mr"] = 7488,
        },
        ["Holy Shroud"] = {
            ["H3254"] = 21500,
            ["mr"] = 21500,
        },
        ["Scouting Cloak of Nature's Wrath"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Healing Potion"] = {
            ["H3254"] = 338,
            ["mr"] = 338,
        },
        ["Bard's Belt of the Whale"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Schematic: World Enlarger"] = {
            ["H3254"] = 11500,
            ["mr"] = 11500,
        },
        ["Forest Tracker Epaulets"] = {
            ["H3254"] = 75500,
            ["mr"] = 75500,
        },
        ["Decapitating Sword of the Monkey"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Pattern: Chimeric Leggings"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Mighty Troll's Blood Potion"] = {
            ["H3254"] = 910,
            ["mr"] = 910,
        },
        ["Flask of the Titans"] = {
            ["H3254"] = 599900,
            ["mr"] = 599900,
        },
        ["Silver Contact"] = {
            ["H3254"] = 43,
            ["mr"] = 43,
        },
        ["Soulkeeper"] = {
            ["H3254"] = 95099,
            ["mr"] = 95099,
        },
        ["Champion's Cape of the Monkey"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Sentinel Trousers of the Bear"] = {
            ["H3254"] = 9826,
            ["mr"] = 9826,
        },
        ["Plans: Mithril Shield Spike"] = {
            ["H3254"] = 50500,
            ["mr"] = 50500,
        },
        ["Boots of Avoidance"] = {
            ["H3254"] = 400000,
            ["mr"] = 400000,
        },
        ["Whirring Bronze Gizmo"] = {
            ["H3254"] = 374,
            ["mr"] = 374,
        },
        ["Stone Hammer of the Eagle"] = {
            ["H3254"] = 16900,
            ["mr"] = 16900,
        },
        ["Willow Pants of the Owl"] = {
            ["H3254"] = 1600,
            ["mr"] = 1600,
        },
        ["Elegant Boots of Frozen Wrath"] = {
            ["H3254"] = 210000,
            ["mr"] = 210000,
        },
        ["Heavy Scorpid Scale"] = {
            ["H3254"] = 3580,
            ["mr"] = 3580,
        },
        ["Runed Golem Shackles"] = {
            ["H3254"] = 260403,
            ["mr"] = 260403,
        },
        ["Conjurer's Breeches of the Whale"] = {
            ["H3254"] = 7506,
            ["mr"] = 7506,
        },
        ["Light Armor Kit"] = {
            ["H3254"] = 18,
            ["mr"] = 18,
        },
        ["Medium Armor Kit"] = {
            ["H3254"] = 699,
            ["mr"] = 699,
        },
        ["Cindercloth Robe"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Dark Iron Bar"] = {
            ["H3254"] = 29742,
            ["mr"] = 29742,
        },
        ["Glowstar Rod of Frozen Wrath"] = {
            ["H3254"] = 800000,
            ["mr"] = 800000,
        },
        ["Recipe: Crocolisk Gumbo"] = {
            ["H3254"] = 800,
            ["mr"] = 800,
        },
        ["Pattern: Earthen Leather Shoulders"] = {
            ["H3254"] = 5073,
            ["mr"] = 5073,
        },
        ["Fortified Belt of the Bear"] = {
            ["H3254"] = 1400,
            ["mr"] = 1400,
        },
        ["Elixir of Firepower"] = {
            ["H3254"] = 300,
            ["mr"] = 300,
        },
        ["Cavalier Two-hander of the Eagle"] = {
            ["H3254"] = 19884,
            ["mr"] = 19884,
        },
        ["Shadow Weaver Leggings"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Twilight Cape of Fiery Wrath"] = {
            ["H3254"] = 3552,
            ["mr"] = 3552,
        },
        ["Hefty Battlehammer of the Gorilla"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Small Silk Pack"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Hefty Battlehammer of the Boar"] = {
            ["H3254"] = 5250,
            ["mr"] = 5250,
        },
        ["Enchanted Mageweave Pouch"] = {
            ["H3254"] = 20200,
            ["mr"] = 20200,
        },
        ["Pattern: Heavy Scorpid Gauntlets"] = {
            ["H3254"] = 7499,
            ["mr"] = 7499,
        },
        ["Raider's Legguards of the Whale"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Glyphed Leggings"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Nightscape Boots"] = {
            ["H3254"] = 13599,
            ["mr"] = 13599,
        },
        ["Scaled Leather Gloves of Spirit"] = {
            ["H3254"] = 2700,
            ["mr"] = 2700,
        },
        ["Elder's Sash of the Whale"] = {
            ["H3254"] = 4693,
            ["mr"] = 4693,
        },
        ["Wizbang's Special Brew"] = {
            ["H3254"] = 1255,
            ["mr"] = 1255,
        },
        ["Blood Shard"] = {
            ["H3254"] = 131,
            ["mr"] = 131,
        },
        ["Superior Tunic of the Owl"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Birchwood Maul of the Monkey"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Black Lotus"] = {
            ["H3254"] = 330100,
            ["mr"] = 330100,
        },
        ["Book: Gift of the Wild"] = {
            ["H3254"] = 9749,
            ["mr"] = 9749,
        },
        ["Greater Astral Essence"] = {
            ["H3254"] = 4980,
            ["mr"] = 4980,
        },
        ["Vorpal Dagger of Shadow Wrath"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Elegant Boots of the Whale"] = {
            ["H3254"] = 168562,
            ["mr"] = 168562,
        },
        ["Magician Staff of the Wolf"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Scorpid Scale"] = {
            ["H3254"] = 994,
            ["mr"] = 994,
        },
        ["Bard's Trousers of the Whale"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Green Hills of Stranglethorn - Page 27"] = {
            ["H3254"] = 520,
            ["mr"] = 520,
        },
        ["Bronze Tube"] = {
            ["H3254"] = 1250,
            ["mr"] = 1250,
        },
        ["Chimera Leather"] = {
            ["H3254"] = 658,
            ["mr"] = 658,
        },
        ["Myrmidon's Breastplate"] = {
            ["H3254"] = 54999,
            ["mr"] = 54999,
        },
        ["Notched Shortsword of Power"] = {
            ["H3254"] = 4222,
            ["mr"] = 4222,
        },
        ["Jazeraint Leggings of the Wolf"] = {
            ["H3254"] = 23226,
            ["mr"] = 23226,
        },
        ["Trickster's Headdress of the Monkey"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Elegant Gloves of Healing"] = {
            ["H3254"] = 65000,
            ["mr"] = 65000,
        },
        ["Training Sword of the Boar"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Pattern: Red Mageweave Vest"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Warmonger"] = {
            ["H3254"] = 700000,
            ["mr"] = 700000,
        },
        ["Training Sword of the Monkey"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Globe of Water"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Boar Intestines"] = {
            ["H3254"] = 21,
            ["mr"] = 21,
        },
        ["Double-stitched Woolen Shoulders"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Glimmering Mail Bracers"] = {
            ["H3254"] = 4875,
            ["mr"] = 4875,
        },
        ["Scroll of Stamina III"] = {
            ["H3254"] = 1129,
            ["mr"] = 1129,
        },
        ["Conjurer's Vest of the Owl"] = {
            ["H3254"] = 8999,
            ["mr"] = 8999,
        },
        ["Cow King's Hide"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Middle Map Fragment"] = {
            ["H3254"] = 8699,
            ["mr"] = 8699,
        },
        ["Hillborne Axe of the Wolf"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Formula: Enchant Shield - Stamina"] = {
            ["H3254"] = 8300,
            ["mr"] = 8300,
        },
        ["Raider's Chestpiece of Power"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Plans: Thorium Boots"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Quickdraw Quiver"] = {
            ["H3254"] = 16400,
            ["mr"] = 16400,
        },
        ["Righteous Helmet of the Wolf"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Sentinel Buckler of the Tiger"] = {
            ["H3254"] = 22497,
            ["mr"] = 22497,
        },
        ["Pioneer Trousers of Spirit"] = {
            ["H3254"] = 1498,
            ["mr"] = 1498,
        },
        ["Crafted Light Shot"] = {
            ["H3254"] = 2,
            ["mr"] = 2,
        },
        ["Rage Potion"] = {
            ["H3254"] = 799,
            ["mr"] = 799,
        },
        ["Magic Resistance Potion"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Nightblade"] = {
            ["H3254"] = 350000,
            ["mr"] = 350000,
        },
        ["Hydralick Armor"] = {
            ["H3254"] = 186970,
            ["mr"] = 186970,
        },
        ["Hi-Explosive Bomb"] = {
            ["H3254"] = 7699,
            ["mr"] = 7699,
        },
        ["Silksand Tunic"] = {
            ["H3254"] = 19800,
            ["mr"] = 19800,
        },
        ["Nightsky Armor"] = {
            ["H3254"] = 7800,
            ["mr"] = 7800,
        },
        ["Troll's Bane Leggings"] = {
            ["H3254"] = 79500,
            ["mr"] = 79500,
        },
        ["Tuxedo Pants"] = {
            ["H3254"] = 19497,
            ["mr"] = 19497,
        },
        ["Sorcerer Gloves of Fiery Wrath"] = {
            ["H3254"] = 16000,
            ["mr"] = 16000,
        },
        ["Oak Mallet of the Monkey"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Elunarian Cuffs"] = {
            ["H3254"] = 29249,
            ["mr"] = 29249,
        },
        ["Knightly Longsword of the Tiger"] = {
            ["H3254"] = 20200,
            ["mr"] = 20200,
        },
        ["Dense Grinding Stone"] = {
            ["H3254"] = 4275,
            ["mr"] = 4275,
        },
        ["Recipe: Superior Mana Potion"] = {
            ["H3254"] = 17200,
            ["mr"] = 17200,
        },
        ["Gryphonwing Long Bow"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Spongy Morel"] = {
            ["H3254"] = 58,
            ["mr"] = 58,
        },
        ["Buccaneer's Robes of the Whale"] = {
            ["H3254"] = 3760,
            ["mr"] = 3760,
        },
        ["Thick Wolfhide"] = {
            ["H3254"] = 1085,
            ["mr"] = 1085,
        },
        ["Sorcerer Cloak of the Owl"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Recipe: Elixir of Detect Lesser Invisibility"] = {
            ["H3254"] = 9400,
            ["mr"] = 9400,
        },
        ["Lunar Handwraps of the Whale"] = {
            ["H3254"] = 8457,
            ["mr"] = 8457,
        },
        ["Templar Shield of the Gorilla"] = {
            ["H3254"] = 64938,
            ["mr"] = 64938,
        },
        ["Tracker's Headband of Defense"] = {
            ["H3254"] = 14200,
            ["mr"] = 14200,
        },
        ["Defender Tunic of the Monkey"] = {
            ["H3254"] = 6995,
            ["mr"] = 6995,
        },
        ["Deathweed"] = {
            ["H3254"] = 815,
            ["mr"] = 815,
        },
        ["Thaumaturgist Staff of Shadow Wrath"] = {
            ["H3254"] = 279900,
            ["mr"] = 279900,
        },
        ["Black Metal War Axe"] = {
            ["H3254"] = 7499,
            ["mr"] = 7499,
        },
        ["Defender Spaulders"] = {
            ["H3254"] = 3658,
            ["mr"] = 3658,
        },
        ["Mystical Boots of Frozen Wrath"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Silksand Circlet"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Crag Boar Rib"] = {
            ["H3254"] = 196,
            ["mr"] = 196,
        },
        ["Schematic: Voice Amplification Modulator"] = {
            ["H3254"] = 39999,
            ["mr"] = 39999,
        },
        ["Aegis of Stormwind"] = {
            ["H3254"] = 115928,
            ["mr"] = 115928,
        },
        ["Razor Axe of the Monkey"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Phalanx Gauntlets of the Whale"] = {
            ["H3254"] = 6763,
            ["mr"] = 6763,
        },
        ["Tough Jerky"] = {
            ["H3254"] = 99,
            ["mr"] = 99,
        },
        ["Blesswind Hammer of the Bear"] = {
            ["H3254"] = 59500,
            ["mr"] = 59500,
        },
        ["Scroll of Protection"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Sequoia Branch of Stamina"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Wicked Leather Gauntlets"] = {
            ["H3254"] = 13999,
            ["mr"] = 13999,
        },
        ["Sentinel Gloves of the Falcon"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Engraved Bracers of the Falcon"] = {
            ["H3254"] = 40500,
            ["mr"] = 40500,
        },
        ["Bearded Boneaxe"] = {
            ["H3254"] = 19499,
            ["mr"] = 19499,
        },
        ["Burnished Cloak"] = {
            ["H3254"] = 1378,
            ["mr"] = 1378,
        },
        ["Mature Blue Dragon Sinew"] = {
            ["H3254"] = 4507500,
            ["mr"] = 4507500,
        },
        ["Huntsman's Boots of Power"] = {
            ["H3254"] = 6500,
            ["mr"] = 6500,
        },
        ["Core of Earth"] = {
            ["H3254"] = 1090,
            ["mr"] = 1090,
        },
        ["Trueshot Bow of Spirit"] = {
            ["H3254"] = 26166,
            ["mr"] = 26166,
        },
        ["Pattern: Big Voodoo Cloak"] = {
            ["H3254"] = 1865,
            ["mr"] = 1865,
        },
        ["Empty Vial"] = {
            ["H3254"] = 697,
            ["mr"] = 697,
        },
        ["Taran Icebreaker"] = {
            ["H3254"] = 243750,
            ["mr"] = 243750,
        },
        ["Conjurer's Robe of the Whale"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Golden Sansam"] = {
            ["H3254"] = 469,
            ["mr"] = 469,
        },
        ["Crystal Basilisk Spine"] = {
            ["H3254"] = 94,
            ["mr"] = 94,
        },
        ["Magician Staff of Arcane Wrath"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Sequoia Branch of Strength"] = {
            ["H3254"] = 30600,
            ["mr"] = 30600,
        },
        ["Renegade Belt of the Whale"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Massacre Sword of the Tiger"] = {
            ["H3254"] = 570372,
            ["mr"] = 570372,
        },
        ["Combat Shield"] = {
            ["H3254"] = 8800,
            ["mr"] = 8800,
        },
        ["Inscribed Leather Gloves"] = {
            ["H3254"] = 990,
            ["mr"] = 990,
        },
        ["Shimmering Boots of Frozen Wrath"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Gauntlets of Elements"] = {
            ["H3254"] = 51042,
            ["mr"] = 51042,
        },
        ["Sheepshear Mantle"] = {
            ["H3254"] = 99090,
            ["mr"] = 99090,
        },
        ["Hook Dagger of Nature's Wrath"] = {
            ["H3254"] = 4053,
            ["mr"] = 4053,
        },
        ["Mightfish Steak"] = {
            ["H3254"] = 1970,
            ["mr"] = 1970,
        },
        ["Thick Armor Kit"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Forester's Axe of the Bear"] = {
            ["H3254"] = 3100,
            ["mr"] = 3100,
        },
        ["Recipe: Mighty Rage Potion"] = {
            ["H3254"] = 35219,
            ["mr"] = 35219,
        },
        ["Dreadblade of Fiery Wrath"] = {
            ["H3254"] = 39914,
            ["mr"] = 39914,
        },
        ["Plans: Jade Serpentblade"] = {
            ["H3254"] = 3600,
            ["mr"] = 3600,
        },
        ["Chieftain's Gloves of the Monkey"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Beer Basted Boar Ribs"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Pattern: Tough Scorpid Breastplate"] = {
            ["H3254"] = 1825,
            ["mr"] = 1825,
        },
        ["Venomshroud Mitts"] = {
            ["H3254"] = 13200,
            ["mr"] = 13200,
        },
        ["Blazing Wand"] = {
            ["H3254"] = 1910,
            ["mr"] = 1910,
        },
        ["Raincaller Scepter of Fiery Wrath"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Pattern: Living Shoulders"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Glimmering Mail Gauntlets"] = {
            ["H3254"] = 2100,
            ["mr"] = 2100,
        },
        ["Shell Launcher Shotgun"] = {
            ["H3254"] = 87750,
            ["mr"] = 87750,
        },
        ["Battle Knife of the Monkey"] = {
            ["H3254"] = 7893,
            ["mr"] = 7893,
        },
        ["Archer's Jerkin of Stamina"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Sungrass"] = {
            ["H3254"] = 1731,
            ["mr"] = 1731,
        },
        ["Beazel's Basher"] = {
            ["H3254"] = 20941,
            ["mr"] = 20941,
        },
        ["Khan's Helmet"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Fortified Cloak of the Whale"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Greenstone Circle of the Boar"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Battle Slayer of Agility"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Royal Boots of Frozen Wrath"] = {
            ["H3254"] = 49999,
            ["mr"] = 49999,
        },
        ["Stranglekelp"] = {
            ["H3254"] = 266,
            ["mr"] = 266,
        },
        ["Wizard's Hand of Spirit"] = {
            ["H3254"] = 116394,
            ["mr"] = 116394,
        },
        ["Cadet Shield of Strength"] = {
            ["H3254"] = 1999,
            ["mr"] = 1999,
        },
        ["Tellurium Necklace of Strength"] = {
            ["H3254"] = 12900,
            ["mr"] = 12900,
        },
        ["Councillor's Cloak of the Eagle"] = {
            ["H3254"] = 24900,
            ["mr"] = 24900,
        },
        ["Codex: Prayer of Fortitude"] = {
            ["H3254"] = 8970,
            ["mr"] = 8970,
        },
        ["Burning Charm"] = {
            ["H3254"] = 192,
            ["mr"] = 192,
        },
        ["Breath of Wind"] = {
            ["H3254"] = 1299,
            ["mr"] = 1299,
        },
        ["Gloves of Old"] = {
            ["H3254"] = 29999,
            ["mr"] = 29999,
        },
        ["Volatile Rum"] = {
            ["H3254"] = 599,
            ["mr"] = 599,
        },
        ["Pattern: Wicked Leather Pants"] = {
            ["H3254"] = 14017,
            ["mr"] = 14017,
        },
        ["Southshore Stout"] = {
            ["H3254"] = 3218,
            ["mr"] = 3218,
        },
        ["Arena Bands"] = {
            ["H3254"] = 69500,
            ["mr"] = 69500,
        },
        ["Gigantic War Axe of the Wolf"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Monk's Staff of the Wolf"] = {
            ["H3254"] = 199680,
            ["mr"] = 199680,
        },
        ["Blackfang"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Hefty Battlehammer of Spirit"] = {
            ["H3254"] = 6492,
            ["mr"] = 6492,
        },
        ["Banded Leggings of Power"] = {
            ["H3254"] = 7100,
            ["mr"] = 7100,
        },
        ["Rumsey Rum Light"] = {
            ["H3254"] = 1474,
            ["mr"] = 1474,
        },
        ["Renegade Pauldrons of the Boar"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Ogron's Sash"] = {
            ["H3254"] = 68152,
            ["mr"] = 68152,
        },
        ["Aurora Cloak"] = {
            ["H3254"] = 3861,
            ["mr"] = 3861,
        },
        ["Ebon Scimitar of Strength"] = {
            ["H3254"] = 31000,
            ["mr"] = 31000,
        },
        ["Ivycloth Pants of the Eagle"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Pattern: Ironfeather Breastplate"] = {
            ["H3254"] = 109500,
            ["mr"] = 109500,
        },
        ["Orb of Mistmantle"] = {
            ["H3254"] = 10599,
            ["mr"] = 10599,
        },
        ["Shadowskin Gloves"] = {
            ["H3254"] = 38750,
            ["mr"] = 38750,
        },
        ["Yellow Power Crystal"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Deadly Scope"] = {
            ["H3254"] = 22499,
            ["mr"] = 22499,
        },
        ["Superior Gloves of the Owl"] = {
            ["H3254"] = 9996,
            ["mr"] = 9996,
        },
        ["Elemental Sharpening Stone"] = {
            ["H3254"] = 49401,
            ["mr"] = 49401,
        },
        ["Elder's Gloves of the Owl"] = {
            ["H3254"] = 4582,
            ["mr"] = 4582,
        },
        ["Dreadblade of the Bear"] = {
            ["H3254"] = 30800,
            ["mr"] = 30800,
        },
        ["Copper Chain Vest"] = {
            ["H3254"] = 450,
            ["mr"] = 450,
        },
        ["Pattern: Tough Scorpid Shoulders"] = {
            ["H3254"] = 4450,
            ["mr"] = 4450,
        },
        ["Wildvine"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Green Hills of Stranglethorn - Page 24"] = {
            ["H3254"] = 570,
            ["mr"] = 570,
        },
        ["Barbecued Buzzard Wing"] = {
            ["H3254"] = 595,
            ["mr"] = 595,
        },
        ["White Bandit Mask"] = {
            ["H3254"] = 10995,
            ["mr"] = 10995,
        },
        ["Blackened Defias Gloves"] = {
            ["H3254"] = 1495,
            ["mr"] = 1495,
        },
        ["Elixir of Superior Defense"] = {
            ["H3254"] = 6397,
            ["mr"] = 6397,
        },
        ["Magus Long Staff of Healing"] = {
            ["H3254"] = 289000,
            ["mr"] = 289000,
        },
        ["Rugged Hide"] = {
            ["H3254"] = 525,
            ["mr"] = 525,
        },
        ["Red Swashbuckler's Shirt"] = {
            ["H3254"] = 1845,
            ["mr"] = 1845,
        },
        ["Forest Hoop of the Monkey"] = {
            ["H3254"] = 27000,
            ["mr"] = 27000,
        },
        ["Ravasaur Scale Boots"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Jet Black Feather"] = {
            ["H3254"] = 195,
            ["mr"] = 195,
        },
        ["Elixir of the Sages"] = {
            ["H3254"] = 9555,
            ["mr"] = 9555,
        },
        ["Pattern: Cindercloth Vest"] = {
            ["H3254"] = 39999,
            ["mr"] = 39999,
        },
        ["Northern Shortsword of Stamina"] = {
            ["H3254"] = 2200,
            ["mr"] = 2200,
        },
        ["Splitting Hatchet of the Wolf"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Polished Jazeraint Armor"] = {
            ["H3254"] = 38000,
            ["mr"] = 38000,
        },
        ["Swashbuckler's Gloves of the Bear"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Twin-bladed Axe of the Boar"] = {
            ["H3254"] = 2472,
            ["mr"] = 2472,
        },
        ["Rune Thread"] = {
            ["H3254"] = 17000,
            ["mr"] = 17000,
        },
        ["Thaumaturgist Staff of the Boar"] = {
            ["H3254"] = 428568,
            ["mr"] = 428568,
        },
        ["Buccaneer's Cape of Intellect"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Plans: Heartseeker"] = {
            ["H3254"] = 54998,
            ["mr"] = 54998,
        },
        ["Sorcerer Mantle of Shadow Wrath"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Thaumaturgist Staff of the Owl"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Pattern: Frostweave Robe"] = {
            ["H3254"] = 7400,
            ["mr"] = 7400,
        },
        ["Khadgar's Whisker"] = {
            ["H3254"] = 239,
            ["mr"] = 239,
        },
        ["Ballast Maul of Strength"] = {
            ["H3254"] = 26411,
            ["mr"] = 26411,
        },
        ["Small Blue Pouch"] = {
            ["H3254"] = 750,
            ["mr"] = 750,
        },
        ["Swashbuckler's Bracers of the Monkey"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Raw Rainbow Fin Albacore"] = {
            ["H3254"] = 49,
            ["mr"] = 49,
        },
        ["Nightfin Soup"] = {
            ["H3254"] = 4652,
            ["mr"] = 4652,
        },
        ["Thallium Hoop of Concentration"] = {
            ["H3254"] = 5777,
            ["mr"] = 5777,
        },
        ["Dwarven Hatchet of Agility"] = {
            ["H3254"] = 1141,
            ["mr"] = 1141,
        },
        ["Rumsey Rum Black Label"] = {
            ["H3254"] = 1495,
            ["mr"] = 1495,
        },
        ["Razor's Edge"] = {
            ["H3254"] = 9899,
            ["mr"] = 9899,
        },
        ["Spiked Club of the Monkey"] = {
            ["H3254"] = 3975,
            ["mr"] = 3975,
        },
        ["Scorpok Pincer"] = {
            ["H3254"] = 5900,
            ["mr"] = 5900,
        },
        ["Conjurer's Cinch of the Whale"] = {
            ["H3254"] = 4703,
            ["mr"] = 4703,
        },
        ["Twilight Pants of the Falcon"] = {
            ["H3254"] = 5090,
            ["mr"] = 5090,
        },
        ["Infantry Leggings of the Eagle"] = {
            ["H3254"] = 2300,
            ["mr"] = 2300,
        },
        ["Sorcerer Hat of Intellect"] = {
            ["H3254"] = 6998,
            ["mr"] = 6998,
        },
        ["Schematic: Moonsight Rifle"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Wolf Rider's Cloak of the Whale"] = {
            ["H3254"] = 41604,
            ["mr"] = 41604,
        },
        ["Ironweb Spider Silk"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Conjurer's Mantle of the Whale"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Lifeless Skull"] = {
            ["H3254"] = 400,
            ["mr"] = 400,
        },
        ["Forest Pendant of the Wolf"] = {
            ["H3254"] = 19449,
            ["mr"] = 19449,
        },
        ["Archer's Jerkin of Power"] = {
            ["H3254"] = 5999,
            ["mr"] = 5999,
        },
        ["Archer's Jerkin of the Whale"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Bandit Cinch of the Eagle"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Blackmouth Oil"] = {
            ["H3254"] = 1210,
            ["mr"] = 1210,
        },
        ["Glowstar Rod of the Eagle"] = {
            ["H3254"] = 190000,
            ["mr"] = 190000,
        },
        ["Phalanx Headguard of the Tiger"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Soldier's Leggings of the Whale"] = {
            ["H3254"] = 1200,
            ["mr"] = 1200,
        },
        ["Black Mageweave Vest"] = {
            ["H3254"] = 7998,
            ["mr"] = 7998,
        },
        ["Elegant Gloves of Fire Resistance"] = {
            ["H3254"] = 37600,
            ["mr"] = 37600,
        },
        ["Rough Stone"] = {
            ["H3254"] = 4,
            ["mr"] = 4,
        },
        ["Battle Slayer of the Monkey"] = {
            ["H3254"] = 3861,
            ["mr"] = 3861,
        },
        ["Rough Boomstick"] = {
            ["H3254"] = 290,
            ["mr"] = 290,
        },
        ["Wild Hog Shank"] = {
            ["H3254"] = 85,
            ["mr"] = 85,
        },
        ["Scroll of Intellect IV"] = {
            ["H3254"] = 993,
            ["mr"] = 993,
        },
        ["Marsh Ring of the Monkey"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["War Knife of Power"] = {
            ["H3254"] = 1499,
            ["mr"] = 1499,
        },
        ["Jagged Star of the Monkey"] = {
            ["H3254"] = 4388,
            ["mr"] = 4388,
        },
        ["Skycaller"] = {
            ["H3254"] = 54500,
            ["mr"] = 54500,
        },
        ["Recipe: Greater Nature Protection Potion"] = {
            ["H3254"] = 905000,
            ["mr"] = 905000,
        },
        ["Resilient Cord"] = {
            ["H3254"] = 5513,
            ["mr"] = 5513,
        },
        ["Serpentskin Girdle"] = {
            ["H3254"] = 11600,
            ["mr"] = 11600,
        },
        ["Elder's Pants of the Eagle"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Runecloth Bandage"] = {
            ["H3254"] = 923,
            ["mr"] = 923,
        },
        ["Dalaran Sharp"] = {
            ["H3254"] = 19,
            ["mr"] = 19,
        },
        ["Lord's Armguards of Healing"] = {
            ["H3254"] = 16487,
            ["mr"] = 16487,
        },
        ["Papal Fez"] = {
            ["H3254"] = 119500,
            ["mr"] = 119500,
        },
        ["Sentinel Bracers of Stamina"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Archer's Bracers of the Monkey"] = {
            ["H3254"] = 4710,
            ["mr"] = 4710,
        },
        ["Recipe: Swiftness Potion"] = {
            ["H3254"] = 17999,
            ["mr"] = 17999,
        },
        ["Embossed Leather Pants"] = {
            ["H3254"] = 2377,
            ["mr"] = 2377,
        },
        ["Twilight Pants of the Owl"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Councillor's Cuffs of the Eagle"] = {
            ["H3254"] = 18999,
            ["mr"] = 18999,
        },
        ["Magician Staff of Stamina"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Greater Eternal Essence"] = {
            ["H3254"] = 32564,
            ["mr"] = 32564,
        },
        ["Minor Mana Potion"] = {
            ["H3254"] = 40,
            ["mr"] = 40,
        },
        ["Scroll of Spirit III"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Frost Protection Potion"] = {
            ["H3254"] = 899,
            ["mr"] = 899,
        },
        ["Obsidian Greaves"] = {
            ["H3254"] = 117200,
            ["mr"] = 117200,
        },
        ["Gleaming Claymore of the Bear"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Copper Ore"] = {
            ["H3254"] = 43,
            ["mr"] = 43,
        },
        ["Banded Helm of Stamina"] = {
            ["H3254"] = 12500,
            ["mr"] = 12500,
        },
        ["Massive Battle Axe of the Monkey"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Arachnidian Bracelets of Fiery Wrath"] = {
            ["H3254"] = 36551,
            ["mr"] = 36551,
        },
        ["Shadoweave Shoulders"] = {
            ["H3254"] = 58000,
            ["mr"] = 58000,
        },
        ["Crisp Spider Meat"] = {
            ["H3254"] = 51,
            ["mr"] = 51,
        },
        ["Dragonbreath Chili"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Heavy Lamellar Vambraces of the Boar"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Schematic: Goblin Jumper Cables XL"] = {
            ["H3254"] = 22600,
            ["mr"] = 22600,
        },
        ["Libram of Voracity"] = {
            ["H3254"] = 520000,
            ["mr"] = 520000,
        },
        ["Burnished Gloves"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Large Radiant Shard"] = {
            ["H3254"] = 17999,
            ["mr"] = 17999,
        },
        ["Pattern: Chimeric Boots"] = {
            ["H3254"] = 10336,
            ["mr"] = 10336,
        },
        ["Greater Magic Wand"] = {
            ["H3254"] = 1800,
            ["mr"] = 1800,
        },
        ["Shadow Protection Potion"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Green Hills of Stranglethorn - Page 8"] = {
            ["H3254"] = 566,
            ["mr"] = 566,
        },
        ["Ember Wand of the Falcon"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Swashbuckler's Eyepatch of the Eagle"] = {
            ["H3254"] = 49500,
            ["mr"] = 49500,
        },
        ["Assassination Blade"] = {
            ["H3254"] = 579999,
            ["mr"] = 579999,
        },
        ["Smooth Raptor Skin"] = {
            ["H3254"] = 2086,
            ["mr"] = 2086,
        },
        ["Gloves of Holy Might"] = {
            ["H3254"] = 259900,
            ["mr"] = 259900,
        },
        ["Scroll of Protection IV"] = {
            ["H3254"] = 4582,
            ["mr"] = 4582,
        },
        ["Heaven's Light"] = {
            ["H3254"] = 69571,
            ["mr"] = 69571,
        },
        ["Green Hills of Stranglethorn - Page 20"] = {
            ["H3254"] = 554,
            ["mr"] = 554,
        },
        ["Huntsman's Bands of the Eagle"] = {
            ["H3254"] = 7832,
            ["mr"] = 7832,
        },
        ["The Butcher"] = {
            ["H3254"] = 510000,
            ["mr"] = 510000,
        },
        ["Sage's Stave of the Whale"] = {
            ["H3254"] = 3914,
            ["mr"] = 3914,
        },
        ["Minor Recombobulator"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Rune Sword of Stamina"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Elder's Pants of the Whale"] = {
            ["H3254"] = 3828,
            ["mr"] = 3828,
        },
        ["Petrified Shinbone"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Gromsblood"] = {
            ["H3254"] = 3784,
            ["mr"] = 3784,
        },
        ["Stout Battlehammer of the Monkey"] = {
            ["H3254"] = 3918,
            ["mr"] = 3918,
        },
        ["Iron Buckle"] = {
            ["H3254"] = 913,
            ["mr"] = 913,
        },
        ["Stonecloth Belt"] = {
            ["H3254"] = 8998,
            ["mr"] = 8998,
        },
        ["Ivycloth Boots of the Whale"] = {
            ["H3254"] = 3399,
            ["mr"] = 3399,
        },
        ["Pattern: Greater Adept's Robe"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Turtle Meat"] = {
            ["H3254"] = 199,
            ["mr"] = 199,
        },
        ["Defender Gauntlets of the Bear"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Priest's Mace of Healing"] = {
            ["H3254"] = 998,
            ["mr"] = 998,
        },
        ["Royal Gloves of the Falcon"] = {
            ["H3254"] = 12824,
            ["mr"] = 12824,
        },
        ["Mindthrust Bracers"] = {
            ["H3254"] = 419999,
            ["mr"] = 419999,
        },
        ["Stringy Vulture Meat"] = {
            ["H3254"] = 127,
            ["mr"] = 127,
        },
        ["Frostsaber E'ko"] = {
            ["H3254"] = 26204,
            ["mr"] = 26204,
        },
        ["Turtle Scale Gloves"] = {
            ["H3254"] = 53000,
            ["mr"] = 53000,
        },
        ["Veteran Armor"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Enchanter's Cowl"] = {
            ["H3254"] = 10200,
            ["mr"] = 10200,
        },
        ["Truesilver Ore"] = {
            ["H3254"] = 1426,
            ["mr"] = 1426,
        },
        ["Dreadblade of Strength"] = {
            ["H3254"] = 46394,
            ["mr"] = 46394,
        },
        ["Ghost Dye"] = {
            ["H3254"] = 49800,
            ["mr"] = 49800,
        },
        ["Aquadynamic Fish Attractor"] = {
            ["H3254"] = 862,
            ["mr"] = 862,
        },
        ["High Councillor's Cloak of Frozen Wrath"] = {
            ["H3254"] = 890000,
            ["mr"] = 890000,
        },
        ["Rugged Leather"] = {
            ["H3254"] = 799,
            ["mr"] = 799,
        },
        ["Recipe: Transmute Mithril to Truesilver"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Large Blue Sack"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Lesser Invisibility Potion"] = {
            ["H3254"] = 300,
            ["mr"] = 300,
        },
        ["Catseye Elixir"] = {
            ["H3254"] = 240,
            ["mr"] = 240,
        },
        ["Fighter Broadsword of the Monkey"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Schematic: Arcanite Dragonling"] = {
            ["H3254"] = 299999,
            ["mr"] = 299999,
        },
        ["Jade Bracers of Stamina"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Heartseeking Crossbow"] = {
            ["H3254"] = 409000,
            ["mr"] = 409000,
        },
        ["Grilled Squid"] = {
            ["H3254"] = 1525,
            ["mr"] = 1525,
        },
        ["Green Dragonscale"] = {
            ["H3254"] = 525,
            ["mr"] = 525,
        },
        ["Hawkeye's Gloves"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Golden Pearl"] = {
            ["H3254"] = 145000,
            ["mr"] = 145000,
        },
        ["Longjaw Mud Snapper"] = {
            ["H3254"] = 20,
            ["mr"] = 20,
        },
        ["Great Rage Potion"] = {
            ["H3254"] = 995,
            ["mr"] = 995,
        },
        ["Blue Power Crystal"] = {
            ["H3254"] = 654,
            ["mr"] = 654,
        },
        ["Dervish Belt of the Owl"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Shield of Thorsen"] = {
            ["H3254"] = 23300,
            ["mr"] = 23300,
        },
        ["Imposing Shoulders of Defense"] = {
            ["H3254"] = 24000,
            ["mr"] = 24000,
        },
        ["White Spider Meat"] = {
            ["H3254"] = 269,
            ["mr"] = 269,
        },
        ["Severing Axe of Strength"] = {
            ["H3254"] = 3576,
            ["mr"] = 3576,
        },
        ["Tellurium Necklace of the Eagle"] = {
            ["H3254"] = 21000,
            ["mr"] = 21000,
        },
        ["Ebonhold Shoulderpads"] = {
            ["H3254"] = 28519,
            ["mr"] = 28519,
        },
        ["Recipe: Soothing Turtle Bisque"] = {
            ["H3254"] = 6900,
            ["mr"] = 6900,
        },
        ["Pattern: Red Linen Bag"] = {
            ["H3254"] = 395,
            ["mr"] = 395,
        },
        ["Goblin Jumper Cables XL"] = {
            ["H3254"] = 320000,
            ["mr"] = 320000,
        },
        ["Scarlet Wristguards"] = {
            ["H3254"] = 5750,
            ["mr"] = 5750,
        },
        ["Recipe: Mighty Troll's Blood Potion"] = {
            ["H3254"] = 11682,
            ["mr"] = 11682,
        },
        ["Ranger Bow"] = {
            ["H3254"] = 48750,
            ["mr"] = 48750,
        },
        ["Hellslayer Battle Axe"] = {
            ["H3254"] = 49800,
            ["mr"] = 49800,
        },
        ["Wanderer's Gloves of the Owl"] = {
            ["H3254"] = 28448,
            ["mr"] = 28448,
        },
        ["Recipe: Mystery Stew"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Abjurer's Pants of the Eagle"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Training Sword of the Bear"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Stonecutter Claymore of the Boar"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Tracker's Shoulderpads of the Boar"] = {
            ["H3254"] = 12900,
            ["mr"] = 12900,
        },
        ["Mail Combat Headguard"] = {
            ["H3254"] = 4575,
            ["mr"] = 4575,
        },
        ["Hillman's Shoulders"] = {
            ["H3254"] = 1798,
            ["mr"] = 1798,
        },
        ["40 Pound Grouper"] = {
            ["H3254"] = 22900,
            ["mr"] = 22900,
        },
        ["Silksand Wraps"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Refreshing Spring Water"] = {
            ["H3254"] = 50,
            ["mr"] = 50,
        },
        ["Ivycloth Tunic of the Owl"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Viking Sword of Power"] = {
            ["H3254"] = 43232,
            ["mr"] = 43232,
        },
        ["Recipe: Transmute Earth to Life"] = {
            ["H3254"] = 978900,
            ["mr"] = 978900,
        },
        ["Chromite Barbute"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Abjurer's Hood of the Owl"] = {
            ["H3254"] = 28882,
            ["mr"] = 28882,
        },
        ["Huge Emerald"] = {
            ["H3254"] = 19499,
            ["mr"] = 19499,
        },
        ["Shadow Oil"] = {
            ["H3254"] = 8090,
            ["mr"] = 8090,
        },
        ["Wild Steelbloom"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Heavy Leather Ball"] = {
            ["H3254"] = 4469,
            ["mr"] = 4469,
        },
        ["Demon Blade of Healing"] = {
            ["H3254"] = 239661,
            ["mr"] = 239661,
        },
        ["Trueshot Bow of the Eagle"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Hi-Impact Mithril Slugs"] = {
            ["H3254"] = 13,
            ["mr"] = 13,
        },
        ["Lambent Scale Cloak"] = {
            ["H3254"] = 3681,
            ["mr"] = 3681,
        },
        ["Insignia Belt"] = {
            ["H3254"] = 1990,
            ["mr"] = 1990,
        },
        ["Wanderer's Gloves of the Monkey"] = {
            ["H3254"] = 23000,
            ["mr"] = 23000,
        },
        ["Councillor's Sash of Healing"] = {
            ["H3254"] = 42500,
            ["mr"] = 42500,
        },
        ["Incendicite Ore"] = {
            ["H3254"] = 514,
            ["mr"] = 514,
        },
        ["Bandit Pants of the Owl"] = {
            ["H3254"] = 3413,
            ["mr"] = 3413,
        },
        ["Greater Scythe of the Boar"] = {
            ["H3254"] = 22900,
            ["mr"] = 22900,
        },
        ["Greater Mystic Wand"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Ritual Bands of Healing"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Cabalist Belt of the Eagle"] = {
            ["H3254"] = 10499,
            ["mr"] = 10499,
        },
        ["Silk-threaded Trousers"] = {
            ["H3254"] = 1393,
            ["mr"] = 1393,
        },
        ["Mighty Rage Potion"] = {
            ["H3254"] = 13190,
            ["mr"] = 13190,
        },
        ["Jade Breastplate of the Whale"] = {
            ["H3254"] = 56934,
            ["mr"] = 56934,
        },
        ["Conjurer's Mantle of Frozen Wrath"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Formula: Enchant Cloak - Minor Agility"] = {
            ["H3254"] = 11798,
            ["mr"] = 11798,
        },
        ["Bright Gloves"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Huge Venom Sac"] = {
            ["H3254"] = 1725,
            ["mr"] = 1725,
        },
        ["War Knife of Stamina"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Monstrous War Axe of the Boar"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Blinding Powder"] = {
            ["H3254"] = 469,
            ["mr"] = 469,
        },
        ["Raw Spinefin Halibut"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Insignia Boots"] = {
            ["H3254"] = 30755,
            ["mr"] = 30755,
        },
        ["Infiltrator Pants of the Owl"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Barbaric Battle Axe of Strength"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Greater Maul of the Gorilla"] = {
            ["H3254"] = 49800,
            ["mr"] = 49800,
        },
        ["Hefty Battlehammer of the Whale"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Black Velvet Robes"] = {
            ["H3254"] = 79500,
            ["mr"] = 79500,
        },
        ["Heavy Stone"] = {
            ["H3254"] = 400,
            ["mr"] = 400,
        },
        ["Pattern: Robe of the Archmage"] = {
            ["H3254"] = 899800,
            ["mr"] = 899800,
        },
        ["Schematic: Gnomish Alarm-O-Bot"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Birchwood Maul of Spirit"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Seer's Pants"] = {
            ["H3254"] = 1267,
            ["mr"] = 1267,
        },
        ["Trueshot Bow of the Monkey"] = {
            ["H3254"] = 40666,
            ["mr"] = 40666,
        },
        ["Pattern: Devilsaur Gauntlets"] = {
            ["H3254"] = 249000,
            ["mr"] = 249000,
        },
        ["Mystical Headwrap of Nature Resistance"] = {
            ["H3254"] = 65863,
            ["mr"] = 65863,
        },
        ["Morning Glory Dew"] = {
            ["H3254"] = 326,
            ["mr"] = 326,
        },
        ["Twilight Pants of Healing"] = {
            ["H3254"] = 14113,
            ["mr"] = 14113,
        },
        ["Conjurer's Bracers of the Whale"] = {
            ["H3254"] = 8200,
            ["mr"] = 8200,
        },
        ["Formula: Enchant Gloves - Greater Agility"] = {
            ["H3254"] = 149899,
            ["mr"] = 149899,
        },
        ["Thick Scale Belt of the Bear"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Infiltrator Armor of the Owl"] = {
            ["H3254"] = 6900,
            ["mr"] = 6900,
        },
        ["Dwarven Magestaff of the Owl"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Plans: Inlaid Mithril Cylinder"] = {
            ["H3254"] = 5700,
            ["mr"] = 5700,
        },
        ["Plaguebloom"] = {
            ["H3254"] = 2377,
            ["mr"] = 2377,
        },
        ["Greater Magic Essence"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Pattern: Guardian Cloak"] = {
            ["H3254"] = 1890,
            ["mr"] = 1890,
        },
        ["Scroll of Agility"] = {
            ["H3254"] = 299,
            ["mr"] = 299,
        },
        ["Dense Weightstone"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Jade"] = {
            ["H3254"] = 1193,
            ["mr"] = 1193,
        },
        ["Marble Circle of the Bear"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Wing of the Whelpling"] = {
            ["H3254"] = 32499,
            ["mr"] = 32499,
        },
        ["Trueshot Bow of Agility"] = {
            ["H3254"] = 19999,
            ["mr"] = 19999,
        },
        ["Archer's Belt of the Bear"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Hillborne Axe of Strength"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Thick Hide"] = {
            ["H3254"] = 508,
            ["mr"] = 508,
        },
        ["Tasty Lion Steak"] = {
            ["H3254"] = 891,
            ["mr"] = 891,
        },
        ["Battering Hammer of the Gorilla"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Clam Meat"] = {
            ["H3254"] = 194,
            ["mr"] = 194,
        },
        ["Emblazoned Boots"] = {
            ["H3254"] = 5007,
            ["mr"] = 5007,
        },
        ["Mistscape Boots"] = {
            ["H3254"] = 8962,
            ["mr"] = 8962,
        },
        ["Greenstone Circle of the Gorilla"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Knight's Breastplate of the Bear"] = {
            ["H3254"] = 9750,
            ["mr"] = 9750,
        },
        ["Knight's Legguards of Strength"] = {
            ["H3254"] = 11874,
            ["mr"] = 11874,
        },
        ["Yorgen Bracers"] = {
            ["H3254"] = 97500,
            ["mr"] = 97500,
        },
        ["Pattern: Reinforced Woolen Shoulders"] = {
            ["H3254"] = 8798,
            ["mr"] = 8798,
        },
        ["Training Sword of the Eagle"] = {
            ["H3254"] = 1899,
            ["mr"] = 1899,
        },
        ["Ridge Cleaver of Power"] = {
            ["H3254"] = 7999,
            ["mr"] = 7999,
        },
        ["Pattern: Thick Murloc Armor"] = {
            ["H3254"] = 734,
            ["mr"] = 734,
        },
        ["Silver Ore"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Thick Furry Mane"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Dervish Leggings of Power"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Superior Cloak of Spirit"] = {
            ["H3254"] = 2176,
            ["mr"] = 2176,
        },
        ["Polished Zweihander of Power"] = {
            ["H3254"] = 10193,
            ["mr"] = 10193,
        },
        ["Phalanx Shield of the Tiger"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Cuergo's Gold"] = {
            ["H3254"] = 1599,
            ["mr"] = 1599,
        },
        ["Ebonhold Helmet"] = {
            ["H3254"] = 35600,
            ["mr"] = 35600,
        },
        ["Dreadmist Bracers"] = {
            ["H3254"] = 54999,
            ["mr"] = 54999,
        },
        ["Recipe: Savory Deviate Delight"] = {
            ["H3254"] = 249000,
            ["mr"] = 249000,
        },
        ["Celestial Bindings of Intellect"] = {
            ["H3254"] = 81000,
            ["mr"] = 81000,
        },
        ["Pattern: Runecloth Headband"] = {
            ["H3254"] = 31691,
            ["mr"] = 31691,
        },
        ["Ridge Cleaver of Stamina"] = {
            ["H3254"] = 3700,
            ["mr"] = 3700,
        },
        ["Cured Rugged Hide"] = {
            ["H3254"] = 188899,
            ["mr"] = 188899,
        },
        ["Frostbit Staff"] = {
            ["H3254"] = 34399,
            ["mr"] = 34399,
        },
        ["Archer's Gloves of the Falcon"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Battle Slayer of the Wolf"] = {
            ["H3254"] = 5343,
            ["mr"] = 5343,
        },
        ["Bristle Whisker Catfish"] = {
            ["H3254"] = 26,
            ["mr"] = 26,
        },
        ["Steel Bar"] = {
            ["H3254"] = 1560,
            ["mr"] = 1560,
        },
        ["Cadet Shield of the Bear"] = {
            ["H3254"] = 1950,
            ["mr"] = 1950,
        },
        ["Training Sword of Strength"] = {
            ["H3254"] = 2799,
            ["mr"] = 2799,
        },
        ["Schematic: Minor Recombobulator"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Gypsy Trousers of the Eagle"] = {
            ["H3254"] = 1393,
            ["mr"] = 1393,
        },
        ["Gleaming Claymore of Stamina"] = {
            ["H3254"] = 6218,
            ["mr"] = 6218,
        },
        ["Winterfall E'ko"] = {
            ["H3254"] = 8900,
            ["mr"] = 8900,
        },
        ["Gray Woolen Shirt"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Battleforge Legguards of the Bear"] = {
            ["H3254"] = 6442,
            ["mr"] = 6442,
        },
        ["Doombringer"] = {
            ["H3254"] = 165000,
            ["mr"] = 165000,
        },
        ["Pattern: Brightcloth Cloak"] = {
            ["H3254"] = 19800,
            ["mr"] = 19800,
        },
        ["Vanadium Loop of the Owl"] = {
            ["H3254"] = 39900,
            ["mr"] = 39900,
        },
        ["Giant Egg"] = {
            ["H3254"] = 1195,
            ["mr"] = 1195,
        },
        ["Gossamer Tunic of the Owl"] = {
            ["H3254"] = 24800,
            ["mr"] = 24800,
        },
        ["Chromite Bracers"] = {
            ["H3254"] = 7200,
            ["mr"] = 7200,
        },
        ["Dervish Belt of Intellect"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Blue Sapphire"] = {
            ["H3254"] = 199500,
            ["mr"] = 199500,
        },
        ["Battleforge Girdle of the Bear"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Medium Hide"] = {
            ["H3254"] = 579,
            ["mr"] = 579,
        },
        ["Dwarven Magestaff of the Bear"] = {
            ["H3254"] = 11400,
            ["mr"] = 11400,
        },
        ["Boar Ribs"] = {
            ["H3254"] = 159,
            ["mr"] = 159,
        },
        ["Medallion of Faith"] = {
            ["H3254"] = 9533,
            ["mr"] = 9533,
        },
        ["Demon Band"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Ring of the Moon"] = {
            ["H3254"] = 11499,
            ["mr"] = 11499,
        },
        ["Greater Frost Protection Potion"] = {
            ["H3254"] = 29995,
            ["mr"] = 29995,
        },
        ["Razor Blade of the Monkey"] = {
            ["H3254"] = 24400,
            ["mr"] = 24400,
        },
        ["Sorcerer Sash of the Whale"] = {
            ["H3254"] = 6300,
            ["mr"] = 6300,
        },
        ["Knight's Crest of Stamina"] = {
            ["H3254"] = 12500,
            ["mr"] = 12500,
        },
        ["Huntsman's Belt of the Owl"] = {
            ["H3254"] = 7153,
            ["mr"] = 7153,
        },
        ["Soldier's Shield of the Gorilla"] = {
            ["H3254"] = 4012,
            ["mr"] = 4012,
        },
        ["Melon Juice"] = {
            ["H3254"] = 84,
            ["mr"] = 84,
        },
        ["Nightsky Cloak"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Green Leather Bag"] = {
            ["H3254"] = 1410,
            ["mr"] = 1410,
        },
        ["Pattern: Tough Scorpid Boots"] = {
            ["H3254"] = 5700,
            ["mr"] = 5700,
        },
        ["Viking Warhammer"] = {
            ["H3254"] = 94500,
            ["mr"] = 94500,
        },
        ["Superior Tunic of Spirit"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Linen Bag"] = {
            ["H3254"] = 396,
            ["mr"] = 396,
        },
        ["Dream Dust"] = {
            ["H3254"] = 2600,
            ["mr"] = 2600,
        },
        ["Disciple's Stein of Intellect"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Fish Oil"] = {
            ["H3254"] = 7,
            ["mr"] = 7,
        },
        ["Sequoia Branch of the Tiger"] = {
            ["H3254"] = 29967,
            ["mr"] = 29967,
        },
        ["Schematic: Dark Iron Bomb"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Mystical Armor of the Owl"] = {
            ["H3254"] = 38500,
            ["mr"] = 38500,
        },
        ["Elder's Sash of the Owl"] = {
            ["H3254"] = 5999,
            ["mr"] = 5999,
        },
        ["Ruined Leather Scraps"] = {
            ["H3254"] = 14,
            ["mr"] = 14,
        },
        ["Small Leather Ammo Pouch"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Stormwind Brie"] = {
            ["H3254"] = 146,
            ["mr"] = 146,
        },
        ["Recipe: Shadow Oil"] = {
            ["H3254"] = 19799,
            ["mr"] = 19799,
        },
        ["Ring of the Underwood"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Scouting Tunic of Spirit"] = {
            ["H3254"] = 2200,
            ["mr"] = 2200,
        },
        ["Viking Sword of Strength"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Recipe: Tender Wolf Steak"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Mageweave Bag"] = {
            ["H3254"] = 9200,
            ["mr"] = 9200,
        },
        ["Bloodrazor"] = {
            ["H3254"] = 915000,
            ["mr"] = 915000,
        },
        ["Thornstone Sledgehammer"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Glowing Magical Bracelets"] = {
            ["H3254"] = 28710,
            ["mr"] = 28710,
        },
        ["Tigerseye"] = {
            ["H3254"] = 187,
            ["mr"] = 187,
        },
        ["Durable Belt of the Whale"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Crawler Claw"] = {
            ["H3254"] = 119,
            ["mr"] = 119,
        },
        ["Silver-lined Belt"] = {
            ["H3254"] = 34999,
            ["mr"] = 34999,
        },
        ["Pattern: Pilferer's Gloves"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Azure Silk Pants"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Fiery Core"] = {
            ["H3254"] = 95500,
            ["mr"] = 95500,
        },
        ["Formula: Enchant Bracer - Superior Stamina"] = {
            ["H3254"] = 58500,
            ["mr"] = 58500,
        },
        ["Archer's Cloak of the Owl"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Battleforge Gauntlets of Power"] = {
            ["H3254"] = 14406,
            ["mr"] = 14406,
        },
        ["Mooncloth Bag"] = {
            ["H3254"] = 252400,
            ["mr"] = 252400,
        },
        ["Percussion Shotgun of the Falcon"] = {
            ["H3254"] = 213492,
            ["mr"] = 213492,
        },
        ["Small Glowing Shard"] = {
            ["H3254"] = 295,
            ["mr"] = 295,
        },
        ["Short Bastard Sword of Stamina"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Coarse Stone"] = {
            ["H3254"] = 416,
            ["mr"] = 416,
        },
        ["Regal Cloak of the Whale"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Dwarven Hand Cannon"] = {
            ["H3254"] = 2270000,
            ["mr"] = 2270000,
        },
        ["Mithril Tube"] = {
            ["H3254"] = 9660,
            ["mr"] = 9660,
        },
        ["Superior Buckler of the Boar"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Pointy Crocolisk Tooth"] = {
            ["H3254"] = 498,
            ["mr"] = 498,
        },
        ["Gallant Flamberge of Stamina"] = {
            ["H3254"] = 94036,
            ["mr"] = 94036,
        },
        ["Silk Bandage"] = {
            ["H3254"] = 245,
            ["mr"] = 245,
        },
        ["Elixir of the Mongoose"] = {
            ["H3254"] = 16880,
            ["mr"] = 16880,
        },
        ["Mystical Orb of the Owl"] = {
            ["H3254"] = 25953,
            ["mr"] = 25953,
        },
        ["Green Hills of Stranglethorn - Page 21"] = {
            ["H3254"] = 488,
            ["mr"] = 488,
        },
        ["Slimy Murloc Scale"] = {
            ["H3254"] = 82,
            ["mr"] = 82,
        },
        ["Rethban Ore"] = {
            ["H3254"] = 1750,
            ["mr"] = 1750,
        },
        ["Cresting Charm"] = {
            ["H3254"] = 4375,
            ["mr"] = 4375,
        },
        ["Severing Axe of Power"] = {
            ["H3254"] = 15124,
            ["mr"] = 15124,
        },
        ["Recipe: Greater Stoneshield Potion"] = {
            ["H3254"] = 89500,
            ["mr"] = 89500,
        },
        ["Overlord's Vambraces of the Bear"] = {
            ["H3254"] = 17976,
            ["mr"] = 17976,
        },
        ["Spellbinder Robe"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Greater Scythe of Agility"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Small Flame Sac"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Sentinel Bracers of Spirit"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Elixir of Greater Agility"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Plans: Mithril Spurs"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Undamaged Hippogryph Feather"] = {
            ["H3254"] = 487,
            ["mr"] = 487,
        },
        ["Cassandra's Grace"] = {
            ["H3254"] = 1499999,
            ["mr"] = 1499999,
        },
        ["Ivycloth Cloak of Fiery Wrath"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Earthborn Kilt"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Strange Dust"] = {
            ["H3254"] = 200,
            ["mr"] = 200,
        },
        ["Windchaser Coronet"] = {
            ["H3254"] = 12800,
            ["mr"] = 12800,
        },
        ["Heavy Leather"] = {
            ["H3254"] = 187,
            ["mr"] = 187,
        },
        ["Viking Sword of the Bear"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Recipe: Invisibility Potion"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Short Bastard Sword of Power"] = {
            ["H3254"] = 1195,
            ["mr"] = 1195,
        },
        ["Arachnidian Pauldrons of Frozen Wrath"] = {
            ["H3254"] = 106998,
            ["mr"] = 106998,
        },
        ["Jungle Remedy"] = {
            ["H3254"] = 387,
            ["mr"] = 387,
        },
        ["Lifestone"] = {
            ["H3254"] = 400000,
            ["mr"] = 400000,
        },
        ["Shimmering Boots of the Whale"] = {
            ["H3254"] = 2438,
            ["mr"] = 2438,
        },
        ["Scroll of Strength"] = {
            ["H3254"] = 564,
            ["mr"] = 564,
        },
        ["Jazeraint Cloak of Strength"] = {
            ["H3254"] = 7129,
            ["mr"] = 7129,
        },
        ["Disciple's Stein of Healing"] = {
            ["H3254"] = 1919,
            ["mr"] = 1919,
        },
        ["Sentinel Breastplate of the Whale"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Shellfish"] = {
            ["H3254"] = 66,
            ["mr"] = 66,
        },
        ["Liferoot"] = {
            ["H3254"] = 94,
            ["mr"] = 94,
        },
        ["Medicine Staff of Stamina"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Deeprock Salt"] = {
            ["H3254"] = 250,
            ["mr"] = 250,
        },
        ["Stonesplinter Axe"] = {
            ["H3254"] = 965,
            ["mr"] = 965,
        },
        ["Large Glowing Shard"] = {
            ["H3254"] = 1425,
            ["mr"] = 1425,
        },
        ["Elder's Pants of the Monkey"] = {
            ["H3254"] = 9293,
            ["mr"] = 9293,
        },
        ["Giant Club of Strength"] = {
            ["H3254"] = 11999,
            ["mr"] = 11999,
        },
        ["Nightshade Boots of the Bear"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Libram of Resilience"] = {
            ["H3254"] = 47579,
            ["mr"] = 47579,
        },
        ["Shimmering Robe of Arcane Wrath"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Unearthed Bands of the Bear"] = {
            ["H3254"] = 119999,
            ["mr"] = 119999,
        },
        ["Schematic: Mechanical Squirrel"] = {
            ["H3254"] = 989,
            ["mr"] = 989,
        },
        ["Ardent Custodian"] = {
            ["H3254"] = 399999,
            ["mr"] = 399999,
        },
        ["Exploding Shot"] = {
            ["H3254"] = 99,
            ["mr"] = 99,
        },
        ["Golden Skeleton Key"] = {
            ["H3254"] = 695,
            ["mr"] = 695,
        },
        ["Large Glimmering Shard"] = {
            ["H3254"] = 816,
            ["mr"] = 816,
        },
        ["Wolfrunner Shoes"] = {
            ["H3254"] = 420000,
            ["mr"] = 420000,
        },
        ["Billy Club"] = {
            ["H3254"] = 1132,
            ["mr"] = 1132,
        },
        ["Gossamer Gloves of Spirit"] = {
            ["H3254"] = 17005,
            ["mr"] = 17005,
        },
        ["Twilight Pants of the Whale"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Seer's Gloves"] = {
            ["H3254"] = 1300,
            ["mr"] = 1300,
        },
        ["Warbear Leather"] = {
            ["H3254"] = 3850,
            ["mr"] = 3850,
        },
        ["Elemental Earth"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Sentinel Gloves of the Monkey"] = {
            ["H3254"] = 8771,
            ["mr"] = 8771,
        },
        ["Lunar Slippers of the Whale"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Lunar Slippers of the Owl"] = {
            ["H3254"] = 16000,
            ["mr"] = 16000,
        },
        ["Recipe: Goretusk Liver Pie"] = {
            ["H3254"] = 300,
            ["mr"] = 300,
        },
        ["Crusader's Armguards of the Wolf"] = {
            ["H3254"] = 32929,
            ["mr"] = 32929,
        },
        ["Warbringer's Shield of Stamina"] = {
            ["H3254"] = 37900,
            ["mr"] = 37900,
        },
        ["Firefin Snapper"] = {
            ["H3254"] = 95,
            ["mr"] = 95,
        },
        ["Raw Brilliant Smallfish"] = {
            ["H3254"] = 84,
            ["mr"] = 84,
        },
        ["Dreamweave Circlet"] = {
            ["H3254"] = 99900,
            ["mr"] = 99900,
        },
        ["\"Mage-Eye\" Blunderbuss"] = {
            ["H3254"] = 5700,
            ["mr"] = 5700,
        },
        ["Elixir of Detect Undead"] = {
            ["H3254"] = 529,
            ["mr"] = 529,
        },
        ["Driftwood Club"] = {
            ["H3254"] = 3200,
            ["mr"] = 3200,
        },
        ["Evil Bat Eye"] = {
            ["H3254"] = 2800,
            ["mr"] = 2800,
        },
        ["Illusion Dust"] = {
            ["H3254"] = 5299,
            ["mr"] = 5299,
        },
        ["Conjurer's Mantle of the Owl"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Hacking Cleaver of the Bear"] = {
            ["H3254"] = 11220,
            ["mr"] = 11220,
        },
        ["Winter Squid"] = {
            ["H3254"] = 1385,
            ["mr"] = 1385,
        },
        ["Turtle Scale Breastplate"] = {
            ["H3254"] = 89998,
            ["mr"] = 89998,
        },
        ["Headhunter's Mitts of the Bear"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Hollowfang Blade"] = {
            ["H3254"] = 2300,
            ["mr"] = 2300,
        },
        ["Glyphed Breastplate"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Faded Photograph"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Formula: Enchant Boots - Greater Stamina"] = {
            ["H3254"] = 35499,
            ["mr"] = 35499,
        },
        ["Chunk of Boar Meat"] = {
            ["H3254"] = 199,
            ["mr"] = 199,
        },
        ["Flask of Mojo"] = {
            ["H3254"] = 306,
            ["mr"] = 306,
        },
        ["Bloodwoven Bracers of the Owl"] = {
            ["H3254"] = 8050,
            ["mr"] = 8050,
        },
        ["Cabalist Chestpiece of Intellect"] = {
            ["H3254"] = 33852,
            ["mr"] = 33852,
        },
        ["Brutal War Axe of Strength"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Ghost Mushroom"] = {
            ["H3254"] = 11800,
            ["mr"] = 11800,
        },
        ["Libram of Rumination"] = {
            ["H3254"] = 4800,
            ["mr"] = 4800,
        },
        ["Pattern: Wicked Leather Belt"] = {
            ["H3254"] = 49999,
            ["mr"] = 49999,
        },
        ["Rod of Molten Fire"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Glimmering Mail Breastplate"] = {
            ["H3254"] = 7800,
            ["mr"] = 7800,
        },
        ["Bone Dust"] = {
            ["H3254"] = 293,
            ["mr"] = 293,
        },
        ["Devout Gloves"] = {
            ["H3254"] = 34599,
            ["mr"] = 34599,
        },
        ["Plans: Runed Mithril Hammer"] = {
            ["H3254"] = 26114,
            ["mr"] = 26114,
        },
        ["Cuergo's Gold with Worm"] = {
            ["H3254"] = 160158,
            ["mr"] = 160158,
        },
        ["Sniper Rifle of the Wolf"] = {
            ["H3254"] = 35000,
            ["mr"] = 35000,
        },
        ["Vibroblade"] = {
            ["H3254"] = 59999,
            ["mr"] = 59999,
        },
        ["Formula: Enchant Gloves - Mining"] = {
            ["H3254"] = 1462,
            ["mr"] = 1462,
        },
        ["Knight's Breastplate of the Whale"] = {
            ["H3254"] = 17745,
            ["mr"] = 17745,
        },
        ["Dreamfoil"] = {
            ["H3254"] = 4882,
            ["mr"] = 4882,
        },
        ["Harpyclaw Short Bow"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Stout Battlehammer of the Tiger"] = {
            ["H3254"] = 3341,
            ["mr"] = 3341,
        },
        ["Major Mana Potion"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Pristine Black Diamond"] = {
            ["H3254"] = 199899,
            ["mr"] = 199899,
        },
        ["Formula: Enchant Cloak - Lesser Shadow Resistance"] = {
            ["H3254"] = 3707,
            ["mr"] = 3707,
        },
        ["Blasted Boar Lung"] = {
            ["H3254"] = 3899,
            ["mr"] = 3899,
        },
        ["Stonegrip Gauntlets"] = {
            ["H3254"] = 909000,
            ["mr"] = 909000,
        },
        ["Large Opal"] = {
            ["H3254"] = 8900,
            ["mr"] = 8900,
        },
        ["Gypsy Trousers of the Whale"] = {
            ["H3254"] = 1900,
            ["mr"] = 1900,
        },
        ["Savage Axe of the Monkey"] = {
            ["H3254"] = 16999,
            ["mr"] = 16999,
        },
        ["Royal Trousers of the Owl"] = {
            ["H3254"] = 13200,
            ["mr"] = 13200,
        },
        ["Shimmering Gloves of the Eagle"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Barbed Club of Strength"] = {
            ["H3254"] = 2200,
            ["mr"] = 2200,
        },
        ["Thallium Choker of Stamina"] = {
            ["H3254"] = 29999,
            ["mr"] = 29999,
        },
        ["Razor Axe of the Eagle"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Emblazoned Leggings"] = {
            ["H3254"] = 3595,
            ["mr"] = 3595,
        },
        ["Banded Helm of the Bear"] = {
            ["H3254"] = 18001,
            ["mr"] = 18001,
        },
        ["Herb Baked Egg"] = {
            ["H3254"] = 28,
            ["mr"] = 28,
        },
        ["Black Mageweave Headband"] = {
            ["H3254"] = 9700,
            ["mr"] = 9700,
        },
        ["Resplendent Bracelets of Arcane Wrath"] = {
            ["H3254"] = 250000,
            ["mr"] = 250000,
        },
        ["Lapidis Tankard of Tidesippe"] = {
            ["H3254"] = 65500,
            ["mr"] = 65500,
        },
        ["Dervish Buckler of the Tiger"] = {
            ["H3254"] = 5186,
            ["mr"] = 5186,
        },
        ["Savage Axe of the Bear"] = {
            ["H3254"] = 43390,
            ["mr"] = 43390,
        },
        ["Stoneraven"] = {
            ["H3254"] = 88555,
            ["mr"] = 88555,
        },
        ["Elixir of Wisdom"] = {
            ["H3254"] = 200,
            ["mr"] = 200,
        },
        ["Bright Cloak"] = {
            ["H3254"] = 1170,
            ["mr"] = 1170,
        },
        ["Raider's Belt of the Bear"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Sniper Rifle of the Whale"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Dwarven Magestaff of the Whale"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Schematic: Gyrofreeze Ice Reflector"] = {
            ["H3254"] = 14699,
            ["mr"] = 14699,
        },
        ["Brigade Gauntlets of the Monkey"] = {
            ["H3254"] = 8520,
            ["mr"] = 8520,
        },
        ["Pattern: Runic Leather Headband"] = {
            ["H3254"] = 120000,
            ["mr"] = 120000,
        },
        ["Razor Blade of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Knight's Gauntlets of the Eagle"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Minor Healing Potion"] = {
            ["H3254"] = 20,
            ["mr"] = 20,
        },
        ["Goblin Rocket Fuel"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Troll Sweat"] = {
            ["H3254"] = 497,
            ["mr"] = 497,
        },
        ["Winter's Bite"] = {
            ["H3254"] = 52000,
            ["mr"] = 52000,
        },
        ["Pattern: Swift Boots"] = {
            ["H3254"] = 3600,
            ["mr"] = 3600,
        },
        ["Gyromatic Micro-Adjustor"] = {
            ["H3254"] = 11799,
            ["mr"] = 11799,
        },
        ["Formula: Enchant Chest - Major Mana"] = {
            ["H3254"] = 399999,
            ["mr"] = 399999,
        },
        ["Hook Dagger of Power"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Roasted Quail"] = {
            ["H3254"] = 332,
            ["mr"] = 332,
        },
        ["Duskwoven Gloves of the Falcon"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Green Hills of Stranglethorn - Page 4"] = {
            ["H3254"] = 1540,
            ["mr"] = 1540,
        },
        ["Arcane Sash"] = {
            ["H3254"] = 29890,
            ["mr"] = 29890,
        },
        ["Training Sword of the Wolf"] = {
            ["H3254"] = 8400,
            ["mr"] = 8400,
        },
        ["Plans: Thorium Belt"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Gossamer Cape of Stamina"] = {
            ["H3254"] = 49900,
            ["mr"] = 49900,
        },
        ["Diviner Long Staff of the Eagle"] = {
            ["H3254"] = 49999,
            ["mr"] = 49999,
        },
        ["Durable Belt of the Owl"] = {
            ["H3254"] = 10368,
            ["mr"] = 10368,
        },
        ["Skullcrusher Mace of Nature's Wrath"] = {
            ["H3254"] = 35000,
            ["mr"] = 35000,
        },
        ["Giant Club of the Monkey"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Emblazoned Gloves"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Imperial Leather Pants"] = {
            ["H3254"] = 11600,
            ["mr"] = 11600,
        },
        ["Darkshore Grouper"] = {
            ["H3254"] = 34,
            ["mr"] = 34,
        },
        ["Superior Tunic of the Eagle"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Plans: Frost Tiger Blade"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Thundering Charm"] = {
            ["H3254"] = 2699,
            ["mr"] = 2699,
        },
        ["Black Vitriol"] = {
            ["H3254"] = 1670,
            ["mr"] = 1670,
        },
        ["Plans: Dark Iron Pulverizer"] = {
            ["H3254"] = 4600,
            ["mr"] = 4600,
        },
        ["Poison-tipped Bone Spear"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Savage Axe of the Whale"] = {
            ["H3254"] = 27159,
            ["mr"] = 27159,
        },
        ["Pattern: Guardian Leather Bracers"] = {
            ["H3254"] = 4400,
            ["mr"] = 4400,
        },
        ["Frostsaber Leather"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Thistlefur Mantle of the Owl"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Archer's Shoulderpads of the Owl"] = {
            ["H3254"] = 14334,
            ["mr"] = 14334,
        },
        ["Imposing Shoulders of the Whale"] = {
            ["H3254"] = 17810,
            ["mr"] = 17810,
        },
        ["Acrobatic Staff of Power"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Buccaneer's Gloves of Healing"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Oil of Immolation"] = {
            ["H3254"] = 5265,
            ["mr"] = 5265,
        },
        ["Shimmering Stave of Frozen Wrath"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Sentinel Shoulders of the Wolf"] = {
            ["H3254"] = 43920,
            ["mr"] = 43920,
        },
        ["Kang the Decapitator"] = {
            ["H3254"] = 726375,
            ["mr"] = 726375,
        },
        ["Monstrous War Axe of the Monkey"] = {
            ["H3254"] = 38551,
            ["mr"] = 38551,
        },
        ["Cabalist Belt of the Monkey"] = {
            ["H3254"] = 10500,
            ["mr"] = 10500,
        },
        ["Pattern: Earthen Silk Belt"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Bandit Cinch of the Owl"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Recipe: Roast Raptor"] = {
            ["H3254"] = 10700,
            ["mr"] = 10700,
        },
        ["Ballast Maul of the Tiger"] = {
            ["H3254"] = 29825,
            ["mr"] = 29825,
        },
        ["Belt of the Gladiator"] = {
            ["H3254"] = 47579,
            ["mr"] = 47579,
        },
        ["Pattern: Fine Leather Pants"] = {
            ["H3254"] = 1200,
            ["mr"] = 1200,
        },
        ["Bard's Trousers of the Monkey"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Westfall Stew"] = {
            ["H3254"] = 167,
            ["mr"] = 167,
        },
        ["Dwarven Hatchet of Power"] = {
            ["H3254"] = 5498,
            ["mr"] = 5498,
        },
        ["Worn Dragonscale"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Revenant Shoulders of Stamina"] = {
            ["H3254"] = 29407,
            ["mr"] = 29407,
        },
        ["BKP \"Sparrow\" Smallbore"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Heavy Runecloth Bandage"] = {
            ["H3254"] = 1462,
            ["mr"] = 1462,
        },
        ["Spiritchaser Staff of Nature's Wrath"] = {
            ["H3254"] = 39999,
            ["mr"] = 39999,
        },
        ["Banded Girdle of Power"] = {
            ["H3254"] = 4499,
            ["mr"] = 4499,
        },
        ["Pattern: Admiral's Hat"] = {
            ["H3254"] = 12090,
            ["mr"] = 12090,
        },
        ["Topaz Ring of Fire Resistance"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Heavy Armor Kit"] = {
            ["H3254"] = 1041,
            ["mr"] = 1041,
        },
        ["Resplendent Belt of Healing"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Gnomish Cloaking Device"] = {
            ["H3254"] = 126500,
            ["mr"] = 126500,
        },
        ["Recipe: Barbecued Buzzard Wing"] = {
            ["H3254"] = 746,
            ["mr"] = 746,
        },
        ["Windchaser Footpads"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Pattern: Warbear Harness"] = {
            ["H3254"] = 650000,
            ["mr"] = 650000,
        },
        ["Recipe: Philosopher's Stone"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Imposing Shoulders of the Owl"] = {
            ["H3254"] = 15468,
            ["mr"] = 15468,
        },
        ["Nightsky Robe"] = {
            ["H3254"] = 8895,
            ["mr"] = 8895,
        },
        ["Nightcrawlers"] = {
            ["H3254"] = 51,
            ["mr"] = 51,
        },
        ["Restorative Potion"] = {
            ["H3254"] = 8526,
            ["mr"] = 8526,
        },
        ["Ring of Defense"] = {
            ["H3254"] = 18000,
            ["mr"] = 18000,
        },
        ["Relic Coffer Key"] = {
            ["H3254"] = 3899,
            ["mr"] = 3899,
        },
        ["Huntsman's Bands of the Whale"] = {
            ["H3254"] = 8610,
            ["mr"] = 8610,
        },
        ["Formula: Enchant Weapon - Fiery Weapon"] = {
            ["H3254"] = 38000,
            ["mr"] = 38000,
        },
        ["Archer's Boots of the Gorilla"] = {
            ["H3254"] = 4600,
            ["mr"] = 4600,
        },
        ["Embossed Plate Gauntlets of the Bear"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Briarthorn"] = {
            ["H3254"] = 142,
            ["mr"] = 142,
        },
        ["Red Mageweave Shoulders"] = {
            ["H3254"] = 22992,
            ["mr"] = 22992,
        },
        ["Fighter Broadsword of Strength"] = {
            ["H3254"] = 7564,
            ["mr"] = 7564,
        },
        ["Handstitched Linen Britches"] = {
            ["H3254"] = 1990,
            ["mr"] = 1990,
        },
        ["Recipe: Nature Protection Potion"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Gnomish Shrink Ray"] = {
            ["H3254"] = 78900,
            ["mr"] = 78900,
        },
        ["Sniper Rifle of Spirit"] = {
            ["H3254"] = 52312,
            ["mr"] = 52312,
        },
        ["Scarlet Belt"] = {
            ["H3254"] = 5600,
            ["mr"] = 5600,
        },
        ["Bronze Framework"] = {
            ["H3254"] = 995,
            ["mr"] = 995,
        },
        ["Large Green Sack"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Anti-Venom"] = {
            ["H3254"] = 800,
            ["mr"] = 800,
        },
        ["Expert Cookbook"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Glimmering Flamberge of the Boar"] = {
            ["H3254"] = 13770,
            ["mr"] = 13770,
        },
        ["Grime-Encrusted Object"] = {
            ["H3254"] = 108,
            ["mr"] = 108,
        },
        ["Hefty Battlehammer of Stamina"] = {
            ["H3254"] = 4999,
            ["mr"] = 4999,
        },
        ["Savage Axe of the Wolf"] = {
            ["H3254"] = 17800,
            ["mr"] = 17800,
        },
        ["Defias Renegade Ring"] = {
            ["H3254"] = 5900,
            ["mr"] = 5900,
        },
        ["Golden Rod"] = {
            ["H3254"] = 9899,
            ["mr"] = 9899,
        },
        ["Tuft of Gorilla Hair"] = {
            ["H3254"] = 5100,
            ["mr"] = 5100,
        },
        ["Inlaid Mithril Cylinder"] = {
            ["H3254"] = 39400,
            ["mr"] = 39400,
        },
        ["Royal Trousers of Spirit"] = {
            ["H3254"] = 39800,
            ["mr"] = 39800,
        },
        ["Curiously Tasty Omelet"] = {
            ["H3254"] = 210,
            ["mr"] = 210,
        },
        ["Recipe: Gooey Spider Cake"] = {
            ["H3254"] = 699,
            ["mr"] = 699,
        },
        ["Ornate Legguards of Agility"] = {
            ["H3254"] = 244752,
            ["mr"] = 244752,
        },
        ["Schematic: Delicate Arcanite Converter"] = {
            ["H3254"] = 19799,
            ["mr"] = 19799,
        },
        ["Plans: Radiant Breastplate"] = {
            ["H3254"] = 4800,
            ["mr"] = 4800,
        },
        ["Black Metal Shortsword"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Solid Stone"] = {
            ["H3254"] = 249,
            ["mr"] = 249,
        },
        ["Tome of Arcane Brilliance"] = {
            ["H3254"] = 15500,
            ["mr"] = 15500,
        },
        ["Swashbuckler's Bracers of the Owl"] = {
            ["H3254"] = 14998,
            ["mr"] = 14998,
        },
        ["Regal Boots of the Whale"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Banded Helm of the Whale"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Rodentia Flint Axe"] = {
            ["H3254"] = 2300,
            ["mr"] = 2300,
        },
        ["Sauteed Sunfish"] = {
            ["H3254"] = 66,
            ["mr"] = 66,
        },
        ["Crude Scope"] = {
            ["H3254"] = 195,
            ["mr"] = 195,
        },
        ["Shadowcraft Gloves"] = {
            ["H3254"] = 82500,
            ["mr"] = 82500,
        },
        ["Opulent Crown of Healing"] = {
            ["H3254"] = 108084,
            ["mr"] = 108084,
        },
        ["Bonesnapper"] = {
            ["H3254"] = 52000,
            ["mr"] = 52000,
        },
        ["Pattern: Tough Scorpid Gloves"] = {
            ["H3254"] = 1900,
            ["mr"] = 1900,
        },
        ["Spinel Ring of Fire Resistance"] = {
            ["H3254"] = 13358,
            ["mr"] = 13358,
        },
        ["Sorcerer Slippers of the Owl"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Firestarter"] = {
            ["H3254"] = 6400,
            ["mr"] = 6400,
        },
        ["Pattern: Dark Leather Tunic"] = {
            ["H3254"] = 1049,
            ["mr"] = 1049,
        },
        ["Mistscape Sash"] = {
            ["H3254"] = 8300,
            ["mr"] = 8300,
        },
        ["Rumsey Rum Dark"] = {
            ["H3254"] = 1280,
            ["mr"] = 1280,
        },
        ["Overlord's Vambraces of Stamina"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Councillor's Tunic of Frozen Wrath"] = {
            ["H3254"] = 45000,
            ["mr"] = 45000,
        },
        ["Bolt of Runecloth"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Shadowblade"] = {
            ["H3254"] = 389927,
            ["mr"] = 389927,
        },
        ["Searing Needle"] = {
            ["H3254"] = 49999,
            ["mr"] = 49999,
        },
        ["Grunt Axe of Stamina"] = {
            ["H3254"] = 3464,
            ["mr"] = 3464,
        },
        ["Boiled Clams"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Renegade Cloak of the Whale"] = {
            ["H3254"] = 4699,
            ["mr"] = 4699,
        },
        ["Hefty Battlehammer of the Eagle"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Formula: Enchant Weapon - Minor Beastslayer"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Wizard's Hand of the Whale"] = {
            ["H3254"] = 74909,
            ["mr"] = 74909,
        },
        ["Lion Meat"] = {
            ["H3254"] = 149,
            ["mr"] = 149,
        },
        ["Captain's Bracers of Power"] = {
            ["H3254"] = 800000,
            ["mr"] = 800000,
        },
        ["Gooey Spider Leg"] = {
            ["H3254"] = 148,
            ["mr"] = 148,
        },
        ["Zircon Band of Fire Resistance"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Thaumaturgist Staff of the Monkey"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Mithril Casing"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Twisted Chanter's Staff"] = {
            ["H3254"] = 55555,
            ["mr"] = 55555,
        },
        ["Mistscape Pants"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Glimmering Flamberge of the Bear"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Stone Hammer of the Boar"] = {
            ["H3254"] = 34668,
            ["mr"] = 34668,
        },
        ["Schematic: Shadow Goggles"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Sorcerer Gloves of the Eagle"] = {
            ["H3254"] = 25560,
            ["mr"] = 25560,
        },
        ["Bracers of Valor"] = {
            ["H3254"] = 55000,
            ["mr"] = 55000,
        },
        ["Battleforge Legguards of the Monkey"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Formula: Enchant Cloak - Superior Defense"] = {
            ["H3254"] = 63500,
            ["mr"] = 63500,
        },
        ["Spellbinder Vest"] = {
            ["H3254"] = 1499,
            ["mr"] = 1499,
        },
        ["Formula: Enchant Bracer - Superior Spirit"] = {
            ["H3254"] = 43499,
            ["mr"] = 43499,
        },
        ["Malachite"] = {
            ["H3254"] = 254,
            ["mr"] = 254,
        },
        ["Headhunter's Slippers of Stamina"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Mageweave Cloth"] = {
            ["H3254"] = 333,
            ["mr"] = 333,
        },
        ["Crystal Starfire Medallion"] = {
            ["H3254"] = 39999,
            ["mr"] = 39999,
        },
        ["Twilight Cowl of the Whale"] = {
            ["H3254"] = 5900,
            ["mr"] = 5900,
        },
        ["Ember Wand of the Eagle"] = {
            ["H3254"] = 103872,
            ["mr"] = 103872,
        },
        ["Savory Deviate Delight"] = {
            ["H3254"] = 1888,
            ["mr"] = 1888,
        },
        ["Vorpal Dagger of Agility"] = {
            ["H3254"] = 49999,
            ["mr"] = 49999,
        },
        ["Mystical Headwrap of Healing"] = {
            ["H3254"] = 116567,
            ["mr"] = 116567,
        },
        ["Thorbia's Gauntlets"] = {
            ["H3254"] = 99999,
            ["mr"] = 99999,
        },
        ["Expert First Aid - Under Wraps"] = {
            ["H3254"] = 12500,
            ["mr"] = 12500,
        },
        ["Formula: Enchant Weapon - Unholy"] = {
            ["H3254"] = 669900,
            ["mr"] = 669900,
        },
        ["Pattern: Red Mageweave Pants"] = {
            ["H3254"] = 9899,
            ["mr"] = 9899,
        },
        ["Pattern: Big Voodoo Pants"] = {
            ["H3254"] = 3961,
            ["mr"] = 3961,
        },
        ["Hot Wolf Ribs"] = {
            ["H3254"] = 1398,
            ["mr"] = 1398,
        },
        ["Cross Dagger of Power"] = {
            ["H3254"] = 45960,
            ["mr"] = 45960,
        },
        ["Archer's Jerkin of the Monkey"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Linen Cloth"] = {
            ["H3254"] = 29,
            ["mr"] = 29,
        },
        ["Thunder Lizard Tail"] = {
            ["H3254"] = 774,
            ["mr"] = 774,
        },
        ["Silvered Bronze Boots"] = {
            ["H3254"] = 2200,
            ["mr"] = 2200,
        },
        ["Greater Maul of Power"] = {
            ["H3254"] = 39900,
            ["mr"] = 39900,
        },
        ["Buccaneer's Cape of the Whale"] = {
            ["H3254"] = 3600,
            ["mr"] = 3600,
        },
        ["Knight's Breastplate of the Gorilla"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Slimescale Bracers"] = {
            ["H3254"] = 39000,
            ["mr"] = 39000,
        },
        ["Bright Sphere"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Wildheart Gloves"] = {
            ["H3254"] = 23999,
            ["mr"] = 23999,
        },
        ["Siege Bow of the Monkey"] = {
            ["H3254"] = 60135,
            ["mr"] = 60135,
        },
        ["Elixir of Ogre's Strength"] = {
            ["H3254"] = 1200,
            ["mr"] = 1200,
        },
        ["Purple Lotus"] = {
            ["H3254"] = 320,
            ["mr"] = 320,
        },
        ["Pattern: Wicked Leather Gauntlets"] = {
            ["H3254"] = 107988,
            ["mr"] = 107988,
        },
        ["White Leather Bag"] = {
            ["H3254"] = 1075,
            ["mr"] = 1075,
        },
        ["Deviate Fish"] = {
            ["H3254"] = 2998,
            ["mr"] = 2998,
        },
        ["Conjurer's Gloves of the Eagle"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Gloom Reaper of Stamina"] = {
            ["H3254"] = 16000,
            ["mr"] = 16000,
        },
        ["Imperial Red Circlet"] = {
            ["H3254"] = 14647,
            ["mr"] = 14647,
        },
        ["Essence of Fire"] = {
            ["H3254"] = 59500,
            ["mr"] = 59500,
        },
        ["Scaled Leather Leggings of the Bear"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Steel Weapon Chain"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Raw Rockscale Cod"] = {
            ["H3254"] = 16,
            ["mr"] = 16,
        },
        ["Journeyman's Pants"] = {
            ["H3254"] = 1463,
            ["mr"] = 1463,
        },
        ["Journeyman's Backpack"] = {
            ["H3254"] = 27000,
            ["mr"] = 27000,
        },
        ["Cross Dagger of the Tiger"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Large Fang"] = {
            ["H3254"] = 849,
            ["mr"] = 849,
        },
        ["Formula: Enchant 2H Weapon - Major Intellect"] = {
            ["H3254"] = 819000,
            ["mr"] = 819000,
        },
        ["Recipe: Elixir of Lesser Agility"] = {
            ["H3254"] = 14900,
            ["mr"] = 14900,
        },
        ["Formula: Enchant Gloves - Fishing"] = {
            ["H3254"] = 990,
            ["mr"] = 990,
        },
        ["Notched Shortsword of Strength"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Iridium Chain of the Whale"] = {
            ["H3254"] = 19500,
            ["mr"] = 19500,
        },
        ["Cured Light Hide"] = {
            ["H3254"] = 129,
            ["mr"] = 129,
        },
        ["Tracker's Leggings of the Eagle"] = {
            ["H3254"] = 15800,
            ["mr"] = 15800,
        },
        ["Basalt Ring of the Bear"] = {
            ["H3254"] = 9998,
            ["mr"] = 9998,
        },
        ["Hibernal Bracers"] = {
            ["H3254"] = 19799,
            ["mr"] = 19799,
        },
        ["Copper Rod"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Formula: Enchant Cloak - Greater Resistance"] = {
            ["H3254"] = 390000,
            ["mr"] = 390000,
        },
        ["Crimson Silk Belt"] = {
            ["H3254"] = 4600,
            ["mr"] = 4600,
        },
        ["Thick Murloc Scale"] = {
            ["H3254"] = 545,
            ["mr"] = 545,
        },
        ["Roast Raptor"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Greater Maul of the Monkey"] = {
            ["H3254"] = 48624,
            ["mr"] = 48624,
        },
        ["Tigerbane"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Royal Mallet of the Tiger"] = {
            ["H3254"] = 52000,
            ["mr"] = 52000,
        },
        ["Dervish Cape of the Monkey"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Headsplitter"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Gothic Plate Leggings of the Boar"] = {
            ["H3254"] = 19000,
            ["mr"] = 19000,
        },
        ["Pattern: Frostsaber Tunic"] = {
            ["H3254"] = 15399,
            ["mr"] = 15399,
        },
        ["Heavy Linen Bandage"] = {
            ["H3254"] = 29,
            ["mr"] = 29,
        },
        ["Renegade Circlet of the Gorilla"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Essence of Water"] = {
            ["H3254"] = 126500,
            ["mr"] = 126500,
        },
        ["Forgotten Wraps"] = {
            ["H3254"] = 80095,
            ["mr"] = 80095,
        },
        ["Sword of Corruption"] = {
            ["H3254"] = 39900,
            ["mr"] = 39900,
        },
        ["Righteous Orb"] = {
            ["H3254"] = 390000,
            ["mr"] = 390000,
        },
        ["Renegade Belt of the Boar"] = {
            ["H3254"] = 7310,
            ["mr"] = 7310,
        },
        ["Pristine Hide of the Beast"] = {
            ["H3254"] = 1670000,
            ["mr"] = 1670000,
        },
        ["Solid Sharpening Stone"] = {
            ["H3254"] = 1447,
            ["mr"] = 1447,
        },
        ["Formula: Enchant 2H Weapon - Major Spirit"] = {
            ["H3254"] = 223938,
            ["mr"] = 223938,
        },
        ["Keeper's Cord"] = {
            ["H3254"] = 65500,
            ["mr"] = 65500,
        },
        ["Small Furry Paw"] = {
            ["H3254"] = 96,
            ["mr"] = 96,
        },
        ["Bottom Half of Advanced Armorsmithing: Volume I"] = {
            ["H3254"] = 69999,
            ["mr"] = 69999,
        },
        ["Recipe: Heavy Kodo Stew"] = {
            ["H3254"] = 11505,
            ["mr"] = 11505,
        },
        ["Rough Grinding Stone"] = {
            ["H3254"] = 26,
            ["mr"] = 26,
        },
        ["Defender Leggings of the Bear"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Sentinel Bracers of the Eagle"] = {
            ["H3254"] = 4700,
            ["mr"] = 4700,
        },
        ["Staff of Horrors"] = {
            ["H3254"] = 3023,
            ["mr"] = 3023,
        },
        ["Sage's Sash of Healing"] = {
            ["H3254"] = 5323,
            ["mr"] = 5323,
        },
        ["Jade Gauntlets of Strength"] = {
            ["H3254"] = 18800,
            ["mr"] = 18800,
        },
        ["Copper Modulator"] = {
            ["H3254"] = 126,
            ["mr"] = 126,
        },
        ["Royal Gloves of Spirit"] = {
            ["H3254"] = 10900,
            ["mr"] = 10900,
        },
        ["Pattern: Red Linen Robe"] = {
            ["H3254"] = 90,
            ["mr"] = 90,
        },
        ["Spider Sausage"] = {
            ["H3254"] = 999,
            ["mr"] = 999,
        },
        ["Gossamer Cape of Arcane Wrath"] = {
            ["H3254"] = 27300,
            ["mr"] = 27300,
        },
        ["Ornate Gauntlets of the Monkey"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Priest's Mace of Strength"] = {
            ["H3254"] = 1394,
            ["mr"] = 1394,
        },
        ["Plans: Copper Chain Vest"] = {
            ["H3254"] = 88,
            ["mr"] = 88,
        },
        ["Pressed Felt Robe"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Troll Tribal Necklace"] = {
            ["H3254"] = 448,
            ["mr"] = 448,
        },
        ["Skeletal Longsword"] = {
            ["H3254"] = 6500,
            ["mr"] = 6500,
        },
        ["Elemental Water"] = {
            ["H3254"] = 531,
            ["mr"] = 531,
        },
        ["Tiger Meat"] = {
            ["H3254"] = 888,
            ["mr"] = 888,
        },
        ["Cerulean Ring of Concentration"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Ice Thistle E'ko"] = {
            ["H3254"] = 1199,
            ["mr"] = 1199,
        },
        ["Viking Sword of the Tiger"] = {
            ["H3254"] = 6999,
            ["mr"] = 6999,
        },
        ["Spidersilk Boots"] = {
            ["H3254"] = 13900,
            ["mr"] = 13900,
        },
        ["Infiltrator Shoulders of the Monkey"] = {
            ["H3254"] = 27372,
            ["mr"] = 27372,
        },
        ["Elder's Hat of Healing"] = {
            ["H3254"] = 35000,
            ["mr"] = 35000,
        },
        ["Formula: Enchant Bracer - Greater Stamina"] = {
            ["H3254"] = 12999,
            ["mr"] = 12999,
        },
        ["Recipe: Magic Resistance Potion"] = {
            ["H3254"] = 39800,
            ["mr"] = 39800,
        },
        ["Scorpashi Leggings"] = {
            ["H3254"] = 17800,
            ["mr"] = 17800,
        },
        ["Infiltrator Pants of Healing"] = {
            ["H3254"] = 7200,
            ["mr"] = 7200,
        },
        ["Heraldic Belt"] = {
            ["H3254"] = 34800,
            ["mr"] = 34800,
        },
        ["Sequoia Hammer of Stamina"] = {
            ["H3254"] = 9926,
            ["mr"] = 9926,
        },
        ["Twilight Orb of the Owl"] = {
            ["H3254"] = 34383,
            ["mr"] = 34383,
        },
        ["Rune Sword of the Tiger"] = {
            ["H3254"] = 48653,
            ["mr"] = 48653,
        },
        ["Small Brown Pouch"] = {
            ["H3254"] = 398,
            ["mr"] = 398,
        },
        ["Seer's Boots"] = {
            ["H3254"] = 1200,
            ["mr"] = 1200,
        },
        ["Forest Leather Gloves"] = {
            ["H3254"] = 2999,
            ["mr"] = 2999,
        },
        ["Needle Threader"] = {
            ["H3254"] = 37500,
            ["mr"] = 37500,
        },
        ["Arctic Pendant of the Wolf"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Double Link Tunic"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Acrobatic Staff of the Bear"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Plans: Volcanic Hammer"] = {
            ["H3254"] = 99998,
            ["mr"] = 99998,
        },
        ["Blesswind Hammer of the Tiger"] = {
            ["H3254"] = 62934,
            ["mr"] = 62934,
        },
        ["Chief Brigadier Leggings"] = {
            ["H3254"] = 11500,
            ["mr"] = 11500,
        },
        ["Sentinel Gloves of the Eagle"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Khan's Legguards"] = {
            ["H3254"] = 15900,
            ["mr"] = 15900,
        },
        ["Trueshot Bow of the Whale"] = {
            ["H3254"] = 19000,
            ["mr"] = 19000,
        },
        ["Moss Agate"] = {
            ["H3254"] = 899,
            ["mr"] = 899,
        },
        ["The Silencer"] = {
            ["H3254"] = 146250,
            ["mr"] = 146250,
        },
        ["Bard's Tunic of the Whale"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Cured Ham Steak"] = {
            ["H3254"] = 164,
            ["mr"] = 164,
        },
        ["Councillor's Gloves of Healing"] = {
            ["H3254"] = 39899,
            ["mr"] = 39899,
        },
        ["Pattern: Heavy Scorpid Leggings"] = {
            ["H3254"] = 8900,
            ["mr"] = 8900,
        },
        ["Militant Shortsword of the Monkey"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Recipe: Monster Omelet"] = {
            ["H3254"] = 22799,
            ["mr"] = 22799,
        },
        ["Mail Combat Belt"] = {
            ["H3254"] = 4800,
            ["mr"] = 4800,
        },
        ["Bandit Cloak of Defense"] = {
            ["H3254"] = 1607,
            ["mr"] = 1607,
        },
        ["Spider Ichor"] = {
            ["H3254"] = 38,
            ["mr"] = 38,
        },
        ["Green Hills of Stranglethorn - Page 1"] = {
            ["H3254"] = 511,
            ["mr"] = 511,
        },
        ["Coyote Meat"] = {
            ["H3254"] = 399,
            ["mr"] = 399,
        },
        ["Shimmering Stave of the Whale"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Schematic: Masterwork Target Dummy"] = {
            ["H3254"] = 17000,
            ["mr"] = 17000,
        },
        ["Long Elegant Feather"] = {
            ["H3254"] = 1501,
            ["mr"] = 1501,
        },
        ["Lava Core"] = {
            ["H3254"] = 89999,
            ["mr"] = 89999,
        },
        ["Mystical Headwrap of the Eagle"] = {
            ["H3254"] = 29500,
            ["mr"] = 29500,
        },
        ["Brightcloth Cloak"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Dark Dwarven Lager"] = {
            ["H3254"] = 735,
            ["mr"] = 735,
        },
        ["Gorilla Fang"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Barbaric Shoulders"] = {
            ["H3254"] = 3173,
            ["mr"] = 3173,
        },
        ["Archer's Shoulderpads of Stamina"] = {
            ["H3254"] = 9595,
            ["mr"] = 9595,
        },
        ["Explosive Shotgun"] = {
            ["H3254"] = 6752,
            ["mr"] = 6752,
        },
        ["Linen Bandage"] = {
            ["H3254"] = 15,
            ["mr"] = 15,
        },
        ["Runecloth Gloves"] = {
            ["H3254"] = 10500,
            ["mr"] = 10500,
        },
        ["Archer's Gloves of the Monkey"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Elixir of Greater Water Breathing"] = {
            ["H3254"] = 1155,
            ["mr"] = 1155,
        },
        ["Raw Greater Sagefish"] = {
            ["H3254"] = 742,
            ["mr"] = 742,
        },
        ["Buccaneer's Bracers of Healing"] = {
            ["H3254"] = 49900,
            ["mr"] = 49900,
        },
        ["Honed Stiletto of the Monkey"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Arctic Ring of the Monkey"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Scaled Cloak of the Eagle"] = {
            ["H3254"] = 4444,
            ["mr"] = 4444,
        },
        ["Goblin Nutcracker of the Monkey"] = {
            ["H3254"] = 32000,
            ["mr"] = 32000,
        },
        ["Cabalist Chestpiece of the Monkey"] = {
            ["H3254"] = 29000,
            ["mr"] = 29000,
        },
        ["Elixir of Demonslaying"] = {
            ["H3254"] = 9500,
            ["mr"] = 9500,
        },
        ["Grunt Axe of Power"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Inscribed Leather Boots"] = {
            ["H3254"] = 9750,
            ["mr"] = 9750,
        },
        ["Knight's Crest of the Tiger"] = {
            ["H3254"] = 80964,
            ["mr"] = 80964,
        },
        ["Iridescent Pearl"] = {
            ["H3254"] = 2100,
            ["mr"] = 2100,
        },
        ["Scroll of Strength II"] = {
            ["H3254"] = 295,
            ["mr"] = 295,
        },
        ["Buccaneer's Robes of the Eagle"] = {
            ["H3254"] = 4200,
            ["mr"] = 4200,
        },
        ["Ornate Legguards of Power"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Sorcerer Robe of the Owl"] = {
            ["H3254"] = 30540,
            ["mr"] = 30540,
        },
        ["Celestial Handwraps of the Eagle"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Lambent Scale Shield"] = {
            ["H3254"] = 6533,
            ["mr"] = 6533,
        },
        ["Jazeraint Gauntlets of the Falcon"] = {
            ["H3254"] = 9792,
            ["mr"] = 9792,
        },
        ["Mystical Leggings of the Eagle"] = {
            ["H3254"] = 49600,
            ["mr"] = 49600,
        },
        ["Cat Carrier (Siamese)"] = {
            ["H3254"] = 3920,
            ["mr"] = 3920,
        },
        ["Cat Figurine"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Deadmines Cleaver"] = {
            ["H3254"] = 1800,
            ["mr"] = 1800,
        },
        ["Beguiler Robes"] = {
            ["H3254"] = 117000,
            ["mr"] = 117000,
        },
        ["Severing Axe of the Boar"] = {
            ["H3254"] = 2567,
            ["mr"] = 2567,
        },
        ["Red Mageweave Bag"] = {
            ["H3254"] = 8700,
            ["mr"] = 8700,
        },
        ["Pillager's Cloak of the Bear"] = {
            ["H3254"] = 5900,
            ["mr"] = 5900,
        },
        ["The Pariah's Instructions"] = {
            ["H3254"] = 39800,
            ["mr"] = 39800,
        },
        ["Engraved Helm of the Monkey"] = {
            ["H3254"] = 35555,
            ["mr"] = 35555,
        },
        ["Nightsky Trousers"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Gossamer Gloves of the Owl"] = {
            ["H3254"] = 17567,
            ["mr"] = 17567,
        },
        ["Truesilver Skeleton Key"] = {
            ["H3254"] = 1990,
            ["mr"] = 1990,
        },
        ["Pattern: Green Whelp Armor"] = {
            ["H3254"] = 1445,
            ["mr"] = 1445,
        },
        ["Harpy Needler of the Tiger"] = {
            ["H3254"] = 54999,
            ["mr"] = 54999,
        },
        ["Duskwoven Bracers of Healing"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Dried King Bolete"] = {
            ["H3254"] = 397,
            ["mr"] = 397,
        },
        ["Royal Headband of the Owl"] = {
            ["H3254"] = 42600,
            ["mr"] = 42600,
        },
        ["Pattern: Black Dragonscale Breastplate"] = {
            ["H3254"] = 199000,
            ["mr"] = 199000,
        },
        ["Necklace of Calisea"] = {
            ["H3254"] = 68250,
            ["mr"] = 68250,
        },
        ["Pattern: Stormshroud Armor"] = {
            ["H3254"] = 400000,
            ["mr"] = 400000,
        },
        ["Heavy Mageweave Bandage"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Conjurer's Bracers of Spirit"] = {
            ["H3254"] = 8405,
            ["mr"] = 8405,
        },
        ["Kaleidoscope Chain"] = {
            ["H3254"] = 115555,
            ["mr"] = 115555,
        },
        ["Sardonyx Knuckle of Frost Resistance"] = {
            ["H3254"] = 39899,
            ["mr"] = 39899,
        },
        ["Lesser Moonstone"] = {
            ["H3254"] = 1068,
            ["mr"] = 1068,
        },
        ["Formula: Enchant Bracer - Minor Strength"] = {
            ["H3254"] = 1990,
            ["mr"] = 1990,
        },
        ["Medicine Staff of Fiery Wrath"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Banded Leggings of the Monkey"] = {
            ["H3254"] = 7899,
            ["mr"] = 7899,
        },
        ["Blindweed"] = {
            ["H3254"] = 2100,
            ["mr"] = 2100,
        },
        ["Black Diamond"] = {
            ["H3254"] = 93,
            ["mr"] = 93,
        },
        ["Serenity Belt"] = {
            ["H3254"] = 49800,
            ["mr"] = 49800,
        },
        ["Templar Shield of the Bear"] = {
            ["H3254"] = 299256,
            ["mr"] = 299256,
        },
        ["Opaque Wand"] = {
            ["H3254"] = 1715,
            ["mr"] = 1715,
        },
        ["Emerald Girdle of the Bear"] = {
            ["H3254"] = 39505,
            ["mr"] = 39505,
        },
        ["Dwarven Mild"] = {
            ["H3254"] = 57,
            ["mr"] = 57,
        },
        ["Lightforge Belt"] = {
            ["H3254"] = 29999,
            ["mr"] = 29999,
        },
        ["Cavalier Two-hander of the Bear"] = {
            ["H3254"] = 8041,
            ["mr"] = 8041,
        },
        ["Large Brilliant Shard"] = {
            ["H3254"] = 19599,
            ["mr"] = 19599,
        },
        ["Elegant Mantle of the Eagle"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Huntsman's Gloves of the Bear"] = {
            ["H3254"] = 6900,
            ["mr"] = 6900,
        },
        ["Stonecutter Claymore of Strength"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Scorpashi Wristbands"] = {
            ["H3254"] = 7400,
            ["mr"] = 7400,
        },
        ["Scouting Trousers of the Whale"] = {
            ["H3254"] = 5100,
            ["mr"] = 5100,
        },
        ["Conjurer's Mantle of Intellect"] = {
            ["H3254"] = 6418,
            ["mr"] = 6418,
        },
        ["Indurium Ore"] = {
            ["H3254"] = 47,
            ["mr"] = 47,
        },
        ["Crystal Sword of the Bear"] = {
            ["H3254"] = 150000,
            ["mr"] = 150000,
        },
        ["Living Essence"] = {
            ["H3254"] = 105500,
            ["mr"] = 105500,
        },
        ["Devout Belt"] = {
            ["H3254"] = 31497,
            ["mr"] = 31497,
        },
        ["Gaea's Circlet of Healing"] = {
            ["H3254"] = 500000,
            ["mr"] = 500000,
        },
        ["Crusader's Armor of Power"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Glowing Scorpid Blood"] = {
            ["H3254"] = 3398,
            ["mr"] = 3398,
        },
        ["Plans: Radiant Leggings"] = {
            ["H3254"] = 11492,
            ["mr"] = 11492,
        },
        ["Schematic: Gnomish Universal Remote"] = {
            ["H3254"] = 7782,
            ["mr"] = 7782,
        },
        ["Jagged Star of Power"] = {
            ["H3254"] = 7781,
            ["mr"] = 7781,
        },
        ["White Linen Robe"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Twilight Pants of Spirit"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Scroll of Strength III"] = {
            ["H3254"] = 469,
            ["mr"] = 469,
        },
        ["Battle Slayer of the Boar"] = {
            ["H3254"] = 4400,
            ["mr"] = 4400,
        },
        ["Combatant Claymore"] = {
            ["H3254"] = 34200,
            ["mr"] = 34200,
        },
        ["Cerulean Ring of the Owl"] = {
            ["H3254"] = 7800,
            ["mr"] = 7800,
        },
        ["Raw Redgill"] = {
            ["H3254"] = 193,
            ["mr"] = 193,
        },
        ["Gauntlets of Ogre Strength"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Recipe: Free Action Potion"] = {
            ["H3254"] = 14600,
            ["mr"] = 14600,
        },
        ["Blackforge Pauldrons"] = {
            ["H3254"] = 11500,
            ["mr"] = 11500,
        },
        ["Vanadium Talisman of Concentration"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Templar Crown of Healing"] = {
            ["H3254"] = 79900,
            ["mr"] = 79900,
        },
        ["Flesh Eating Worm"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Heavy Grinding Stone"] = {
            ["H3254"] = 1334,
            ["mr"] = 1334,
        },
        ["Elegant Boots of the Owl"] = {
            ["H3254"] = 32500,
            ["mr"] = 32500,
        },
        ["Nightwalker Armor"] = {
            ["H3254"] = 5900,
            ["mr"] = 5900,
        },
        ["Bright Belt"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Swashbuckler's Gloves of the Monkey"] = {
            ["H3254"] = 19999,
            ["mr"] = 19999,
        },
        ["Fine Leather Belt"] = {
            ["H3254"] = 558,
            ["mr"] = 558,
        },
        ["Regal Robe of Arcane Wrath"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Percussion Shotgun of the Eagle"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Solid Grinding Stone"] = {
            ["H3254"] = 1050,
            ["mr"] = 1050,
        },
        ["Wyrmslayer Spaulders"] = {
            ["H3254"] = 509998,
            ["mr"] = 509998,
        },
        ["Tracker's Gloves of the Eagle"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Jouster's Gauntlets"] = {
            ["H3254"] = 13000,
            ["mr"] = 13000,
        },
        ["Master's Cloak of Intellect"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Flurry Axe"] = {
            ["H3254"] = 1900000,
            ["mr"] = 1900000,
        },
        ["Elegant Mantle of the Whale"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Ironweaver"] = {
            ["H3254"] = 92625,
            ["mr"] = 92625,
        },
        ["Sacrificial Kris of Power"] = {
            ["H3254"] = 33306,
            ["mr"] = 33306,
        },
        ["Lady Alizabeth's Pendant"] = {
            ["H3254"] = 129998,
            ["mr"] = 129998,
        },
        ["Smoldering Pants"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Raw Spotted Yellowtail"] = {
            ["H3254"] = 1887,
            ["mr"] = 1887,
        },
        ["Schematic: Spellpower Goggles Xtreme"] = {
            ["H3254"] = 49600,
            ["mr"] = 49600,
        },
        ["Veteran Leggings"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Ivycloth Mantle of the Owl"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Ridge Cleaver of Agility"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Geomancer's Gloves of the Eagle"] = {
            ["H3254"] = 16381,
            ["mr"] = 16381,
        },
        ["Black Mageweave Leggings"] = {
            ["H3254"] = 6500,
            ["mr"] = 6500,
        },
        ["Monk's Staff of Healing"] = {
            ["H3254"] = 33280,
            ["mr"] = 33280,
        },
        ["Pattern: Runecloth Bag"] = {
            ["H3254"] = 449000,
            ["mr"] = 449000,
        },
        ["Raw Glossy Mightfish"] = {
            ["H3254"] = 86,
            ["mr"] = 86,
        },
        ["Thistle Tea"] = {
            ["H3254"] = 3899,
            ["mr"] = 3899,
        },
        ["Lesser Wizard's Robe"] = {
            ["H3254"] = 13900,
            ["mr"] = 13900,
        },
        ["Light Feather"] = {
            ["H3254"] = 353,
            ["mr"] = 353,
        },
        ["Archer's Jerkin of the Owl"] = {
            ["H3254"] = 12652,
            ["mr"] = 12652,
        },
        ["Rakzur Club"] = {
            ["H3254"] = 42000,
            ["mr"] = 42000,
        },
        ["Commander's Girdle of Strength"] = {
            ["H3254"] = 52574,
            ["mr"] = 52574,
        },
        ["Upper Map Fragment"] = {
            ["H3254"] = 6699,
            ["mr"] = 6699,
        },
        ["Copper Tube"] = {
            ["H3254"] = 193,
            ["mr"] = 193,
        },
        ["Dark Runner Boots"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Jeweled Amulet of Cainwyn"] = {
            ["H3254"] = 1245000,
            ["mr"] = 1245000,
        },
        ["Smoldering Boots"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Superior Gloves of the Eagle"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Dusky Bracers"] = {
            ["H3254"] = 4100,
            ["mr"] = 4100,
        },
        ["Formula: Enchant Weapon - Demonslaying"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Tin Bar"] = {
            ["H3254"] = 169,
            ["mr"] = 169,
        },
        ["Bandit Gloves of the Monkey"] = {
            ["H3254"] = 1599,
            ["mr"] = 1599,
        },
        ["Scroll of Stamina IV"] = {
            ["H3254"] = 5098,
            ["mr"] = 5098,
        },
        ["Windweaver Staff"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Pattern: White Bandit Mask"] = {
            ["H3254"] = 17800,
            ["mr"] = 17800,
        },
        ["Greater Nether Essence"] = {
            ["H3254"] = 26000,
            ["mr"] = 26000,
        },
        ["Sapphiron's Scale Boots"] = {
            ["H3254"] = 339900,
            ["mr"] = 339900,
        },
        ["Sorcerer Mantle of Arcane Wrath"] = {
            ["H3254"] = 12200,
            ["mr"] = 12200,
        },
        ["Rhahk'Zor's Hammer"] = {
            ["H3254"] = 2093,
            ["mr"] = 2093,
        },
        ["Jacinth Circle of Arcane Resistance"] = {
            ["H3254"] = 3899,
            ["mr"] = 3899,
        },
        ["Libram of Constitution"] = {
            ["H3254"] = 110101,
            ["mr"] = 110101,
        },
        ["Ivycloth Sash of the Whale"] = {
            ["H3254"] = 3978,
            ["mr"] = 3978,
        },
        ["Pattern: Stylish Blue Shirt"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Dense Blasting Powder"] = {
            ["H3254"] = 1900,
            ["mr"] = 1900,
        },
        ["Swiftness Potion"] = {
            ["H3254"] = 2819,
            ["mr"] = 2819,
        },
        ["Huntsman's Cap of the Boar"] = {
            ["H3254"] = 16021,
            ["mr"] = 16021,
        },
        ["Forester's Axe of the Monkey"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Silk Headband"] = {
            ["H3254"] = 2475,
            ["mr"] = 2475,
        },
        ["Dense Stone"] = {
            ["H3254"] = 1800,
            ["mr"] = 1800,
        },
        ["Engraved Girdle of Healing"] = {
            ["H3254"] = 149900,
            ["mr"] = 149900,
        },
        ["Morrowgrain"] = {
            ["H3254"] = 4400,
            ["mr"] = 4400,
        },
        ["Greater Mana Potion"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Stonescale Oil"] = {
            ["H3254"] = 1400,
            ["mr"] = 1400,
        },
        ["Stout Battlehammer of Arcane Wrath"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Snickerfang Jowl"] = {
            ["H3254"] = 960,
            ["mr"] = 960,
        },
        ["Small Lustrous Pearl"] = {
            ["H3254"] = 285,
            ["mr"] = 285,
        },
        ["Regal Cloak of Healing"] = {
            ["H3254"] = 9842,
            ["mr"] = 9842,
        },
        ["Light Hide"] = {
            ["H3254"] = 74,
            ["mr"] = 74,
        },
        ["Woolen Bag"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Coarse Sharpening Stone"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Recipe: Minor Magic Resistance Potion"] = {
            ["H3254"] = 3400,
            ["mr"] = 3400,
        },
        ["Amethyst Band of Fire Resistance"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Smooth Stone Chip"] = {
            ["H3254"] = 565,
            ["mr"] = 565,
        },
        ["Essence of Earth"] = {
            ["H3254"] = 99500,
            ["mr"] = 99500,
        },
        ["Lesser Stoneshield Potion"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Ironfeather"] = {
            ["H3254"] = 172,
            ["mr"] = 172,
        },
        ["Shadow Silk"] = {
            ["H3254"] = 1035,
            ["mr"] = 1035,
        },
        ["Small Egg"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Darkmist Wizard Hat of Arcane Wrath"] = {
            ["H3254"] = 19594,
            ["mr"] = 19594,
        },
        ["Lambent Scale Breastplate"] = {
            ["H3254"] = 3663,
            ["mr"] = 3663,
        },
        ["Stringy Wolf Meat"] = {
            ["H3254"] = 300,
            ["mr"] = 300,
        },
        ["Wintersbite"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Greater Scythe of Stamina"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Heavy Lamellar Leggings of the Eagle"] = {
            ["H3254"] = 68027,
            ["mr"] = 68027,
        },
        ["Giantslayer Bracers"] = {
            ["H3254"] = 159500,
            ["mr"] = 159500,
        },
        ["Small Black Pouch"] = {
            ["H3254"] = 638,
            ["mr"] = 638,
        },
        ["Cavalier Two-hander of the Boar"] = {
            ["H3254"] = 8367,
            ["mr"] = 8367,
        },
        ["Icemail Jerkin"] = {
            ["H3254"] = 101010,
            ["mr"] = 101010,
        },
        ["Splitting Hatchet of the Monkey"] = {
            ["H3254"] = 15099,
            ["mr"] = 15099,
        },
        ["Nightscape Headband"] = {
            ["H3254"] = 5900,
            ["mr"] = 5900,
        },
        ["Recipe: Greater Frost Protection Potion"] = {
            ["H3254"] = 450000,
            ["mr"] = 450000,
        },
        ["Schematic: Mechanical Dragonling"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Pattern: Red Mageweave Gloves"] = {
            ["H3254"] = 7020,
            ["mr"] = 7020,
        },
        ["Pattern: Cindercloth Cloak"] = {
            ["H3254"] = 66665,
            ["mr"] = 66665,
        },
        ["Sentinel Trousers of the Eagle"] = {
            ["H3254"] = 20017,
            ["mr"] = 20017,
        },
        ["Pattern: Runic Leather Pants"] = {
            ["H3254"] = 18867,
            ["mr"] = 18867,
        },
        ["Durable Bracers of the Eagle"] = {
            ["H3254"] = 11570,
            ["mr"] = 11570,
        },
        ["Splitting Hatchet of the Boar"] = {
            ["H3254"] = 10250,
            ["mr"] = 10250,
        },
        ["Massive Battle Axe of the Eagle"] = {
            ["H3254"] = 5192,
            ["mr"] = 5192,
        },
        ["Formula: Smoking Heart of the Mountain"] = {
            ["H3254"] = 7200,
            ["mr"] = 7200,
        },
        ["Tracker's Headband of Healing"] = {
            ["H3254"] = 72554,
            ["mr"] = 72554,
        },
        ["Giant Clam Meat"] = {
            ["H3254"] = 236,
            ["mr"] = 236,
        },
        ["Conjurer's Robe of Arcane Wrath"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Lambent Scale Legguards"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Battering Hammer of the Tiger"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Brutal War Axe of the Bear"] = {
            ["H3254"] = 16878,
            ["mr"] = 16878,
        },
        ["Beheading Blade of the Tiger"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Shimmering Trousers of the Eagle"] = {
            ["H3254"] = 10333,
            ["mr"] = 10333,
        },
        ["Scroll of Stamina"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Battle Slayer of the Eagle"] = {
            ["H3254"] = 3399,
            ["mr"] = 3399,
        },
        ["Councillor's Tunic of the Owl"] = {
            ["H3254"] = 181644,
            ["mr"] = 181644,
        },
        ["Gossamer Boots of the Owl"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Twilight Boots of the Whale"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Northern Shortsword of Strength"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Huntsman's Armor of the Eagle"] = {
            ["H3254"] = 11100,
            ["mr"] = 11100,
        },
        ["Mechanical Repair Kit"] = {
            ["H3254"] = 6099,
            ["mr"] = 6099,
        },
        ["Vanadium Talisman of the Eagle"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Pattern: White Leather Jerkin"] = {
            ["H3254"] = 399,
            ["mr"] = 399,
        },
        ["Pattern: Heavy Woolen Cloak"] = {
            ["H3254"] = 809,
            ["mr"] = 809,
        },
        ["Insignia Gloves"] = {
            ["H3254"] = 6200,
            ["mr"] = 6200,
        },
        ["Raptor Hide"] = {
            ["H3254"] = 243,
            ["mr"] = 243,
        },
        ["Bear Meat"] = {
            ["H3254"] = 833,
            ["mr"] = 833,
        },
        ["Scouting Cloak of the Whale"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Thick Leather Ammo Pouch"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Silver Bar"] = {
            ["H3254"] = 1305,
            ["mr"] = 1305,
        },
        ["Pattern: Black Swashbuckler's Shirt"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Basilisk Hide Pants"] = {
            ["H3254"] = 114780,
            ["mr"] = 114780,
        },
        ["Pattern: Frostweave Tunic"] = {
            ["H3254"] = 7605,
            ["mr"] = 7605,
        },
        ["Giant Club of the Bear"] = {
            ["H3254"] = 11919,
            ["mr"] = 11919,
        },
        ["Knight's Girdle of the Monkey"] = {
            ["H3254"] = 8786,
            ["mr"] = 8786,
        },
        ["Recipe: Cooked Crab Claw"] = {
            ["H3254"] = 1074,
            ["mr"] = 1074,
        },
        ["Huntsman's Cap of Defense"] = {
            ["H3254"] = 7999,
            ["mr"] = 7999,
        },
        ["Advanced Target Dummy"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Siege Bow of Spirit"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Dark Leather Boots"] = {
            ["H3254"] = 3200,
            ["mr"] = 3200,
        },
        ["Silver-thread Rod"] = {
            ["H3254"] = 3600,
            ["mr"] = 3600,
        },
        ["Big Bear Meat"] = {
            ["H3254"] = 483,
            ["mr"] = 483,
        },
        ["Knight's Bracers of the Monkey"] = {
            ["H3254"] = 6191,
            ["mr"] = 6191,
        },
        ["Swashbuckler's Boots of Agility"] = {
            ["H3254"] = 131448,
            ["mr"] = 131448,
        },
        ["Rockshard Pellets"] = {
            ["H3254"] = 1994,
            ["mr"] = 1994,
        },
        ["Schematic: Mithril Heavy-bore Rifle"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Scaled Leather Boots of the Whale"] = {
            ["H3254"] = 5600,
            ["mr"] = 5600,
        },
        ["Staff of Hale Magefire"] = {
            ["H3254"] = 458000,
            ["mr"] = 458000,
        },
        ["Pattern: Volcanic Leggings"] = {
            ["H3254"] = 7499,
            ["mr"] = 7499,
        },
        ["Schematic: Thorium Rifle"] = {
            ["H3254"] = 8700,
            ["mr"] = 8700,
        },
        ["Archer's Bracers of the Eagle"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Sorcerer Pants of Spirit"] = {
            ["H3254"] = 16969,
            ["mr"] = 16969,
        },
        ["Oil of Olaf"] = {
            ["H3254"] = 446,
            ["mr"] = 446,
        },
        ["Sentinel Shoulders of the Eagle"] = {
            ["H3254"] = 10998,
            ["mr"] = 10998,
        },
        ["Superior Belt of the Owl"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Black Whelp Scale"] = {
            ["H3254"] = 43,
            ["mr"] = 43,
        },
        ["Scouting Cloak of the Wolf"] = {
            ["H3254"] = 2144,
            ["mr"] = 2144,
        },
        ["Inscribed Buckler"] = {
            ["H3254"] = 1695,
            ["mr"] = 1695,
        },
        ["Scroll of Intellect II"] = {
            ["H3254"] = 164,
            ["mr"] = 164,
        },
        ["Dense Sharpening Stone"] = {
            ["H3254"] = 3100,
            ["mr"] = 3100,
        },
        ["Edged Bastard Sword of the Tiger"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Blackened Defias Leggings"] = {
            ["H3254"] = 38756,
            ["mr"] = 38756,
        },
        ["Knight's Girdle of Power"] = {
            ["H3254"] = 18396,
            ["mr"] = 18396,
        },
        ["Gossamer Tunic of Frozen Wrath"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Felcloth Pants"] = {
            ["H3254"] = 189999,
            ["mr"] = 189999,
        },
        ["Saltstone Gauntlets of Strength"] = {
            ["H3254"] = 9473,
            ["mr"] = 9473,
        },
        ["Ballast Maul of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Libram of Tenacity"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Explosive Sheep"] = {
            ["H3254"] = 1539,
            ["mr"] = 1539,
        },
        ["Tender Crocolisk Meat"] = {
            ["H3254"] = 119,
            ["mr"] = 119,
        },
        ["Dark Leather Pants"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Formula: Enchant Chest - Minor Mana"] = {
            ["H3254"] = 1900,
            ["mr"] = 1900,
        },
        ["Green Hills of Stranglethorn - Page 16"] = {
            ["H3254"] = 569,
            ["mr"] = 569,
        },
        ["Torch of Austen"] = {
            ["H3254"] = 120000,
            ["mr"] = 120000,
        },
        ["Cadet Vest of the Boar"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Cured Thick Hide"] = {
            ["H3254"] = 1440,
            ["mr"] = 1440,
        },
        ["Nightshade Spaulders of the Eagle"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Brutal War Axe of the Monkey"] = {
            ["H3254"] = 11000,
            ["mr"] = 11000,
        },
        ["Stout Battlehammer of the Bear"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Arachnidian Pauldrons of Stamina"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Myrmidon's Signet"] = {
            ["H3254"] = 2000000,
            ["mr"] = 2000000,
        },
        ["Sequoia Hammer of the Tiger"] = {
            ["H3254"] = 9174,
            ["mr"] = 9174,
        },
        ["Greenstone Circle of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Beaststalker's Bindings"] = {
            ["H3254"] = 28600,
            ["mr"] = 28600,
        },
        ["Gigantic War Axe of the Bear"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Cabalist Gloves of the Owl"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Cavalier Two-hander of the Tiger"] = {
            ["H3254"] = 11400,
            ["mr"] = 11400,
        },
        ["Jaina's Firestarter"] = {
            ["H3254"] = 172500,
            ["mr"] = 172500,
        },
        ["Shimmering Amice"] = {
            ["H3254"] = 1099,
            ["mr"] = 1099,
        },
        ["Stone Hammer of the Monkey"] = {
            ["H3254"] = 17400,
            ["mr"] = 17400,
        },
        ["Razor Axe of Stamina"] = {
            ["H3254"] = 155220,
            ["mr"] = 155220,
        },
        ["Redridge Goulash"] = {
            ["H3254"] = 297,
            ["mr"] = 297,
        },
        ["Heavy Hide"] = {
            ["H3254"] = 376,
            ["mr"] = 376,
        },
        ["Archer's Bracers of Agility"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Skibi's Pendant"] = {
            ["H3254"] = 170000,
            ["mr"] = 170000,
        },
        ["Marble Circle of the Boar"] = {
            ["H3254"] = 103500,
            ["mr"] = 103500,
        },
        ["Banded Leggings of the Whale"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Pattern: Gem-studded Leather Belt"] = {
            ["H3254"] = 8613,
            ["mr"] = 8613,
        },
        ["Fadeleaf"] = {
            ["H3254"] = 800,
            ["mr"] = 800,
        },
        ["Schematic: Goblin Land Mine"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Elegant Mantle of Shadow Wrath"] = {
            ["H3254"] = 179992,
            ["mr"] = 179992,
        },
        ["Mountain Silversage"] = {
            ["H3254"] = 4600,
            ["mr"] = 4600,
        },
        ["Gallant Flamberge of Power"] = {
            ["H3254"] = 69200,
            ["mr"] = 69200,
        },
        ["Ebonclaw Reaver of the Tiger"] = {
            ["H3254"] = 32062,
            ["mr"] = 32062,
        },
        ["Ichor of Undeath"] = {
            ["H3254"] = 404,
            ["mr"] = 404,
        },
        ["Executioner's Cleaver"] = {
            ["H3254"] = 398900,
            ["mr"] = 398900,
        },
        ["Cured Heavy Hide"] = {
            ["H3254"] = 1833,
            ["mr"] = 1833,
        },
        ["Recipe: Beer Basted Boar Ribs"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Unearthed Bands of the Owl"] = {
            ["H3254"] = 59999,
            ["mr"] = 59999,
        },
        ["Belt of Valor"] = {
            ["H3254"] = 128000,
            ["mr"] = 128000,
        },
        ["Elixir of Dream Vision"] = {
            ["H3254"] = 3800,
            ["mr"] = 3800,
        },
        ["Lodestone Necklace of Spirit"] = {
            ["H3254"] = 12999,
            ["mr"] = 12999,
        },
        ["Pattern: Frostsaber Gloves"] = {
            ["H3254"] = 9801,
            ["mr"] = 9801,
        },
        ["Ravager's Crown"] = {
            ["H3254"] = 19798,
            ["mr"] = 19798,
        },
        ["Sequoia Hammer of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Viking Sword of Stamina"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Bolt of Linen Cloth"] = {
            ["H3254"] = 66,
            ["mr"] = 66,
        },
        ["Renegade Belt of the Tiger"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Archer's Trousers of the Whale"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Dreadmist Belt"] = {
            ["H3254"] = 85998,
            ["mr"] = 85998,
        },
        ["Black Ogre Kickers"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Raptor Flesh"] = {
            ["H3254"] = 170,
            ["mr"] = 170,
        },
        ["Bloodwoven Bracers of the Whale"] = {
            ["H3254"] = 17083,
            ["mr"] = 17083,
        },
        ["Gossamer Headpiece of Intellect"] = {
            ["H3254"] = 49488,
            ["mr"] = 49488,
        },
        ["Pattern: Frostsaber Leggings"] = {
            ["H3254"] = 7599,
            ["mr"] = 7599,
        },
        ["Mystical Mantle of the Whale"] = {
            ["H3254"] = 17245,
            ["mr"] = 17245,
        },
        ["Dusky Belt"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Plans: Radiant Boots"] = {
            ["H3254"] = 9948,
            ["mr"] = 9948,
        },
        ["Limited Invulnerability Potion"] = {
            ["H3254"] = 22797,
            ["mr"] = 22797,
        },
        ["Magister's Gloves"] = {
            ["H3254"] = 73500,
            ["mr"] = 73500,
        },
        ["Green Leather Bracers"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Formula: Enchant Cloak - Lesser Agility"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Purification Potion"] = {
            ["H3254"] = 18024,
            ["mr"] = 18024,
        },
        ["Cord of Elements"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Greater Arcane Protection Potion"] = {
            ["H3254"] = 24400,
            ["mr"] = 24400,
        },
        ["Razor Axe of Power"] = {
            ["H3254"] = 180000,
            ["mr"] = 180000,
        },
        ["Sentinel Buckler of the Monkey"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Tough Condor Meat"] = {
            ["H3254"] = 1485,
            ["mr"] = 1485,
        },
        ["Thick War Axe"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Royal Mallet of Spirit"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Plans: Silvered Bronze Shoulders"] = {
            ["H3254"] = 995,
            ["mr"] = 995,
        },
        ["Burning War Axe"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Recipe: Flask of Distilled Wisdom"] = {
            ["H3254"] = 1447875,
            ["mr"] = 1447875,
        },
        ["Pattern: Murloc Scale Breastplate"] = {
            ["H3254"] = 400,
            ["mr"] = 400,
        },
        ["Thallium Hoop of Intellect"] = {
            ["H3254"] = 13816,
            ["mr"] = 13816,
        },
        ["Larval Acid"] = {
            ["H3254"] = 73665,
            ["mr"] = 73665,
        },
        ["Pattern: Truefaith Vestments"] = {
            ["H3254"] = 779902,
            ["mr"] = 779902,
        },
        ["Plans: Golden Scale Cuirass"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Schematic: Powerful Seaforium Charge"] = {
            ["H3254"] = 18600,
            ["mr"] = 18600,
        },
        ["Gold Ore"] = {
            ["H3254"] = 999,
            ["mr"] = 999,
        },
        ["Blue Leather Bag"] = {
            ["H3254"] = 1380,
            ["mr"] = 1380,
        },
        ["Recipe: Lean Venison"] = {
            ["H3254"] = 9800,
            ["mr"] = 9800,
        },
        ["Wall of the Dead"] = {
            ["H3254"] = 550000,
            ["mr"] = 550000,
        },
        ["Raw Whitescale Salmon"] = {
            ["H3254"] = 979,
            ["mr"] = 979,
        },
        ["Resplendent Guardian"] = {
            ["H3254"] = 23600,
            ["mr"] = 23600,
        },
        ["Essence of Air"] = {
            ["H3254"] = 79998,
            ["mr"] = 79998,
        },
        ["Black Whelp Cloak"] = {
            ["H3254"] = 5400,
            ["mr"] = 5400,
        },
        ["Green Lens of Healing"] = {
            ["H3254"] = 42510,
            ["mr"] = 42510,
        },
        ["Bandit Pants of Power"] = {
            ["H3254"] = 2100,
            ["mr"] = 2100,
        },
        ["Battle Slayer of the Bear"] = {
            ["H3254"] = 4616,
            ["mr"] = 4616,
        },
        ["Grim Reaper"] = {
            ["H3254"] = 34800,
            ["mr"] = 34800,
        },
        ["Heart of the Wild"] = {
            ["H3254"] = 406,
            ["mr"] = 406,
        },
        ["Superior Buckler of Strength"] = {
            ["H3254"] = 2672,
            ["mr"] = 2672,
        },
        ["Tyrant's Helm"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Schematic: Accurate Scope"] = {
            ["H3254"] = 9575,
            ["mr"] = 9575,
        },
        ["Gut Ripper"] = {
            ["H3254"] = 596969,
            ["mr"] = 596969,
        },
        ["Schematic: Large Seaforium Charge"] = {
            ["H3254"] = 14799,
            ["mr"] = 14799,
        },
        ["Thorium Tube"] = {
            ["H3254"] = 10724,
            ["mr"] = 10724,
        },
        ["Diamond Hammer"] = {
            ["H3254"] = 29205,
            ["mr"] = 29205,
        },
        ["Huntsman's Belt of the Wolf"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Infiltrator Pants of the Monkey"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Gossamer Cape of Healing"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Plans: Polished Steel Boots"] = {
            ["H3254"] = 2509,
            ["mr"] = 2509,
        },
        ["Vital Boots of the Owl"] = {
            ["H3254"] = 8595,
            ["mr"] = 8595,
        },
        ["Raw Slitherskin Mackerel"] = {
            ["H3254"] = 47,
            ["mr"] = 47,
        },
        ["Wand of Eventide"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Claw of the Shadowmancer"] = {
            ["H3254"] = 52696,
            ["mr"] = 52696,
        },
        ["Tough Hunk of Bread"] = {
            ["H3254"] = 121,
            ["mr"] = 121,
        },
        ["Winged Helm"] = {
            ["H3254"] = 36978,
            ["mr"] = 36978,
        },
        ["Compact Shotgun"] = {
            ["H3254"] = 2194,
            ["mr"] = 2194,
        },
        ["Elder Wizard's Mantle"] = {
            ["H3254"] = 339999,
            ["mr"] = 339999,
        },
        ["Acrobatic Staff of Frozen Wrath"] = {
            ["H3254"] = 29900,
            ["mr"] = 29900,
        },
        ["Schematic: EZ-Thro Dynamite"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Sentinel Breastplate of Stamina"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Hunting Bow"] = {
            ["H3254"] = 3300,
            ["mr"] = 3300,
        },
        ["Edged Bastard Sword of the Boar"] = {
            ["H3254"] = 3325,
            ["mr"] = 3325,
        },
        ["Tigerstrike Mantle"] = {
            ["H3254"] = 95535,
            ["mr"] = 95535,
        },
        ["Green Lens of Shadow Wrath"] = {
            ["H3254"] = 120000,
            ["mr"] = 120000,
        },
        ["Rough Copper Bomb"] = {
            ["H3254"] = 550,
            ["mr"] = 550,
        },
        ["Azure Shoulders"] = {
            ["H3254"] = 11674,
            ["mr"] = 11674,
        },
        ["Bolt of Mageweave"] = {
            ["H3254"] = 2800,
            ["mr"] = 2800,
        },
        ["Monstrous War Axe of the Eagle"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Plans: Shadow Crescent Axe"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Arachnidian Bracelets of Arcane Wrath"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Goblin Nutcracker of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Coarse Blasting Powder"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Vorpal Dagger of the Bear"] = {
            ["H3254"] = 69852,
            ["mr"] = 69852,
        },
        ["Oak Mallet of Stamina"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Plans: Thorium Leggings"] = {
            ["H3254"] = 13599,
            ["mr"] = 13599,
        },
        ["Dark Iron Residue"] = {
            ["H3254"] = 134,
            ["mr"] = 134,
        },
        ["Recipe: Elixir of Superior Defense"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Conjurer's Gloves of Fiery Wrath"] = {
            ["H3254"] = 4040,
            ["mr"] = 4040,
        },
        ["Vision Dust"] = {
            ["H3254"] = 845,
            ["mr"] = 845,
        },
        ["Green Whelp Scale"] = {
            ["H3254"] = 1195,
            ["mr"] = 1195,
        },
        ["Necromancer Leggings"] = {
            ["H3254"] = 85000,
            ["mr"] = 85000,
        },
        ["Free Action Potion"] = {
            ["H3254"] = 3899,
            ["mr"] = 3899,
        },
        ["Goblin Deviled Clams"] = {
            ["H3254"] = 99,
            ["mr"] = 99,
        },
        ["Braincage"] = {
            ["H3254"] = 20573,
            ["mr"] = 20573,
        },
        ["Delicious Cave Mold"] = {
            ["H3254"] = 59,
            ["mr"] = 59,
        },
        ["Scaled Leather Boots of the Wolf"] = {
            ["H3254"] = 11103,
            ["mr"] = 11103,
        },
        ["Twilight Cowl of the Eagle"] = {
            ["H3254"] = 33300,
            ["mr"] = 33300,
        },
        ["Stag Meat"] = {
            ["H3254"] = 745,
            ["mr"] = 745,
        },
        ["Chieftain's Breastplate of the Bear"] = {
            ["H3254"] = 40266,
            ["mr"] = 40266,
        },
        ["Plans: Hammer of the Titans"] = {
            ["H3254"] = 37440,
            ["mr"] = 37440,
        },
        ["Green Hills of Stranglethorn - Page 25"] = {
            ["H3254"] = 595,
            ["mr"] = 595,
        },
        ["Vanadium Loop of Spirit"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Major Rejuvenation Potion"] = {
            ["H3254"] = 195000,
            ["mr"] = 195000,
        },
        ["Pattern: Rugged Leather Pants"] = {
            ["H3254"] = 1155,
            ["mr"] = 1155,
        },
        ["Dervish Belt of the Boar"] = {
            ["H3254"] = 6600,
            ["mr"] = 6600,
        },
        ["Bloodwoven Cord of Intellect"] = {
            ["H3254"] = 37308,
            ["mr"] = 37308,
        },
        ["Axe of Rin'ji"] = {
            ["H3254"] = 93355,
            ["mr"] = 93355,
        },
        ["Chief Brigadier Cloak"] = {
            ["H3254"] = 3335,
            ["mr"] = 3335,
        },
        ["Plans: Runed Copper Breastplate"] = {
            ["H3254"] = 291,
            ["mr"] = 291,
        },
        ["Rune of Portals"] = {
            ["H3254"] = 4531,
            ["mr"] = 4531,
        },
        ["Elegant Mantle of the Owl"] = {
            ["H3254"] = 53900,
            ["mr"] = 53900,
        },
        ["Edged Bastard Sword of the Whale"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Jungle Necklace of Eluding"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Earthroot"] = {
            ["H3254"] = 33,
            ["mr"] = 33,
        },
        ["Blackforge Cowl"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Mystical Orb of Arcane Wrath"] = {
            ["H3254"] = 28650,
            ["mr"] = 28650,
        },
        ["Green Lens of Nature's Wrath"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Pads of the Venom Spider"] = {
            ["H3254"] = 79000,
            ["mr"] = 79000,
        },
        ["Ballast Maul of the Eagle"] = {
            ["H3254"] = 18000,
            ["mr"] = 18000,
        },
        ["Tel'Abim Banana"] = {
            ["H3254"] = 15,
            ["mr"] = 15,
        },
        ["Goldthorn"] = {
            ["H3254"] = 1267,
            ["mr"] = 1267,
        },
        ["Twin-bladed Axe of Agility"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Recipe: Gift of Arthas"] = {
            ["H3254"] = 9500,
            ["mr"] = 9500,
        },
        ["Magister's Belt"] = {
            ["H3254"] = 48600,
            ["mr"] = 48600,
        },
        ["Spiritchaser Staff of the Monkey"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Willow Branch of the Eagle"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Arcanite Bar"] = {
            ["H3254"] = 548999,
            ["mr"] = 548999,
        },
        ["Shiny Fish Scales"] = {
            ["H3254"] = 7,
            ["mr"] = 7,
        },
        ["Swashbuckler's Leggings of the Eagle"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Truesilver Bar"] = {
            ["H3254"] = 1734,
            ["mr"] = 1734,
        },
        ["Nightscape Pants"] = {
            ["H3254"] = 10399,
            ["mr"] = 10399,
        },
        ["Top Half of Advanced Armorsmithing: Volume II"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Tundra Ring of the Tiger"] = {
            ["H3254"] = 4557,
            ["mr"] = 4557,
        },
        ["Sweet Nectar"] = {
            ["H3254"] = 67,
            ["mr"] = 67,
        },
        ["Frostreaver Crown"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Light Bow"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Forest Hoop of the Tiger"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Band of the Unicorn"] = {
            ["H3254"] = 252525,
            ["mr"] = 252525,
        },
        ["Buzzard Wing"] = {
            ["H3254"] = 261,
            ["mr"] = 261,
        },
        ["Gloom Reaper of Power"] = {
            ["H3254"] = 107568,
            ["mr"] = 107568,
        },
        ["Jagged Star of the Tiger"] = {
            ["H3254"] = 4370,
            ["mr"] = 4370,
        },
        ["Zircon Band of Frost Resistance"] = {
            ["H3254"] = 2999,
            ["mr"] = 2999,
        },
        ["The Eye of Shadow"] = {
            ["H3254"] = 2335000,
            ["mr"] = 2335000,
        },
        ["Sentinel Shoulders of the Boar"] = {
            ["H3254"] = 7871,
            ["mr"] = 7871,
        },
        ["Valorous Chestguard"] = {
            ["H3254"] = 39000,
            ["mr"] = 39000,
        },
        ["Lesser Magic Essence"] = {
            ["H3254"] = 504,
            ["mr"] = 504,
        },
        ["Security DELTA Data Access Card"] = {
            ["H3254"] = 790,
            ["mr"] = 790,
        },
        ["High Councillor's Bracers of the Owl"] = {
            ["H3254"] = 57184,
            ["mr"] = 57184,
        },
        ["Mechanical Chicken"] = {
            ["H3254"] = 73125,
            ["mr"] = 73125,
        },
        ["Regal Leggings of the Owl"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Dreadblade of Nature's Wrath"] = {
            ["H3254"] = 35000,
            ["mr"] = 35000,
        },
        ["Massive Battle Axe of Agility"] = {
            ["H3254"] = 6035,
            ["mr"] = 6035,
        },
        ["Weighted Sap"] = {
            ["H3254"] = 1499,
            ["mr"] = 1499,
        },
        ["Formula: Runed Arcanite Rod"] = {
            ["H3254"] = 34400,
            ["mr"] = 34400,
        },
        ["Warlord's Axe of Stamina"] = {
            ["H3254"] = 437952,
            ["mr"] = 437952,
        },
        ["Kingsblood"] = {
            ["H3254"] = 454,
            ["mr"] = 454,
        },
        ["Pattern: Guardian Armor"] = {
            ["H3254"] = 1455,
            ["mr"] = 1455,
        },
        ["Glimmering Mail Girdle"] = {
            ["H3254"] = 4699,
            ["mr"] = 4699,
        },
        ["Warlord's Axe of the Whale"] = {
            ["H3254"] = 435000,
            ["mr"] = 435000,
        },
        ["Reticulated Bone Gauntlets"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Recipe: Elixir of Ogre's Strength"] = {
            ["H3254"] = 8775,
            ["mr"] = 8775,
        },
        ["Cold Basilisk Eye"] = {
            ["H3254"] = 13500,
            ["mr"] = 13500,
        },
        ["Conjurer's Robe of the Eagle"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Frostweave Gloves"] = {
            ["H3254"] = 19800,
            ["mr"] = 19800,
        },
        ["Lesser Nether Essence"] = {
            ["H3254"] = 7197,
            ["mr"] = 7197,
        },
        ["Massacre Sword of the Whale"] = {
            ["H3254"] = 175723,
            ["mr"] = 175723,
        },
        ["Devilsaur Leggings"] = {
            ["H3254"] = 1729999,
            ["mr"] = 1729999,
        },
        ["Infantry Leggings of the Bear"] = {
            ["H3254"] = 1999,
            ["mr"] = 1999,
        },
        ["Shimmering Trousers of the Whale"] = {
            ["H3254"] = 3682,
            ["mr"] = 3682,
        },
        ["Dervish Cape of the Eagle"] = {
            ["H3254"] = 3100,
            ["mr"] = 3100,
        },
        ["Mail Combat Boots"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Elegant Mantle of Frozen Wrath"] = {
            ["H3254"] = 275500,
            ["mr"] = 275500,
        },
        ["Pattern: Felcloth Shoulders"] = {
            ["H3254"] = 150000,
            ["mr"] = 150000,
        },
        ["Recipe: Goblin Rocket Fuel"] = {
            ["H3254"] = 8800,
            ["mr"] = 8800,
        },
        ["Destiny"] = {
            ["H3254"] = 666666,
            ["mr"] = 666666,
        },
        ["Bolt of Silk Cloth"] = {
            ["H3254"] = 1187,
            ["mr"] = 1187,
        },
        ["Embossed Leather Gloves"] = {
            ["H3254"] = 124,
            ["mr"] = 124,
        },
        ["Plans: Golden Scale Coif"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Jagged Star of Nature's Wrath"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Elegant Boots of Stamina"] = {
            ["H3254"] = 46969,
            ["mr"] = 46969,
        },
        ["Soft-soled Linen Boots"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Witchfury"] = {
            ["H3254"] = 39000,
            ["mr"] = 39000,
        },
        ["Sequoia Branch of the Boar"] = {
            ["H3254"] = 23900,
            ["mr"] = 23900,
        },
        ["Plans: Mighty Iron Hammer"] = {
            ["H3254"] = 1499,
            ["mr"] = 1499,
        },
        ["Deanship Claymore"] = {
            ["H3254"] = 24998,
            ["mr"] = 24998,
        },
        ["Haunch of Meat"] = {
            ["H3254"] = 101,
            ["mr"] = 101,
        },
        ["Plans: Storm Gauntlets"] = {
            ["H3254"] = 38799,
            ["mr"] = 38799,
        },
        ["Pattern: Rich Purple Silk Shirt"] = {
            ["H3254"] = 298999,
            ["mr"] = 298999,
        },
        ["Arctic Ring of the Tiger"] = {
            ["H3254"] = 46600,
            ["mr"] = 46600,
        },
        ["Scouting Tunic of Stamina"] = {
            ["H3254"] = 4475,
            ["mr"] = 4475,
        },
        ["Archer's Trousers of Healing"] = {
            ["H3254"] = 39500,
            ["mr"] = 39500,
        },
        ["Killmaim"] = {
            ["H3254"] = 19999,
            ["mr"] = 19999,
        },
        ["Ring of Saviors"] = {
            ["H3254"] = 149999,
            ["mr"] = 149999,
        },
        ["Green Hills of Stranglethorn - Page 14"] = {
            ["H3254"] = 1435,
            ["mr"] = 1435,
        },
        ["Stone Hammer of the Gorilla"] = {
            ["H3254"] = 17500,
            ["mr"] = 17500,
        },
        ["Ornate Legguards of Arcane Resistance"] = {
            ["H3254"] = 40396,
            ["mr"] = 40396,
        },
        ["Sentinel Cloak of the Owl"] = {
            ["H3254"] = 12598,
            ["mr"] = 12598,
        },
        ["Archer's Shoulderpads of the Wolf"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Sliverblade"] = {
            ["H3254"] = 145000,
            ["mr"] = 145000,
        },
        ["Mystical Mantle of Frozen Wrath"] = {
            ["H3254"] = 178030,
            ["mr"] = 178030,
        },
        ["Essence of Undeath"] = {
            ["H3254"] = 7800,
            ["mr"] = 7800,
        },
        ["Lil Timmy's Peashooter"] = {
            ["H3254"] = 57038,
            ["mr"] = 57038,
        },
        ["Recipe: Limited Invulnerability Potion"] = {
            ["H3254"] = 15400,
            ["mr"] = 15400,
        },
        ["Thick Spider's Silk"] = {
            ["H3254"] = 965,
            ["mr"] = 965,
        },
        ["Opulent Crown of Frozen Wrath"] = {
            ["H3254"] = 129999,
            ["mr"] = 129999,
        },
        ["Frenzied Striker"] = {
            ["H3254"] = 352998,
            ["mr"] = 352998,
        },
        ["Chief Brigadier Girdle"] = {
            ["H3254"] = 5589,
            ["mr"] = 5589,
        },
        ["Scroll of Spirit IV"] = {
            ["H3254"] = 655,
            ["mr"] = 655,
        },
        ["Fused Wiring"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Imposing Gloves of the Monkey"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Rageclaw Gloves of the Monkey"] = {
            ["H3254"] = 15054,
            ["mr"] = 15054,
        },
        ["Enduring Cap"] = {
            ["H3254"] = 21800,
            ["mr"] = 21800,
        },
        ["Shadow Wand"] = {
            ["H3254"] = 1300,
            ["mr"] = 1300,
        },
        ["Hunting Pants"] = {
            ["H3254"] = 2298,
            ["mr"] = 2298,
        },
        ["Magister's Bindings"] = {
            ["H3254"] = 170000,
            ["mr"] = 170000,
        },
        ["Scaled Leather Gloves of the Bear"] = {
            ["H3254"] = 8677,
            ["mr"] = 8677,
        },
        ["Recipe: Transmute Water to Undeath"] = {
            ["H3254"] = 700000,
            ["mr"] = 700000,
        },
        ["Green Lens of Frozen Wrath"] = {
            ["H3254"] = 180000,
            ["mr"] = 180000,
        },
        ["Huntsman's Cap of the Monkey"] = {
            ["H3254"] = 12600,
            ["mr"] = 12600,
        },
        ["Recipe: Greater Arcane Protection Potion"] = {
            ["H3254"] = 49599,
            ["mr"] = 49599,
        },
        ["Dense Triangle Mace"] = {
            ["H3254"] = 39999,
            ["mr"] = 39999,
        },
        ["Emblazoned Cloak"] = {
            ["H3254"] = 2925,
            ["mr"] = 2925,
        },
        ["Jazeraint Gauntlets of the Owl"] = {
            ["H3254"] = 9146,
            ["mr"] = 9146,
        },
        ["Elder's Sash of Intellect"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Gallant Flamberge of the Tiger"] = {
            ["H3254"] = 68048,
            ["mr"] = 68048,
        },
        ["Pattern: Robes of Arcana"] = {
            ["H3254"] = 28999,
            ["mr"] = 28999,
        },
        ["Stonescale Eel"] = {
            ["H3254"] = 1245,
            ["mr"] = 1245,
        },
        ["Scroll of Intellect"] = {
            ["H3254"] = 95,
            ["mr"] = 95,
        },
        ["Plans: Thorium Bracers"] = {
            ["H3254"] = 12700,
            ["mr"] = 12700,
        },
        ["Gossamer Belt of Healing"] = {
            ["H3254"] = 24000,
            ["mr"] = 24000,
        },
        ["Wildvine Potion"] = {
            ["H3254"] = 1300,
            ["mr"] = 1300,
        },
        ["Mystical Headwrap of the Whale"] = {
            ["H3254"] = 37510,
            ["mr"] = 37510,
        },
        ["Venomshroud Leggings"] = {
            ["H3254"] = 29250,
            ["mr"] = 29250,
        },
        ["Merc Sword of the Eagle"] = {
            ["H3254"] = 5999,
            ["mr"] = 5999,
        },
        ["Fortified Gauntlets of the Whale"] = {
            ["H3254"] = 2582,
            ["mr"] = 2582,
        },
        ["Fire Oil"] = {
            ["H3254"] = 247,
            ["mr"] = 247,
        },
        ["Pattern: Phoenix Pants"] = {
            ["H3254"] = 5200,
            ["mr"] = 5200,
        },
        ["Magician Staff of Fiery Wrath"] = {
            ["H3254"] = 10815,
            ["mr"] = 10815,
        },
        ["Arcane Crystal"] = {
            ["H3254"] = 464999,
            ["mr"] = 464999,
        },
        ["Barbaric Bracers"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Krol Blade"] = {
            ["H3254"] = 4450000,
            ["mr"] = 4450000,
        },
        ["Ghostwalker Gloves of the Monkey"] = {
            ["H3254"] = 14999,
            ["mr"] = 14999,
        },
        ["Sergeant's Warhammer of Strength"] = {
            ["H3254"] = 1400,
            ["mr"] = 1400,
        },
        ["Twilight Cuffs of the Whale"] = {
            ["H3254"] = 6336,
            ["mr"] = 6336,
        },
        ["Practice Lock"] = {
            ["H3254"] = 298,
            ["mr"] = 298,
        },
        ["Splitting Hatchet of the Tiger"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Greater Healing Potion"] = {
            ["H3254"] = 447,
            ["mr"] = 447,
        },
        ["Thaumaturgist Staff of Intellect"] = {
            ["H3254"] = 99889,
            ["mr"] = 99889,
        },
        ["Mystical Headwrap of Stamina"] = {
            ["H3254"] = 108120,
            ["mr"] = 108120,
        },
        ["Diamond-Tip Bludgeon of the Monkey"] = {
            ["H3254"] = 47439,
            ["mr"] = 47439,
        },
        ["Lean Wolf Flank"] = {
            ["H3254"] = 180,
            ["mr"] = 180,
        },
        ["Poached Sunscale Salmon"] = {
            ["H3254"] = 48,
            ["mr"] = 48,
        },
        ["Percussion Shotgun of Spirit"] = {
            ["H3254"] = 213492,
            ["mr"] = 213492,
        },
        ["Coal"] = {
            ["H3254"] = 291,
            ["mr"] = 291,
        },
        ["Girdle of Golem Strength"] = {
            ["H3254"] = 25400,
            ["mr"] = 25400,
        },
        ["Iron Bar"] = {
            ["H3254"] = 795,
            ["mr"] = 795,
        },
        ["Jazeraint Helm of the Whale"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Siege Bow of the Whale"] = {
            ["H3254"] = 241620,
            ["mr"] = 241620,
        },
        ["Packet of Tharlendris Seeds"] = {
            ["H3254"] = 1055,
            ["mr"] = 1055,
        },
        ["Vanadium Talisman of the Whale"] = {
            ["H3254"] = 35000,
            ["mr"] = 35000,
        },
        ["Hillman's Leather Gloves"] = {
            ["H3254"] = 1795,
            ["mr"] = 1795,
        },
        ["Bright Bracers"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Elegant Boots of Shadow Wrath"] = {
            ["H3254"] = 97500,
            ["mr"] = 97500,
        },
        ["Recipe: Transmute Arcanite"] = {
            ["H3254"] = 69999,
            ["mr"] = 69999,
        },
        ["Jazeraint Helm of the Wolf"] = {
            ["H3254"] = 14000,
            ["mr"] = 14000,
        },
        ["Twilight Cuffs of the Eagle"] = {
            ["H3254"] = 17674,
            ["mr"] = 17674,
        },
        ["Steadfast Buckler of Arcane Wrath"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Silithid Chitin"] = {
            ["H3254"] = 1800,
            ["mr"] = 1800,
        },
        ["Hibernal Mantle"] = {
            ["H3254"] = 31200,
            ["mr"] = 31200,
        },
        ["Green Hills of Stranglethorn - Page 26"] = {
            ["H3254"] = 563,
            ["mr"] = 563,
        },
        ["Julie's Dagger"] = {
            ["H3254"] = 164000,
            ["mr"] = 164000,
        },
        ["Gothic Plate Helmet of Stamina"] = {
            ["H3254"] = 61776,
            ["mr"] = 61776,
        },
        ["Sentinel Gloves of the Tiger"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Ivycloth Boots of Stamina"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Archer's Gloves of the Whale"] = {
            ["H3254"] = 21036,
            ["mr"] = 21036,
        },
        ["Siege Bow of the Tiger"] = {
            ["H3254"] = 59500,
            ["mr"] = 59500,
        },
        ["Gloom Reaper of Agility"] = {
            ["H3254"] = 26892,
            ["mr"] = 26892,
        },
        ["Mageroyal"] = {
            ["H3254"] = 24,
            ["mr"] = 24,
        },
        ["Flask of Oil"] = {
            ["H3254"] = 35,
            ["mr"] = 35,
        },
        ["Blood of the Mountain"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Mystical Orb of Intellect"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Commander's Girdle of the Gorilla"] = {
            ["H3254"] = 44027,
            ["mr"] = 44027,
        },
        ["Cabalist Belt of the Owl"] = {
            ["H3254"] = 16530,
            ["mr"] = 16530,
        },
        ["Acrobatic Staff of the Whale"] = {
            ["H3254"] = 17965,
            ["mr"] = 17965,
        },
        ["Dragon Finger of Healing"] = {
            ["H3254"] = 367536,
            ["mr"] = 367536,
        },
        ["Precision Bow"] = {
            ["H3254"] = 4919,
            ["mr"] = 4919,
        },
        ["Turtle Scale"] = {
            ["H3254"] = 350,
            ["mr"] = 350,
        },
        ["Formula: Enchant Boots - Lesser Spirit"] = {
            ["H3254"] = 2600,
            ["mr"] = 2600,
        },
        ["Serpentine Sash"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Archer's Trousers of the Eagle"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Gossamer Belt of Fiery Wrath"] = {
            ["H3254"] = 10020,
            ["mr"] = 10020,
        },
        ["Plans: Golden Scale Leggings"] = {
            ["H3254"] = 1950,
            ["mr"] = 1950,
        },
        ["Widow Blade of the Monkey"] = {
            ["H3254"] = 47500,
            ["mr"] = 47500,
        },
        ["Sequoia Hammer of the Monkey"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Raider's Shoulderpads"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Darkmist Wizard Hat of the Owl"] = {
            ["H3254"] = 8800,
            ["mr"] = 8800,
        },
        ["Glorious Belt of the Bear"] = {
            ["H3254"] = 89247,
            ["mr"] = 89247,
        },
        ["Unadorned Seal of Ascension"] = {
            ["H3254"] = 6600,
            ["mr"] = 6600,
        },
        ["Supercharger Battle Axe"] = {
            ["H3254"] = 19899,
            ["mr"] = 19899,
        },
        ["Barbaric Leggings"] = {
            ["H3254"] = 9199,
            ["mr"] = 9199,
        },
        ["Defender Cloak of the Bear"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Overlord's Vambraces of Power"] = {
            ["H3254"] = 47856,
            ["mr"] = 47856,
        },
        ["Aurora Gloves"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Zealot Blade"] = {
            ["H3254"] = 179975,
            ["mr"] = 179975,
        },
        ["Recipe: Elixir of Poison Resistance"] = {
            ["H3254"] = 8100,
            ["mr"] = 8100,
        },
        ["Knight's Boots of the Bear"] = {
            ["H3254"] = 9990,
            ["mr"] = 9990,
        },
        ["Mercurial Greaves of Power"] = {
            ["H3254"] = 326500,
            ["mr"] = 326500,
        },
        ["Beaststalker's Gloves"] = {
            ["H3254"] = 59999,
            ["mr"] = 59999,
        },
        ["Wizard's Hand of Fiery Wrath"] = {
            ["H3254"] = 49500,
            ["mr"] = 49500,
        },
        ["Chieftain's Boots of the Monkey"] = {
            ["H3254"] = 12500,
            ["mr"] = 12500,
        },
        ["Soldier's Armor of the Boar"] = {
            ["H3254"] = 2099,
            ["mr"] = 2099,
        },
        ["Commander's Girdle of the Bear"] = {
            ["H3254"] = 35980,
            ["mr"] = 35980,
        },
        ["Lunar Mantle of the Owl"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Ribsplitter of the Whale"] = {
            ["H3254"] = 114616,
            ["mr"] = 114616,
        },
        ["Thorium Headed Arrow"] = {
            ["H3254"] = 89,
            ["mr"] = 89,
        },
        ["Recipe: Flask of the Titans"] = {
            ["H3254"] = 2519310,
            ["mr"] = 2519310,
        },
        ["Regal Boots of Frozen Wrath"] = {
            ["H3254"] = 15069,
            ["mr"] = 15069,
        },
        ["Sentinel Trousers of Agility"] = {
            ["H3254"] = 9898,
            ["mr"] = 9898,
        },
        ["Greater Maul of the Tiger"] = {
            ["H3254"] = 47472,
            ["mr"] = 47472,
        },
        ["Winterfall Firewater"] = {
            ["H3254"] = 22999,
            ["mr"] = 22999,
        },
        ["Magefist Gloves"] = {
            ["H3254"] = 95000,
            ["mr"] = 95000,
        },
        ["Mystical Mantle of Shadow Wrath"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Deviate Scale"] = {
            ["H3254"] = 619,
            ["mr"] = 619,
        },
        ["Gauntlets of Valor"] = {
            ["H3254"] = 99000,
            ["mr"] = 99000,
        },
        ["Captain's Breastplate of Stamina"] = {
            ["H3254"] = 42100,
            ["mr"] = 42100,
        },
        ["Manual: Mageweave Bandage"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Felcloth"] = {
            ["H3254"] = 38331,
            ["mr"] = 38331,
        },
        ["Ebonhold Leggings"] = {
            ["H3254"] = 32200,
            ["mr"] = 32200,
        },
        ["Bloodforged Shield of the Monkey"] = {
            ["H3254"] = 55695,
            ["mr"] = 55695,
        },
        ["Silksand Gloves"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Soothing Turtle Bisque"] = {
            ["H3254"] = 493,
            ["mr"] = 493,
        },
        ["High Bergg Helm"] = {
            ["H3254"] = 32512,
            ["mr"] = 32512,
        },
        ["Glimmering Flamberge of the Eagle"] = {
            ["H3254"] = 8700,
            ["mr"] = 8700,
        },
        ["Bloodforged Bindings of the Bear"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Schematic: Flawless Arcanite Rifle"] = {
            ["H3254"] = 293600,
            ["mr"] = 293600,
        },
        ["Toughened Leather Gloves"] = {
            ["H3254"] = 7499,
            ["mr"] = 7499,
        },
        ["Gothic Plate Leggings of the Bear"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Blood Sausage"] = {
            ["H3254"] = 124,
            ["mr"] = 124,
        },
        ["Oily Blackmouth"] = {
            ["H3254"] = 300,
            ["mr"] = 300,
        },
        ["Battleforge Gauntlets of the Whale"] = {
            ["H3254"] = 7203,
            ["mr"] = 7203,
        },
        ["Magician Staff of the Whale"] = {
            ["H3254"] = 10503,
            ["mr"] = 10503,
        },
        ["Homemade Cherry Pie"] = {
            ["H3254"] = 393,
            ["mr"] = 393,
        },
        ["Bright Pants"] = {
            ["H3254"] = 6600,
            ["mr"] = 6600,
        },
        ["Battleforge Shield of Strength"] = {
            ["H3254"] = 5800,
            ["mr"] = 5800,
        },
        ["Defender Cloak of Defense"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Savage Axe of the Boar"] = {
            ["H3254"] = 31428,
            ["mr"] = 31428,
        },
        ["Lord Alexander's Battle Axe"] = {
            ["H3254"] = 199900,
            ["mr"] = 199900,
        },
        ["Azure Silk Cloak"] = {
            ["H3254"] = 4600,
            ["mr"] = 4600,
        },
        ["Twilight Cape of the Owl"] = {
            ["H3254"] = 4990,
            ["mr"] = 4990,
        },
        ["Defender Boots of the Boar"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Ebon Scimitar of Power"] = {
            ["H3254"] = 34111,
            ["mr"] = 34111,
        },
        ["Large Knapsack"] = {
            ["H3254"] = 9499,
            ["mr"] = 9499,
        },
        ["Colossal Great Axe of Agility"] = {
            ["H3254"] = 192668,
            ["mr"] = 192668,
        },
        ["Infiltrator Pants of the Eagle"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Light Leather Quiver"] = {
            ["H3254"] = 550,
            ["mr"] = 550,
        },
        ["Imposing Shoulders of the Eagle"] = {
            ["H3254"] = 15800,
            ["mr"] = 15800,
        },
        ["Pattern: Boots of the Enchanter"] = {
            ["H3254"] = 29999,
            ["mr"] = 29999,
        },
        ["Nature Protection Potion"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Un'Goro Soil"] = {
            ["H3254"] = 1695,
            ["mr"] = 1695,
        },
        ["Plans: Dark Iron Plate"] = {
            ["H3254"] = 108405,
            ["mr"] = 108405,
        },
        ["Lifeless Stone"] = {
            ["H3254"] = 492,
            ["mr"] = 492,
        },
        ["Windchaser Cloak"] = {
            ["H3254"] = 19400,
            ["mr"] = 19400,
        },
        ["Schematic: Arcane Bomb"] = {
            ["H3254"] = 148000,
            ["mr"] = 148000,
        },
        ["Large Venom Sac"] = {
            ["H3254"] = 320,
            ["mr"] = 320,
        },
        ["Heavy Silk Bandage"] = {
            ["H3254"] = 424,
            ["mr"] = 424,
        },
        ["Buccaneer's Cape of the Owl"] = {
            ["H3254"] = 2136,
            ["mr"] = 2136,
        },
        ["Phalanx Spaulders of the Bear"] = {
            ["H3254"] = 8800,
            ["mr"] = 8800,
        },
        ["Antipodean Rod"] = {
            ["H3254"] = 9555,
            ["mr"] = 9555,
        },
        ["Icecap"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Conjurer's Mantle of Arcane Wrath"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Sentinel Breastplate of the Eagle"] = {
            ["H3254"] = 10850,
            ["mr"] = 10850,
        },
        ["Scouting Trousers of the Owl"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Goblin Mail Leggings"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Flask of Distilled Wisdom"] = {
            ["H3254"] = 590000,
            ["mr"] = 590000,
        },
        ["Bloodwoven Mitts of the Eagle"] = {
            ["H3254"] = 37872,
            ["mr"] = 37872,
        },
        ["Wolf Rider's Wristbands of Stamina"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Citrine"] = {
            ["H3254"] = 1150,
            ["mr"] = 1150,
        },
        ["Ivycloth Cloak of the Owl"] = {
            ["H3254"] = 2764,
            ["mr"] = 2764,
        },
        ["Gigantic War Axe of Strength"] = {
            ["H3254"] = 39900,
            ["mr"] = 39900,
        },
        ["Desert Ring of the Tiger"] = {
            ["H3254"] = 30926,
            ["mr"] = 30926,
        },
        ["Lunar Handwraps of the Owl"] = {
            ["H3254"] = 6799,
            ["mr"] = 6799,
        },
        ["Archer's Gloves of the Owl"] = {
            ["H3254"] = 7487,
            ["mr"] = 7487,
        },
        ["Raider's Legguards of the Bear"] = {
            ["H3254"] = 2653,
            ["mr"] = 2653,
        },
        ["Recipe: Flask of Supreme Power"] = {
            ["H3254"] = 990000,
            ["mr"] = 990000,
        },
        ["Sentinel Shoulders of the Whale"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Banded Cloak of the Boar"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Devilsaur Gauntlets"] = {
            ["H3254"] = 1049997,
            ["mr"] = 1049997,
        },
        ["Mystical Leggings of the Owl"] = {
            ["H3254"] = 62986,
            ["mr"] = 62986,
        },
        ["Truesilver Rod"] = {
            ["H3254"] = 9000,
            ["mr"] = 9000,
        },
        ["Raw Black Truffle"] = {
            ["H3254"] = 304,
            ["mr"] = 304,
        },
        ["Lunar Handwraps of Healing"] = {
            ["H3254"] = 10252,
            ["mr"] = 10252,
        },
        ["Ricochet Blunderbuss of Stamina"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Elixir of Minor Defense"] = {
            ["H3254"] = 51,
            ["mr"] = 51,
        },
        ["Raider's Belt of the Gorilla"] = {
            ["H3254"] = 4999,
            ["mr"] = 4999,
        },
        ["Edged Bastard Sword of the Eagle"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Fire Protection Potion"] = {
            ["H3254"] = 6700,
            ["mr"] = 6700,
        },
        ["Edged Bastard Sword of the Bear"] = {
            ["H3254"] = 4985,
            ["mr"] = 4985,
        },
        ["Short Bastard Sword of the Monkey"] = {
            ["H3254"] = 795,
            ["mr"] = 795,
        },
        ["Thallium Choker of the Eagle"] = {
            ["H3254"] = 45000,
            ["mr"] = 45000,
        },
        ["Clamlette Surprise"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Schematic: Bright-Eye Goggles"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Sentinel Cap of the Owl"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Thaumaturgist Staff of Healing"] = {
            ["H3254"] = 82000,
            ["mr"] = 82000,
        },
        ["Cavalier Two-hander of the Whale"] = {
            ["H3254"] = 12821,
            ["mr"] = 12821,
        },
        ["Frost Oil"] = {
            ["H3254"] = 6400,
            ["mr"] = 6400,
        },
        ["Dark Rune"] = {
            ["H3254"] = 15399,
            ["mr"] = 15399,
        },
        ["Soldier's Leggings of the Bear"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Elegant Circlet of the Owl"] = {
            ["H3254"] = 38000,
            ["mr"] = 38000,
        },
        ["Arctic Ring of Agility"] = {
            ["H3254"] = 22347,
            ["mr"] = 22347,
        },
        ["Sentinel Trousers of the Monkey"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Mistscape Gloves"] = {
            ["H3254"] = 8999,
            ["mr"] = 8999,
        },
        ["River Pride Choker"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Birchwood Maul of the Whale"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Blade of the Titans"] = {
            ["H3254"] = 160000,
            ["mr"] = 160000,
        },
        ["Plans: Iron Counterweight"] = {
            ["H3254"] = 3300,
            ["mr"] = 3300,
        },
        ["Commander's Vambraces of the Bear"] = {
            ["H3254"] = 38572,
            ["mr"] = 38572,
        },
        ["Lesser Magic Wand"] = {
            ["H3254"] = 900,
            ["mr"] = 900,
        },
        ["Mithril Bar"] = {
            ["H3254"] = 1555,
            ["mr"] = 1555,
        },
        ["Giant Club of Power"] = {
            ["H3254"] = 13054,
            ["mr"] = 13054,
        },
        ["Chief Brigadier Bracers"] = {
            ["H3254"] = 5752,
            ["mr"] = 5752,
        },
        ["Percussion Shotgun of the Wolf"] = {
            ["H3254"] = 39999,
            ["mr"] = 39999,
        },
        ["Renegade Gauntlets of the Boar"] = {
            ["H3254"] = 8324,
            ["mr"] = 8324,
        },
        ["Plans: Wildthorn Mail"] = {
            ["H3254"] = 6800,
            ["mr"] = 6800,
        },
        ["Ridge Cleaver of the Bear"] = {
            ["H3254"] = 4200,
            ["mr"] = 4200,
        },
        ["Royal Gloves of the Whale"] = {
            ["H3254"] = 12025,
            ["mr"] = 12025,
        },
        ["Ice Cold Milk"] = {
            ["H3254"] = 18,
            ["mr"] = 18,
        },
        ["Pattern: Brightcloth Robe"] = {
            ["H3254"] = 20347,
            ["mr"] = 20347,
        },
        ["Conjurer's Robe of Frozen Wrath"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Vorpal Dagger of the Tiger"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Traveler's Helm"] = {
            ["H3254"] = 29799,
            ["mr"] = 29799,
        },
        ["Renegade Gauntlets of the Bear"] = {
            ["H3254"] = 12445,
            ["mr"] = 12445,
        },
        ["Dire Wand"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Greater Maul of the Boar"] = {
            ["H3254"] = 38530,
            ["mr"] = 38530,
        },
        ["Sage's Mantle of the Owl"] = {
            ["H3254"] = 5503,
            ["mr"] = 5503,
        },
        ["Whipwood Recurve Bow"] = {
            ["H3254"] = 6500,
            ["mr"] = 6500,
        },
        ["Swampwalker Boots"] = {
            ["H3254"] = 149998,
            ["mr"] = 149998,
        },
        ["Emerald Girdle of Healing"] = {
            ["H3254"] = 250000,
            ["mr"] = 250000,
        },
        ["Bandit Boots of the Wolf"] = {
            ["H3254"] = 900,
            ["mr"] = 900,
        },
        ["Savage Axe of Power"] = {
            ["H3254"] = 16969,
            ["mr"] = 16969,
        },
        ["Mark of Kern"] = {
            ["H3254"] = 102000,
            ["mr"] = 102000,
        },
        ["Mug O' Hurt"] = {
            ["H3254"] = 229900,
            ["mr"] = 229900,
        },
        ["Wolffear Harness"] = {
            ["H3254"] = 60450,
            ["mr"] = 60450,
        },
        ["Lower Map Fragment"] = {
            ["H3254"] = 7912,
            ["mr"] = 7912,
        },
        ["Serpentine Loop of Frost Resistance"] = {
            ["H3254"] = 99999,
            ["mr"] = 99999,
        },
        ["Mana Potion"] = {
            ["H3254"] = 888,
            ["mr"] = 888,
        },
        ["Green Lens of Arcane Wrath"] = {
            ["H3254"] = 59900,
            ["mr"] = 59900,
        },
        ["Tin Ore"] = {
            ["H3254"] = 81,
            ["mr"] = 81,
        },
        ["Shimmering Cloak of the Owl"] = {
            ["H3254"] = 1400,
            ["mr"] = 1400,
        },
        ["Schematic: Parachute Cloak"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Fire Wand"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Accurate Scope"] = {
            ["H3254"] = 9700,
            ["mr"] = 9700,
        },
        ["Ranger Leggings of the Eagle"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Ranger Leggings of the Monkey"] = {
            ["H3254"] = 15125,
            ["mr"] = 15125,
        },
        ["Glorious Belt of Power"] = {
            ["H3254"] = 99500,
            ["mr"] = 99500,
        },
        ["Nightshade Helmet of the Owl"] = {
            ["H3254"] = 51500,
            ["mr"] = 51500,
        },
        ["Royal Mallet of the Boar"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Raw Sunscale Salmon"] = {
            ["H3254"] = 98,
            ["mr"] = 98,
        },
        ["Councillor's Cuffs of Arcane Wrath"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Saltstone Gauntlets of the Bear"] = {
            ["H3254"] = 23000,
            ["mr"] = 23000,
        },
        ["Dreamweave Gloves"] = {
            ["H3254"] = 89899,
            ["mr"] = 89899,
        },
        ["Pattern: Dark Leather Shoulders"] = {
            ["H3254"] = 1575,
            ["mr"] = 1575,
        },
        ["Archer's Shoulderpads of the Whale"] = {
            ["H3254"] = 8999,
            ["mr"] = 8999,
        },
        ["Plans: Green Iron Boots"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Twilight Gloves of Healing"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Assassin's Blade"] = {
            ["H3254"] = 3459999,
            ["mr"] = 3459999,
        },
        ["Brigade Gauntlets of the Owl"] = {
            ["H3254"] = 51444,
            ["mr"] = 51444,
        },
        ["Recipe: Blood Sausage"] = {
            ["H3254"] = 180,
            ["mr"] = 180,
        },
        ["Handful of Copper Bolts"] = {
            ["H3254"] = 135,
            ["mr"] = 135,
        },
        ["Swashbuckler's Breastplate of Stamina"] = {
            ["H3254"] = 38370,
            ["mr"] = 38370,
        },
        ["Archer's Shoulderpads of the Eagle"] = {
            ["H3254"] = 14800,
            ["mr"] = 14800,
        },
        ["Stone Hammer of the Bear"] = {
            ["H3254"] = 23000,
            ["mr"] = 23000,
        },
        ["Grave Moss"] = {
            ["H3254"] = 574,
            ["mr"] = 574,
        },
        ["Beheading Blade of the Bear"] = {
            ["H3254"] = 93698,
            ["mr"] = 93698,
        },
        ["Training Sword of the Whale"] = {
            ["H3254"] = 900,
            ["mr"] = 900,
        },
        ["Jasper Link of Shadow Resistance"] = {
            ["H3254"] = 120000,
            ["mr"] = 120000,
        },
        ["Mistscape Wizard Hat"] = {
            ["H3254"] = 8910,
            ["mr"] = 8910,
        },
        ["Rabbit Crate (Snowshoe)"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Elder's Robe of the Whale"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Seeping Gizzard"] = {
            ["H3254"] = 965,
            ["mr"] = 965,
        },
        ["Sagefish Delight"] = {
            ["H3254"] = 399,
            ["mr"] = 399,
        },
        ["Pattern: Green Silk Armor"] = {
            ["H3254"] = 8800,
            ["mr"] = 8800,
        },
        ["Heart of Fire"] = {
            ["H3254"] = 1340,
            ["mr"] = 1340,
        },
        ["Azure Silk Belt"] = {
            ["H3254"] = 11500,
            ["mr"] = 11500,
        },
        ["Ridge Cleaver of the Tiger"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Harpy Needler of the Wolf"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Sage's Cloak of Spirit"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Sergeant's Warhammer of Power"] = {
            ["H3254"] = 2800,
            ["mr"] = 2800,
        },
        ["Goretusk Liver"] = {
            ["H3254"] = 570,
            ["mr"] = 570,
        },
        ["Murloc Fin Soup"] = {
            ["H3254"] = 391,
            ["mr"] = 391,
        },
        ["Pattern: Green Dragonscale Leggings"] = {
            ["H3254"] = 10800,
            ["mr"] = 10800,
        },
        ["War Knife of Arcane Wrath"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Ranger Gloves of the Monkey"] = {
            ["H3254"] = 8900,
            ["mr"] = 8900,
        },
        ["Viking Sword of the Monkey"] = {
            ["H3254"] = 10500,
            ["mr"] = 10500,
        },
        ["Carefully Folded Note"] = {
            ["H3254"] = 89990,
            ["mr"] = 89990,
        },
        ["Green Hills of Stranglethorn - Page 11"] = {
            ["H3254"] = 550,
            ["mr"] = 550,
        },
        ["Dreadblade of the Monkey"] = {
            ["H3254"] = 27800,
            ["mr"] = 27800,
        },
        ["Dark Leather Belt"] = {
            ["H3254"] = 1355,
            ["mr"] = 1355,
        },
        ["Brutish Legguards of the Bear"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Hook Dagger of Arcane Wrath"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Bottom Half of Advanced Armorsmithing: Volume III"] = {
            ["H3254"] = 109499,
            ["mr"] = 109499,
        },
        ["Royal Sash of the Owl"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Willow Pants of the Eagle"] = {
            ["H3254"] = 1900,
            ["mr"] = 1900,
        },
        ["Gyrochronatom"] = {
            ["H3254"] = 1930,
            ["mr"] = 1930,
        },
        ["Gloom Reaper of the Wolf"] = {
            ["H3254"] = 13900,
            ["mr"] = 13900,
        },
        ["Brilliant Chromatic Scale"] = {
            ["H3254"] = 11400,
            ["mr"] = 11400,
        },
        ["Formula: Enchant Shield - Frost Resistance"] = {
            ["H3254"] = 107500,
            ["mr"] = 107500,
        },
        ["Dervish Belt of the Eagle"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Blackrock Champion's Axe"] = {
            ["H3254"] = 10400,
            ["mr"] = 10400,
        },
        ["Inscribed Leather Breastplate"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Red Rose"] = {
            ["H3254"] = 1460,
            ["mr"] = 1460,
        },
        ["Wicked Claw"] = {
            ["H3254"] = 497,
            ["mr"] = 497,
        },
        ["Percussion Shotgun of the Tiger"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Vanadium Talisman of Stamina"] = {
            ["H3254"] = 29999,
            ["mr"] = 29999,
        },
        ["Enchanted Thorium Bar"] = {
            ["H3254"] = 14999,
            ["mr"] = 14999,
        },
        ["Raw Longjaw Mud Snapper"] = {
            ["H3254"] = 59,
            ["mr"] = 59,
        },
        ["Bloodforged Bindings of Strength"] = {
            ["H3254"] = 800000,
            ["mr"] = 800000,
        },
        ["Overlord's Legplates of the Bear"] = {
            ["H3254"] = 34800,
            ["mr"] = 34800,
        },
        ["Heavy Blasting Powder"] = {
            ["H3254"] = 297,
            ["mr"] = 297,
        },
        ["Battlefield Destroyer of the Tiger"] = {
            ["H3254"] = 39000,
            ["mr"] = 39000,
        },
        ["Gnomish Universal Remote"] = {
            ["H3254"] = 23400,
            ["mr"] = 23400,
        },
        ["Hillborne Axe of the Eagle"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Hunter's Muzzle Loader"] = {
            ["H3254"] = 1910,
            ["mr"] = 1910,
        },
        ["Rough Blasting Powder"] = {
            ["H3254"] = 14,
            ["mr"] = 14,
        },
        ["Mugthol's Helm"] = {
            ["H3254"] = 95267,
            ["mr"] = 95267,
        },
        ["Recipe: Curiously Tasty Omelet"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Scroll of Strength IV"] = {
            ["H3254"] = 2100,
            ["mr"] = 2100,
        },
        ["Tracker's Belt of the Tiger"] = {
            ["H3254"] = 9500,
            ["mr"] = 9500,
        },
        ["Jouster's Pauldrons"] = {
            ["H3254"] = 11900,
            ["mr"] = 11900,
        },
        ["Magician's Mantle"] = {
            ["H3254"] = 149500,
            ["mr"] = 149500,
        },
        ["Chieftain's Headdress of the Monkey"] = {
            ["H3254"] = 34000,
            ["mr"] = 34000,
        },
        ["Archer's Gloves of the Bear"] = {
            ["H3254"] = 5943,
            ["mr"] = 5943,
        },
        ["Recipe: Elixir of the Sages"] = {
            ["H3254"] = 69998,
            ["mr"] = 69998,
        },
        ["Incendosaur Scale"] = {
            ["H3254"] = 348,
            ["mr"] = 348,
        },
        ["Swiftthistle"] = {
            ["H3254"] = 1390,
            ["mr"] = 1390,
        },
        ["Pattern: Brightcloth Gloves"] = {
            ["H3254"] = 21500,
            ["mr"] = 21500,
        },
        ["Knight's Gauntlets of the Bear"] = {
            ["H3254"] = 6999,
            ["mr"] = 6999,
        },
        ["Wildheart Belt"] = {
            ["H3254"] = 78585,
            ["mr"] = 78585,
        },
        ["Pattern: Volcanic Breastplate"] = {
            ["H3254"] = 28665,
            ["mr"] = 28665,
        },
        ["Battle Chain Pants"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Book: Gift of the Wild II"] = {
            ["H3254"] = 28800,
            ["mr"] = 28800,
        },
        ["Vanadium Talisman of the Owl"] = {
            ["H3254"] = 36323,
            ["mr"] = 36323,
        },
        ["Tangy Clam Meat"] = {
            ["H3254"] = 199,
            ["mr"] = 199,
        },
        ["Slayer's Surcoat"] = {
            ["H3254"] = 9942,
            ["mr"] = 9942,
        },
        ["Honed Stiletto of Nature's Wrath"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Battering Hammer of the Boar"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Hacking Cleaver of the Monkey"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Fine Thread"] = {
            ["H3254"] = 290,
            ["mr"] = 290,
        },
        ["Geomancer's Gloves of the Whale"] = {
            ["H3254"] = 6381,
            ["mr"] = 6381,
        },
        ["Hefty Battlehammer of Power"] = {
            ["H3254"] = 6999,
            ["mr"] = 6999,
        },
        ["Big Bronze Knife"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Heavy Lamellar Vambraces of the Bear"] = {
            ["H3254"] = 62967,
            ["mr"] = 62967,
        },
        ["Gossamer Headpiece of the Owl"] = {
            ["H3254"] = 11000,
            ["mr"] = 11000,
        },
        ["Strider Stew"] = {
            ["H3254"] = 50,
            ["mr"] = 50,
        },
        ["Archer's Boots of Stamina"] = {
            ["H3254"] = 4500,
            ["mr"] = 4500,
        },
        ["Regal Sash of the Whale"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Duskwoven Gloves of the Eagle"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Murloc Eye"] = {
            ["H3254"] = 20,
            ["mr"] = 20,
        },
        ["Mystical Robe of the Owl"] = {
            ["H3254"] = 195881,
            ["mr"] = 195881,
        },
        ["Huntsman's Boots of Agility"] = {
            ["H3254"] = 12537,
            ["mr"] = 12537,
        },
        ["Emerald Helm of the Bear"] = {
            ["H3254"] = 98621,
            ["mr"] = 98621,
        },
        ["Soldier's Armor of the Whale"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Forest Pendant of the Monkey"] = {
            ["H3254"] = 49968,
            ["mr"] = 49968,
        },
        ["Commander's Girdle of Healing"] = {
            ["H3254"] = 200000,
            ["mr"] = 200000,
        },
        ["Battleforge Girdle of the Whale"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Glowstar Rod of Healing"] = {
            ["H3254"] = 390000,
            ["mr"] = 390000,
        },
        ["Valorous Gauntlets"] = {
            ["H3254"] = 200000,
            ["mr"] = 200000,
        },
        ["Plans: Green Iron Shoulders"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Swashbuckler's Breastplate of the Monkey"] = {
            ["H3254"] = 76816,
            ["mr"] = 76816,
        },
        ["Forester's Axe of the Tiger"] = {
            ["H3254"] = 4999,
            ["mr"] = 4999,
        },
        ["Formula: Enchant Gloves - Advanced Mining"] = {
            ["H3254"] = 1475,
            ["mr"] = 1475,
        },
        ["Enchanted Leather"] = {
            ["H3254"] = 16085,
            ["mr"] = 16085,
        },
        ["Hillborne Axe of the Whale"] = {
            ["H3254"] = 8999,
            ["mr"] = 8999,
        },
        ["Conjurer's Cloak of the Whale"] = {
            ["H3254"] = 700000,
            ["mr"] = 700000,
        },
        ["Magic Dust"] = {
            ["H3254"] = 8970,
            ["mr"] = 8970,
        },
        ["Gypsy Trousers of Spirit"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Pattern: Felcloth Robe"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Small Glimmering Shard"] = {
            ["H3254"] = 160,
            ["mr"] = 160,
        },
        ["Dreadblade of the Tiger"] = {
            ["H3254"] = 35139,
            ["mr"] = 35139,
        },
        ["Large Red Sack"] = {
            ["H3254"] = 4700,
            ["mr"] = 4700,
        },
        ["Deep Fried Plantains"] = {
            ["H3254"] = 2199,
            ["mr"] = 2199,
        },
        ["Pattern: Azure Silk Cloak"] = {
            ["H3254"] = 13500,
            ["mr"] = 13500,
        },
        ["Windchaser Amice"] = {
            ["H3254"] = 6400,
            ["mr"] = 6400,
        },
        ["Manual: Heavy Silk Bandage"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Recipe: Lesser Stoneshield Potion"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["The Queen's Jewel"] = {
            ["H3254"] = 23000,
            ["mr"] = 23000,
        },
        ["Imposing Vest of the Monkey"] = {
            ["H3254"] = 33766,
            ["mr"] = 33766,
        },
        ["Aurora Cowl"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Plans: Thorium Shield Spike"] = {
            ["H3254"] = 500000,
            ["mr"] = 500000,
        },
        ["Widow Blade of the Bear"] = {
            ["H3254"] = 77772,
            ["mr"] = 77772,
        },
        ["Accurate Slugs"] = {
            ["H3254"] = 22,
            ["mr"] = 22,
        },
        ["Schematic: Dark Iron Rifle"] = {
            ["H3254"] = 14000,
            ["mr"] = 14000,
        },
        ["Scouting Trousers of Agility"] = {
            ["H3254"] = 3438,
            ["mr"] = 3438,
        },
        ["Lord's Pauldrons of Stamina"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Twilight Boots of Frozen Wrath"] = {
            ["H3254"] = 50538,
            ["mr"] = 50538,
        },
        ["Runecloth Bag"] = {
            ["H3254"] = 29400,
            ["mr"] = 29400,
        },
        ["Steelclaw Reaver"] = {
            ["H3254"] = 32200,
            ["mr"] = 32200,
        },
        ["Mystical Cape of the Whale"] = {
            ["H3254"] = 79945,
            ["mr"] = 79945,
        },
        ["Formula: Enchant Chest - Major Health"] = {
            ["H3254"] = 56999,
            ["mr"] = 56999,
        },
        ["Blazing Emblem"] = {
            ["H3254"] = 312000,
            ["mr"] = 312000,
        },
        ["Formula: Enchant Bracer - Deflection"] = {
            ["H3254"] = 36500,
            ["mr"] = 36500,
        },
        ["Severing Axe of the Tiger"] = {
            ["H3254"] = 1294,
            ["mr"] = 1294,
        },
        ["Ranger Helm of Stamina"] = {
            ["H3254"] = 22465,
            ["mr"] = 22465,
        },
        ["Bruiseweed"] = {
            ["H3254"] = 31,
            ["mr"] = 31,
        },
        ["Thick Leather"] = {
            ["H3254"] = 624,
            ["mr"] = 624,
        },
        ["Glowstar Rod of the Owl"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Mulgore Spice Bread"] = {
            ["H3254"] = 152,
            ["mr"] = 152,
        },
        ["Unstable Trigger"] = {
            ["H3254"] = 7899,
            ["mr"] = 7899,
        },
        ["Scroll of Agility IV"] = {
            ["H3254"] = 4699,
            ["mr"] = 4699,
        },
        ["Imposing Shoulders of the Bear"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Glimmering Mail Greaves"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Lesser Healing Potion"] = {
            ["H3254"] = 62,
            ["mr"] = 62,
        },
        ["Plans: Annihilator"] = {
            ["H3254"] = 60000,
            ["mr"] = 60000,
        },
        ["Gloom Reaper of the Tiger"] = {
            ["H3254"] = 14800,
            ["mr"] = 14800,
        },
        ["Enchanted Water"] = {
            ["H3254"] = 132,
            ["mr"] = 132,
        },
        ["Barbarian War Axe of the Whale"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Strong Fishing Pole"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Copper Bar"] = {
            ["H3254"] = 125,
            ["mr"] = 125,
        },
        ["Recipe: Great Rage Potion"] = {
            ["H3254"] = 3100,
            ["mr"] = 3100,
        },
        ["Battleforge Gauntlets of the Bear"] = {
            ["H3254"] = 6059,
            ["mr"] = 6059,
        },
        ["Dark Iron Ore"] = {
            ["H3254"] = 1430,
            ["mr"] = 1430,
        },
        ["Runecloth Belt"] = {
            ["H3254"] = 13900,
            ["mr"] = 13900,
        },
        ["Pattern: Red Whelp Gloves"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Imperial Leather Belt"] = {
            ["H3254"] = 65000,
            ["mr"] = 65000,
        },
        ["Welken Ring"] = {
            ["H3254"] = 6094,
            ["mr"] = 6094,
        },
        ["Cabalist Chestpiece of the Bear"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Tender Wolf Meat"] = {
            ["H3254"] = 600,
            ["mr"] = 600,
        },
        ["Jazeraint Gauntlets of the Bear"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Renegade Bracers of Stamina"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Mithril Ore"] = {
            ["H3254"] = 1650,
            ["mr"] = 1650,
        },
        ["Stout Battlehammer of Power"] = {
            ["H3254"] = 2970,
            ["mr"] = 2970,
        },
        ["Lunar Leggings of the Eagle"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Swashbuckler's Leggings of Agility"] = {
            ["H3254"] = 29635,
            ["mr"] = 29635,
        },
        ["Small Brilliant Shard"] = {
            ["H3254"] = 3700,
            ["mr"] = 3700,
        },
        ["Chesterfall Musket"] = {
            ["H3254"] = 109999,
            ["mr"] = 109999,
        },
        ["Blue Pearl"] = {
            ["H3254"] = 5400,
            ["mr"] = 5400,
        },
        ["Zesty Clam Meat"] = {
            ["H3254"] = 110,
            ["mr"] = 110,
        },
        ["Elder's Hat of the Eagle"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Aquamarine"] = {
            ["H3254"] = 1575,
            ["mr"] = 1575,
        },
        ["Pattern: Tough Scorpid Leggings"] = {
            ["H3254"] = 2099,
            ["mr"] = 2099,
        },
        ["Defender Leggings of Strength"] = {
            ["H3254"] = 3906,
            ["mr"] = 3906,
        },
        ["Pattern: Red Mageweave Headband"] = {
            ["H3254"] = 7158,
            ["mr"] = 7158,
        },
        ["Black Dragonscale"] = {
            ["H3254"] = 7300,
            ["mr"] = 7300,
        },
        ["Renegade Belt of Power"] = {
            ["H3254"] = 6900,
            ["mr"] = 6900,
        },
        ["Large Brown Sack"] = {
            ["H3254"] = 3700,
            ["mr"] = 3700,
        },
        ["Darkclaw Lobster"] = {
            ["H3254"] = 1395,
            ["mr"] = 1395,
        },
        ["Infiltrator Shoulders of the Owl"] = {
            ["H3254"] = 11040,
            ["mr"] = 11040,
        },
        ["Sentinel Cloak of Stamina"] = {
            ["H3254"] = 9337,
            ["mr"] = 9337,
        },
        ["Warmonger's Cloak of Arcane Wrath"] = {
            ["H3254"] = 11100,
            ["mr"] = 11100,
        },
        ["Bandit Cloak of the Monkey"] = {
            ["H3254"] = 2492,
            ["mr"] = 2492,
        },
        ["Black Metal Axe"] = {
            ["H3254"] = 3861,
            ["mr"] = 3861,
        },
        ["Burnside Rifle of the Monkey"] = {
            ["H3254"] = 80000,
            ["mr"] = 80000,
        },
        ["Bandit Jerkin of the Whale"] = {
            ["H3254"] = 6500,
            ["mr"] = 6500,
        },
        ["Willow Branch of the Falcon"] = {
            ["H3254"] = 4017,
            ["mr"] = 4017,
        },
        ["Blackrock Mace"] = {
            ["H3254"] = 2472,
            ["mr"] = 2472,
        },
        ["Barbaric Battle Axe of the Monkey"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Spider's Silk"] = {
            ["H3254"] = 1149,
            ["mr"] = 1149,
        },
        ["Soul Dust"] = {
            ["H3254"] = 450,
            ["mr"] = 450,
        },
        ["Recipe: Elixir of the Mongoose"] = {
            ["H3254"] = 7410,
            ["mr"] = 7410,
        },
        ["Iron Ore"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Sage's Stave of the Owl"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Recipe: Rage Potion"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Glowstar Rod of the Falcon"] = {
            ["H3254"] = 205500,
            ["mr"] = 205500,
        },
        ["Ivycloth Boots of Spirit"] = {
            ["H3254"] = 3999,
            ["mr"] = 3999,
        },
        ["Wildkin E'ko"] = {
            ["H3254"] = 2099,
            ["mr"] = 2099,
        },
        ["Lunar Mantle of Frozen Wrath"] = {
            ["H3254"] = 47200,
            ["mr"] = 47200,
        },
        ["The Shadowfoot Stabber"] = {
            ["H3254"] = 249999,
            ["mr"] = 249999,
        },
        ["Scouting Tunic of Power"] = {
            ["H3254"] = 11000,
            ["mr"] = 11000,
        },
        ["Demon Blade of Agility"] = {
            ["H3254"] = 164072,
            ["mr"] = 164072,
        },
        ["Revenant Helmet of the Bear"] = {
            ["H3254"] = 11900,
            ["mr"] = 11900,
        },
        ["Mercurial Cloak of the Monkey"] = {
            ["H3254"] = 34332,
            ["mr"] = 34332,
        },
        ["Razor Axe of the Tiger"] = {
            ["H3254"] = 70000,
            ["mr"] = 70000,
        },
        ["Ivory Band of the Bear"] = {
            ["H3254"] = 29942,
            ["mr"] = 29942,
        },
        ["Gloom Reaper of the Whale"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Core Leather"] = {
            ["H3254"] = 14528,
            ["mr"] = 14528,
        },
        ["Metalworking Gloves"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Schematic: Truesilver Transformer"] = {
            ["H3254"] = 27000,
            ["mr"] = 27000,
        },
        ["Ember Wand of Arcane Wrath"] = {
            ["H3254"] = 14000,
            ["mr"] = 14000,
        },
        ["Amethyst Band of Shadow Resistance"] = {
            ["H3254"] = 18075,
            ["mr"] = 18075,
        },
        ["Ivory Band of the Boar"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Headstriker Sword of the Boar"] = {
            ["H3254"] = 28000,
            ["mr"] = 28000,
        },
        ["Conjurer's Robe of Spirit"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Burnished Boots"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Hook Dagger of Strength"] = {
            ["H3254"] = 5325,
            ["mr"] = 5325,
        },
        ["Short Bastard Sword of Strength"] = {
            ["H3254"] = 2925,
            ["mr"] = 2925,
        },
        ["Bandit Boots of Spirit"] = {
            ["H3254"] = 1300,
            ["mr"] = 1300,
        },
        ["Ravager's Woolies"] = {
            ["H3254"] = 10200,
            ["mr"] = 10200,
        },
        ["Long Tail Feather"] = {
            ["H3254"] = 329,
            ["mr"] = 329,
        },
        ["Dwarven Hatchet of Strength"] = {
            ["H3254"] = 8904,
            ["mr"] = 8904,
        },
        ["Greater Stoneshield Potion"] = {
            ["H3254"] = 12498,
            ["mr"] = 12498,
        },
        ["Wild Leather Shoulders of the Eagle"] = {
            ["H3254"] = 12000,
            ["mr"] = 12000,
        },
        ["Barbed Club of Nature's Wrath"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Buccaneer's Pants of the Whale"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Fortified Belt of the Tiger"] = {
            ["H3254"] = 3900,
            ["mr"] = 3900,
        },
        ["Recipe: Smoked Bear Meat"] = {
            ["H3254"] = 3800,
            ["mr"] = 3800,
        },
        ["Nightsky Mantle"] = {
            ["H3254"] = 6400,
            ["mr"] = 6400,
        },
        ["Arctic Pendant of the Falcon"] = {
            ["H3254"] = 28000,
            ["mr"] = 28000,
        },
        ["Elder's Robe of Shadow Wrath"] = {
            ["H3254"] = 40000,
            ["mr"] = 40000,
        },
        ["Birchwood Maul of Strength"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Elixir of Greater Intellect"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Councillor's Gloves of the Eagle"] = {
            ["H3254"] = 45000,
            ["mr"] = 45000,
        },
        ["Thorium Ore"] = {
            ["H3254"] = 5311,
            ["mr"] = 5311,
        },
        ["Elder's Pants of the Owl"] = {
            ["H3254"] = 10938,
            ["mr"] = 10938,
        },
        ["Pattern: Runecloth Robe"] = {
            ["H3254"] = 213699,
            ["mr"] = 213699,
        },
        ["Pattern: Runecloth Gloves"] = {
            ["H3254"] = 158499,
            ["mr"] = 158499,
        },
        ["Cured Medium Hide"] = {
            ["H3254"] = 605,
            ["mr"] = 605,
        },
        ["Sniper Scope"] = {
            ["H3254"] = 38318,
            ["mr"] = 38318,
        },
        ["Banded Helm of the Monkey"] = {
            ["H3254"] = 12499,
            ["mr"] = 12499,
        },
        ["Small Spider Leg"] = {
            ["H3254"] = 20,
            ["mr"] = 20,
        },
        ["Sorcerer Pants of the Whale"] = {
            ["H3254"] = 12200,
            ["mr"] = 12200,
        },
        ["Monster Omelet"] = {
            ["H3254"] = 2399,
            ["mr"] = 2399,
        },
        ["Thorium Shells"] = {
            ["H3254"] = 65,
            ["mr"] = 65,
        },
        ["Recipe: Major Healing Potion"] = {
            ["H3254"] = 22898,
            ["mr"] = 22898,
        },
        ["Recipe: Mithril Head Trout"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Recipe: Westfall Stew"] = {
            ["H3254"] = 300,
            ["mr"] = 300,
        },
        ["Ghostwalker Gloves of the Bear"] = {
            ["H3254"] = 7700,
            ["mr"] = 7700,
        },
        ["Nightshade Gloves of the Eagle"] = {
            ["H3254"] = 96399,
            ["mr"] = 96399,
        },
        ["Pattern: Felcloth Boots"] = {
            ["H3254"] = 29799,
            ["mr"] = 29799,
        },
        ["Black Pearl"] = {
            ["H3254"] = 1575,
            ["mr"] = 1575,
        },
        ["Scroll of Agility II"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Falcon's Hook"] = {
            ["H3254"] = 8900,
            ["mr"] = 8900,
        },
        ["Righteous Gloves of the Monkey"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Pattern: Black Dragonscale Leggings"] = {
            ["H3254"] = 84900,
            ["mr"] = 84900,
        },
        ["Spiritchaser Staff of the Bear"] = {
            ["H3254"] = 34125,
            ["mr"] = 34125,
        },
        ["Ravager's Shield"] = {
            ["H3254"] = 19600,
            ["mr"] = 19600,
        },
        ["Dry Pork Ribs"] = {
            ["H3254"] = 125,
            ["mr"] = 125,
        },
        ["Stone Hammer of Spirit"] = {
            ["H3254"] = 27968,
            ["mr"] = 27968,
        },
        ["Pattern: Wicked Leather Bracers"] = {
            ["H3254"] = 15500,
            ["mr"] = 15500,
        },
        ["Plans: Blue Glittering Axe"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Green Power Crystal"] = {
            ["H3254"] = 386,
            ["mr"] = 386,
        },
        ["Dazzling Longsword"] = {
            ["H3254"] = 207777,
            ["mr"] = 207777,
        },
        ["Militant Shortsword of the Bear"] = {
            ["H3254"] = 23034,
            ["mr"] = 23034,
        },
        ["Formula: Enchant Gloves - Skinning"] = {
            ["H3254"] = 3400,
            ["mr"] = 3400,
        },
        ["Pattern: Heavy Leather Ball"] = {
            ["H3254"] = 21600,
            ["mr"] = 21600,
        },
        ["Scorpashi Sash"] = {
            ["H3254"] = 29275,
            ["mr"] = 29275,
        },
        ["Green Woolen Bag"] = {
            ["H3254"] = 1478,
            ["mr"] = 1478,
        },
        ["Twin-bladed Axe of Stamina"] = {
            ["H3254"] = 4647,
            ["mr"] = 4647,
        },
        ["Skullance Shield"] = {
            ["H3254"] = 53138,
            ["mr"] = 53138,
        },
        ["Sharp Claw"] = {
            ["H3254"] = 72,
            ["mr"] = 72,
        },
        ["Lord's Pauldrons of the Monkey"] = {
            ["H3254"] = 78223,
            ["mr"] = 78223,
        },
        ["Red Woolen Bag"] = {
            ["H3254"] = 1227,
            ["mr"] = 1227,
        },
        ["Elemental Fire"] = {
            ["H3254"] = 19800,
            ["mr"] = 19800,
        },
        ["Wastewander Water Pouch"] = {
            ["H3254"] = 525,
            ["mr"] = 525,
        },
        ["Sage's Mantle of the Wolf"] = {
            ["H3254"] = 6500,
            ["mr"] = 6500,
        },
        ["Raptor Egg"] = {
            ["H3254"] = 150,
            ["mr"] = 150,
        },
        ["Ranger Boots of the Monkey"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Seer's Fine Stein"] = {
            ["H3254"] = 2890,
            ["mr"] = 2890,
        },
        ["Cavalier Two-hander of Strength"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Gnoll Skull Basher"] = {
            ["H3254"] = 1995,
            ["mr"] = 1995,
        },
        ["Swashbuckler's Breastplate of the Owl"] = {
            ["H3254"] = 62000,
            ["mr"] = 62000,
        },
        ["Trueshot Bow of the Owl"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Cabalist Chestpiece of Stamina"] = {
            ["H3254"] = 20000,
            ["mr"] = 20000,
        },
        ["Ballast Maul of the Boar"] = {
            ["H3254"] = 11499,
            ["mr"] = 11499,
        },
        ["Wolfshead Helm"] = {
            ["H3254"] = 65000,
            ["mr"] = 65000,
        },
        ["Bloodspiller"] = {
            ["H3254"] = 24000,
            ["mr"] = 24000,
        },
        ["Mithril Spurs"] = {
            ["H3254"] = 1999,
            ["mr"] = 1999,
        },
        ["Twilight Orb of the Whale"] = {
            ["H3254"] = 64644,
            ["mr"] = 64644,
        },
        ["Seer's Belt"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Naraxis' Fang"] = {
            ["H3254"] = 13059,
            ["mr"] = 13059,
        },
        ["Schematic: Goblin Jumper Cables"] = {
            ["H3254"] = 17200,
            ["mr"] = 17200,
        },
        ["Bandit Cloak of the Eagle"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Greater Maul of Stamina"] = {
            ["H3254"] = 96688,
            ["mr"] = 96688,
        },
        ["Elixir of Water Breathing"] = {
            ["H3254"] = 1795,
            ["mr"] = 1795,
        },
        ["Mystical Leggings of Healing"] = {
            ["H3254"] = 109000,
            ["mr"] = 109000,
        },
        ["Parrot Cage (Green Wing Macaw)"] = {
            ["H3254"] = 1794,
            ["mr"] = 1794,
        },
        ["Twilight Boots of Shadow Wrath"] = {
            ["H3254"] = 48000,
            ["mr"] = 48000,
        },
        ["Pattern: Enchanted Mageweave Pouch"] = {
            ["H3254"] = 10436,
            ["mr"] = 10436,
        },
        ["Twilight Cowl of Healing"] = {
            ["H3254"] = 16969,
            ["mr"] = 16969,
        },
        ["Crusader's Boots of the Wolf"] = {
            ["H3254"] = 29700,
            ["mr"] = 29700,
        },
        ["Lesser Eternal Essence"] = {
            ["H3254"] = 10199,
            ["mr"] = 10199,
        },
        ["Formula: Enchant Weapon - Crusader"] = {
            ["H3254"] = 2744999,
            ["mr"] = 2744999,
        },
        ["Sentinel Cap of the Bear"] = {
            ["H3254"] = 12165,
            ["mr"] = 12165,
        },
        ["Thorium Bar"] = {
            ["H3254"] = 4250,
            ["mr"] = 4250,
        },
        ["Greater Arcane Elixir"] = {
            ["H3254"] = 41500,
            ["mr"] = 41500,
        },
        ["Red Fireworks Rocket"] = {
            ["H3254"] = 1550,
            ["mr"] = 1550,
        },
        ["Plans: Iron Shield Spike"] = {
            ["H3254"] = 3300,
            ["mr"] = 3300,
        },
        ["Invisibility Potion"] = {
            ["H3254"] = 19000,
            ["mr"] = 19000,
        },
        ["Spellbinder Orb"] = {
            ["H3254"] = 2298,
            ["mr"] = 2298,
        },
        ["Massacre Sword of the Bear"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Mystery Meat"] = {
            ["H3254"] = 162,
            ["mr"] = 162,
        },
        ["Silk Cloth"] = {
            ["H3254"] = 188,
            ["mr"] = 188,
        },
        ["Mystical Headwrap of the Owl"] = {
            ["H3254"] = 61380,
            ["mr"] = 61380,
        },
        ["Pattern: Crimson Silk Shoulders"] = {
            ["H3254"] = 1399,
            ["mr"] = 1399,
        },
        ["Pattern: Runic Leather Bracers"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Councillor's Cuffs of Intellect"] = {
            ["H3254"] = 35023,
            ["mr"] = 35023,
        },
        ["Burnished Tunic"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Pattern: Frostsaber Boots"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Banded Leggings of the Bear"] = {
            ["H3254"] = 5500,
            ["mr"] = 5500,
        },
        ["Hematite Link of Fire Resistance"] = {
            ["H3254"] = 39900,
            ["mr"] = 39900,
        },
        ["Green Lens of Fiery Wrath"] = {
            ["H3254"] = 45000,
            ["mr"] = 45000,
        },
        ["Patched Leather Shoulderpads"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Edgemaster's Handguards"] = {
            ["H3254"] = 3652500,
            ["mr"] = 3652500,
        },
        ["Hibernal Cowl"] = {
            ["H3254"] = 31000,
            ["mr"] = 31000,
        },
        ["Small Radiant Shard"] = {
            ["H3254"] = 37999,
            ["mr"] = 37999,
        },
        ["Deepfury Bracers"] = {
            ["H3254"] = 891666,
            ["mr"] = 891666,
        },
        ["Hillborne Axe of the Bear"] = {
            ["H3254"] = 17240,
            ["mr"] = 17240,
        },
        ["Durable Cape of Shadow Wrath"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Scalping Tomahawk of Strength"] = {
            ["H3254"] = 500,
            ["mr"] = 500,
        },
        ["Headstriker Sword of the Tiger"] = {
            ["H3254"] = 24021,
            ["mr"] = 24021,
        },
        ["Devilsaur Leather"] = {
            ["H3254"] = 99000,
            ["mr"] = 99000,
        },
        ["Vibrant Plume"] = {
            ["H3254"] = 1258,
            ["mr"] = 1258,
        },
        ["Spiritchaser Staff of Healing"] = {
            ["H3254"] = 50000,
            ["mr"] = 50000,
        },
        ["Elder's Gloves of the Monkey"] = {
            ["H3254"] = 4584,
            ["mr"] = 4584,
        },
        ["Blanchard's Stout"] = {
            ["H3254"] = 280000,
            ["mr"] = 280000,
        },
        ["Ghostwalker Pads of Stamina"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Formula: Enchant Boots - Spirit"] = {
            ["H3254"] = 41000,
            ["mr"] = 41000,
        },
        ["Codex: Prayer of Fortitude II"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Rough Sharpening Stone"] = {
            ["H3254"] = 22,
            ["mr"] = 22,
        },
        ["Bandit Cloak of Agility"] = {
            ["H3254"] = 2400,
            ["mr"] = 2400,
        },
        ["Recipe: Tasty Lion Steak"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Recipe: Redridge Goulash"] = {
            ["H3254"] = 331,
            ["mr"] = 331,
        },
        ["Hefty Battlehammer of the Tiger"] = {
            ["H3254"] = 10077,
            ["mr"] = 10077,
        },
        ["Soldier's Leggings of the Eagle"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Gold Bar"] = {
            ["H3254"] = 1100,
            ["mr"] = 1100,
        },
        ["Sanguine Trousers"] = {
            ["H3254"] = 8500,
            ["mr"] = 8500,
        },
        ["Diamond-Tip Bludgeon of the Tiger"] = {
            ["H3254"] = 65775,
            ["mr"] = 65775,
        },
        ["Traveler's Gloves"] = {
            ["H3254"] = 25400,
            ["mr"] = 25400,
        },
        ["Knight's Girdle of the Bear"] = {
            ["H3254"] = 7586,
            ["mr"] = 7586,
        },
        ["Pattern: Black Silk Pack"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Ridge Cleaver of the Boar"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Elixir of Fortitude"] = {
            ["H3254"] = 2900,
            ["mr"] = 2900,
        },
        ["Plans: Iridescent Hammer"] = {
            ["H3254"] = 3120,
            ["mr"] = 3120,
        },
        ["Birchwood Maul of the Gorilla"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Lord's Crest of the Monkey"] = {
            ["H3254"] = 29999,
            ["mr"] = 29999,
        },
        ["Pearl-handled Dagger"] = {
            ["H3254"] = 9999,
            ["mr"] = 9999,
        },
        ["Red Power Crystal"] = {
            ["H3254"] = 448,
            ["mr"] = 448,
        },
        ["Dervish Leggings of the Gorilla"] = {
            ["H3254"] = 7100,
            ["mr"] = 7100,
        },
        ["Polished Zweihander of the Bear"] = {
            ["H3254"] = 12500,
            ["mr"] = 12500,
        },
        ["Raider's Shield of Blocking"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Bloodwoven Cord of Healing"] = {
            ["H3254"] = 120000,
            ["mr"] = 120000,
        },
        ["Councillor's Shoulders of Shadow Wrath"] = {
            ["H3254"] = 72500,
            ["mr"] = 72500,
        },
        ["Medallion of Grand Marshal Morris"] = {
            ["H3254"] = 1199998,
            ["mr"] = 1199998,
        },
        ["Brocade Shoulderpads"] = {
            ["H3254"] = 1400,
            ["mr"] = 1400,
        },
        ["Fortified Bracers of the Boar"] = {
            ["H3254"] = 3338,
            ["mr"] = 3338,
        },
        ["Long Battle Bow"] = {
            ["H3254"] = 4300,
            ["mr"] = 4300,
        },
        ["Captain's Gauntlets of the Gorilla"] = {
            ["H3254"] = 11053,
            ["mr"] = 11053,
        },
        ["Dervish Leggings of the Whale"] = {
            ["H3254"] = 26424,
            ["mr"] = 26424,
        },
        ["17 Pound Catfish"] = {
            ["H3254"] = 995,
            ["mr"] = 995,
        },
        ["Peacebloom"] = {
            ["H3254"] = 2,
            ["mr"] = 2,
        },
        ["Recipe: Greater Fire Protection Potion"] = {
            ["H3254"] = 1900000,
            ["mr"] = 1900000,
        },
        ["Runed Copper Breastplate"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Gossamer Pants of the Whale"] = {
            ["H3254"] = 100000,
            ["mr"] = 100000,
        },
        ["Raider's Chestpiece of the Boar"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Forest Leather Mantle"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Soft Bushy Tail"] = {
            ["H3254"] = 745,
            ["mr"] = 745,
        },
        ["Rune Sword of the Bear"] = {
            ["H3254"] = 39990,
            ["mr"] = 39990,
        },
        ["Cerulean Ring of Spirit"] = {
            ["H3254"] = 5753,
            ["mr"] = 5753,
        },
        ["Refined Deeprock Salt"] = {
            ["H3254"] = 138999,
            ["mr"] = 138999,
        },
        ["Ivycloth Cloak of the Eagle"] = {
            ["H3254"] = 3990,
            ["mr"] = 3990,
        },
        ["Fighter Broadsword of the Bear"] = {
            ["H3254"] = 8185,
            ["mr"] = 8185,
        },
        ["Superior Mana Potion"] = {
            ["H3254"] = 5699,
            ["mr"] = 5699,
        },
        ["Insignia Chestguard"] = {
            ["H3254"] = 6400,
            ["mr"] = 6400,
        },
        ["Twilight Cape of Frozen Wrath"] = {
            ["H3254"] = 4645,
            ["mr"] = 4645,
        },
        ["Elixir of Agility"] = {
            ["H3254"] = 1100,
            ["mr"] = 1100,
        },
        ["Medium Leather"] = {
            ["H3254"] = 72,
            ["mr"] = 72,
        },
        ["Conjurer's Mantle of the Eagle"] = {
            ["H3254"] = 14200,
            ["mr"] = 14200,
        },
        ["Shardtooth E'ko"] = {
            ["H3254"] = 4000,
            ["mr"] = 4000,
        },
        ["Infiltrator Armor of Intellect"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Archer's Cloak of Stamina"] = {
            ["H3254"] = 7146,
            ["mr"] = 7146,
        },
        ["Lord's Crest of the Bear"] = {
            ["H3254"] = 49488,
            ["mr"] = 49488,
        },
        ["Barbaric Battle Axe of the Wolf"] = {
            ["H3254"] = 3014,
            ["mr"] = 3014,
        },
        ["Barbed Club of Stamina"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Tribal Raptor Feathers"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Schematic: Catseye Ultra Goggles"] = {
            ["H3254"] = 5800,
            ["mr"] = 5800,
        },
        ["Target Dummy"] = {
            ["H3254"] = 2499,
            ["mr"] = 2499,
        },
        ["Blackwater Cutlass"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Overlord's Chestplate of the Whale"] = {
            ["H3254"] = 249528,
            ["mr"] = 249528,
        },
        ["Red Dragonscale"] = {
            ["H3254"] = 2144,
            ["mr"] = 2144,
        },
        ["Tiny Crimson Whelpling"] = {
            ["H3254"] = 260000,
            ["mr"] = 260000,
        },
        ["Pattern: Tough Scorpid Bracers"] = {
            ["H3254"] = 1500,
            ["mr"] = 1500,
        },
        ["Fortified Belt of the Gorilla"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Knight's Crest of Defense"] = {
            ["H3254"] = 15000,
            ["mr"] = 15000,
        },
        ["Speedsteel Rapier"] = {
            ["H3254"] = 140000,
            ["mr"] = 140000,
        },
        ["Arthas' Tears"] = {
            ["H3254"] = 299,
            ["mr"] = 299,
        },
        ["Swashbuckler's Shoulderpads of the Monkey"] = {
            ["H3254"] = 47997,
            ["mr"] = 47997,
        },
        ["Elegant Cloak of Fiery Wrath"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Harpy Needler of Agility"] = {
            ["H3254"] = 39900,
            ["mr"] = 39900,
        },
        ["Abjurer's Hood of Intellect"] = {
            ["H3254"] = 25000,
            ["mr"] = 25000,
        },
        ["Greenstone Circle of Strength"] = {
            ["H3254"] = 19800,
            ["mr"] = 19800,
        },
        ["Large Raw Mightfish"] = {
            ["H3254"] = 1464,
            ["mr"] = 1464,
        },
        ["Pattern: Green Leather Armor"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Plated Fist of Hakoo"] = {
            ["H3254"] = 90000,
            ["mr"] = 90000,
        },
        ["Greater Mystic Essence"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
        ["Mutton Chop"] = {
            ["H3254"] = 40,
            ["mr"] = 40,
        },
        ["Bard's Tunic of the Falcon"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["Formula: Enchant Boots - Greater Agility"] = {
            ["H3254"] = 85000,
            ["mr"] = 85000,
        },
        ["Tellurium Necklace of the Owl"] = {
            ["H3254"] = 14913,
            ["mr"] = 14913,
        },
        ["Drakewing Bands"] = {
            ["H3254"] = 23600,
            ["mr"] = 23600,
        },
        ["Blue Dragonscale"] = {
            ["H3254"] = 598,
            ["mr"] = 598,
        },
        ["Huntsman's Armor of the Owl"] = {
            ["H3254"] = 12213,
            ["mr"] = 12213,
        },
        ["Wool Cloth"] = {
            ["H3254"] = 83,
            ["mr"] = 83,
        },
        ["Superior Tunic of the Wolf"] = {
            ["H3254"] = 3800,
            ["mr"] = 3800,
        },
        ["Magus Long Staff of the Bear"] = {
            ["H3254"] = 120000,
            ["mr"] = 120000,
        },
        ["Brutal War Axe of the Wolf"] = {
            ["H3254"] = 7900,
            ["mr"] = 7900,
        },
        ["Emerald Legplates of the Tiger"] = {
            ["H3254"] = 99999,
            ["mr"] = 99999,
        },
        ["Bloodwoven Cloak of the Owl"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Plans: Heavy Mithril Helm"] = {
            ["H3254"] = 6000,
            ["mr"] = 6000,
        },
        ["Gypsy Buckler of the Bear"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Scaled Leather Tunic of Agility"] = {
            ["H3254"] = 13062,
            ["mr"] = 13062,
        },
        ["Twilight Gloves of the Falcon"] = {
            ["H3254"] = 6491,
            ["mr"] = 6491,
        },
        ["Elixir of Greater Defense"] = {
            ["H3254"] = 4900,
            ["mr"] = 4900,
        },
        ["Robe of the Magi"] = {
            ["H3254"] = 85364,
            ["mr"] = 85364,
        },
        ["Recipe: Ghost Dye"] = {
            ["H3254"] = 12699,
            ["mr"] = 12699,
        },
        ["Elixir of Giants"] = {
            ["H3254"] = 7100,
            ["mr"] = 7100,
        },
        ["Dalewind Trousers"] = {
            ["H3254"] = 74999,
            ["mr"] = 74999,
        },
        ["Glimmering Flamberge of Strength"] = {
            ["H3254"] = 10399,
            ["mr"] = 10399,
        },
        ["Recipe: Wildvine Potion"] = {
            ["H3254"] = 2221,
            ["mr"] = 2221,
        },
        ["Crusader's Armguards of the Falcon"] = {
            ["H3254"] = 47353,
            ["mr"] = 47353,
        },
        ["Pattern: Red Woolen Bag"] = {
            ["H3254"] = 1084,
            ["mr"] = 1084,
        },
        ["Superior Cloak of the Wolf"] = {
            ["H3254"] = 1100,
            ["mr"] = 1100,
        },
        ["Knight's Headguard of the Eagle"] = {
            ["H3254"] = 11577,
            ["mr"] = 11577,
        },
        ["Scarlet Gauntlets"] = {
            ["H3254"] = 6700,
            ["mr"] = 6700,
        },
        ["Northern Shortsword of Power"] = {
            ["H3254"] = 2000,
            ["mr"] = 2000,
        },
        ["High Councillor's Cloak of the Whale"] = {
            ["H3254"] = 42274,
            ["mr"] = 42274,
        },
        ["Bard's Belt of Defense"] = {
            ["H3254"] = 499,
            ["mr"] = 499,
        },
        ["Superior Healing Potion"] = {
            ["H3254"] = 875,
            ["mr"] = 875,
        },
        ["Major Healing Potion"] = {
            ["H3254"] = 6397,
            ["mr"] = 6397,
        },
        ["Scouting Belt of Defense"] = {
            ["H3254"] = 2539,
            ["mr"] = 2539,
        },
        ["Ricochet Blunderbuss of the Monkey"] = {
            ["H3254"] = 30076,
            ["mr"] = 30076,
        },
        ["Brutal War Axe of the Boar"] = {
            ["H3254"] = 7500,
            ["mr"] = 7500,
        },
        ["Renegade Belt of the Bear"] = {
            ["H3254"] = 8118,
            ["mr"] = 8118,
        },
        ["Brutal War Axe of Stamina"] = {
            ["H3254"] = 10223,
            ["mr"] = 10223,
        },
        ["Willow Branch of the Owl"] = {
            ["H3254"] = 2999,
            ["mr"] = 2999,
        },
        ["Lesser Mystic Wand"] = {
            ["H3254"] = 7000,
            ["mr"] = 7000,
        },
        ["Monk's Staff of the Owl"] = {
            ["H3254"] = 39594,
            ["mr"] = 39594,
        },
        ["Acrobatic Staff of Spirit"] = {
            ["H3254"] = 16500,
            ["mr"] = 16500,
        },
        ["Traveler's Cloak"] = {
            ["H3254"] = 19500,
            ["mr"] = 19500,
        },
        ["Raider's Legguards of Strength"] = {
            ["H3254"] = 1875,
            ["mr"] = 1875,
        },
        ["Brutish Riverpaw Axe"] = {
            ["H3254"] = 1600,
            ["mr"] = 1600,
        },
        ["Orb of Deception"] = {
            ["H3254"] = 2499900,
            ["mr"] = 2499900,
        },
        ["Ivycloth Pants of the Monkey"] = {
            ["H3254"] = 5000,
            ["mr"] = 5000,
        },
        ["Hematite Link of Arcane Resistance"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Glowstar Rod of Stamina"] = {
            ["H3254"] = 70516,
            ["mr"] = 70516,
        },
        ["Speckled Shell Fragment"] = {
            ["H3254"] = 2300,
            ["mr"] = 2300,
        },
        ["Regal Gloves of the Eagle"] = {
            ["H3254"] = 4910,
            ["mr"] = 4910,
        },
        ["Staff of Jordan"] = {
            ["H3254"] = 1300000,
            ["mr"] = 1300000,
        },
        ["Madwolf Bracers"] = {
            ["H3254"] = 4704,
            ["mr"] = 4704,
        },
        ["Revenant Gauntlets of the Bear"] = {
            ["H3254"] = 30000,
            ["mr"] = 30000,
        },
        ["Hematite Link of Nature Resistance"] = {
            ["H3254"] = 19652,
            ["mr"] = 19652,
        },
        ["Elder's Hat of the Whale"] = {
            ["H3254"] = 11012,
            ["mr"] = 11012,
        },
        ["Ornate Mithril Pants"] = {
            ["H3254"] = 31552,
            ["mr"] = 31552,
        },
        ["Precisely Calibrated Boomstick"] = {
            ["H3254"] = 999900,
            ["mr"] = 999900,
        },
        ["Green Hills of Stranglethorn - Page 6"] = {
            ["H3254"] = 595,
            ["mr"] = 595,
        },
        ["Bandit Pants of the Whale"] = {
            ["H3254"] = 2539,
            ["mr"] = 2539,
        },
        ["Elder's Robe of the Eagle"] = {
            ["H3254"] = 17400,
            ["mr"] = 17400,
        },
        ["Bard's Tunic of the Wolf"] = {
            ["H3254"] = 2086,
            ["mr"] = 2086,
        },
        ["Gnoll Punisher"] = {
            ["H3254"] = 1595,
            ["mr"] = 1595,
        },
        ["Lesser Mystic Essence"] = {
            ["H3254"] = 1659,
            ["mr"] = 1659,
        },
        ["Templar Crown of the Bear"] = {
            ["H3254"] = 49648,
            ["mr"] = 49648,
        },
        ["Ballast Maul of the Gorilla"] = {
            ["H3254"] = 19900,
            ["mr"] = 19900,
        },
        ["Sentinel Gloves of Spirit"] = {
            ["H3254"] = 2777,
            ["mr"] = 2777,
        },
        ["Battle Knife of Power"] = {
            ["H3254"] = 6930,
            ["mr"] = 6930,
        },
        ["Short Ash Bow"] = {
            ["H3254"] = 2500,
            ["mr"] = 2500,
        },
        ["Headstriker Sword of the Whale"] = {
            ["H3254"] = 29900,
            ["mr"] = 29900,
        },
        ["Nightscape Tunic"] = {
            ["H3254"] = 9900,
            ["mr"] = 9900,
        },
        ["Amethyst Band of Frost Resistance"] = {
            ["H3254"] = 11380,
            ["mr"] = 11380,
        },
        ["Plans: Gemmed Copper Gauntlets"] = {
            ["H3254"] = 100,
            ["mr"] = 100,
        },
        ["Serpentine Loop of Fire Resistance"] = {
            ["H3254"] = 199999,
            ["mr"] = 199999,
        },
        ["Short Bastard Sword of the Tiger"] = {
            ["H3254"] = 1000,
            ["mr"] = 1000,
        },
        ["Twilight Cuffs of the Owl"] = {
            ["H3254"] = 3365,
            ["mr"] = 3365,
        },
        ["Conjurer's Breeches of Spirit"] = {
            ["H3254"] = 42288,
            ["mr"] = 42288,
        },
        ["Heavy Shortbow"] = {
            ["H3254"] = 3500,
            ["mr"] = 3500,
        },
        ["Harpy Needler of the Whale"] = {
            ["H3254"] = 39899,
            ["mr"] = 39899,
        },
        ["Fortified Belt of the Monkey"] = {
            ["H3254"] = 10000,
            ["mr"] = 10000,
        },
        ["Pattern: Fine Leather Boots"] = {
            ["H3254"] = 800,
            ["mr"] = 800,
        },
        ["Recipe: Transmute Iron to Gold"] = {
            ["H3254"] = 19585,
            ["mr"] = 19585,
        },
        ["Elixir of Shadow Power"] = {
            ["H3254"] = 48500,
            ["mr"] = 48500,
        },
        ["Fighter Broadsword of the Tiger"] = {
            ["H3254"] = 8000,
            ["mr"] = 8000,
        },
        ["Disciple's Stein of the Eagle"] = {
            ["H3254"] = 3000,
            ["mr"] = 3000,
        },
    },
}
AUCTIONATOR_LAST_SCAN_TIME = 1570960417
AUCTIONATOR_TOONS = {
    ["Josephballin"] = {
        ["firstSeen"] = 1568045202,
        ["firstVersion"] = "8.1.0",
        ["guid"] = "Player-4706-01870A54",
    },
    ["Bonser"] = {
        ["firstSeen"] = 1570960126,
        ["guid"] = "Player-4742-014261C7",
        ["firstVersion"] = "8.1.0",
    },
}
AUCTIONATOR_STACKING_PREFS = {
    ["light feather"] = {
        ["stacksize"] = 1,
        ["numstacks"] = 0,
    },
    ["linen cloth"] = {
        ["stacksize"] = 1,
        ["numstacks"] = 0,
    },
    ["wool cloth"] = {
        ["stacksize"] = 1,
        ["numstacks"] = 0,
    },
    ["red firework"] = {
        ["numstacks"] = 0,
        ["stacksize"] = 5,
    },
}
AUCTIONATOR_SCAN_MINLEVEL = 1
AUCTIONATOR_DB_MAXITEM_AGE = 180
AUCTIONATOR_DB_MAXHIST_AGE = -1
AUCTIONATOR_DB_MAXHIST_DAYS = 5
AUCTIONATOR_FS_CHUNK = nil
AUCTIONATOR_DE_DATA = nil
AUCTIONATOR_DE_DATA_BAK = nil
ITEM_ID_VERSION = "3.2.6"
AUCTIONATOR_SHOW_MAILBOX_TIPS = 1
