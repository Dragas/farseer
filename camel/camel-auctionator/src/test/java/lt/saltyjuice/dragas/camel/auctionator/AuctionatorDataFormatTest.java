package lt.saltyjuice.dragas.camel.auctionator;

import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.NotifyBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit5.CamelTestSupport;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import static lt.saltyjuice.dragas.camel.auctionator.LoadingUtility.loadClasspathFile;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuctionatorDataFormatTest extends CamelTestSupport {

    @Test
    public void dataFormatIsConfigured() throws ExecutionException, InterruptedException {
        String data = loadClasspathFile("array.lua");
        NotifyBuilder b = new NotifyBuilder(context).from("direct:test").whenDone(1).create();
        Map<String, Object> result = (Map<String, Object>) template.asyncSendBody("direct:test", data).get();
        assertTrue(b.matches());

    }

    @Override
    protected RoutesBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:test")
                        .unmarshal("auctionator_lua");
            }
        };
    }
}
