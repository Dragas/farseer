package lt.saltyjuice.dragas.camel.auctionator;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static lt.saltyjuice.dragas.camel.auctionator.LoadingUtility.parse;
import static lt.saltyjuice.dragas.camel.auctionator.LoadingUtility.parseClasspathScript;
import static org.junit.jupiter.api.Assertions.*;

public class AuctionatorParserTest {
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void parsesNullLiteral() throws IOException {
        Map<String, Object> parsed = parse("NULLABLE = nil\n");
        assertTrue(parsed.containsKey("NULLABLE"), "Key  NULLABLE must be present");
        assertNull(parsed.get("NULLABLE"), "Key NULLABLE is expected to be null");
    }

    @Test
    public void parsesTrueLiteral() throws IOException {
        Map<String, Object> parsed = parse("TRUTHY = true\n");
        assertTrue((boolean) parsed.get("TRUTHY"), "Key TRUTHY is expected to be true");
    }

    @Test
    public void parsesFalseLiteral() throws IOException {
        Map<String, Object> parsed = parse("FALSY = false\n");
        assertFalse((boolean) parsed.get("FALSY"), "Key FALSY is expected to be false");
    }

    @Test
    public void parsesIntegerLiteral() throws IOException {
        Map<String, Object> parsed = parse("INTEGERABLE = 248557\n");
        assertEquals(248557L, (long) parsed.get("INTEGERABLE"), "Key INTEGERABLE is expected to be 248557");
    }

    @Test
    public void parsesNegativeIntegerLiteral() throws IOException {
        Map<String, Object> parsed = parse("INTEGERABLE = -248557\n");
        assertEquals(-248557L, (long) parsed.get("INTEGERABLE"), "Key INTEGERABLE is expected to be -248557");
    }

    @Test
    public void parsesStringLiteral() throws IOException {
        Map<String, Object> parsed = parse("Stringable = \"248557\"\n");
        assertEquals((String) parsed.get("Stringable"), "248557", "Key Stringable is expected to be \"248557\"");
    }

    @Test
    public void parsesSimpleObjectLiteral() throws IOException {
        Map<String, Object> parsed = parseClasspathScript("simple_object.lua");
        assertTrue(parsed.containsKey("AUCTIONATOR_SAVEDVARS"), "Contains key AUCTIONATOR_SAVEDVARS");
        Map<String, Object> savedvars = (Map<String, Object>) parsed.get("AUCTIONATOR_SAVEDVARS");
        Pair[] pairs = new Pair[]{
                new Pair("_50000", 500L),
                new Pair("_2000", 100L),
                new Pair("STARTING_DISCOUNT", 5L),
                new Pair("LOG_DE_DATA_X", true),
                new Pair("_1000000", 2500L),
                new Pair("_5000000", 10000L),
                new Pair("_500", 5L),
                new Pair("_10000", 200L),
                new Pair("_200000", 1000L)
        };
        for (Pair p : pairs) {
            assertTrue(savedvars.containsKey(p.getKey()), String.format("Must contain key %s", p.getKey()));
            assertEquals(p.getValue(), savedvars.get(p.getKey()));
        }
    }

    @Test
    public void parsesMultipleSimpleObjectLiterals() throws IOException {
        Map<String, Object> parsed = parseClasspathScript("multiple_simple_objects.lua");
        assertTrue(parsed.containsKey("AUCTIONATOR_SAVEDVARS"), "Contains key AUCTIONATOR_SAVEDVARS");
        assertTrue(parsed.containsKey("AUCTIONATOR_SAVEDVAR2"), "Contains key AUCTIONATOR_SAVEDVAR2");
        Map<String, Object> savedvars = (Map<String, Object>) parsed.get("AUCTIONATOR_SAVEDVARS");
        assertEquals(9, savedvars.size());
        Map<String, Object> savedvars2 = (Map<String, Object>) parsed.get("AUCTIONATOR_SAVEDVAR2");
        assertEquals(9, savedvars2.size());
    }

    @Test
    public void parsesArrayLiteral() throws IOException {
        Map<String, Object> parsed = parseClasspathScript("array.lua");
        assertTrue(parsed.containsKey("AUCTIONATOR_SHOPPING_LISTS"), "Contains key AUCTIONATOR_SHOPPING_LISTS");
        parsed = (Map<String, Object>) parsed.get("AUCTIONATOR_SHOPPING_LISTS");
        assertEquals(2, parsed.size());
        assertTrue(parsed.containsKey("0"));
        assertTrue(parsed.containsKey("1"));
        parsed = (Map<String, Object>) parsed.get("0");
        assertTrue(parsed.containsKey("items"));
        assertTrue(parsed.containsKey("isRecents"));
        assertTrue(parsed.containsKey("name"));
        assertEquals(45, ((Map<String, Object>) parsed.get("items")).size());
    }

    @Test
    public void parsesReallyBigFile() throws IOException {
        // idk if its that big but 29k lines
        // of lua is a lot
        Map<String, Object> parsed = parseClasspathScript("Auctionator.lua");
        assertEquals(17, parsed.size());
        assertTrue(parsed.containsKey("AUCTIONATOR_SAVEDVARS"));
        assertTrue(parsed.containsKey("AUCTIONATOR_PRICING_HISTORY"));
        assertTrue(parsed.containsKey("AUCTIONATOR_SHOPPING_LISTS"));
        assertTrue(parsed.containsKey("AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2"));
        assertTrue(parsed.containsKey("AUCTIONATOR_PRICE_DATABASE"));
        assertTrue(parsed.containsKey("AUCTIONATOR_LAST_SCAN_TIME"));
        assertTrue(parsed.containsKey("AUCTIONATOR_TOONS"));
        assertTrue(parsed.containsKey("AUCTIONATOR_STACKING_PREFS"));
        assertTrue(parsed.containsKey("AUCTIONATOR_SCAN_MINLEVEL"));
        assertTrue(parsed.containsKey("AUCTIONATOR_DB_MAXITEM_AGE"));
        assertTrue(parsed.containsKey("AUCTIONATOR_DB_MAXHIST_AGE"));
        assertTrue(parsed.containsKey("AUCTIONATOR_DB_MAXHIST_DAYS"));
        assertTrue(parsed.containsKey("AUCTIONATOR_FS_CHUNK"));
        assertTrue(parsed.containsKey("AUCTIONATOR_DE_DATA"));
        assertTrue(parsed.containsKey("AUCTIONATOR_DE_DATA_BAK"));
        assertTrue(parsed.containsKey("ITEM_ID_VERSION"));
        assertTrue(parsed.containsKey("AUCTIONATOR_SHOW_MAILBOX_TIPS"));
        assertEquals(1L, parsed.get("AUCTIONATOR_SCAN_MINLEVEL"));
        assertEquals(180L, parsed.get("AUCTIONATOR_DB_MAXITEM_AGE"));
        assertEquals(-1L, parsed.get("AUCTIONATOR_DB_MAXHIST_AGE"));
        assertEquals(5L, parsed.get("AUCTIONATOR_DB_MAXHIST_DAYS"));
        assertNull(parsed.get("AUCTIONATOR_FS_CHUNK"));
        assertNull(parsed.get("AUCTIONATOR_DE_DATA"));
        assertNull(parsed.get("AUCTIONATOR_DE_DATA_BAK"));
        assertEquals("3.2.6", parsed.get("ITEM_ID_VERSION"));
        assertEquals(1L, parsed.get("AUCTIONATOR_SHOW_MAILBOX_TIPS"));
        assertTrue((boolean) parsed.get("AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2"));
        assertEquals(1570960417L, parsed.get("AUCTIONATOR_LAST_SCAN_TIME"));
        assertTrue(parsed.get("AUCTIONATOR_SAVEDVARS") instanceof Map);
        assertTrue(parsed.get("AUCTIONATOR_PRICING_HISTORY") instanceof Map);
        assertTrue(parsed.get("AUCTIONATOR_SHOPPING_LISTS") instanceof Map);
        assertTrue(parsed.get("AUCTIONATOR_PRICE_DATABASE") instanceof Map);
        assertTrue(parsed.get("AUCTIONATOR_TOONS") instanceof Map);
        assertTrue(parsed.get("AUCTIONATOR_STACKING_PREFS") instanceof Map);
    }

    private static class Pair {
        private final String key;
        private final Object value;

        private Pair(String key, Object value) {
            this.key = key;
            this.value = value;
        }


        public String getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }
    }
}
