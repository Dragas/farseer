package lt.saltyjuice.dragas.camel.auctionator;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;

public class LoadingUtility {

    public static String loadClasspathFile(String filename) {
        InputStream ris = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
        Scanner clin = new Scanner(ris);
        StringBuilder sb = new StringBuilder();
        while (clin.hasNextLine()) {
            sb.append(clin.nextLine());
            sb.append("\n");
        }
        return sb.toString();
    }

    public static Map<String, Object> parseClasspathScript(String filename) throws IOException {
        // ResourceInputStream
        InputStream ris = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
        return parse(ris);
    }

    public static Map<String, Object> parse(InputStream is) throws IOException {
        AuctionatorParser parser = new AuctionatorParser(is);
        return parser.parse();
    }

    public static Map<String, Object> parse(String skript) throws IOException {
        return parse(new ByteArrayInputStream(skript.getBytes()));
    }
}
