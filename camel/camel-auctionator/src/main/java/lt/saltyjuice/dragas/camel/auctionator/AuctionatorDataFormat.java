package lt.saltyjuice.dragas.camel.auctionator;

import org.apache.camel.Exchange;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spi.DataFormatName;
import org.apache.camel.spi.annotations.Dataformat;
import org.apache.camel.support.service.ServiceSupport;

import java.io.InputStream;
import java.io.OutputStream;

@Dataformat("auctionator_lua")
public class AuctionatorDataFormat extends ServiceSupport implements DataFormat, DataFormatName {


    @Override
    public void marshal(Exchange exchange, Object graph, OutputStream stream) throws Exception {
        throw new AuctionatorParsingException("Unsupported operation");
    }

    @Override
    public Object unmarshal(Exchange exchange, InputStream stream) throws Exception {
        AuctionatorParser parser = new AuctionatorParser(stream);
        return parser.parse();
    }

    @Override
    public String getDataFormatName() {
        return "auctionator_lua";
    }

    @Override
    protected void doStart() throws Exception {

    }

    @Override
    protected void doStop() throws Exception {

    }
}
