package lt.saltyjuice.dragas.camel.auctionator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class AuctionatorParser {
    private static final int ASSIGNMENT = '=';
    private static final int START_OF_OBJECT = '{';
    private static final int END_OF_OBJECT = '}';
    private static final int STRING_WRAPPER = '"';
    private static final int START_OF_FIELD = '[';
    private static final int END_OF_FIELD = ']';
    private static final int LINE_SEPARATOR = '\n';
    // dumb ass windows exception
    private static final int CARRIAGE_RETURN = '\r';
    private static final int END_OF_VALUE = ',';
    private static final int ESCAPE = '\\';
    private static final int EOF = -1;
    private static final int SPACE = ' ';
    private static final int[] WHITESPACE = new int[]{SPACE, '\t', LINE_SEPARATOR, CARRIAGE_RETURN};
    private static final int[] NUMBERS = new int[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private static final int[] END_OF_STATEMENT = new int[]{CARRIAGE_RETURN, LINE_SEPARATOR, END_OF_VALUE};
    private static final int DASH = '-';
    private static final int[] NAME = new int[]{
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '_'
    };

    static {
        Arrays.sort(WHITESPACE);
        Arrays.sort(NUMBERS);
        Arrays.sort(NAME);
        Arrays.sort(END_OF_STATEMENT);
    }

    private final InputStream is;
    private int line = 1;
    private int column = 0;

    public AuctionatorParser(InputStream is) {
        this.is = is;
    }

    public Map<String, Object> parse() throws IOException {
        Map<String, Object> parsed = new HashMap<>();
        // lua tables are just fancy arrays so if key is not present you might as well just kys
        int arrayIdx = 0;
        while (true) {
            int character = read();
            if (character == START_OF_FIELD) {
                Map<String, Object> result = parseField();
                parsed.putAll(result);
            } else if (Arrays.binarySearch(WHITESPACE, character) >= 0)
                continue;
            else if (Arrays.binarySearch(END_OF_STATEMENT, character) >= 0)
                continue;
            else if (character == END_OF_OBJECT)
                break;
            else if (character == EOF)
                break;
            else if (Arrays.binarySearch(NAME, character) >= 0) {
                Map<String, Object> result = parseLiteralField(character);
                parsed.putAll(result);
            } else {
                Object value = parseValue(character);
                if (value != this) {
                    parsed.put(String.valueOf(arrayIdx), value);
                    arrayIdx++;
                }
            }
        }

        return parsed;
    }

    private Map<String, Object> parseLiteralField(int character) throws IOException {
        Map<String, Object> parsed = new HashMap<>();
        // field literal can only be in NAME + NUMBERS
        // range, so I cannot reuse the parse field function
        StringBuilder sb = new StringBuilder();
        sb.append((char) character);
        while (true) {
            character = is.read();
            if (Arrays.binarySearch(NAME, character) >= 0 || sb.length() != 0 && Arrays.binarySearch(NUMBERS, character) >= 0)
                sb.append((char) character);
            else if (character == SPACE) {
                expect(ASSIGNMENT);
                expect(SPACE);
                Object value = parseValue();
                parsed.put(sb.toString(), value);
                break;
            } else
                throwUnexpectedSymbolException(NAME, character);
        }
        return parsed;
    }

    private String parseString() throws IOException {
        StringBuilder sb = new StringBuilder();
        while (true) {
            int character = read();
            if (character == ESCAPE)
                sb.append((char) read());
            else if (character == STRING_WRAPPER)
                break;
            sb.append((char) character);
        }
        return sb.toString();
    }

    private Map<String, Object> parseField() throws IOException {
        Map<String, Object> parsed = new HashMap<>();
        // it is expected that next character will be string wrapper
        expect(STRING_WRAPPER);

        String key = parseString();
        // it is expected that next character combination will be "] = "
        expect(END_OF_FIELD);
        expect(SPACE);
        expect(ASSIGNMENT);
        expect(SPACE);

        // field can only contain one of the three values:
        // object
        // String literal
        // integer literal
        // nil
        // boolean literal
        // here it is safe to read up to next ,\n combo
        // unless its string literal

        Object value = parseValue();
        parsed.put(key, value);
        return parsed;
    }

    private Object parseValue(int character) throws IOException {
        if (character == START_OF_OBJECT)
            return parse();
        else if (character == STRING_WRAPPER)
            return parseString();
        else if (character == 't')
            return parseTrue();
        else if (character == 'f')
            return parseFalse();
        else if (character == 'n')
            return parseNil();
        else if (character == DASH) {
            character = read();
            if (character != DASH)
                return -parseIntegerLiteral(character);
            else {
                // gotta skip until new line
                // because it's a comment
                do {
                    character = read();
                } while (character != LINE_SEPARATOR);
                return this; // an awful fucking hack to prevent comments being added into the parsed map
            }
        }
        return parseIntegerLiteral(character);
    }

    private Object parseValue() throws IOException {
        return parseValue(read());
    }

    private boolean parseTrue() throws IOException {
        // since first symbol is already read, i only
        // need to expect rue in that order
        expect('r');
        expect('u');
        expect('e');
        // now all i need is a comma or new line
        expectEndOfStatement();
        return true;
    }

    private boolean parseFalse() throws IOException {
        //see parseTrue()
        expect('a');
        expect('l');
        expect('s');
        expect('e');
        expectEndOfStatement();
        return false;
    }

    /**
     * Null doesn't have a type, so method's signature is Object()
     *
     * @return null, when the input stream contains "il" in that order
     */
    private Object parseNil() throws IOException {
        expect('i');
        expect('l');
        expectEndOfStatement();
        return null;
    }

    /**
     * Reads an integer literal as is in the source. Does not support non decimal integer format.
     *
     * @param character starting character
     * @return an integer from the file
     * @throws IOException         when there's something wrong with the stream
     * @throws AuctionatorParsingException when there's an unexpected symbol in the stream
     */
    private long parseIntegerLiteral(int character) throws IOException {
        StringBuilder result = new StringBuilder();
        result.append((char) character);
        while (true) {
            // integers can be any length as far as i remember
            // as a result it's reasonable to append anything
            // i read into sb as long as it's either a number
            // or a dash, in latter case only when a number
            // starts with it
            int readed = read();
            if ((result.length() == 0 || readed != DASH) && Arrays.binarySearch(NUMBERS, readed) >= 0)
                // must cast to character here, otherwise
                // integer string literal will be appended
                // instead
                result.append((char) readed);
                // still might be an end of statement
            else if (Arrays.binarySearch(END_OF_STATEMENT, readed) >= 0)
                break;
            else
                throwUnexpectedSymbolException(NUMBERS, readed); // tbh it makes sense to only throw a number related error

        }
        return Long.valueOf(result.toString());
    }

    /**
     * Reads a character from input stream and advances the necessary statistics.
     *
     * @return the character that was read
     * @throws IOException when there's something with input stream
     */
    private int read() throws IOException {
        int character = is.read();
        if (character == LINE_SEPARATOR || character == CARRIAGE_RETURN) {
            line++;
            column = 0;
        } else {
            column++;
        }
        return character;
    }

    /**
     * Naive implementation of what an "end of statement" should be.
     * <p>
     * When declaring objects, end of statement might be new line or comma followed with new line,
     * meanwhile when declaring literal variables, end of statement must be only new line.
     * I guess it should peek forward to check if it's an end of object or another field declaration
     *
     * @throws IOException         when there's something wrong with input stream
     * @throws AuctionatorParsingException when there's an unexpected symbol in the stream
     */
    private void expectEndOfStatement() throws IOException {
        expect(END_OF_STATEMENT);
    }

    /**
     * Checks if the next character in input stream is one of those characters. Returns that character
     *
     * @param expected the array of expected characters
     * @throws IOException         when something is wrong with input
     * @throws AuctionatorParsingException when an unexpected character is read
     */
    private int expect(int[] expected) throws IOException {
        int readed = read();
        int[] cloned = Arrays.copyOf(expected, expected.length);
        Arrays.sort(cloned);
        if (Arrays.binarySearch(cloned, readed) < 0)
            throwUnexpectedSymbolException(expected, readed);
        return readed;
    }

    /**
     * Reads the character in input stream and checks if its the expected character. Returns that character.
     *
     * @param character the expected character
     * @return the provided character
     * @throws AuctionatorParsingException when characters dont match
     * @throws IOException         when something is wrong with input
     */
    private int expect(int character) throws IOException {
        return expect(new int[]{character});
    }

    /**
     * Helper method in case i need to throw this in more than one context
     *
     * @param expected the expected symbol
     * @param got      the symbol that was actually read
     * @throws AuctionatorParsingException always
     */
    private void throwUnexpectedSymbolException(int expected, int got) throws AuctionatorParsingException {
        throwUnexpectedSymbolException(new int[]{expected}, got);
    }

    private void throwUnexpectedSymbolException(int[] expected, int got) throws AuctionatorParsingException {
        throwUnexpectedSymbolException(
                Arrays
                        .stream(expected)
                        .boxed()
                        .map((it) -> String.format("%1$c (0x%1$x)", it))
                        .collect(Collectors.joining(", ", "Expected one of [", "]")),
                got
        );
    }

    private void throwUnexpectedSymbolException(String message, int got) throws AuctionatorParsingException {
        throw new AuctionatorParsingException(String.format("Malformed lua file. %1$s, got %2$c (0x%2$x) at line %3$s column %4$s", message, got, line, column));
    }
}
