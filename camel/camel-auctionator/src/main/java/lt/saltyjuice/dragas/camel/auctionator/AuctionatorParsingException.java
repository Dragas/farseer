package lt.saltyjuice.dragas.camel.auctionator;

import java.io.IOException;

public class AuctionatorParsingException extends IOException {

    public AuctionatorParsingException() {
        super();
    }

    public AuctionatorParsingException(String message) {
        super(message);
    }

    public AuctionatorParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuctionatorParsingException(Throwable cause) {
        super(cause);
    }
}
