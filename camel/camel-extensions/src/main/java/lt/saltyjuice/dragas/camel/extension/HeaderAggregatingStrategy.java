package lt.saltyjuice.dragas.camel.extension;

import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;

public class HeaderAggregatingStrategy implements AggregationStrategy {
    private final String headerName;

    public HeaderAggregatingStrategy(String headerName) {
        this.headerName = headerName;
    }

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        oldExchange.getIn().setHeader(headerName, newExchange.getIn().getBody());
        return oldExchange;
    }
}
