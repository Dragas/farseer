package lt.saltyjuice.dragas.camel.extension;

import org.apache.camel.AggregationStrategy;
import org.apache.camel.builder.RouteBuilder;

public abstract class ExtendedRouteBuilder extends RouteBuilder {

    public AggregationStrategy asHeader(String headerName) {
        return new HeaderAggregatingStrategy(headerName);
    }
}
