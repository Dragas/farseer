package lt.saltyjuice.dragas.camel.jsoup;

import org.apache.camel.Exchange;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spi.DataFormatName;
import org.apache.camel.spi.annotations.Dataformat;
import org.apache.camel.support.service.ServiceSupport;
import org.jsoup.Jsoup;

import java.io.InputStream;
import java.io.OutputStream;

@Dataformat("jsoup")
public class JsoupDataFormat extends ServiceSupport implements DataFormat, DataFormatName {

    @Override
    public void marshal(Exchange exchange, Object graph, OutputStream stream) throws Exception {
        throw new JsoupMarshallingException("I'm not sure what is supposed to happen here");
    }

    @Override
    public Object unmarshal(Exchange exchange, InputStream stream) throws Exception {
        return Jsoup.parse(stream, null, "");
    }

    @Override
    public String getDataFormatName() {
        return "jsoup";
    }

    @Override
    protected void doStart() throws Exception {

    }

    @Override
    protected void doStop() throws Exception {

    }
}
