package lt.saltyjuice.dragas.camel.jsoup;

public class JsoupMarshallingException extends Exception {

    public JsoupMarshallingException() {
        super();
    }

    public JsoupMarshallingException(String message) {
        super(message);
    }

    public JsoupMarshallingException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsoupMarshallingException(Throwable cause) {
        super(cause);
    }

    protected JsoupMarshallingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
