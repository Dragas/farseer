package lt.saltyjuice.dragas.camel.jsoup;

import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.NotifyBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit5.CamelTestSupport;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class JsoupDataFormatTest extends CamelTestSupport {

    @Test
    public void dataFormatIsConfigured() throws ExecutionException, InterruptedException {
        String data = "<html><body><div>memes</div></body></html>";
        NotifyBuilder b = new NotifyBuilder(context).from("direct:test").whenDone(1).create();
        Document result = (Document) template.asyncSendBody("direct:test", data).get();
        assertTrue(b.matches());

    }

    @Override
    protected RoutesBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:test")
                        .unmarshal("jsoup");
            }
        };
    }
}
