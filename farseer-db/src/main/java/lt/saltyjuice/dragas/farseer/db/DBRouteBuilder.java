package lt.saltyjuice.dragas.farseer.db;

import lt.saltyjuice.dragas.camel.extension.ExtendedRouteBuilder;

public class DBRouteBuilder extends ExtendedRouteBuilder {

    public static final String SEDA_GET_ALL_SERVERS = "seda:get_all_servers";
    public static final String SEDA_GET_ITEM_ID = "seda:get_item_id";
    public static final String FARSEER_DB_BACKUP = "FarseerDb.BACKUP";

    @Override
    public void configure() throws Exception {
        from(SEDA_GET_ALL_SERVERS)
                .to("sql:classpath:sql/select_servers.sql?dataSource=#psqlds");

        from(SEDA_GET_ITEM_ID)
                .setBody(simple("${body.key}"))
                .setHeader(FARSEER_DB_BACKUP, body())
                .to("sql:classpath:sql/select_item_id_query.sql?dataSource=#psqlds")
                .choice().when(method(DatabaseUtilities.class, "isEmptyResultSet"))
                .bean(DatabaseUtilities.class, "extractName")
                .to("sql:classpath:sql/select_item_id_by_likeness.sql?dataSource=#psqlds")
                .endChoice();
    }
}
