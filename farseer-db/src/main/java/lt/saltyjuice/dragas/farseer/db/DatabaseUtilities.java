package lt.saltyjuice.dragas.farseer.db;

import org.apache.camel.Header;

import java.util.List;

public class DatabaseUtilities {

    public String extractName(@Header(DBRouteBuilder.FARSEER_DB_BACKUP) String originalName) {
        int index = originalName.indexOf(" of ");
        if(index != -1)
            return originalName.substring(0, index + 1);
        return originalName;
    }

    public boolean isEmptyResultSet(List result) {
        return result.isEmpty();
    }
}
