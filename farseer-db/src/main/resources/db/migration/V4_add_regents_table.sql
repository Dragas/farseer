create table regents
(
  item_id  integer not null
    constraint regents_items_id_fk
      references items
      on update restrict on delete cascade,
  spell_id integer not null
    constraint regents_spells_id_fk
      references spells
      on update restrict on delete cascade,
  count    integer not null,
  constraint regents_pk
    unique (item_id, spell_id)
);
