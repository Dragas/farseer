drop function get_price_comparison_for_item(item_id integer, side wow_side, server varchar, required_depth integer);
create or replace function
  get_price_comparison_for_item(in item_id integer, in side wow_side, in server varchar(255),
                                in required_depth integer = 0)
  returns TABLE
          (
            name  varchar(255),
            price integer,
            depth integer,
            id    integer
          ) as
$$
begin
  for i in 0..$4
    loop
      return query select (select items.name from items where items.id = $1), sum(explanation.price)::int, i, $1
                   from explain_depth_for_item($1, $2, $3, i) as explanation;
    end loop;
end;
$$ language plpgsql;


