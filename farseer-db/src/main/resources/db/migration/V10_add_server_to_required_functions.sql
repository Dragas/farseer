create or replace function
  get_price_comparison_for_item(in item_id integer, in side wow_side, in server varchar(255),
                                in required_depth integer = 0)
  returns TABLE
          (
            name  varchar(255),
            price integer,
            depth integer
          ) as
$$
begin
  for i in 0..$4
    loop
      return query select (select items.name from items where items.id = $1), sum(explanation.price)::int, i
                   from explain_depth_for_item($1, $2, $3, i) as explanation;
    end loop;
end;
$$ language plpgsql;


create or replace function
  explain_depth_for_item(in item_id integer, in side wow_side, in server varchar(255), in depth integer DEFAULT 0)
  returns TABLE
          (
            name   character varying,
            amount integer,
            price  integer,
            id     integer
          )
  language plpgsql
as
$$
declare
  id    integer := 0;
  count integer := 0;
begin
  if ($4 > 0 and (select count(*) > 0 from spells where creates_item_id = $1)) then
    for id, count in select regents.item_id, regents.count
                     from regents
                            inner join spells on regents.spell_id = spells.id
                     where spells.creates_item_id = $1
      loop
        return query select res.name, res.amount * count, res.price * count, res.id
                     from explain_depth_for_item(id, $2, $3, $4 - 1) as res;
      end loop;
  else
    return query select items.name,
                        1,
                        (select prices.price
                         from prices
                         where prices.item_id = $1
                           and prices.side = $2
                           and prices.server = $3
                         order by prices.date desc
                         limit 1),
                        $1
                 from items
                 where items.id = $1;
  end if;
end;
$$;
