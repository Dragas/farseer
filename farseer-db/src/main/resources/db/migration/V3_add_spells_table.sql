create table spells
(
  id              integer not null
    constraint spells_pk
      primary key,
  name            varchar(255),
  creates_item_id integer
    constraint spells_items_id_fk
      references items
      on update restrict on delete cascade
);

create unique index spells_id_uindex
  on spells (id);

create index spells_name_uindex
  on spells (name);
