create table prices
(
  item_id     integer                             not null
    constraint prices_items_id_fk
      references items on delete cascade,
  date        integer                             not null,
  price       integer                             not null,
  server      varchar(255)                        not null,
  side        wow_side                            not null,
  imported_at timestamp default CURRENT_TIMESTAMP not null
);

create index prices_date_index
  on prices (date);

create index prices_item_id_index
  on prices (item_id);

