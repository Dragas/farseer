create or replace function
  get_price_comparison_for_item(in item_id integer, in side wow_side, in required_depth integer = 0)
  returns TABLE
          (
            name  varchar(255),
            price integer,
            depth integer
          ) as
$$
begin
  for i in 0..$3
    loop
      return query select (select items.name from items where items.id = $1), sum(explanation.price)::int, i
                   from explain_depth_for_item($1, $2, i) as explanation;
    end loop;
end;
$$ language plpgsql;
