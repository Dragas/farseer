create or replace function get_all_servers() returns table
                                                     (
                                                       server_name varchar(255)
                                                     ) as
$$
begin
  return query select distinct server from prices;
end;
$$ language plpgsql;
