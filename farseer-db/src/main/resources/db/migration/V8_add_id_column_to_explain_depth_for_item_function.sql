DROP FUNCTION explain_depth_for_item(integer, wow_side, integer);
create or replace function
  explain_depth_for_item(in item_id integer, in side wow_side, in depth integer DEFAULT 0)
  returns TABLE
          (
            name   character varying,
            amount integer,
            price  integer,
            id     integer
          )
  language plpgsql
as
$$
declare
  id    integer := 0;
  count integer := 0;
begin
  if ($3 > 0 and (select count(*) > 0 from spells where creates_item_id = $1)) then
    for id, count in select regents.item_id, regents.count
                     from regents
                            inner join spells on regents.spell_id = spells.id
                     where spells.creates_item_id = $1
      loop
        return query select res.name, res.amount * count, res.price * count, res.id
                     from explain_depth_for_item(id, $2, $3 - 1) as res;
      end loop;
  else
    return query select items.name,
                        1,
                        (select prices.price
                         from prices
                         where prices.item_id = $1
                           and prices.side = $2
                         order by prices.date desc
                         limit 1),
                        $1
                 from items
                 where items.id = $1;
  end if;
end;
$$;
