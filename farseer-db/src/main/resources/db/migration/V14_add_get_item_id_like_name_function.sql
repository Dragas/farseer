create or replace function get_item_id_like_name(in item_name varchar(255), in amount int default 5)
  returns table
          (
            id   int,
            name varchar(255)
          )
as
$$
return query select items.id, items.name
             from items
             where name like '%' || item_name
             limit amount;
$$ language plpgsql;
