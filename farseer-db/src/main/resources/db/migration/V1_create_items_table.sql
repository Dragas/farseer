create table items
(
  id   integer not null
    constraint items_pk
      primary key,
  name varchar(255)
);

create unique index items_id_uindex
  on items (id);

create index items_name_uindex
  on items (name);
