package lt.saltyjuice.dragas.farseer.auctionator;

public class AuctionatorConstants {
    public static final String AUCTIONATOR_OWNER_ID = "Auctionator.OWNER_ID";
    public static final String AUCTIONATOR_SCANNED_AT = "Auctionator.SCANNED_AT";
    public static final String AUCTIONATOR_SERVER = "Auctionator.SERVER";
    public static final String AUCTIONATOR_FACTION = "Auctionator.FACTION";
    public static final String AUCTIONATOR_ITEM_ID = "Auctionator.ITEM_ID";
    public static final String AUCTIONATOR_ITEM_NAME = "Auctionator.ITEM_NAME";
}
