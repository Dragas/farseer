package lt.saltyjuice.dragas.farseer.auctionator;

public class PricePlaceholder {
    private long mr;
    private String server;
    private String faction;
    private int itemId;
    private long date;
    private String guid;

    public long getMr() {
        return mr;
    }

    public void setMr(long mr) {
        this.mr = mr;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}

