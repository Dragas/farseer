package lt.saltyjuice.dragas.farseer.auctionator;

import org.apache.camel.Body;
import org.apache.camel.Headers;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static lt.saltyjuice.dragas.farseer.auctionator.AuctionatorConstants.*;

public class AuctionatorUtilities {
    private static final String AUCTIONATOR_SAVEDVARS = "AUCTIONATOR_SAVEDVARS";
    private static final String AUCTIONATOR_PRICING_HISTORY = "AUCTIONATOR_PRICING_HISTORY";
    private static final String AUCTIONATOR_SHOPPING_LISTS = "AUCTIONATOR_SHOPPING_LISTS";
    private static final String AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2 = "AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2";
    private static final String AUCTIONATOR_PRICE_DATABASE = "AUCTIONATOR_PRICE_DATABASE";
    private static final String AUCTIONATOR_LAST_SCAN_TIME = "AUCTIONATOR_LAST_SCAN_TIME";
    private static final String AUCTIONATOR_TOONS = "AUCTIONATOR_TOONS";
    private static final String AUCTIONATOR_STACKING_PREFS = "AUCTIONATOR_STACKING_PREFS";
    private static final String AUCTIONATOR_SCAN_MINLEVEL = "AUCTIONATOR_SCAN_MINLEVEL";
    private static final String AUCTIONATOR_DB_MAXITEM_AGE = "AUCTIONATOR_DB_MAXITEM_AGE";
    private static final String AUCTIONATOR_DB_MAXHIST_AGE = "AUCTIONATOR_DB_MAXHIST_AGE";
    private static final String AUCTIONATOR_DB_MAXHIST_DAYS = "AUCTIONATOR_DB_MAXHIST_DAYS";
    private static final String AUCTIONATOR_FS_CHUNK = "AUCTIONATOR_FS_CHUNK";
    private static final String AUCTIONATOR_DE_DATA = "AUCTIONATOR_DE_DATA";
    private static final String AUCTIONATOR_DE_DATA_BAK = "AUCTIONATOR_DE_DATA_BAK";
    private static final String ITEM_ID_VERSION = "ITEM_ID_VERSION";
    private static final String AUCTIONATOR_SHOW_MAILBOX_TIPS = "AUCTIONATOR_SHOW_MAILBOX_TIPS";

    public String selectLowestIdFromToons(Map<String, Object> body) {
        // fuck everything if this doesn't exist
        Map<String, Map<String, Object>> toons = (Map<String, Map<String, Object>>) body.get(AUCTIONATOR_TOONS);
        return toons.values().stream().map((it) -> (String) it.get("guid")).min(Comparator.comparing((it) -> it)).get();
    }

    public long selectScannedAt(Map<String, Object> body) {
        return (long) body.get(AUCTIONATOR_LAST_SCAN_TIME);
    }

    public Object unsetDBVersion(Map<String, Object> body) {
        body.remove("__dbversion");
        return body;
    }

    public String getServer(Map.Entry<String, Object> body) {
        return body.getKey().split("_")[0];
    }

    public String getFaction(Map.Entry<String, Object> body) {
        return body.getKey().split("_")[1].toLowerCase();
    }

    public PricePlaceholder createPricePlaceholder(@Body Map.Entry<String, Map<String, Object>> body, @Headers Map<String, Object> headers) {
        PricePlaceholder p = new PricePlaceholder();
        p.setGuid((String) headers.get(AUCTIONATOR_OWNER_ID));
        p.setDate((Long) headers.get(AUCTIONATOR_SCANNED_AT));
        p.setMr((Long) body.getValue().get("mr"));
        p.setServer((String) headers.get(AUCTIONATOR_SERVER));
        p.setFaction((String) headers.get(AUCTIONATOR_FACTION));
        List<Map<String, Object>> queryResult = (List<Map<String, Object>>) headers.get(AUCTIONATOR_ITEM_ID);
        if (queryResult.size() > 0)
            p.setItemId((int) queryResult.get(0).get("id"));
        return p;
    }
}
