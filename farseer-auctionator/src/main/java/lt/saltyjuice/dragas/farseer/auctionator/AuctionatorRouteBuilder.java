package lt.saltyjuice.dragas.farseer.auctionator;

import lt.saltyjuice.dragas.camel.extension.ExtendedRouteBuilder;
import lt.saltyjuice.dragas.farseer.db.DBRouteBuilder;

import static lt.saltyjuice.dragas.farseer.auctionator.AuctionatorConstants.*;

public class AuctionatorRouteBuilder extends ExtendedRouteBuilder {

    public void configure() {
        from("file:output/auctionator_inbox?readLock=markerFile")
                .unmarshal("auctionator_lua")
                .setHeader(AUCTIONATOR_OWNER_ID, method(AuctionatorUtilities.class, "selectLowestIdFromToons"))
                .setHeader(AUCTIONATOR_SCANNED_AT, method(AuctionatorUtilities.class, "selectScannedAt"))
                .setBody(simple("${body['AUCTIONATOR_PRICE_DATABASE']}"))
                .bean(AuctionatorUtilities.class, "unsetDBVersion")
                .setBody(simple("${body.entrySet}"))
                .split(body())
                .parallelProcessing()
                .setHeader(AUCTIONATOR_SERVER, method(AuctionatorUtilities.class, "getServer"))
                .setHeader(AUCTIONATOR_FACTION, method(AuctionatorUtilities.class, "getFaction"))
                .setBody(simple("${body.value.entrySet}"))
                .split(body())
                .enrich(DBRouteBuilder.SEDA_GET_ITEM_ID, asHeader(AUCTIONATOR_ITEM_ID))
                .bean(AuctionatorUtilities.class, "createPricePlaceholder")
                .filter(simple("${body.itemId} != 0"))
                .to("sql:classpath:sql/insert_price_query.sql");
    }
}
