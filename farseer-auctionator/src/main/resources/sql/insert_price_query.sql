insert into prices (item_id, date, price, server, side, imported_by)
values (
        :#${body.itemId},
        :#${body.date},
        :#${body.mr},
        :#${body.server},
        :#${body.faction}::wow_side,
        :#${body.guid}
        );
